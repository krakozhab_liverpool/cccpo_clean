module OrtMod
    use precision_mod
    logical :: flagNorm = .true.
    logical :: flagNormVol = .true.
contains

subroutine GetOrthogonalCoefficients(nVert, vertX, vertY, nCirc, Rad, nZones, nHarm, vol, ort)
  use GlobalAssemblyMod, only : verbosity
!
! Returns orthogonal coefficients for each zone of the cell consisting od outer convex polygon
! and inscribed circles. The circles with the centers at point (0.0_rp,0.0_rp) are supported at
! the moment only.
!
  implicit none

  integer, intent(in) :: nVert
  real(rp), intent(in) :: vertX(:)
  real(rp), intent(in) :: vertY(:)
  integer, intent(in) :: nCirc
  real(rp), intent(in), optional :: Rad(:)
  integer, intent(in) :: nZones
  integer, intent(in) :: nHarm
  real(rp), intent(in) :: vol(:)
  real(rp), intent(inout) :: ort(:,:,:)

  real(rp), allocatable :: b(:,:)
  integer :: i, j, iZone
  real(rp) :: R1, R2
  real(rp) :: c


  if (verbosity > 0) then
    write(*, *) "GetOrthogonalCoefficients:"
    write(*, *) "  nHarm = ", nHarm
  end if
  allocate(b(nHarm,nHarm))
  b = 0.0_rp

!  allocate(ort(nZones,nHarm,nHarm))
!  ort = 0.0_rp
! Get orthogonal coefficients for circles
  R1 = 0.0_rp
  R2 = 0.0_rp
  if (verbosity > 0) then
    write(*, *) "  Getting orthogonal coefficients for circles/rings..."
  end if
  do i = 1, nCirc
    b = 0.0_rp
    do j = 1, nHarm
      b(j,j) = 1.0_rp
    end do
    R1 = R2
    R2 = Rad(i)
    call OrtCoeffsRing(R1, R2, nHarm, b)
    iZone = i
    if (flagNormVol) then
      c = sqrt(vol(iZone))
      ort(iZone,1:nHarm,1:nHarm) = c*b(1:nHarm,1:nHarm)
    else
      ort(iZone,1:nHarm,1:nHarm) = b(1:nHarm,1:nHarm)
    end if
  end do

! Get coefficients for polygon-circle zone
  if (verbosity > 0) then
    write(*, *) "  Getting orthogonal coefficients for circle-ring region..."
  end if
  b = 0.0_rp
  do i = 1, nHarm
    b(i,i) = 1.0_rp
  end do
  if (nCirc > 0) then
    R1 = Rad(nCirc)
    call OrtCoeffsPolygonRing(nVert, vertX, vertY, R1, nHarm, b)
  else
    R1 = 0.0_rp
    call OrtCoeffsPolygonRing(nVert, vertX, vertY, R1, nHarm, b)
  end if

  if (flagNormVol) then
    c = sqrt(vol(nZones))
    ort(nZones,1:nHarm,1:nHarm) = c*b(1:nHarm,1:nHarm)
  else
    ort(nZones,1:nHarm,1:nHarm) = b(1:nHarm,1:nHarm)
  end if

  return

end subroutine GetOrthogonalCoefficients

subroutine OrtCoeffsRing(R1, R2, nHarm, b)
    implicit none

    real(rp), intent(in) :: R1, R2
    integer, intent(in) :: nHarm

    real(rp), intent(inout) :: b(:,:)

    integer :: nh
    integer :: i, j, n, m
    real(rp)  :: c(size(b,1),size(b,2))
    real(rp)  :: sum

    b = 0.0d0
    c = 0.0d0

    ! Decreasing number of the harmonics to make the algorithm stable for thin regions
    if (abs(R2 - R1) < 0.019 .and. nHarm > 3) then
      nh = 3
    else
      nh = nHarm
    end if

    do i = 1, nh
        b(i,i) = 1.0d0
    end do

    do i = 1, nh
        c(i,i) = 1.0d0
    end do

    do n = 1, nh
        do m = 1, nh
            sum = 0.0d0
            do j = n, m - 1
                sum = sum + c(j,n)*scalProdBaseNormRing(R1,R2,m,j,c)/scalProdNormNormRing(R1,R2,j,j,c)
            end do
            if (n /= m) c(m,n) = -1.0_rp*sum
        end do
    end do

    b = c
! Normalization
    if (flagNorm) then
        do i = 1, nh
            do j = 1, i
                c(i,j) = b(i,j)/sqrt(scalProdNormNormRing(R1,R2,i,i,b))
            end do
        end do
        do i = 1, nh
            do j = 1, i
                b(i,j) = c(i,j)
            end do
        end do
    end if

end subroutine OrtCoeffsRing

subroutine OrtCoeffsHexRing(step, R, nHarm, b)
    implicit none

    real(rp) :: step, R
    real(rp) :: b(:,:)
    integer :: nHarm

    integer :: i, j, n, m
    real(rp)  :: c(size(b,1),size(b,2))
    real(rp)  :: xi(6), yi(6), Rad
    real(rp)  :: sum

    b = 0.0_rp
    c = 0.0_rp

    do i = 1, nHarm
        b(i,i) = 1.0_rp
    end do

! x and y coords of the outer hexagon with the center in the origin
    Rad = step/sqrt(3.0_rp)
    xi = (/ -step*0.5_rp, -step*0.5_rp, 0.0_rp, step*0.5_rp, step*0.5_rp, 0.0_rp /)
    yi = (/ -Rad*0.5_rp,   Rad*0.5_rp,  Rad,   Rad*0.5_rp,  -Rad*0.5_rp, -Rad  /)

    do i = 1, nHarm
        do j = 1, i - 1
            b(i,j) = -1.0_rp*scalProdBaseNormHexRing(xi,yi,R,i,j,b)/scalProdNormNormHexRing(xi,yi,R,j,j,b)
        end do
    end do

    do i = 1, nHarm
        c(i,i) = 1.0_rp
    end do

    do n = 1, nHarm
        do m = 1, nHarm
            sum = 0.0_rp
            do j = n, m - 1
                sum = sum + c(j,n)*scalProdBaseNormHexRing(xi,yi,R,m,j,c)/scalProdNormNormHexRing(xi,yi,R,j,j,c)
            end do
            if (n /= m) c(m,n) = -1.0_rp*sum
        end do
    end do

    b = c

! Normalization
    if (flagNorm) then
    c = 0.0_rp
    do i = 1, nHarm
        do j = 1, i
            c(i,j) = b(i,j)/sqrt(scalProdNormNormHexRing(xi,yi,R,i,i,b))
        end do
    end do

    do i = 1, nHarm
        do j = 1, i
            b(i,j) = c(i,j)
        end do
    end do
    end if

end subroutine OrtCoeffsHexRing

subroutine OrtCoeffsPolygonRing(nVert, vertX, vertY, R, nHarm, b)
!
! Performs triangulation of the outer convex polygon and calculates orthogonal
! coefficients for the polygon-circle zone
!
  use GeometryMod, only : triangulate_polygon
  implicit none
  integer, intent(in)  :: nVert
  real(rp), intent(in)  :: vertX(:)
  real(rp), intent(in)  :: vertY(:)
  real(rp), intent(in)  :: R
  integer, intent(in)  :: nHarm
  real(rp), intent(out) :: b(:,:)

  real(rp), allocatable :: triangles(:,:,:)
  integer :: nTriang

  integer :: i, j, n, m
  real(rp) :: c(size(b,1),size(b,2))
  real(rp) :: sum

! Triangulation of the outer polygon
  call triangulate_polygon(nVert, vertX, vertY, nTriang, triangles)

  b = 0.0_rp
  c = 0.0_rp

  do i = 1, nHarm
    c(i,i) = 1.0_rp
  end do

  do m = 1, nHarm
    do n = 1, m
      sum = 0.0_rp
        do j = n, m - 1
          sum = sum + c(j,n)*scalProdBaseNormPolygonRing(nTriang,triangles,R,m,j,c)/ &
                             scalProdNormNormPolygonRing(nTriang,triangles,R,j,j,c)
        end do
        if (n /= m) c(m,n) = -1.0_rp*sum
    end do
  end do

  b = c

! Normalization
  if (flagNorm) then
    c = 0.0_rp
    do i = 1, nHarm
      do j = 1, i
        c(i,j) = b(i,j)/sqrt(scalProdNormNormPolygonRing(nTriang,triangles,R,i,i,b))
      end do
    end do

    do i = 1, nHarm
      do j = 1, i
        b(i,j) = c(i,j)
      end do
    end do
  end if

end subroutine OrtCoeffsPolygonRing

real(rp) function scalProdBaseNormRing(R1, R2, kb, kn, b)
    use ConstantsMod, only : PI
    use MathMod, only : PPnorm, RingIntegral
    implicit none

    real(rp)  :: R1, R2
    integer :: kb, kn
    real(rp)  :: b(:,:)

    call RingIntegral(R1, R2, 0.0_rp, 2.0_rp*PI, PPnorm, kb, kn, b, scalProdBaseNormRing)

end function scalProdBaseNormRing

real(rp) function scalProdNormNormRing(R1, R2, kb, kn, b)
!    use Global
!    use FuncMod
!    use IntegrationMod
    use ConstantsMod, only : PI
    use MathMod, only : PnormPnorm, RingIntegral
    implicit none

    real(rp)  :: R1, R2
    integer :: kb, kn
    real(rp)  :: b(:,:)

    call RingIntegral(R1, R2, 0.0_rp, 2.0_rp*PI, PnormPnorm, kb, kn, b, scalProdNormNormRing)

end function scalProdNormNormRing

real(rp) function scalProdBaseNormHexRing(xi, yi, R, i, j, b)
    use MathMod, only : PPnorm, HexRingIntegral
    implicit none

    real(rp) :: xi(6), yi(6)
    real(rp) :: R
    real(rp) :: b(:,:)
    integer :: i, j

    call HexRingIntegral(xi, yi, R, PPnorm, i, j, b, scalProdBaseNormHexRing)

end function scalProdBaseNormHexRing

real(rp) function scalProdNormNormHexRing(xi, yi, R, i, j, b)
    use MathMod, only : PnormPnorm, HexRingIntegral
    implicit none

    real(rp) :: xi(6), yi(6)
    real(rp) :: R
    real(rp) :: b(:,:)
    integer :: i, j

    call HexRingIntegral(xi, yi, R, PnormPnorm, i, j, b, scalProdNormNormHexRing)

end function scalProdNormNormHexRing

function scalProdBaseNormPolygonRing(nTriang, triangles, R, i, j, b) result(scalProd)

  use MathMod, only : PPnorm, PolygonRingIntegral
  implicit none

  integer, intent(in) :: nTriang
  real(rp), intent(in) :: triangles(:,:,:)
  real(rp), intent(in) :: R
  integer, intent(in) :: i
  integer, intent(in) :: j
  real(rp), intent(in) :: b(:,:)

  real(rp) :: scalProd

  call PolygonRingIntegral(nTriang, triangles, R, PPnorm, i, j, b, scalProd)

  return

end function scalProdBaseNormPolygonRing

function scalProdNormNormPolygonRing(nTriang, triangles, R, i, j, b) result(scalProd)

  use MathMod, only : PnormPnorm, PolygonRingIntegral
  implicit none

  integer, intent(in) :: nTriang
  real(rp), intent(in) :: triangles(:,:,:)
  real(rp), intent(in) :: R
  integer, intent(in) :: i
  integer, intent(in) :: j
  real(rp), intent(in) :: b(:,:)

  real(rp) :: scalProd

  call PolygonRingIntegral(nTriang, triangles, R, PnormPnorm, i, j, b, scalProd)

  return

end function

end module OrtMod
