module BetaMod
    use precision_mod
    integer :: nChordBet         ! number of chords for integration of surface-to-surface CP (beta)
    integer :: nBetaElem         ! total number of beta elements
    integer, allocatable :: nSegChordBet(:)   ! number of segments along the chord for integration beta
    real(rp), allocatable :: lenChordBet(:,:)  ! length of segments along the chord
    real(rp), allocatable :: wghtChordBet(:)   ! weight of the beta chord
    integer, allocatable :: zoneChordBet(:,:) ! zones' numbers along the chord (for definition of the materials)
    real(rp), allocatable :: xChordBet(:,:)    ! x-coordinates of the segments along the chord
    real(rp), allocatable :: yChordBet(:,:)    ! y-coordinates of the segments along the chord
    integer, allocatable :: inSideBet(:)      ! number of input side for chord
    integer, allocatable :: inSegBet(:)       ! number of input segment for chord
    integer, allocatable :: inSectBet(:)      ! number of input sector for chord
    integer, allocatable :: outSideBet(:)     ! number of output side for chord
    integer, allocatable :: outSegBet(:)      ! number of output segment for chord
    integer, allocatable :: outSectBet(:)     ! number of output segment for chord
    real(rp), allocatable :: brc(:)            ! array with beta reciprocity coefficients
    integer, allocatable :: nBeta(:)          ! number of beta element where where chord contributes

end module
