module CPMod
  !-----------------------------------------------
  ! This module contains procedures and functions
  ! for integration collision probabilities beta,
  ! gamma, v, u and support values
  !-----------------------------------------------
  use precision_mod

  type bmg
    integer, allocatable :: nbetaelem(:)      ! Number of non-zero beta elements in the Beta matrix cell_nbetaelem(icell)
    real(rp), allocatable :: beta(:,:)         ! Beta elements for the cell cell_beta(icell,maxval(cell_nbetaelem))
    integer, allocatable :: insidebet(:,:)    ! input side of the beta elements
    integer, allocatable :: insegbet(:,:)     ! input segment of the beta elements
    integer, allocatable :: insectbet(:,:)    ! input sector of the beta elements
    integer, allocatable :: outsidebet(:,:)   ! output side of the beta elements
    integer, allocatable :: outsegbet(:,:)    ! output segment of the beta elements
    integer, allocatable :: outsectbet(:,:)   ! output sector of the beta elements
    integer, allocatable :: thetasectbet(:,:) ! theta sector of the beta elements
    real(rp), allocatable :: brc(:,:)
  end type bmg

  type gmg
    real(rp), allocatable :: g(:,:,:,:,:,:,:)
  end type gmg

  type vmg
    real(rp), allocatable :: v(:,:,:,:,:,:,:)
  end type vmg

  type umg
    real(rp), allocatable :: u(:,:,:,:,:)
  end type umg

  type StoSCP
    integer :: ind(6) ! ind(1) side in; ind(2) segment in; ind(3) sector in; ind(4) side out; ind(5) segment out; ind(6) sector out
    real(rp) :: val
    real(rp) :: rc
  end type

  type StoRCP
    integer :: ind(5)  ! ind(1) side in; ind(2) segment(in); ind(3) sector in; ind(4) region to; ind(5) mode to
    real(rp) :: val
  end type

  type RtoSCP
    integer :: ind(5)  ! ind(1) region from; ind(2) mode from; ind(3) side out; ind(4) segment out; ind(5) sector out
    real(rp) :: val
  end type

  type(bmg), allocatable :: b_mg(:)
  type(gmg), allocatable :: g_mg(:)
  type(vmg), allocatable :: v_mg(:)
  type(umg), allocatable :: u_mg(:)

  type(StoSCP), allocatable :: beta_cp(:,:,:,:)
  integer, allocatable      :: n_beta_cp(:)

  type(RtoSCP), allocatable :: v_cp(:,:,:,:)
  integer, allocatable      :: n_v_cp(:)

  type(StoRCP), allocatable :: g_cp(:,:,:,:)
  integer, allocatable      :: n_g_cp(:)


  contains

  subroutine allocate_cp_mg(n_groups)
    implicit none
    integer, intent(in) ::  n_groups

    allocate(b_mg(n_groups))
    allocate(g_mg(n_groups))
    allocate(v_mg(n_groups))
    allocate(u_mg(n_groups))

  end subroutine

  subroutine allocate_cp_arrays(ig,nCellTypes, nBetaElem, nPhi, nTheta, nzones, &
                                nharms, nsides, cell_nsegments)
    implicit none

    integer, intent(in) :: ig
    integer, intent(in) :: nCellTypes
    integer, intent(in) :: nBetaElem
    integer, intent(in) :: nPhi
    integer, intent(in) :: nTheta
    integer, intent(in) :: nzones(:)
    integer, intent(in) :: nharms(:)
    integer, intent(in) :: nsides(:)
    integer, intent(in) :: cell_nsegments(:,:)

    allocate(b_mg(ig)%nbetaelem(nCellTypes))
    allocate(b_mg(ig)%beta(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%brc(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%insidebet(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%insegbet(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%insectbet(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%outsidebet(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%outsegbet(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%outsectbet(nCellTypes,nBetaElem))
    allocate(b_mg(ig)%thetasectbet(nCellTypes,nBetaElem))
! Gamma
    allocate(g_mg(ig)%g(nCellTypes,maxval(nzones),maxval(nharms),maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
! V
    allocate(v_mg(ig)%v(nCellTypes,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
! U
    allocate(u_mg(ig)%u(nCellTypes,maxval(nzones),maxval(nharms),maxval(nzones),maxval(nharms)))

    return

  end subroutine

  subroutine convert_cps_to_arrays()
    use GlobalAssemblyMod, only : cell_nsegments, nCellTypes, nGroups, nPhi, nsides, nharms, &
                                  nTheta, nzones

    implicit none

    call Beta_to_array(b_mg, nGroups, nCellTypes, nTheta, beta_cp, n_beta_cp)
    call V_to_array(v_mg, nGroups, nCellTypes, nTheta, nzones, nharms, nsides, cell_nsegments, nPhi, v_cp, n_v_cp)
    call G_to_array(g_mg, nGroups, nCellTypes, nTheta, nzones, nharms, nsides, cell_nsegments, nPhi, g_cp, n_g_cp)

  end subroutine

  integer function get_beta_size(ig)
    implicit none

    integer, intent(in) :: ig

    get_beta_size = size(b_mg(ig)%beta,2)

    return

  end function

  subroutine increase_beta_arrays_size(ig, nCellTypes, nBetaElem)
    use ArrayMod, only : SetArraySize2DReal, SetArraySize2DInt
    implicit none

    integer, intent(in) :: ig
    integer, intent(in) :: nCellTypes
    integer, intent(in) :: nBetaElem

    call SetArraySize2DReal(b_mg(ig)%beta, nCellTypes, nBetaElem)
    call SetArraySize2DReal(b_mg(ig)%brc, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%insidebet, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%insegbet, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%insectbet, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%outsidebet, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%outsegbet, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%outsectbet, nCellTypes, nBetaElem)
    call SetArraySize2DInt(b_mg(ig)%thetasectbet, nCellTypes, nBetaElem)

    return

  end subroutine

  subroutine pass_data_to_cp_arrays(ig, icell, nb_elem_cell, nBetaElem, cell_beta, cell_brc,  &
                                    cell_insidebet, cell_insegbet, cell_insectbet, cell_outsidebet, cell_outsegbet, &
                                    cell_outsectbet, cell_thetasectbet, cell_G, cell_V, cell_U)
    use precision_mod
    implicit none

    integer,  intent(in) :: ig
    integer,  intent(in) :: icell
    integer,  intent(in) :: nb_elem_cell
    integer,  intent(in) :: nBetaElem

    real(rp), intent(in) :: cell_beta(:,:)
    real(rp), intent(in) :: cell_brc(:,:)
    integer,  intent(in) :: cell_insidebet(:,:)
    integer,  intent(in) :: cell_insegbet(:,:)
    integer,  intent(in) :: cell_insectbet(:,:)
    integer,  intent(in) :: cell_outsidebet(:,:)
    integer,  intent(in) :: cell_outsegbet(:,:)
    integer,  intent(in) :: cell_outsectbet(:,:)
    integer,  intent(in) :: cell_thetasectbet(:,:)

    real(rp), intent(in) :: cell_G(:,:,:,:,:,:,:)
    real(rp), intent(in) :: cell_V(:,:,:,:,:,:,:)
    real(rp), intent(in) :: cell_U(:,:,:,:,:)

    b_mg(ig)%nbetaelem(icell) = nb_elem_cell
    b_mg(ig)%beta(icell,1:nBetaElem) = cell_beta(icell,1:nBetaElem)
    b_mg(ig)%brc(icell,1:nBetaElem) = cell_brc(icell,1:nBetaElem)
    b_mg(ig)%insidebet(icell,1:nBetaElem) = cell_insidebet(icell,1:nBetaElem)
    b_mg(ig)%insegbet(icell,1:nBetaElem) = cell_insegbet(icell,1:nBetaElem)
    b_mg(ig)%insectbet(icell,1:nBetaElem) = cell_insectbet(icell,1:nBetaElem)
    b_mg(ig)%outsidebet(icell,1:nBetaElem) = cell_outsidebet(icell,1:nBetaElem)
    b_mg(ig)%outsegbet(icell,1:nBetaElem) = cell_outsegbet(icell,1:nBetaElem)
    b_mg(ig)%outsectbet(icell,1:nBetaElem) = cell_outsectbet(icell,1:nBetaElem)
    b_mg(ig)%thetasectbet(icell,1:nBetaElem) = cell_thetasectbet(icell,1:nBetaElem)

    g_mg(ig)%g(icell,:,:,:,:,:,:) = cell_G(icell,:,:,:,:,:,:)

    v_mg(ig)%v(icell,:,:,:,:,:,:) = cell_V(icell,:,:,:,:,:,:)

    u_mg(ig)%u(icell,:,:,:,:) = cell_U(icell,:,:,:,:)

    return

  end subroutine

  subroutine PhiBoundariesBeta(nSides, nSegments, segBoundX, segBoundY, normVecX, normVecY, &
                               nCirc, Rad, cX, cY, phiSect, nPhi, nPhiB, nPhiBet, phiBet)
    use ConstantsMod, only : PI, DELTA_MIN
    use ArrayMod, only : nRealInc, ArrExpReal, ArrInsValReal, SetArraySize2DReal
    use GeometryMod, only : getAngle, GetTangentPoints, getAngle2PI
    use GlobalAssemblyMod, only : verbosity
    implicit none

!   Input
    integer, intent(in) :: nSides         ! Number of the polygon's sides
    integer, intent(in) :: nSegments(:)   ! Number of segments on the polygon's sides
    real(rp), intent(in) :: segBoundX(:,:) ! x-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: segBoundY(:,:) ! y-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: normVecX(:)    ! x-coordinate of the normal vectors to the polygon's side
    real(rp), intent(in) :: normVecY(:)    ! y-coordinate of the normal vectors to the polygon's side
    integer, intent(in) :: nCirc          ! number of the circles inscribed into polygon
    real(rp), intent(in) :: Rad(:)         ! radii of the inscribed circles
    real(rp), intent(in) :: cX(:)          ! x-coordinates of the centres of the inscribed circles
    real(rp), intent(in) :: cY(:)          ! y-coordinates of the centres of the inscribed circles
    real(rp), intent(in) :: phiSect(:)     ! boundaries of the sectors on phi angle (polar angle)
    integer, intent(in) :: nPhi           ! number of the sectors on phi
    integer, intent(in) :: nPhiB          ! minimal number of the intervals for integration over phi

!   Output
    integer, allocatable, intent(out) :: nPhiBet(:)  ! actual number of the intervals for integration on phi (for each side)
    real(rp), allocatable, intent(out) :: phiBet(:,:) ! boundaries of the intervals for integration on phi (for each side)

    real(rp) :: x0, y0, x1, y1, x2, y2
    real(rp) :: xt1, yt1, xt2, yt2
    real(rp) :: ang, dPhiBet, dPhi
    real(rp) :: angMin, angMax
    real(rp), allocatable :: phiArr(:) ! Working array containing the angles
    real(rp), allocatable :: phiTmp(:) ! Working array for insertion of additional angles into array phiArr
    integer :: i, j, k, l, ic
    integer :: nAng, nInsPhi

    if (.not.(allocated(phiBet))) then
      allocate(phiBet(nSides,nRealInc))
      allocate(nPhiBet(nSides))
    end if

    phiBet = 0.0_rp
    nPhiBet = 0
    dPhiBet = PI/nPhiB

!    write(*, *) "Writing input data to <PhiBoundariesBeta>"
!    write(*, *) nSides
!    write(*, *) nSegments
!    write(*, *) segBoundX
!    write(*, *) segBoundY
!    write(*, *) normVecX
!    write(*, *) normVecY
!    write(*, *) nCirc
!    write(*, *) Rad
!    write(*, *) cX
!    write(*, *) cY
!    write(*, *) phiSect
!    write(*, *) nPhi
!    write(*, *) nPhiB
!    write(*, *) nPhiBet
!    write(*, *) phiBet

! Insert maximal and minimal boundaries of integration into array phiArr: (-PI/2;PI/2)
    if (verbosity > 0) then
      write(*, *) "Inserting minimal and maximal boundaries of integration into array phiArr..."
    end if
    do i = 1, nSides
      nAng = 0
      call ArrInsValReal(phiArr, -PI*0.5_rp, nAng)
      call ArrInsValReal(phiArr, PI*0.5_rp, nAng)
      do j = 1, nSegments(i) + 1
        x0 = segBoundX(i,j)
        y0 = segBoundY(i,j)
        ! Take into account tangent lines to the circles and their centres
        do ic = 1, nCirc
          ! Take into account centre of the circle
          x1 = cX(ic)
          y1 = cY(ic)
          ang = getAngle(-normVecX(i), -normVecY(i), x1-x0, y1-y0)
          call ArrInsValReal(phiArr, ang, nAng)
          ! Take into account tangent lines to the circle
          call GetTangentPoints(x0, y0, x1, y1, Rad(ic), xt1, yt1, xt2, yt2)
          ang = getAngle(-normVecX(i), -normVecY(i), xt1-x0, yt1-y0)
          call ArrInsValReal(phiArr, ang, nAng)
          ang = getAngle(-normVecX(i), -normVecY(i), xt2-x0, yt2-y0)
          call ArrInsValReal(phiArr, ang, nAng)
        end do
        ! Take into account boundaries of segments on other sides
        do k = 1, nSides
          if (k == i) cycle
          do l = 1, nSegments(k) + 1
            x1 = segBoundX(k,l)
            y1 = segBoundY(k,l)
            if (abs(x1 - x0) < DELTA_MIN .and. abs(y1 - y0) < DELTA_MIN) cycle
            ang = getAngle(-normVecX(i), -normVecY(i), x1 - x0, y1 - y0)
            call ArrInsValReal(phiArr, ang, nAng)
          end do
        end do
      end do
      ! Add boundaries of sectors on phi
      do k = 1, nPhi + 1
        ang = phiSect(k)
        call ArrInsValReal(phiArr, ang, nAng)
      end do
      ! Add phi projections from other sides
      do j = 1, nSides
        if (i == j) cycle
        x1 = segBoundX(j,1)
        y1 = segBoundY(j,1)
        x2 = segBoundX(i,1)
        y2 = segBoundY(i,1)
        ang = getAngle(-normVecX(j), -normVecY(j), x2 - x1, y2 - y1)
        angMin = ang
        x1 = segBoundX(j, nSegments(j) + 1)
        y1 = segBoundY(j, nSegments(j) + 1)
        x2 = segBoundX(i, nSegments(i) + 1)
        y2 = segBoundY(i, nSegments(i) + 1)
        ang = getAngle(-normVecX(j), -normVecY(j), x2 - x1, y2 - y1)
        if (angMin > ang) then
          angMax = angMin
          angMin = ang
        else
          angMax = ang
        end if
        do k = 2, nPhi
          if (phiSect(k) < angMax .and. phiSect(k) > angMin) then
            ang = phiSect(k) + getAngle(1.0_rp, 0.0_rp, normVecX(j), normVecY(j)) - &
                               getAngle(1.0_rp, 0.0_rp, -normVecX(i), -normVecY(i))
            if (ang > 0.5_rp*PI + 1.0d-8) ang = ang - 2.0_rp*PI
            if (ang < -0.5_rp*PI - 1.0d-8) ang = ang + 2.0_rp*PI
            call ArrInsValReal(phiArr, ang, nAng)
          end if
        end do
      end do
      ! Insert additional angles if interval exceeds maximal value (PI/nPhiBeta)
      allocate(phiTmp(nAng))
      phiTmp(1:nAng) = phiArr(1:nAng)
      do k = 1, nAng - 1
        if (phiTmp(k+1) - phiTmp(k) > dPhiBet) then
          nInsPhi = int((phiTmp(k+1) - phiTmp(k))/dPhiBet) + 1
          dPhi = (phiTmp(k+1) - phiTmp(k))/nInsPhi
          do l = 1, nInsPhi - 1
            ang = phiTmp(k) + l*dPhi
            call ArrInsValReal(phiArr, ang, nAng)
          end do
        end if
      end do
      deallocate(phiTmp)
      if (size(phiBet,2) < nAng) then
        call SetArraySize2DReal(phiBet,nSides,nAng)
      end if
      phiBet(i,1:nAng) = phiArr(1:nAng)
      nPhiBet(i) = nAng
      deallocate(phiArr)
    end do

    return

  end subroutine PhiBoundariesBeta

  subroutine PhiBoundariesGamma(nSides, nSegments, segBoundX, segBoundY, normVecX, normVecY, &
                                nCirc, Rad, cX, cY, phiSect, nPhi, nPhiG, nPhiGam, phiGam)
    use ConstantsMod, only : PI, DELTA_MIN
    use ArrayMod, only : nRealInc, ArrExpReal, ArrInsValReal, SetArraySize2DReal
    use GeometryMod, only : getAngle, GetTangentPoints
    implicit none

    integer, intent(in) :: nSides
    integer, intent(in) :: nSegments(:)
    real(rp), intent(in) :: segBoundX(:,:)
    real(rp), intent(in) :: segBoundY(:,:)
    real(rp), intent(in) :: normVecX(:)
    real(rp), intent(in) :: normVecY(:)
    integer, intent(in) :: nCirc
    real(rp), intent(in) :: Rad(:)
    real(rp), intent(in) :: cX(:)
    real(rp), intent(in) :: cY(:)
    real(rp), intent(in) :: phiSect(:)
    integer, intent(in) :: nPhi
    integer, intent(in) :: nPhiG

    integer, allocatable, intent(out) :: nPhiGam(:)
    real(rp), allocatable, intent(out) :: phiGam(:,:)

    real(rp) :: x0, y0, x1, y1
    real(rp) :: xt1, yt1, xt2, yt2
    real(rp) :: ang, dPhiGam, dPhi
    real(rp), allocatable :: phiArr(:) ! Working array containing the angles
    real(rp), allocatable :: phiTmp(:) ! Working array for insertion of additional angles into array phiArr
    integer :: i, j, k, l, ic
    integer :: nAng, nInsPhi

    if (.not.(allocated(phiGam))) then
      allocate(phiGam(nSides,nRealInc))
      allocate(nPhiGam(nSides))
    end if

    phiGam = 0.0_rp
    nPhiGam = 0
    dPhiGam = PI/nPhiG

! Insert maximal and minimal boundaries of integration into array phiArr: (-PI/2;PI/2)
    do i = 1, nSides
      nAng = 0
      call ArrInsValReal(phiArr, -PI*0.5_rp, nAng)
      call ArrInsValReal(phiArr, PI*0.5_rp, nAng)
      do j = 1, nSegments(i) + 1
        x0 = segBoundX(i,j)
        y0 = segBoundY(i,j)
        ! Take into account boundaries of other sides
        do k = 1, nSides
          if (k == i) cycle
          x1 = segBoundX(k,1)
          y1 = segBoundY(k,1)
          if (abs(x1 - x0) < DELTA_MIN .and. abs(y1 - y0) < DELTA_MIN) cycle
          ang = getAngle(-normVecX(i), -normVecY(i), x1 - x0, y1 - y0)
          call ArrInsValReal(phiArr, ang, nAng)
          x1 = segBoundX(k,nSegments(k) + 1)
          y1 = segBoundY(k,nSegments(k) + 1)
          if (abs(x1 - x0) < DELTA_MIN .and. abs(y1 - y0) < DELTA_MIN) cycle
          ang = getAngle(-normVecX(i), -normVecY(i), x1 - x0, y1 - y0)
          call ArrInsValReal(phiArr, ang, nAng)
        end do
        ! Take into account tangent lines to the circles and their centres
        do ic = 1, nCirc
          ! Take into account centre of the circle
          x1 = cX(ic)
          y1 = cY(ic)
          ang = getAngle(-normVecX(i), -normVecY(i), x1-x0, y1-y0)
          call ArrInsValReal(phiArr, ang, nAng)
          ! Take into account tangent lines to the circle
          call GetTangentPoints(x0, y0, x1, y1, Rad(ic), xt1, yt1, xt2, yt2)
          ang = getAngle(-normVecX(i), -normVecY(i), xt1-x0, yt1-y0)
          call ArrInsValReal(phiArr, ang, nAng)
          ang = getAngle(-normVecX(i), -normVecY(i), xt2-x0, yt2-y0)
          call ArrInsValReal(phiArr, ang, nAng)
        end do
      end do
      ! Add boundaries of sectors on phi
      do k = 1, nPhi + 1
        ang = phiSect(k)
        call ArrInsValReal(phiArr, ang, nAng)
      end do
      ! Insert additional angles if interval exceeds maximal value (PI/nPhiBeta)
      allocate(phiTmp(nAng))
      phiTmp(1:nAng) = phiArr(1:nAng)
      do k = 1, nAng - 1
        if (phiTmp(k+1) - phiTmp(k) > dPhiGam) then
          nInsPhi = int((phiTmp(k+1) - phiTmp(k))/dPhiGam) + 1
          dPhi = (phiTmp(k+1) - phiTmp(k))/nInsPhi
          do l = 1, nInsPhi - 1
            ang = phiTmp(k) + l*dPhi
            call ArrInsValReal(phiArr, ang, nAng)
          end do
        end if
      end do
      deallocate(phiTmp)
      if (size(phiGam,2) < nAng) then
        call SetArraySize2DReal(phiGam,nSides,nAng)
      end if
      phiGam(i,1:nAng) = phiArr(1:nAng)
      nPhiGam(i) = nAng
      deallocate(phiArr)
    end do

    return

  end subroutine PhiBoundariesGamma

  subroutine PhiBoundariesU(nSides, nSegments, segBoundX, segBoundY, normVecX, normVecY, &
                            nCirc, Rad, cX, cY, nPhiU, nAngU, phiU)
    use ConstantsMod, only : PI
    use ArrayMod, only : nRealInc, ArrExpReal, ArrInsValReal, SetArraySize2DReal
    use GeometryMod, only : getAngle, GetTangentPoints
    implicit none

    integer, intent(in) :: nSides
    integer, intent(in) :: nSegments(:)
    real(rp), intent(in) :: segBoundX(:,:)
    real(rp), intent(in) :: segBoundY(:,:)
    real(rp), intent(in) :: normVecX(:)
    real(rp), intent(in) :: normVecY(:)
    integer, intent(in) :: nCirc
    real(rp), intent(in) :: Rad(:)
    real(rp), intent(in) :: cX(:)
    real(rp), intent(in) :: cY(:)
    integer, intent(in) :: nPhiU

    integer, allocatable, intent(out) :: nAngU(:)
    real(rp), allocatable, intent(out) :: phiU(:,:)

    real(rp) :: x0, y0, x1, y1
    real(rp) :: xt1, yt1, xt2, yt2
    real(rp) :: ang, dPhiGam, dPhi
    real(rp), allocatable :: phiArr(:) ! Working array containing the angles
    real(rp), allocatable :: phiTmp(:) ! Working array for insertion of additional angles into array phiArr
    integer :: i, j, k, l, ic
    integer :: nAng, nInsPhi

    if (.not.(allocated(phiU))) then
      allocate(phiU(nSides,nRealInc))
      allocate(nAngU(nSides))
    end if

    phiU = 0.0_rp
    nAngU = 0
    dPhiGam = PI/nPhiU

! Insert maximal and minimal boundaries of integration into array phiArr: (-PI/2;PI/2)
    do i = 1, nSides
      nAng = 0
      call ArrInsValReal(phiArr, -PI*0.5_rp, nAng)
      call ArrInsValReal(phiArr, PI*0.5_rp, nAng)
      do j = 1, nSegments(i) + 1, nSegments(i)
        x0 = segBoundX(i,j)
        y0 = segBoundY(i,j)
        ! Take into account tangent lines to the circles and their centres
        do ic = 1, nCirc
          ! Take into account centre of the circle
          x1 = cX(ic)
          y1 = cY(ic)
          ang = getAngle(-normVecX(i), -normVecY(i), x1-x0, y1-y0)
          call ArrInsValReal(phiArr, ang, nAng)
          ! Take into account tangent lines to the circle
          call GetTangentPoints(x0, y0, x1, y1, Rad(ic), xt1, yt1, xt2, yt2)
          ang = getAngle(-normVecX(i), -normVecY(i), xt1-x0, yt1-y0)
          call ArrInsValReal(phiArr, ang, nAng)
          ang = getAngle(-normVecX(i), -normVecY(i), xt2-x0, yt2-y0)
          call ArrInsValReal(phiArr, ang, nAng)
        end do
        ! Take into account boundaries of other sides
        do k = 1, nSides
          if (k == i) cycle
          x1 = segBoundX(k,1)
          y1 = segBoundY(k,1)
          if (x1 == x0 .and. y1 == y0) cycle
          ang = getAngle(-normVecX(i), -normVecY(i), x1 - x0, y1 - y0)
          call ArrInsValReal(phiArr, ang, nAng)
          x1 = segBoundX(k,nSegments(k) + 1)
          y1 = segBoundY(k,nSegments(k) + 1)
          if (x1 == x0 .and. y1 == y0) cycle
          ang = getAngle(-normVecX(i), -normVecY(i), x1 - x0, y1 - y0)
          call ArrInsValReal(phiArr, ang, nAng)
        end do
      end do
      ! Insert additional angles if interval exceeds maximal value (PI/nPhiBeta)
      allocate(phiTmp(nAng))
      phiTmp(1:nAng) = phiArr(1:nAng)
      do k = 1, nAng - 1
        if (phiTmp(k+1) - phiTmp(k) > dPhiGam) then
          nInsPhi = int((phiTmp(k+1) - phiTmp(k))/dPhiGam) + 1
          dPhi = (phiTmp(k+1) - phiTmp(k))/nInsPhi
          do l = 1, nInsPhi - 1
            ang = phiTmp(k) + l*dPhi
            call ArrInsValReal(phiArr, ang, nAng)
          end do
        end if
      end do
      deallocate(phiTmp)
      if (size(phiU,2) < nAng) then
        call SetArraySize2DReal(phiU,nSides,nAng)
      end if
      phiU(i,1:nAng) = phiArr(1:nAng)
      nAngU(i) = nAng
      deallocate(phiArr)
    end do

    return

  end subroutine PhiBoundariesU

  subroutine ChordsBetaHash(nSides, verX, verY, nSegments, segBoundX, segBoundY, segSdArr, nPhi, phiSect, &
                            normVecX, normVecY, nCirc, Rad, cX, cY, nPhiBet, phiBet, dcb)
    use ArrayMod, only : nRealInc, ArrExpReal, ArrExpInt, ArrInsValReal, SetArraySize2DReal, SetArraySize2DInt
    use GlobalMod, only : nChordBet, nSegChordBet, wghtChordBet, zoneChordBet, lenChordBet, &
                          xChordBet, yChordBet, nBeta_key, nBetaElem, brc
    use ConstantsMod, only : EPS => DELTA_MIN
    use GeometryMod
    use DrawMod
    use Config
    implicit none

! Input
    integer, intent(in) :: nSides         ! Number of the polygon's sides
    real(rp), intent(in) :: verX(:)        ! x coordinates of the polygon's sides
    real(rp), intent(in) :: verY(:)        ! y coordinates of the polygon's sides
    integer, intent(in) :: nSegments(:)   ! Number of segments on the polygon's sides
    real(rp), intent(in) :: segBoundX(:,:) ! x-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: segBoundY(:,:) ! y-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: segSdArr(:,:)  ! One-dimensional coordinates of the segments on each side of the cell
    integer, intent(in) :: nPhi           ! Number of sectors on azimuthal angle
    real(rp), intent(in) :: phiSect(:)     ! Sectors of the subdivision of the azimuthal angle
    real(rp), intent(in) :: normVecX(:)    ! x-coordinate of the normal vectors to the polygon's side
    real(rp), intent(in) :: normVecY(:)    ! y-coordinate of the normal vectors to the polygon's side
    integer, intent(in) :: nCirc          ! number of the circles inscribed into polygon
    real(rp), intent(in) :: Rad(:)         ! radii of the inscribed circles
    real(rp), intent(in) :: cX(:)          ! x-coordinates of the centres of the inscribed circles
    real(rp), intent(in) :: cY(:)          ! y-coordinates of the centres of the inscribed circles
    integer, intent(in) :: nPhiBet(:)     ! (side) number of intervals for integration over phi angle (given for each side of the polygon)
    real(rp), intent(in) :: phiBet(:,:)    ! (side,angles) boundaries of intervals for integration over phi angle (given for each side of the polygon)
    real(rp), intent(in) :: dcb            ! maximal distance between chords

! Output is void. The subroutine changes arrays defined in GlobalMod

! Local
    real(rp), allocatable :: segArr(:), segTmp(:) ! Working array contains boundaries of the intervals of integration over sides
    real(rp), allocatable :: phiWght(:,:)
    real(rp) :: phi ! angle
    real(rp) :: sdLen, sgLngth, dSeg, fact, dist, dstMin
    real(rp) :: xt1, yt1, xt2, yt2, x0, y0
    real(rp) :: A, B, C, A1, B1, C1, A2, B2, C2, p(2), p1(2), p2(2)
    real(rp) :: vecDirX, vecDirY ! x and y coordinates of the directional vector
    real(rp) :: vecDirNormX, vecDirNormY
    integer :: iSide, iSide1, iSeg, iPhi, iCirc, ival, nInsSeg
    integer :: inSide, inSect, inSegm, outSide, outSegm, outSect
    integer :: iChord, nSegMax, nPntsMax, nPnts, nSeg, nBet
    integer :: nSegCur ! Current number of segments in segArr
    integer :: i, l
    logical :: isCrossing

    ! Preparation
    nChordBet = 1
    nSegMax = 2*nCirc + 1
    nPntsMax = 2*(nCirc + 1)
    if (.not. allocated(segArr)) allocate(segArr(nRealInc))
    if (.not. allocated(nSegChordBet)) allocate(nSegChordBet(nRealInc))
    if (.not. allocated(wghtChordBet)) allocate(wghtChordBet(nRealInc))
    if (.not. allocated(zoneChordBet)) allocate(zoneChordBet(nRealInc,nSegMax))
    if (.not. allocated(lenChordBet)) allocate(lenChordBet(nRealInc,nSegMax))
    if (.not. allocated(xChordBet)) allocate(xChordBet(nRealInc,nPntsMax))
    if (.not. allocated(yChordBet)) allocate(yChordBet(nRealInc,nPntsMax))
    if (.not. allocated(nBeta_key)) allocate(nBeta_key(nRealInc,7))
    if (.not. allocated(phiWght)) allocate(phiWght(nPhi,nSides))
    nSegChordBet = 0
    wghtChordBet = 0.0_rp
    zoneChordBet = 0
    lenChordBet = 0.0_rp
    xChordBet = 0.0_rp
    yChordBet = 0.0_rp
    nBeta_key = 0
    phiWght  = 0.0_rp
    nBet = 0

!   Start of the ray tracing
    do iSide = 1, nSides
      ! Getting the boundaries (points) of the side
      if (iSide < nSides) then
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(iSide+1)
        p2(2) = verY(iSide+1)
      else
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(1)
        p2(2) = verY(1)
      end if
      sdLen = getSegmentLength(p1(1),p1(2),p2(1),p2(2)) ! Length of the side
      ! Coefficients for the line containing side
      if (iSide < nSides) then
        call GetCoeffA(verY(iSide), verY(iSide + 1), A2)
        call GetCoeffB(verX(iSide), verX(iSide + 1), B2)
        call GetCoeffC(verX(iSide), verY(iSide), verX(iSide + 1), verY(iSide + 1), C2)
      else
        call GetCoeffA(verY(iSide), verY(1), A2)
        call GetCoeffB(verX(iSide), verX(1), B2)
        call GetCoeffC(verX(iSide), verY(iSide), verX(1), verY(1), C2)
      end if
      ! Cycle on previously calculated angles
      do iPhi = 1, nPhiBet(iSide) - 1
        phi = (phiBet(iSide,iPhi)+phiBet(iSide,iPhi+1))*0.5_rp
        segArr = 0.0_rp
        ! Insert into segArr boundaries of the segments on the side
        nSegCur = 1
        segArr(nSegCur) = 0.0_rp
        do i = 2, nSegments(iSide)
          if (nSegCur >= size(segArr)) call ArrExpReal(segArr)
          sgLngth = getSegmentLength(p1(1), p1(2), segBoundX(iSide,i), segBoundY(iSide,i))
          call ArrInsValReal(segArr,sgLngth,nSegCur)
        end do
        if (nSegCur >= size(segArr)) call ArrExpReal(segArr)
        sgLngth = getSegmentLength(p1(1),p1(2),p2(1),p2(2))
        call ArrInsValReal(segArr,sgLngth,nSegCur)
        ! Coordinates of the directional vector
        vecDirX = -normVecX(iSide)
        vecDirY = -normVecY(iSide)
        call RotateVec(vecDirX, vecDirY, phi)
        ! Normalize vector to unity
        vecDirNormX = vecDirX
        vecDirNormY = vecDirY
        call NormalizeVector(vecDirNormX, vecDirNormY)
        ! Take into account tangential lines of the circles
        do iCirc = 1, nCirc
          ! Getting tangential points on the circle for line containing directional vector (vecDirX,vecDirY)
          call GetTangentPointsVec(vecDirX,vecDirY,cx(iCirc),cy(iCirc),Rad(iCirc),xt1,yt1,xt2,yt2)
          ! Coefficients of the first tangential lines
          A1 =  vecDirY
          B1 = -vecDirX
          C1 =  vecDirX*yt1 - vecDirY*xt1
          ! Intersection with the side (if exists)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          ! Side contains point p
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
!         Second tangential point
          C1 =  vecDirX*yt2 - vecDirY*xt2
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do ! End of cycle on circles
        ! Take into account centres of the circles
        do iCirc = 1, nCirc
          C1 =  vecDirX*cy(iCirc) - vecDirY*cx(iCirc)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do
        ! Take into account segment vertices on the other sides
        ! Get A and B coefficients for line containing directional vector
        A1 =  vecDirY
        B1 = -vecDirX
        do iSide1 = 1, nSides
          if (iSide1 == iSide) cycle
          do iSeg = 1, nSegments(iSide1)
            C1 =  vecDirX*segBoundY(iSide1,iSeg) - vecDirY*segBoundX(iSide1,iSeg)
            call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
            if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
              if (nSegCur > size(segArr)) call ArrExpReal(segArr)
              sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
              call ArrInsValReal(segArr,sgLngth,nSegCur)
            end if
          end do
        end do
!-----------Insert additional points if distance between points is higher than dcb
        allocate(segTmp(size(segArr)))
        segTmp(1:size(segArr)) = segArr(1:size(segArr))
        do i = 1, nSegCur - 1
          if (segTmp(i+1) - segTmp(i) > dcb/cos(PI + phi)) then
          nInsSeg = int((segTmp(i+1) - segTmp(i))*cos(phi)/dcb) + 1
          dSeg = (segTmp(i+1) - segTmp(i))/nInsSeg
          do l = 1, nInsSeg - 1
            sgLngth = segTmp(i) + l*dSeg
            call ArrInsValReal(segArr, sgLngth, nSegCur)
          end do
        end if
        end do
        deallocate(segTmp)
!--------------CREATION OF THE CHORDS---------------
        ! Loop on the boundaries defined for integration (stored in segArr)
        do iChord = 1, nSegCur - 1
          ! Check whether the sizes of the arrays are enough to store new chord
          ! If not, increase the sizes
          if (nChordBet > size(nSegChordBet)) then
            call SetArraySize2DInt(nBeta_key, size(nSegChordBet) + nRealInc, 7)
            call ArrExpInt(nSegChordBet)
            call ArrExpReal(wghtChordBet)
            call SetArraySize2DReal(lenChordBet, size(nSegChordBet) + nRealInc, nSegMax)
            call SetArraySize2DReal(xChordBet, size(nSegChordBet) + nRealInc, nPntsMax)
            call SetArraySize2DReal(yChordBet, size(nSegChordBet) + nRealInc, nPntsMax)
            call SetArraySize2DInt(zoneChordBet, size(nSegChordBet) + nRealInc, nSegMax)
          end if
          ! Get input side for the chord
          inSide = iSide
          ! Get input segment
          sgLngth = (segArr(iChord) + segArr(iChord+1))*0.5_rp
          inSegm = getSegmentNumber(sgLngth, segSdArr(iSide,1:nSegments(iSide)+1), &
                                                 nSegments(iSide)+1)
          ! Get input sector
          inSect = getSectorNumber(-normVecX(iSide), -normVecY(iSide), vecDirX, vecDirY, &
                                                 phiSect, nPhi+1)
          ! Weight of the sector
          if (iChord == 1) phiWght(inSect,iSide) = phiWght(inSect,iSide) + (phiBet(iSide,iPhi+1)-phiBet(iSide,iPhi))*0.5_rp*cos(phi)
          ! Get coordinates x and y for the first crossing point of the chord with outer boundary
          nPnts = 1
          nSeg  = 0
          fact = sgLngth/sdLen
          x0 = p1(1) + (p2(1) - p1(1))*fact
          y0 = p1(2) + (p2(2) - p1(2))*fact
          xChordBet(nChordBet,nPnts) = x0
          yChordBet(nChordBet,nPnts) = y0
          ! Get the output side for the chord
          outSide = getOutSideNumber(iSide, x0, y0, vecDirNormX, vecDirNormY, nSides, verX, verY)
          if (outSide < inSide) cycle
          ! Next crossing points with circles
          isCrossing = .true.
          do while(isCrossing)
            dstMin = huge(dstMin)
            isCrossing = .false.
            do i = 1, nCirc
              dist = getMinDistanceToCircle(x0, y0, vecDirNormX, vecDirNormY, cx(i), cy(i), Rad(i))
              if (dist > 0.0_rp) then
                isCrossing = .true.
                if (dist < dstMin) dstMin = dist
              end if
            end do
            if (isCrossing) then
              dist = dstMin
              nPnts = nPnts + 1
              nSeg  = nSeg + 1
              lenChordBet(nChordBet,nSeg) = dist
              x0 = x0 + dist*vecDirNormX
              y0 = y0 + dist*vecDirNormY
              zoneChordBet(nChordBet,nSeg) = getZoneNumber(x0-0.5_rp*dist*vecDirNormX,y0-0.5_rp*dist*vecDirNormY,nCirc,cx,cy,Rad)
              xChordBet(nChordBet,nPnts) = x0
              yChordBet(nChordBet,nPnts) = y0
            end if
          end do
          ! Crossing point with outer boundary
          nPnts = nPnts + 1
          nSeg  = nSeg + 1
          if (outSide < nSides) then
            call GetCoeffA(verY(outSide), verY(outSide+1), A)
            call GetCoeffB(verX(outSide), verX(outSide+1), B)
            call GetCoeffC(verX(outSide), verY(outSide), verX(outSide+1), verY(outSide+1), C)
          else
            call GetCoeffA(verY(outSide), verY(1), A)
            call GetCoeffB(verX(outSide), verX(1), B)
            call GetCoeffC(verX(outSide), verY(outSide), verX(1), verY(1), C)
          end if
          dist = getMinDistanceToLine(x0,y0,vecDirNormX,vecDirNormY,A,B,C)
          lenChordBet(nChordBet,nSeg) = dist
          x0 = x0 + dist*vecDirNormX
          y0 = y0 + dist*vecDirNormY
          zoneChordBet(nChordBet,nSeg) = getZoneNumber(x0-0.5_rp*dist*vecDirNormX,y0-0.5_rp*dist*vecDirNormY,nCirc,cx,cy,Rad)
          xChordBet(nChordBet,nPnts) = x0
          yChordBet(nChordBet,nPnts) = y0
          nSegChordBet(nChordBet) = nSeg
          iSide1 = outSide
          ! Output segment of the chord
          sgLngth = getSegmentLength(verX(iSide1), verY(iSide1), x0, y0)
          outSegm = getSegmentNumber(sgLngth, segSdArr(iSide1,1:nSegments(iSide1)+1), &
                                                 nSegments(iSide1)+1)
          ! Output sector of the chord
          outSect = getSectorNumber(normVecX(iSide1), normVecY(iSide1), vecDirX, vecDirY, &
                                                 phiSect, nPhi+1)
          ! Number of beta element where chord contributes to
          nBeta_key(nChordBet,1:7) = (/inSide,inSegm,inSect,outSide,outSegm,outSect,1/)
          wghtChordBet(nChordBet) = (segArr(iChord+1) - segArr(iChord))*(phiBet(iSide,iPhi+1)-phiBet(iSide,iPhi))*0.5_rp*cos(phi)
          nChordBet = nChordBet + 1
        end do
      end do ! End of cycles on phi boundaries
    end do ! End of cycle on sides

    nChordBet = nChordBet - 1
    do i = 1, nChordBet
      inSide = nBeta_key(i,1)
      inSegm = nBeta_key(i,2)
      inSect = nBeta_key(i,3)
      sgLngth = segSdArr(inSide,inSegm+1)
      if (inSegm /= 1) sgLngth = sgLngth - segSdArr(inSide,inSegm)
      wghtChordBet(i) = wghtChordBet(i)/(sgLngth*phiWght(inSect,inSide))
    end do

    nBetaElem = nBet

! Extension of the beta elemets using reciprocity equation
    if (2*nBetaElem > nRealInc) then
      if (.not. allocated(brc)) allocate(brc(2*nBetaElem))
    else
      if (.not. allocated(brc)) allocate(brc(nRealInc))
    end if

    brc(1:nBetaElem) = 1.0

    deallocate(segArr)
    deallocate(phiWght)

  end subroutine ChordsBetaHash

  subroutine ChordsGamma(nSides, verX, verY, nSegments, segBoundX, segBoundY, segSdArr, nPhi, phiSect, &
                         normVecX, normVecY, nCirc, Rad, cX, cY, nPhiGam, phiGam, dcg, nChordGam,      &
                         nSegChordGam, lenChordGam, wghtChordGam, zoneChordGam, xChordGam, yChordGam,  &
                         inSideGam, inSegGam, inSectGam, inZoneGam, inHarmGam, nGamma, nGammaElem)
    use ArrayMod, only : nRealInc, ArrExpReal, ArrExpInt, ArrInsValReal, SetArraySize2DReal, SetArraySize2DInt
!    use GlobalMod, only : segSdArr
    use ConstantsMod, only : EPS => DELTA_MIN
    use GeometryMod
    use DrawMod
    use Config
    implicit none

! Input
    integer, intent(in) :: nSides         ! Number of the polygon's sides
    real(rp), intent(in) :: verX(:)        ! x coordinates of the polygon's sides
    real(rp), intent(in) :: verY(:)        ! y coordinates of the polygon's sides
    integer, intent(in) :: nSegments(:)   ! Number of segments on the polygon's sides
    real(rp), intent(in) :: segBoundX(:,:) ! x-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: segBoundY(:,:) ! y-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: segSdArr(:,:)  ! One dimensional coordinates of the segments on the cell's sides
    integer, intent(in) :: nPhi           ! Number of sectors on azimuthal angle
    real(rp), intent(in) :: phiSect(:)     ! Sectors of the subdivision of the azimuthal angle
    real(rp), intent(in) :: normVecX(:)    ! x-coordinate of the normal vectors to the polygon's side
    real(rp), intent(in) :: normVecY(:)    ! y-coordinate of the normal vectors to the polygon's side
    integer, intent(in) :: nCirc          ! number of the circles inscribed into polygon
    real(rp), intent(in) :: Rad(:)         ! radii of the inscribed circles
    real(rp), intent(in) :: cX(:)          ! x-coordinates of the centres of the inscribed circles
    real(rp), intent(in) :: cY(:)          ! y-coordinates of the centres of the inscribed circles
    integer, intent(in) :: nPhiGam(:)     ! (side) number of intervals for integration over phi angle (given for each side of the polygon)
    real(rp), intent(in) :: phiGam(:,:)    ! (side,angles) boundaries of intervals for integration over phi angle (given for each side of the polygon)
    real(rp), intent(in) :: dcg            ! maximal distance between chords

! Output
    integer, intent(out)              :: nChordGam         ! number of chords for integration of surface-to-surface CP (beta)
    integer, allocatable, intent(out) :: nSegChordGam(:)   ! number of segments along the chord for integration beta
    real(rp), allocatable, intent(out) :: lenChordGam(:,:)  ! length of segments along the chord
    real(rp), allocatable, intent(out) :: wghtChordGam(:)   ! weight of the beta chord
    integer, allocatable, intent(out) :: zoneChordGam(:,:) ! zones' numbers along the chord (for definition of the materials)
    real(rp), allocatable, intent(out) :: xChordGam(:,:)    ! x-coordinates of the segments along the chord
    real(rp), allocatable, intent(out) :: yChordGam(:,:)    ! y-coordinates of the segments along the chord
    integer, allocatable, intent(out) :: inSideGam(:)      ! number of input side for chord
    integer, allocatable, intent(out) :: inSegGam(:)       ! number of input segment for chord
    integer, allocatable, intent(out) :: inSectGam(:)      ! number of input sector for chord
    integer, allocatable, intent(out) :: inZoneGam(:)
    integer, allocatable, intent(out) :: inHarmGam(:)
    integer, allocatable, intent(out) :: nGamma(:)          ! number of beta element where where chord contributes
    integer, intent(out)              :: nGammaElem         ! total number of beta elements

! Local
    real(rp), allocatable :: segArr(:), segTmp(:) ! Working array contains boundaries of the intervals of integration over sides
    real(rp), allocatable :: phiWght(:,:)
    real(rp) :: phi ! angle
    real(rp) :: sdLen, sgLngth, dSeg, fact, dist, dstMin, sectWght
    real(rp) :: xt1, yt1, xt2, yt2, x0, y0
    real(rp) :: A, B, C, A1, B1, C1, A2, B2, C2, p(2), p1(2), p2(2)
    real(rp) :: vecDirX, vecDirY ! x and y coordinates of the directional vector
    real(rp) :: vecDirNormX, vecDirNormY
    integer :: iSide, iSide1, iSeg, iPhi, iCirc, ival, nInsSeg
    integer :: inSide, inSect, inSegm, outSide, outSegm, outSect
    integer :: iChord, nSegMax, nPntsMax, nPnts, nSeg, nGam
    integer :: nSegCur ! Current number of segments in segArr
    integer :: i, l
    integer :: nSZHGam
    logical :: isCrossing

    ! Preparation
    nSZHGam = 1
    nChordGam = 1
    nSegMax = 2*nCirc + 1
    nPntsMax = 2*(nCirc + 1)
    allocate(segArr(nRealInc))
    allocate(inSideGam(nRealInc))
    allocate(inSegGam(nRealInc))
    allocate(inSectGam(nRealInc))
    allocate(inZoneGam(nRealInc))
    allocate(inHarmGam(nRealInc))
    allocate(nSegChordGam(nRealInc))
    allocate(wghtChordGam(nRealInc))
    allocate(zoneChordGam(nRealInc,nSegMax))
    allocate(lenChordGam(nRealInc,nSegMax))
    allocate(xChordGam(nRealInc,nPntsMax))
    allocate(yChordGam(nRealInc,nPntsMax))
    allocate(nGamma(nRealInc))
    inSideGam = 0
    inSectGam = 0
    inSegGam  = 0
    inZoneGam = 0
    inHarmGam = 0
    nSegChordGam = 0
    wghtChordGam = 0.0_rp
    zoneChordGam = 0
    lenChordGam = 0.0_rp
    xChordGam = 0.0_rp
    yChordGam = 0.0_rp
    nGamma = 0
    allocate(phiWght(nPhi,nSides))
    phiWght  = 0.0_rp
    nGam = 0

!   Start of the ray tracing
    do iSide = 1, nSides
      ! Getting the boundaries (points) of the side
      if (iSide < nSides) then
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(iSide+1)
        p2(2) = verY(iSide+1)
      else
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(1)
        p2(2) = verY(1)
      end if
      sdLen = getSegmentLength(p1(1),p1(2),p2(1),p2(2)) ! Length of the side
      ! Coefficients for the line containing side
      if (iSide < nSides) then
        call GetCoeffA(verY(iSide), verY(iSide + 1), A2)
        call GetCoeffB(verX(iSide), verX(iSide + 1), B2)
        call GetCoeffC(verX(iSide), verY(iSide), verX(iSide + 1), verY(iSide + 1), C2)
      else
        call GetCoeffA(verY(iSide), verY(1), A2)
        call GetCoeffB(verX(iSide), verX(1), B2)
        call GetCoeffC(verX(iSide), verY(iSide), verX(1), verY(1), C2)
      end if
      ! Cycle on previously calculated angles
      do iPhi = 1, nPhiGam(iSide) - 1
        phi = (phiGam(iSide,iPhi)+phiGam(iSide,iPhi+1))*0.5_rp
        segArr = 0.0_rp
        ! Insert into segArr boundaries of the segments on the side
        nSegCur = 1
        segArr(nSegCur) = 0.0_rp
        do i = 2, nSegments(iSide)
          if (nSegCur >= size(segArr)) call ArrExpReal(segArr)
          sgLngth = getSegmentLength(p1(1), p1(2), segBoundX(iSide,i), segBoundY(iSide,i))
          call ArrInsValReal(segArr,sgLngth,nSegCur)
        end do
        if (nSegCur >= size(segArr)) call ArrExpReal(segArr)
        sgLngth = getSegmentLength(p1(1),p1(2),p2(1),p2(2))
        call ArrInsValReal(segArr,sgLngth,nSegCur)
        ! Coordinates of the directional vector
        vecDirX = -normVecX(iSide)
        vecDirY = -normVecY(iSide)
        call RotateVec(vecDirX, vecDirY, phi)
        ! Normalize vector to unity
        vecDirNormX = vecDirX
        vecDirNormY = vecDirY
        call NormalizeVector(vecDirNormX, vecDirNormY)
        ! Take into account tangential lines of the circles
        do iCirc = 1, nCirc
          ! Getting tangential points on the circle for line containing directional vector (vecDirX,vecDirY)
          call GetTangentPointsVec(vecDirX,vecDirY,cx(iCirc),cy(iCirc),Rad(iCirc),xt1,yt1,xt2,yt2)
          ! Coefficients of the first tangential lines
          A1 =  vecDirY
          B1 = -vecDirX
          C1 =  vecDirX*yt1 - vecDirY*xt1
          ! Intersection with the side (if exists)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          ! Side contains point p
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
!         Second tangential point
          C1 =  vecDirX*yt2 - vecDirY*xt2
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do ! End of cycle on circles
        ! Take into account centres of the circles
        do iCirc = 1, nCirc
          C1 =  vecDirX*cy(iCirc) - vecDirY*cx(iCirc)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do
        ! Take into account side's vertices on the other sides
        ! Get A and B coefficients for line containing directional vector
        A1 =  vecDirY
        B1 = -vecDirX
        do iSide1 = 1, nSides
          if (iSide1 == iSide) cycle
          iSeg = nSegments(iSide1) + 1
          C1 =  vecDirX*segBoundY(iSide1,iSeg) - vecDirY*segBoundX(iSide1,iSeg)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do
!-----------Insert additional points if distance between points is higher than dcb
        allocate(segTmp(size(segArr)))
        segTmp(1:size(segArr)) = segArr(1:size(segArr))
        do i = 1, nSegCur - 1
          if (segTmp(i+1) - segTmp(i) > dcg/cos(PI + phi)) then
          nInsSeg = int((segTmp(i+1) - segTmp(i))*cos(phi)/dcg) + 1
          dSeg = (segTmp(i+1) - segTmp(i))/nInsSeg
          do l = 1, nInsSeg - 1
            sgLngth = segTmp(i) + l*dSeg
            call ArrInsValReal(segArr, sgLngth, nSegCur)
          end do
        end if
        end do
        deallocate(segTmp)
!--------------CREATION OF THE CHORDS---------------
        ! Loop on the boundaries defined for integration (stored in segArr)
        do iChord = 1, nSegCur - 1
          ! Check whether the sizes of the arrays are enough to store new chord
          ! If not, increase the sizes
          if (nChordGam > size(nSegChordGam)) then
            call ArrExpInt(nGamma)
            call ArrExpInt(nSegChordGam)
            call ArrExpReal(wghtChordGam)
            call SetArraySize2DReal(lenChordGam, size(nSegChordGam) + nRealInc, nSegMax)
            call SetArraySize2DReal(xChordGam, size(nSegChordGam) + nRealInc, nPntsMax)
            call SetArraySize2DReal(yChordGam, size(nSegChordGam) + nRealInc, nPntsMax)
            call SetArraySize2DInt(zoneChordGam, size(nSegChordGam) + nRealInc, nSegMax)
          end if
          ! Get input side for the chord
          inSide = iSide
          ! Get input segment
          sgLngth = (segArr(iChord) + segArr(iChord+1))*0.5_rp
          inSegm = getSegmentNumber(sgLngth, segSdArr(iSide,1:nSegments(iSide)+1), &
                                                 nSegments(iSide)+1)
          ! Get input sector
          inSect = getSectorNumber(-normVecX(iSide), -normVecY(iSide), vecDirX, vecDirY, &
                                                 phiSect, nPhi+1)
          if (nChordGam > size(inSideGam)) then
            call ArrExpInt(inSideGam)
            call ArrExpInt(inSegGam)
            call ArrExpInt(inSectGam)
          end if
          inSideGam(nChordGam) = inSide
          inSegGam(nChordGam)  = inSegm
          inSectGam(nChordGam) = inSect
          ! Weight of the sector
          if (iChord == 1) phiWght(inSect,iSide) = phiWght(inSect,iSide) + (phiGam(iSide,iPhi+1)-phiGam(iSide,iPhi))*0.5_rp*cos(phi)
          ! Get coordinates x and y for the first crossing point of the chord with outer boundary
          nPnts = 1
          nSeg  = 0
          fact = sgLngth/sdLen
          x0 = p1(1) + (p2(1) - p1(1))*fact
          y0 = p1(2) + (p2(2) - p1(2))*fact
          xChordGam(nChordGam,nPnts) = x0
          yChordGam(nChordGam,nPnts) = y0
          ! Get the output side for the chord
          outSide = getOutSideNumber(iSide, x0, y0, vecDirNormX, vecDirNormY, nSides, verX, verY)
          ! Next crossing points with circles
          isCrossing = .true.
          do while(isCrossing)
            dstMin = huge(dstMin)
            isCrossing = .false.
            do i = 1, nCirc
              dist = getMinDistanceToCircle(x0, y0, vecDirNormX, vecDirNormY, cx(i), cy(i), Rad(i))
              if (dist > 0.0_rp) then
                isCrossing = .true.
                if (dist < dstMin) dstMin = dist
              end if
            end do
            if (isCrossing) then
              dist = dstMin
              nPnts = nPnts + 1
              nSeg  = nSeg + 1
              lenChordGam(nChordGam,nSeg) = dist
              x0 = x0 + dist*vecDirNormX
              y0 = y0 + dist*vecDirNormY
              zoneChordGam(nChordGam,nSeg) = getZoneNumber(x0-0.5_rp*dist*vecDirNormX,y0-0.5_rp*dist*vecDirNormY,nCirc,cx,cy,Rad)
              xChordGam(nChordGam,nPnts) = x0
              yChordGam(nChordGam,nPnts) = y0
            end if
          end do
          ! Crossing point with outer boundary
          nPnts = nPnts + 1
          nSeg  = nSeg + 1
          if (outSide < nSides) then
            call GetCoeffA(verY(outSide), verY(outSide+1), A)
            call GetCoeffB(verX(outSide), verX(outSide+1), B)
            call GetCoeffC(verX(outSide), verY(outSide), verX(outSide+1), verY(outSide+1), C)
          else
            call GetCoeffA(verY(outSide), verY(1), A)
            call GetCoeffB(verX(outSide), verX(1), B)
            call GetCoeffC(verX(outSide), verY(outSide), verX(1), verY(1), C)
          end if
          dist = getMinDistanceToLine(x0,y0,vecDirNormX,vecDirNormY,A,B,C)
          lenChordGam(nChordGam,nSeg) = dist
          x0 = x0 + dist*vecDirNormX
          y0 = y0 + dist*vecDirNormY
          zoneChordGam(nChordGam,nSeg) = getZoneNumber(x0-0.5_rp*dist*vecDirNormX,y0-0.5_rp*dist*vecDirNormY,nCirc,cx,cy,Rad)
          xChordGam(nChordGam,nPnts) = x0
          yChordGam(nChordGam,nPnts) = y0
          nSegChordGam(nChordGam) = nSeg
          iSide1 = outSide
          ! Output segment of the chord
          sgLngth = getSegmentLength(verX(iSide1), verY(iSide1), x0, y0)
          outSegm = getSegmentNumber(sgLngth, segSdArr(iSide1,1:nSegments(iSide1)+1), &
                                                 nSegments(iSide1)+1)
          ! Output sector of the chord
          outSect = getSectorNumber(normVecX(iSide1), normVecY(iSide1), vecDirX, vecDirY, &
                                                 phiSect, nPhi+1)
          wghtChordGam(nChordGam) = (segArr(iChord+1) - segArr(iChord))*(phiGam(iSide,iPhi+1)-phiGam(iSide,iPhi))*0.125_rp*cos(phi)
          sgLngth = segSdArr(inSide,inSegm + 1) - segSdArr(inSide,inSegm)
          sectWght = (sin(phiSect(inSect+1))-sin(phiSect(inSect)))*0.5_rp
          wghtChordGam(nChordGam) = 4.0_rp*wghtChordGam(nChordGam)/(sgLngth*sectWght)
          nChordGam = nChordGam + 1
        end do
      end do ! End of cycles on phi boundaries
    end do ! End of cycle on sides

    nGammaElem = nGam

    deallocate(segArr)
    deallocate(phiWght)

  end subroutine ChordsGamma

  subroutine ChordsU(nSides, verX, verY, nSegments, segBoundX, segBoundY, nPhi, phiSect, normVecX, normVecY, &
                     nCirc, Rad, cX, cY, nPhiU, phiU, dcu, nChordU, nSegChordU, lenChordU, wghtChordU, zoneChordU,  &
                     xChordU, yChordU, zoneFromU, zoneToU, harmFromU, harmToU, nU, nUElem)

    use ArrayMod, only : nRealInc, ArrExpReal, ArrExpInt, ArrInsValReal, SetArraySize2DReal, SetArraySize2DInt
!    use GlobalMod, only : segSdArr
    use ConstantsMod, only : EPS => DELTA_MIN
    use GeometryMod
    use DrawMod
    use Config

    implicit none

! Input
    integer, intent(in) :: nSides         ! Number of the polygon's sides
    real(rp), intent(in) :: verX(:)        ! x coordinates of the polygon's sides
    real(rp), intent(in) :: verY(:)        ! y coordinates of the polygon's sides
    integer, intent(in) :: nSegments(:)   ! Number of segments on the polygon's sides
    real(rp), intent(in) :: segBoundX(:,:) ! x-coordinates of the segments on the polygon's sides
    real(rp), intent(in) :: segBoundY(:,:) ! y-coordinates of the segments on the polygon's sides
    integer, intent(in) :: nPhi           ! Number of sectors on azimuthal angle
    real(rp), intent(in) :: phiSect(:)     ! Sectors of the subdivision of the azimuthal angle
    real(rp), intent(in) :: normVecX(:)    ! x-coordinate of the normal vectors to the polygon's side
    real(rp), intent(in) :: normVecY(:)    ! y-coordinate of the normal vectors to the polygon's side
    integer, intent(in) :: nCirc          ! number of the circles inscribed into polygon
    real(rp), intent(in) :: Rad(:)         ! radii of the inscribed circles
    real(rp), intent(in) :: cX(:)          ! x-coordinates of the centres of the inscribed circles
    real(rp), intent(in) :: cY(:)          ! y-coordinates of the centres of the inscribed circles
    integer, intent(in) :: nPhiU(:)       ! (side) number of intervals for integration over phi angle (given for each side of the polygon)
    real(rp), intent(in) :: phiU(:,:)      ! (side,angles) boundaries of intervals for integration over phi angle (given for each side of the polygon)
    real(rp), intent(in) :: dcu            ! maximal distance between chords

! Output
    integer, intent(out)              :: nChordU         ! number of chords for integration of region-to-region CP (U)
    integer, allocatable, intent(out) :: nSegChordU(:)   ! number of segments along the chord for integration U
    real(rp), allocatable, intent(out) :: lenChordU(:,:)  ! length of segments along the chord
    real(rp), allocatable, intent(out) :: wghtChordU(:)   ! weight of the U chord
    integer, allocatable, intent(out) :: zoneChordU(:,:) ! zones' numbers along the chord (for definition of the materials)
    real(rp), allocatable, intent(out) :: xChordU(:,:)    ! x-coordinates of the segments along the chord
    real(rp), allocatable, intent(out) :: yChordU(:,:)    ! y-coordinates of the segments along the chord
    integer, allocatable, intent(out) :: zoneFromU(:)    ! number of zone from where neutron going out
    integer, allocatable, intent(out) :: zoneToU(:)      ! number of zone where neutron goes
    integer, allocatable, intent(out) :: harmFromU(:)    ! number of harmonic from
    integer, allocatable, intent(out) :: harmToU(:)      ! number of harmonic to
    integer, allocatable, intent(out) :: nU(:)           ! number of U element where chord contributes
    integer, intent(out)              :: nUElem          ! total number of U elements

! Local
    real(rp), allocatable :: segArr(:), segTmp(:) ! Working array contains boundaries of the intervals of integration over sides
    real(rp), allocatable :: phiWght(:,:)
    real(rp) :: phi ! angle
    real(rp) :: sdLen, sgLngth, dSeg, fact, dist, dstMin
    real(rp) :: xt1, yt1, xt2, yt2, x0, y0
    real(rp) :: A, B, C, A1, B1, C1, A2, B2, C2, p(2), p1(2), p2(2)
    real(rp) :: vecDirX, vecDirY ! x and y coordinates of the directional vector
    real(rp) :: vecDirNormX, vecDirNormY
    integer :: iSide, inSect, iSide1, iSeg, iPhi, iCirc, ival, nInsSeg
    integer :: inSide, outSide, outSegm, outSect
    integer :: iChord, nSegMax, nPntsMax, nPnts, nSeg, nUu
    integer :: nSegCur ! Current number of segments in segArr
    integer :: i, l
    integer :: nSZHU
    logical :: isCrossing

    ! Preparation
    nSZHU = 1
    nChordU = 1
    nSegMax = 2*nCirc + 1
    nPntsMax = 2*(nCirc + 1)
    allocate(segArr(nRealInc))
    allocate(zoneFromU(nRealInc))
    allocate(zoneToU(nRealInc))
    allocate(harmFromU(nRealInc))
    allocate(harmToU(nRealInc))
    allocate(nSegChordU(nRealInc))
    allocate(wghtChordU(nRealInc))
    allocate(zoneChordU(nRealInc,nSegMax))
    allocate(lenChordU(nRealInc,nSegMax))
    allocate(xChordU(nRealInc,nPntsMax))
    allocate(yChordU(nRealInc,nPntsMax))
    allocate(nU(nRealInc))
    zoneFromU = 0
    zoneToU = 0
    harmFromU  = 0
    harmToU = 0
    nSegChordU = 0
    wghtChordU = 0.0_rp
    zoneChordU = 0
    lenChordU = 0.0_rp
    xChordU = 0.0_rp
    yChordU = 0.0_rp
    nU = 0
    allocate(phiWght(nPhi,nSides))
    phiWght  = 0.0_rp
    nUu = 0

!   Start of the ray tracing
    do iSide = 1, nSides
      ! Getting the boundaries (points) of the side
      if (iSide < nSides) then
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(iSide+1)
        p2(2) = verY(iSide+1)
      else
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(1)
        p2(2) = verY(1)
      end if
      sdLen = getSegmentLength(p1(1),p1(2),p2(1),p2(2)) ! Length of the side
      ! Coefficients for the line containing side
      if (iSide < nSides) then
        call GetCoeffA(verY(iSide), verY(iSide + 1), A2)
        call GetCoeffB(verX(iSide), verX(iSide + 1), B2)
        call GetCoeffC(verX(iSide), verY(iSide), verX(iSide + 1), verY(iSide + 1), C2)
      else
        call GetCoeffA(verY(iSide), verY(1), A2)
        call GetCoeffB(verX(iSide), verX(1), B2)
        call GetCoeffC(verX(iSide), verY(iSide), verX(1), verY(1), C2)
      end if
      ! Cycle on previously calculated angles
      do iPhi = 1, nPhiU(iSide) - 1
        phi = (phiU(iSide,iPhi)+phiU(iSide,iPhi+1))*0.5_rp
        segArr = 0.0_rp
        ! Insert into segArr boundaries of the segments on the side
        nSegCur = 1
        segArr(nSegCur) = 0.0_rp
        sgLngth = getSegmentLength(p1(1), p1(2), segBoundX(iSide,nSegments(iSide)+1), segBoundY(iSide,nSegments(iSide)+1))
        call ArrInsValReal(segArr,sgLngth,nSegCur)
        if (nSegCur >= size(segArr)) call ArrExpReal(segArr)
        sgLngth = getSegmentLength(p1(1),p1(2),p2(1),p2(2))
        call ArrInsValReal(segArr,sgLngth,nSegCur)
        ! Coordinates of the directional vector
        vecDirX = -normVecX(iSide)
        vecDirY = -normVecY(iSide)
        call RotateVec(vecDirX, vecDirY, phi)
        ! Normalize vector to unity
        vecDirNormX = vecDirX
        vecDirNormY = vecDirY
        call NormalizeVector(vecDirNormX, vecDirNormY)
        ! Take into account tangential lines of the circles
        do iCirc = 1, nCirc
          ! Getting tangential points on the circle for line containing directional vector (vecDirX,vecDirY)
          call GetTangentPointsVec(vecDirX,vecDirY,cx(iCirc),cy(iCirc),Rad(iCirc),xt1,yt1,xt2,yt2)
          ! Coefficients of the first tangential lines
          A1 =  vecDirY
          B1 = -vecDirX
          C1 =  vecDirX*yt1 - vecDirY*xt1
          ! Intersection with the side (if exists)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          ! Side contains point p
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
!         Second tangential point
          C1 =  vecDirX*yt2 - vecDirY*xt2
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do ! End of cycle on circles
        ! Take into account centres of the circles
        do iCirc = 1, nCirc
          C1 =  vecDirX*cy(iCirc) - vecDirY*cx(iCirc)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do
        ! Take into account side's vertices on the other sides
        ! Get A and B coefficients for line containing directional vector
        A1 =  vecDirY
        B1 = -vecDirX
        do iSide1 = 1, nSides
          if (iSide1 == iSide) cycle
          iSeg = nSegments(iSide1) + 1
          C1 =  vecDirX*segBoundY(iSide1,iSeg) - vecDirY*segBoundX(iSide1,iSeg)
          call lines_imp_int_2d(A1,B1,C1,A2,B2,C2,ival,p)
          if (isOnSegment(p(1),p(2),p1(1),p1(2),p2(1),p2(2))) then
            if (nSegCur > size(segArr)) call ArrExpReal(segArr)
            sgLngth = getSegmentLength(p1(1),p1(2),p(1),p(2))
            call ArrInsValReal(segArr,sgLngth,nSegCur)
          end if
        end do
!-----------Insert additional points if distance between points is higher than dcb
        allocate(segTmp(size(segArr)))
        segTmp(1:size(segArr)) = segArr(1:size(segArr))
        do i = 1, nSegCur - 1
          if (segTmp(i+1) - segTmp(i) > dcu/cos(PI + phi)) then
          nInsSeg = int((segTmp(i+1) - segTmp(i))*cos(phi)/dcu) + 1
          dSeg = (segTmp(i+1) - segTmp(i))/nInsSeg
          do l = 1, nInsSeg - 1
            sgLngth = segTmp(i) + l*dSeg
            call ArrInsValReal(segArr, sgLngth, nSegCur)
          end do
        end if
        end do
        deallocate(segTmp)
!--------------CREATION OF THE CHORDS---------------
        ! Loop on the boundaries defined for integration (stored in segArr)
        do iChord = 1, nSegCur - 1
          ! Check whether the sizes of the arrays are enough to store new chord
          ! If not, increase the sizes
          if (nChordU > size(nSegChordU)) then
            call ArrExpInt(nU)
            call ArrExpInt(nSegChordU)
            call ArrExpReal(wghtChordU)
            call SetArraySize2DReal(lenChordU, size(nSegChordU) + nRealInc, nSegMax)
            call SetArraySize2DReal(xChordU, size(nSegChordU) + nRealInc, nPntsMax)
            call SetArraySize2DReal(yChordU, size(nSegChordU) + nRealInc, nPntsMax)
            call SetArraySize2DInt(zoneChordU, size(nSegChordU) + nRealInc, nSegMax)
          end if
          ! Get input side for the chord
          inSide = iSide
          ! Get input segment
          sgLngth = (segArr(iChord) + segArr(iChord+1))*0.5_rp
          ! Get input sector
          inSect = getSectorNumber(-normVecX(iSide), -normVecY(iSide), vecDirX, vecDirY, &
                                                 phiSect, nPhi+1)
          ! Weight of the sector
          if (iChord == 1) phiWght(inSect,iSide) = phiWght(inSect,iSide) + (phiU(iSide,iPhi+1)-phiU(iSide,iPhi))*0.5_rp*cos(phi)
          ! Get coordinates x and y for the first crossing point of the chord with outer boundary
          nPnts = 1
          nSeg  = 0
          fact = sgLngth/sdLen
          x0 = p1(1) + (p2(1) - p1(1))*fact
          y0 = p1(2) + (p2(2) - p1(2))*fact
          xChordU(nChordU,nPnts) = x0
          yChordU(nChordU,nPnts) = y0
          ! Get the output side for the chord
          outSide = getOutSideNumber(iSide, x0, y0, vecDirNormX, vecDirNormY, nSides, verX, verY)
!DL+ 28.02.2018 Comment
! At the moment, this option (if (outSide < inSide) cycle) is used to decrease the number of chords for evaluation of the U
! However, investigations should be made further to check how will it influence the non-regular outer boundary
!DL-
          if (outSide < inSide) cycle
          ! Next crossing points with circles
          isCrossing = .true.
          do while(isCrossing)
            dstMin = huge(dstMin)
            isCrossing = .false.
            do i = 1, nCirc
              dist = getMinDistanceToCircle(x0, y0, vecDirNormX, vecDirNormY, cx(i), cy(i), Rad(i))
              if (dist > 0.0_rp) then
                isCrossing = .true.
                if (dist < dstMin) dstMin = dist
              end if
            end do
            if (isCrossing) then
              dist = dstMin
              nPnts = nPnts + 1
              nSeg  = nSeg + 1
              lenChordU(nChordU,nSeg) = dist
              x0 = x0 + dist*vecDirNormX
              y0 = y0 + dist*vecDirNormY
              zoneChordU(nChordU,nSeg) = getZoneNumber(x0-0.5_rp*dist*vecDirNormX,y0-0.5_rp*dist*vecDirNormY,nCirc,cx,cy,Rad)
              xChordU(nChordU,nPnts) = x0
              yChordU(nChordU,nPnts) = y0
            end if
          end do
          ! Crossing point with outer boundary
          nPnts = nPnts + 1
          nSeg  = nSeg + 1
          if (outSide < nSides) then
            call GetCoeffA(verY(outSide), verY(outSide+1), A)
            call GetCoeffB(verX(outSide), verX(outSide+1), B)
            call GetCoeffC(verX(outSide), verY(outSide), verX(outSide+1), verY(outSide+1), C)
          else
            call GetCoeffA(verY(outSide), verY(1), A)
            call GetCoeffB(verX(outSide), verX(1), B)
            call GetCoeffC(verX(outSide), verY(outSide), verX(1), verY(1), C)
          end if
          dist = getMinDistanceToLine(x0,y0,vecDirNormX,vecDirNormY,A,B,C)
          lenChordU(nChordU,nSeg) = dist
          x0 = x0 + dist*vecDirNormX
          y0 = y0 + dist*vecDirNormY
          zoneChordU(nChordU,nSeg) = getZoneNumber(x0-0.5_rp*dist*vecDirNormX,y0-0.5_rp*dist*vecDirNormY,nCirc,cx,cy,Rad)
          xChordU(nChordU,nPnts) = x0
          yChordU(nChordU,nPnts) = y0
          nSegChordU(nChordU) = nSeg
          iSide1 = outSide
          ! Output segment of the chord
          sgLngth = getSegmentLength(verX(iSide1), verY(iSide1), x0, y0)
!          outSegm = getSegmentNumber(sgLngth, segSdArr(iSide1,1:nSegments(iSide1)+1), &
!                                                 nSegments(iSide1)+1)
          ! Output sector of the chord
          outSect = getSectorNumber(normVecX(iSide1), normVecY(iSide1), vecDirX, vecDirY, &
                                                 phiSect, nPhi+1)
          ! Number of beta element where chord contributes to
          wghtChordU(nChordU) = (segArr(iChord+1) - segArr(iChord))*(phiU(iSide,iPhi+1)-phiU(iSide,iPhi))*0.125_rp*cos(phi)
          nChordU = nChordU + 1
        end do
      end do ! End of cycles on phi boundaries
    end do ! End of cycle on sides

    nUElem = nUu

    deallocate(segArr)
    deallocate(phiWght)

  end subroutine ChordsU

  subroutine RenormaliseCPs(Beta, nElemBet, inSideBet, inSegBet, inSectBet, &
                            nSides, nZones, vol, Stot, nHarm, nSegments,    &
                            nSect, ort, G, V, U)
  !=============================================================================
  ! Subroutine renormalises collision probabilities implementing the reciprocity
  ! and balance relations.
  ! It is assumed that the accuracy of the CPs evaluation is different
  ! List of the CPs from the highest to lowest accuracy:
  ! Beta
  ! G
  ! V
  ! U
  ! The following balance relations exists:
  ! sum(beta(s, n, m, 1:nSides, 1:nSegm, 1:nSect)) = sum(G(1:nZone, 1, s, n, m))
  ! if k == 1 : sum(V(1:nSides,1:nSegm,1:nSect,i,k)) = 1 - sum(U(i,k,1:nZones,1))
  ! if k /= 1 : sum(V(1:nSides,1:nSegm,1:nSect,i,k)) = -sum(U(i,k,1:nZones,1))
  !
  !=============================================================================
    implicit none

    real(rp), intent(in) :: Beta(:)
    integer, intent(in) :: nElemBet
    integer, intent(in) :: inSideBet(:)
    integer, intent(in) :: inSegBet(:)
    integer, intent(in) :: inSectBet(:)
    integer, intent(in) :: nSides
    integer, intent(in) :: nZones
    real(rp), intent(in) :: vol(:)
    real(rp), intent(in) :: Stot(:)
    integer, intent(in) :: nHarm
    integer, intent(in) :: nSegments(:)
    integer, intent(in) :: nSect
    real(rp), intent(in) :: ort(:,:,:)
    real(rp), intent(inout) :: G(:,:,:,:,:)
    real(rp), intent(inout) :: V(:,:,:,:,:)
    real(rp), intent(inout) :: U(:,:,:,:)

    integer :: i, k, s, n, m, iz
    integer :: iz1
    integer :: maxSeg
    real(rp), allocatable :: sB(:,:,:)
    real(rp), allocatable :: sG(:,:,:)
    real(rp), allocatable :: sV(:,:)
    real(rp), allocatable :: sU(:,:)
    real(rp) :: d

    maxSeg = maxval(nSegments)
    allocate(sB(nSides,maxSeg,nSect))
    allocate(sG(nSides,maxSeg,nSect))
    allocate(sV(nZones,nHarm))
    allocate(sU(nZones,nHarm))

    sG = 0.0_rp
    sB = 0.0_rp
    sV = 0.0_rp
    sU = 0.0_rp

    do i = 1, nElemBet
      s = inSideBet(i)
      n = inSegBet(i)
      m = inSectBet(i)
      sB(s,n,m) = sB(s,n,m) + Beta(i)
    end do
    do s = 1, nSides
      do n = 1, nSegments(s)
        do m = 1, nSect
          sG(s,n,m) = sum(G(1:nZones,1,s,n,m))
        end do
      end do
    end do

    do s = 1, nSides
      do n = 1, nSegments(s)
        do m = 1, nSect
          if (abs(1 - sB(s,n,m) - sG(s,n,m)) < 1.0d-8) then
            cycle
          else
            d = (1 - sB(s,n,m) - sG(s,n,m))/nZones
            do iz = 1, nZones
              G(iz,1,s,n,m) = G(iz,1,s,n,m) + d
            end do
            sG(s,n,m) = sum(G(1:nZones,1,s,n,m))
          end if
        end do
      end do
    end do

    do iz = 1, nZones
      do k = 1, nHarm
        sV(iz,k) = sum(V(1:nSides,1:maxSeg,1:nSect,iz,k))
        sU(iz,k) = sum(U(iz,k,1:nZones,1))
      end do
    end do

    do iz = 1, nZones
      do k = 1, nHarm
        if (abs(ort(iz,1,k)) < 1.0d-8) then
          cycle
        else
          if (k == 1) then
            if (abs(1.0_rp - sV(iz,k) - sU(iz,k)) < 1.0d-8) then
              cycle
            else
              do iz1 = 1, nZones
                d = (1.0_rp - sV(iz,k) - sU(iz,k))*vol(iz1)/sum(vol)
                U(iz,k,iz1,1) = U(iz,k,iz1,1) + d
              end do
            end if
          else
            if (abs(sV(iz,k) + sU(iz,k)) < 1.0d-8) then
              cycle
            else
              do iz1 = 1, nZones
                d = (sV(iz,k) + sU(iz,k))*vol(iz1)/sum(vol)
                U(iz,k,iz1,1) = U(iz,k,iz1,1) - d
              end do
            end if
          end if
        end if
      end do
    end do

    do iz1 = 1, nZones
      do iz = 1, nZones
        if (iz1 == iz) then
          do k = 1, nHarm
            U(iz1,1,iz,k) = U(iz1,k,iz,1)
          end do
        else
          do k = 1, nHarm
            U(iz1,1,iz,k) = U(iz,k,iz1,1)*vol(iz)*Stot(iz)/(vol(iz1)*Stot(iz1))
          end do
        end if
      end do
    end do

  end subroutine RenormaliseCPs

  integer function getSegmentNumber(segLen, segBounds, nSegBounds)
  !------------------------------------------------------
  ! Function returns number of segment where point with
  ! x coordinate = segLen lays. Boundaries of the segments
  ! in array segBounds should be sorted in ascending order
  !-------------------------------------------------------
    implicit none

    real(rp), intent(in) :: segLen
    real(rp), intent(in) :: segBounds(:)
    integer, intent(in) :: nSegBounds

    integer :: i

    do i = 1, nSegBounds
      if (segLen <= segBounds(i)) then
        getSegmentNumber = i - 1
        return
      end if
    end do

    write(*,*) 'Function <getSegmentNumber>'
    write(*,*) 'segLen is higher than segBounds!'
    stop

  end function getSegmentNumber

  integer function getSectorNumber(dirVecX, dirVecY, normVecX, normVecY, sectBounds, nSectBounds)
    use GeometryMod, only: getAngle
    implicit none

    real(rp), intent(in) :: dirVecX
    real(rp), intent(in) :: dirVecY
    real(rp), intent(in) :: normVecX
    real(rp), intent(in) :: normVecY
    real(rp), intent(in) :: sectBounds(:) ! Boundaries of sectors in ascending order
    integer, intent(in) :: nSectBounds

    real(rp) :: ang
    integer :: i

    ang = getAngle(dirVecX,dirVecY,normVecX,normVecY)

    do i = 1, nSectBounds
      if (ang < sectBounds(i)) then
        getSectorNumber = i - 1
        return
      end if
    end do

    write(*,*) 'Function <getSectorNumber>'
    write(*,*) 'ang is higher than sectBounds!'
    stop

  end function getSectorNumber

  integer function getOutSideNumber(iSide, x0, y0, vecDirNormX, vecDirNormY, nSides, vertX, vertY)
    use GeometryMod, only : getMinDistanceToLine, GetCoeffA, GetCoeffB, GetCoeffC
    implicit none
    integer, intent(in) :: iSide
    real(rp), intent(in) :: x0
    real(rp), intent(in) :: y0
    real(rp), intent(in) :: vecDirNormX
    real(rp), intent(in) :: VecDirNormY
    integer, intent(in) :: nSides
    real(rp), intent(in) :: vertX(:), vertY(:)

    integer :: i, iSide1
    real(rp) :: A, B, C, dist, dstMin

    dstMin = huge(dstMin)
    do i = 1, nSides
      if (i == iSide) cycle
      if (i < nSides) then
        call GetCoeffA(vertY(i), vertY(i+1), A)
        call GetCoeffB(vertX(i), vertX(i+1), B)
        call GetCoeffC(vertX(i), vertY(i), vertX(i+1), vertY(i+1), C)
      else
        call GetCoeffA(vertY(i), vertY(1), A)
        call GetCoeffB(vertX(i), vertX(1), B)
        call GetCoeffC(vertX(i), vertY(i), vertX(1), vertY(1), C)
      end if
      dist = getMinDistanceToLine(x0,y0,vecDirNormX,vecDirNormY,A,B,C)
      if (dist < 0.0_rp) cycle
      if (dist < dstMin) then
        dstMin = dist
        iSide1 = i
      end if
    end do

    getOutSideNumber = iSide1
    return

  end function getOutSideNumber

  subroutine Beta_to_array(b_mg, ngroups, ntypes, ntheta, beta, nbeta)
    implicit none

    type(bmg), intent(in) :: b_mg(:)
    integer, intent(in)   :: ngroups
    integer, intent(in)   :: ntypes
    integer, intent(in)   :: ntheta
    type(StoSCP), allocatable, intent(out) :: beta(:,:,:,:)
    integer, allocatable, intent(out)      :: nbeta(:)

    integer :: ig, it, i, l, s1, n1, m1, s, n, m
    integer :: ind(6)
    integer :: ibet(ntheta)
    real(rp) :: bval, brc

    if (.not. allocated(nbeta)) then
      allocate(nbeta(ntypes))
    end if

    nbeta = 0
    do it = 1, ntypes
      nbeta(it) = b_mg(1)%nbetaelem(it)/ntheta
    end do
    if (.not. allocated(beta)) then
      allocate(beta(ngroups,ntheta,ntypes,maxval(nbeta)))
    end if

    do it = 1, ntypes
      do ig = 1, ngroups
        ibet = 0
        do i = 1, b_mg(ig)%nbetaelem(it)
          l = b_mg(ig)%thetasectbet(it,i)
          ind(1) = b_mg(ig)%insidebet(it,i)
          ind(2) = b_mg(ig)%insegbet(it,i)
          ind(3) = b_mg(ig)%insectbet(it,i)
          ind(4) = b_mg(ig)%outsidebet(it,i)
          ind(5) = b_mg(ig)%outsegbet(it,i)
          ind(6) = b_mg(ig)%outsectbet(it,i)
          bval = b_mg(ig)%beta(it,i)
          brc = b_mg(ig)%brc(it,i)
          ibet(l) = ibet(l) + 1
          beta(ig,l,it,ibet(l))%ind = ind
          beta(ig,l,it,ibet(l))%val = bval
          beta(ig,l,it,ibet(l))%rc  = brc
        end do
      end do
    end do

    return

  end subroutine

  subroutine V_to_array(v_mg, ngroups, ntypes, ntheta, nregions, nharms, nsides, nsegments, nphi, v, nv)
    implicit none

    type(vmg), intent(in) :: v_mg(:)
    integer, intent(in)   :: ngroups
    integer, intent(in)   :: ntypes
    integer, intent(in)   :: ntheta
    integer, intent(in)   :: nregions(:)
    integer, intent(in)   :: nharms(:)
    integer, intent(in)   :: nsides(:)
    integer, intent(in)   :: nsegments(:,:)
    integer, intent(in)   :: nphi
    type(RtoSCP), allocatable, intent(out) :: v(:,:,:,:)
    integer, allocatable, intent(out)      :: nv(:)

    integer :: ig, it, l, ir, k, s, n, m, iv, i
    integer :: ind(5)

    if (.not. allocated(nv)) then
      allocate(nv(ntypes))
      nv = 0
    end if

    do it = 1, ntypes
      iv = 0
      do s = 1, nsides(it)
        do n = 1, nsegments(it,s)
          do m = 1, nphi
            do i = 1, nregions(it)
              do k = 1, nharms(it)
                if (abs(v_mg(1)%v(it,s,n,m,1,i,k)) > 1.0d-08) then
                  iv = iv + 1
                end if
              end do
            end do
          end do
        end do
      end do
      nv(it) = iv
    end do

    if (.not. allocated(v)) then
      allocate(v(ngroups,ntheta,ntypes,maxval(nv)))
    end if

    do ig = 1, ngroups
      do l = 1, ntheta
        do it = 1, ntypes
          iv = 0
          do s = 1, nsides(it)
            do n = 1, nsegments(it,s)
              do m = 1, nphi
                do i = 1, nregions(it)
                  do k = 1, nharms(it)
                    if (abs(v_mg(ig)%v(it,s,n,m,l,i,k)) > 1.0d-08) then
                      iv = iv + 1
                      ind(1) = i
                      ind(2) = k
                      ind(3) = s
                      ind(4) = n
                      ind(5) = m
                      v(ig,l,it,iv)%ind = ind
                      v(ig,l,it,iv)%val = v_mg(ig)%v(it,s,n,m,l,i,k)
                    end if
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
    end do

    return

  end subroutine

  subroutine G_to_array(g_mg, ngroups, ntypes, ntheta, nregions, nharms, nsides, nsegments, nphi, g, ng)
    implicit none

    type(gmg), intent(in) :: g_mg(:)
    integer, intent(in)   :: ngroups
    integer, intent(in)   :: ntypes
    integer, intent(in)   :: ntheta
    integer, intent(in)   :: nregions(:)
    integer, intent(in)   :: nharms(:)
    integer, intent(in)   :: nsides(:)
    integer, intent(in)   :: nsegments(:,:)
    integer, intent(in)   :: nphi
    type(StoRCP), allocatable, intent(out) :: g(:,:,:,:)
    integer, allocatable, intent(out)      :: ng(:)

    integer :: ig, it, l, ir, k, s, n, m, igam, i
    integer :: ind(5)

    if (.not. allocated(ng)) then
      allocate(ng(ntypes))
      ng = 0
    end if

    do it = 1, ntypes
      igam = 0
      do s = 1, nsides(it)
        do n = 1, nsegments(it,s)
          do m = 1, nphi
            do i = 1, nregions(it)
              do k = 1, nharms(it)
                if (abs(g_mg(1)%g(it,i,k,s,n,m,1)) > 1.0d-08) then
                  igam = igam + 1
                end if
              end do
            end do
          end do
        end do
      end do
      ng(it) = igam
    end do

    if (.not. allocated(g)) then
      allocate(g(ngroups,ntheta,ntypes,maxval(ng)))
    end if

    do ig = 1, ngroups
      do l = 1, ntheta
        do it = 1, ntypes
          igam = 0
          do s = 1, nsides(it)
            do n = 1, nsegments(it,s)
              do m = 1, nphi
                do i = 1, nregions(it)
                  do k = 1, nharms(it)
                    if (abs(g_mg(ig)%g(it,i,k,s,n,m,l)) > 1.0d-08) then
                      igam = igam + 1
                      ind(1) = s
                      ind(2) = n
                      ind(3) = m
                      ind(4) = i
                      ind(5) = k
                      g(ig,l,it,igam)%ind = ind
                      g(ig,l,it,igam)%val = g_mg(ig)%g(it,i,k,s,n,m,l)
                    end if
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
    end do

  end subroutine

end module
