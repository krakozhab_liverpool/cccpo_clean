module MathMod
  !-----------------------------------------
  ! Module contains procedures and functions
  ! for different mathematical calculations
  !-----------------------------------------
use precision_mod

contains

  function LinearIntExp(x1,x2,y1,y2,x_given)

    ! LinearIntExp performs 2D linear interpolation or extrapolation between
    ! two given points and calculates the unknown dependent variable for a given
    ! value of the independent variable.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! x1: real, scalar. The x-coordinate of the first data pair
    ! x2: real, scalar. The x-coordinate of the second data pair
    ! y1: real, scalar. The y-coordinate of the first data pair
    ! y2: real, scalar. The y-coordinate of the second data pair
    ! x_given: real, scalar. The known x-coordinate of the third point.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! LinearIntExp: real, scalar. The y-coordinate that was calculated.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !function LinearIntExp(x1,x2,y1,y2,x_given)
    !use Config, only: srk
    !implicit none
    !real(rp),intent(in):: x1,x2,y1,y2,x_given
    !real(rp):: LinearIntExp
    !end function LinearIntExp
    !end interface

    !write(*,*)LinearIntExp(2.9_rp,5.5_rp,0.23_rp,0.99_rp,4.1_rp)

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk, warnings_pause

    implicit none

    ! Argument variables:
    real(rp),intent(in):: x1,x2,y1,y2,x_given
    real(rp):: LinearIntExp

    ! ------------------------------------------------------------------------------

    ! Error control:

    if ((x1==x2).and.(y1/=y2)) then
      write(*,*)"LinearIntExp"
      write(*,*)"WARNING: infinite solutions in linear interpolation problem."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((x1==x2).and.(y1==y2)) then
      write(*,*)"LinearIntExp"
      write(*,*)"WARNING: linear interpolation points coincide."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    ! ------------------------------------------------------------------------------

    ! Vertical line:
    if ((x1==x2).and.(y1/=y2)) LinearIntExp=(y1+y2)/2.0

    ! Coinciding points and horizontal line:
    if (((x1==x2).and.(y1==y2)).or.((x1/=x2).and.(y1==y2))) LinearIntExp=y1

    ! Line with slope other than zero:
    if ((x1/=x2).and.(y1/=y2)) LinearIntExp=y1+((y2-y1)/(x2-x1))*(x_given-x1)

  end function LinearIntExp

  function BezierQuadraticControl(a,b,c)

    ! BezierQuadraticControl takes the x and y coordinates of two given points
    ! (a and b) and calculates the x and y coordinate of a control point with
    ! which a quadratic Bezier curve can be plotted that goes through the third
    ! given point (c).

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! a: real, array (1D) with 2 elements. The x and y coordinates (elements 1 and
    !	2 respectively) of the point that is on the left or on the right of the
    !	point represented by argument "c".
    ! b: real, array (1D) with 2 elements. The x and y coordinates (elements 1 and
    !	2 respectively) of the point that is on the right or on the left of the
    !	point represented by argument "c".
    ! c: real, array (1D) with 2 elements. The x and y coordinates (elements 1 and
    !	2 respectively) of the point that is in between the points represented
    !	by arguments "a" and "b".

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! BezierQuadraticControl: real array with 2 elements (1D). The x and y
    !	coordinates (elements 1 and 2 respectively) of the control point that
    !	together with points represented by arguments "a" and "b" draws a Bezier
    !	curve that goes through the point represented by argument "c".

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !function BezierQuadraticControl(a,b,c)
    !use Config, only: srk
    !implicit none
    !real(rp),intent(in):: a(2),b(2),c(2)
    !real(rp):: BezierQuadraticControl(2)
    !end function BezierQuadraticControl
    !end interface

    !write(*,*)BezierQuadraticControl((/1.0,1.0/),(/2.0,2.0/),(/1.5,3.0/))

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk

    implicit none

    ! Argument variables:
    real(rp),intent(in):: a(2),b(2),c(2)
    real(rp):: BezierQuadraticControl(2)

    ! Private variables:
    real(rp),parameter:: step=0.001_rp
    real(rp):: real_i,t,locus_points(nint(1.0_rp/step)-1,2)
    real(rp):: distance(nint(1.0_rp/step)-1)
    integer:: i,closest_point(1)

    ! ------------------------------------------------------------------------------

    ! Since a point of the curve is known, solve the parametric formula for the
    ! quadratic Bezier curve in order to obtain all the possible control points.
    do i=1,size(locus_points,dim=1)
      real_i=i
      t=real_i*step
      locus_points(i,:)=(c-(t**2)*a-((1-t)**2)*b)/(2.0*t*(1-t))
    end do

    ! Calculate the distance of all possible control points from the given curve
    ! point.
    distance(:)=sqrt((c(1)-locus_points(:,1))**2+(c(2)-locus_points(:,2))**2)

    ! The control point in question is the one that is closest to the given curve
    ! point.
    closest_point=minloc(distance)
    BezierQuadraticControl=locus_points(closest_point(1),:)

  end function BezierQuadraticControl

  real(rp) function crossProduct(x1,y1,x2,y2)
  !-----------------------------------------
  ! Returns cross product of two vectors with
  ! coordinates (x1,y1) and (x2,y2)
  !------------------------------------------
    implicit none
    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1
    real(rp), intent(in) :: x2
    real(rp), intent(in) :: y2

    crossProduct = x1*y2 - x2*y1

    return

  end function

  real(rp) function scalarProduct(x1,y1,x2,y2)
    implicit none
    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1
    real(rp), intent(in) :: x2
    real(rp), intent(in) :: y2

    scalarProduct = x1*x2 + y1*y2

    return

  end function

  subroutine readGaussPoints()
    use ConstantsMod, only : N_GAUSS_POINTS
    use GlobalMod, only : xg => xGauss, wg => wGauss
    use Config, only : str_len

    implicit none

    integer :: i, j, nGaussPoints
    character(256) :: datadir
    integer :: statu

    call get_environment_variable("PYCCCPO_HOME", value=datadir, status=statu)

    xg = 0.0
    wg = 0.0

    if (statu == 1) then
      open(101, file = 'data/xGauss.txt')
      open(201, file = 'data/wGauss.txt')
    else
      open(101, file = trim(datadir)//'/data/xGauss.txt')
      open(201, file = trim(datadir)//'/data/wGauss.txt')
    end if

    read(101,*) nGaussPoints
    if (nGaussPoints > N_GAUSS_POINTS) then
      write(*,*) 'Error! The number of the Gauss points exceeds parameter N_GAUSS_POINTS'
      stop
    end if

    read(201,*) nGaussPoints
    if (nGaussPoints > N_GAUSS_POINTS) then
      write(*,*) 'Error! The number of the Gauss points exceeds parameter N_GAUSS_POINTS'
      stop
    end if

    xg(1,1) = 0.0_rp
    wg(1,1) = 2.0_rp

    do i = 2, nGaussPoints
      read(101,*)
      read(201,*)
      do j = 1, i
        read(101,*) xg(i,j)
        read(201,*) wg(i,j)
      end do
    end do
    close(101)
    close(201)

    return
  end subroutine

  real(rp) function P(k, x, y)
  !$omp declare simd (P)
    implicit none
    integer :: k
    real(rp)  :: x, y
    P = 0.0_rp
    if     (k == 1) then
        P = 1.0_rp
        return
    elseif (k == 2) then
        P = x
        return
    elseif (k == 3) then
        P = y
        return
    elseif (k == 4) then
        P = x*y
        return
    elseif (k == 5) then
        P = x**2
        return
    elseif (k == 6) then
        P = y**2
        return
    elseif (k == 7) then
        P = x**3
        return
    elseif (k == 8) then
        P = x**2*y
        return
    elseif (k == 9) then
        P = x*y**2
        return
    elseif (k == 10) then
        P = y**3
        return
    end if

  end function P
  real(rp) function Pnorm(k, b, x, y)
    implicit none

    integer :: k
    real(rp)  :: x, y
    real(rp)  :: b(:,:) ! array with ort. coeffisients

    integer :: i

    Pnorm = 0.0_rp

    do i = 1, k
      Pnorm = Pnorm + b(k, i)*P(i, x, y)
    end do

  end function Pnorm
  real(rp) function PP(k1,k,x,y)
  !
  ! Multiplication of the two base functions with order k1, k
  !
    implicit none

    integer :: k1, k
    real(rp)  :: x, y

    PP = P(k1, x, y)*P(k, x, y)

    return
  end function PP
  real(rp) function PnormPnorm(k1, k, b, x, y)
    implicit none

    integer :: k1, k
    real(rp)  :: x, y
    real(rp)  :: b(:,:)

    PnormPnorm = Pnorm(k1,b,x,y)*Pnorm(k,b,x,y)

  end function PnormPnorm
  real(rp) function PPnorm(kb, kn, b, x, y)
  !
  ! Multiplication of the base function with order kb
  ! and normal function with order kn
  !
    implicit none

    integer :: kb, kn
    real(rp)  :: x, y
    real(rp)  :: b(:,:)

    PPnorm = P(kb, x, y)*Pnorm(kn, b, x, y)

  end function PPnorm

  subroutine GetZoneVolumes(nvert,ncirc,x,y,r,vol)
    use ConstantsMod, only : PI
    implicit none

    integer, intent(in) :: nvert
    integer, intent(in) :: ncirc
    real(rp) :: x(:), y(:)
    real(rp), optional :: r(:)
    real(rp) :: vol(:)

    real(rp) :: r1, r2
    integer :: nZone
    integer :: i

    vol = 0.0d0
    if (.not. present(r)) then
      nZone = 1
      vol(1) = getPolygonArea(nvert,x,y)
      return
    else
      nZone = ncirc + 1
    end if

    r1 = 0.0_rp
    do i = 1, nZone - 1
      r2 = r(i)
      vol(i) = PI*(r2**2 - r1**2)
      r1 = r2
    end do

    r2 = r(nZone-1)
    vol(nZone) = getPolygonArea(nvert,x,y) - pi*r2**2
    return

  end subroutine GetZoneVolumes

  real(rp) function getPolygonArea(nvert,x,y)
    implicit none

    integer, intent(in) :: nvert
    real(rp), intent(in) :: x(:), y(:)

    real(rp) :: s
    integer :: i

    s = 0.0_rp
    do i = 1, nVert - 1
      s = s + (x(i)+x(i+1))*(y(i)-y(i+1))
    end do
    s = s + (x(nVert)+x(1))*(y(nVert)-y(1))
    getPolygonArea = 0.5_rp*abs(s)
  end function getPolygonArea

  subroutine HexIntegral(xi, yi, f, k1, k2, b, ans)
  !
  ! Integration of the function f(x,y) over regular hexagon
  ! with center in the (0.0) and vertices with coords (xi, yi)
  ! Hex. is subdivided on 6-th triangles. In each triangle integration
  ! is performed. Final integral is the sum of three integrals over
  ! each triangle
  !
    implicit none

    interface
      real(rp) function f(k1, k2, b, x, y)
        use precision_mod
        integer :: k1, k2
        real(rp) :: b(:,:)
        real(rp) :: x, y
      end function f
    end interface

    real(rp) :: xi(6), yi(6)
    integer :: k1, k2
    real(rp) :: b(:,:)
    real(rp) :: ans

    real(rp)  :: sum
    integer :: i

    sum = 0.0_rp
    do i = 1, 5
      call TriangleIntegral(xi(i), 0.0_rp, xi(i+1), yi(i), 0.0_rp, yi(i+1), f, k1, k2, b, ans)
      sum = sum + ans
    end do

    call TriangleIntegral(xi(6), 0.0_rp, xi(1), yi(6), 0.0_rp, yi(1), f, k1, k2, b, ans)
    sum = sum + ans

    ans = sum
    if (abs(ans) < 1.0e-8) then
      ans = 0.0_rp
    end if

    return
  end subroutine HexIntegral
  subroutine HexRingIntegral(xi, yi, R, f, k1, k2, b, ans)
  !
  ! Calculation of the integral f(x,y) over the region limited by hexagon
  ! (outer boundary) and circle (inner boundary). Centers of the circle and
  ! hexagon are positioned in the point (0.0, 0.0)
  !
!    use Global
    use ConstantsMod, only : PI
    implicit none
    interface
      real(rp) function f(k1, k2, b, x, y)
        use precision_mod
        integer :: k1, k2
        real(rp) :: b(:,:)
        real(rp) :: x, y
      end function
    end interface

    real(rp) :: xi(6), yi(6), R
    integer :: k1, k2
    real(rp) :: b(:,:)
    real(rp) :: ans

    real(rp) :: ansHex, ansCirc

    call HexIntegral(xi, yi, f, k1, k2, b, ansHex)
    call RingIntegral(0.0_rp, R, 0.0_rp, 2.0_rp*PI, f, k1, k2, b, ansCirc)

    ans = ansHex - ansCirc

  end subroutine HexRingIntegral

  subroutine PolygonRingIntegral(nTriang, triangles, R, f, k1, k2, b, ans)
    use ConstantsMod, only : PI
    implicit none

    interface
      real(rp) function f(k1, k2, b, x, y)
        use precision_mod
        integer :: k1, k2
        real(rp) :: b(:,:)
        real(rp) :: x, y
      end function
    end interface

    integer, intent(in) :: nTriang
    real(rp), intent(in) :: triangles(:,:,:)
    real(rp), intent(in) :: R
    integer, intent(in) :: k1
    integer, intent(in) :: k2
    real(rp), intent(in) :: b(:,:)
    real(rp), intent(out) :: ans

    real(rp) :: ansPolygon, ansCirc

    call PolygonIntegral(nTriang, triangles, f, k1, k2, b, ansPolygon)
    call RingIntegral(0.0_rp, R, 0.0_rp, 2.0_rp*PI, f, k1, k2, b, ansCirc)

    ans = ansPolygon - ansCirc
    if (abs(ans) < 1.0e-8) then
      ans = 0.0_rp
    end if

  end subroutine PolygonRingIntegral

  subroutine PolygonIntegral(nTriang, triangles, f, k1, k2, b, ans)
    implicit none

    interface
      real(rp) function f(k1, k2, b, x, y)
        use precision_mod
        integer :: k1, k2
        real(rp) :: b(:,:)
        real(rp) :: x, y
      end function f
    end interface

    integer, intent(in) :: nTriang
    real(rp), intent(in) :: triangles(:,:,:)
    integer, intent(in) :: k1
    integer, intent(in) :: k2
    real(rp), intent(in) :: b(:,:)
    real(rp), intent(out) :: ans

    real(rp) :: x1, x2, x3, y1, y2, y3
    real(rp) :: sum, ansTri
    integer :: i

    sum = 0.0_rp
    do i = 1, nTriang
      x1 = triangles(1,1,i)
      x2 = triangles(1,2,i)
      x3 = triangles(1,3,i)
      y1 = triangles(2,1,i)
      y2 = triangles(2,2,i)
      y3 = triangles(2,3,i)
      call TriangleIntegral(x1, x2, x3, y1, y2, y3, f, k1, k2, b, ansTri)
      sum = sum + ansTri
    end do

    ans = sum
    if (abs(ans) < 1.0e-8) then
      ans = 0.0_rp
    end if

  end subroutine

  subroutine TriangleIntegral(x1, x2, x3, y1, y2, y3, f, k1, k2, b, ans)
  !
  ! Integration of function f(x,y) on triangle, with
  ! coordinates of vertices in points (x1,y1), (x2,y2), (x3,y3)
  !
    implicit none
    interface
      real(rp) function f(k1, k2, b, x, y)
        use precision_mod
        integer :: k1, k2
        real(rp) :: b(:,:)
        real(rp) :: x, y
      end function
    end interface

    real(rp) :: x1, x2, x3
    real(rp) :: y1, y2, y3
    integer :: k1, k2
    real(rp) :: b(:,:)

    real(rp) :: ans

    real(rp) :: x, y ! global coordinates

    real(rp) :: ri(13), si(13), ti(13), w(13)

    data ri / 0.333333333333333_rp, 0.479308067841923_rp, 0.260345966079038_rp, 0.260345966079038_rp, &
              0.869739794195568_rp, 0.065130102902216_rp, 0.065130102902216_rp, 0.638444188569809_rp, &
              0.638444188569809_rp, 0.312865496004875_rp, 0.048690315425316_rp, 0.312865496004875_rp, &
              0.048690315425316_rp /
    data si / 0.333333333333333_rp, 0.260345966079038_rp, 0.479308067841923_rp, 0.260345966079038_rp, &
              0.065130102902216_rp, 0.869739794195568_rp, 0.065130102902216_rp, 0.312865496004875_rp, &
              0.048690315425316_rp, 0.638444188569809_rp, 0.638444188569809_rp, 0.048690315425316_rp, &
              0.312865496004875_rp  /
    data ti / 0.333333333333333_rp, 0.260345966079038_rp, 0.260345966079038_rp, 0.479308067841923_rp, &
              0.065130102902216_rp, 0.065130102902216_rp, 0.869739794195568_rp, 0.048690315425316_rp, &
              0.312865496004875_rp, 0.048690315425316_rp, 0.312865496004875_rp, 0.638444188569809_rp, &
              0.638444188569809_rp /
    data w  / -0.149570044467670_rp, 3*0.175615257433204_rp, 3*0.053347235608839_rp, 6*0.077113760890257_rp /

    integer :: i

    ans = 0.0_rp

    do i = 1, 13
      x = ri(i)*x1 + si(i)*x2 + ti(i)*x3
      y = ri(i)*y1 + si(i)*y2 + ti(i)*y3
      ans = ans + f(k1, k2, b, x, y)*w(i)
    end do

    ans = ans*triangleArea(x1,x2,x3,y1,y2,y3)

    if (abs(ans) < 1.0e-08) then
      ans = 0.0_rp
      return
    end if

  end subroutine TriangleIntegral

  real(rp) function triangleArea(x1, x2, x3, y1, y2, y3)
    implicit none

    real(rp) x1, x2, x3, y1, y2, y3

    triangleArea = abs((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1))/2.0_rp

  end function triangleArea

  subroutine RingIntegral(R1, R2, alpha1, alpha2, f, k1, k2, b, ans)
    implicit none

    interface
      real(rp) function f(k1, k2, b, x, y)
        use precision_mod
        integer :: k1, k2
        real(rp) :: b(:,:)
        real(rp) :: x, y
      end function f
    end interface

    integer, parameter :: numberOfGaussPoints = 64

    real(rp) :: alpha1, alpha2
    real(rp) :: R1, R2
    integer :: k1, k2
    real(rp) :: b(:,:)
    real(rp) :: ans


    integer :: iAlpha, iR
    real(rp) :: alphai, ri
    real(rp) :: xs, ys

    real(rp) :: w(numberOfGaussPoints), x(numberOfGaussPoints)

    data w /0.0486909570091397_rp, 0.0486909570091397_rp, 0.0485754674415034_rp, 0.0485754674415034_rp, &
            0.0483447622348030_rp, 0.0483447622348030_rp, 0.0479993885964583_rp, 0.0479993885964583_rp, &
            0.0475401657148303_rp, 0.0475401657148303_rp, 0.0469681828162100_rp, 0.0469681828162100_rp, &
            0.0462847965813144_rp, 0.0462847965813144_rp, 0.0454916279274181_rp, 0.0454916279274181_rp, &
            0.0445905581637566_rp, 0.0445905581637566_rp, 0.0435837245293235_rp, 0.0435837245293235_rp, &
            0.0424735151236536_rp, 0.0424735151236536_rp, 0.0412625632426235_rp, 0.0412625632426235_rp, &
            0.0399537411327203_rp, 0.0399537411327203_rp, 0.0385501531786156_rp, 0.0385501531786156_rp, &
            0.0370551285402400_rp, 0.0370551285402400_rp, 0.0354722132568824_rp, 0.0354722132568824_rp, &
            0.0338051618371416_rp, 0.0338051618371416_rp, 0.0320579283548516_rp, 0.0320579283548516_rp, &
            0.0302346570724025_rp, 0.0302346570724025_rp, 0.0283396726142595_rp, 0.0283396726142595_rp, &
            0.0263774697150547_rp, 0.0263774697150547_rp, 0.0243527025687109_rp, 0.0243527025687109_rp, &
            0.0222701738083833_rp, 0.0222701738083833_rp, 0.0201348231535302_rp, 0.0201348231535302_rp, &
            0.0179517157756973_rp, 0.0179517157756973_rp, 0.0157260304760247_rp, 0.0157260304760247_rp, &
            0.0134630478967186_rp, 0.0134630478967186_rp, 0.0111681394601311_rp, 0.0111681394601311_rp, &
            0.0088467598263639_rp, 0.0088467598263639_rp, 0.0065044579689784_rp, 0.0065044579689784_rp, &
            0.0041470332605625_rp, 0.0041470332605625_rp, 0.0017832807216964_rp, 0.0017832807216964_rp  /

    data x /-0.0243502926634244_rp, 0.0243502926634244_rp, -0.0729931217877990_rp, 0.0729931217877990_rp, &
            -0.1214628192961206_rp, 0.1214628192961206_rp, -0.1696444204239928_rp, 0.1696444204239928_rp, &
            -0.2174236437400071_rp, 0.2174236437400071_rp, -0.2646871622087674_rp, 0.2646871622087674_rp, &
            -0.3113228719902110_rp, 0.3113228719902110_rp, -0.3572201583376681_rp, 0.3572201583376681_rp, &
            -0.4022701579639916_rp, 0.4022701579639916_rp, -0.4463660172534641_rp, 0.4463660172534641_rp, &
            -0.4894031457070530_rp, 0.4894031457070530_rp, -0.5312794640198946_rp, 0.5312794640198946_rp, &
            -0.5718956462026340_rp, 0.5718956462026340_rp, -0.6111553551723933_rp, 0.6111553551723933_rp, &
            -0.6489654712546573_rp, 0.6489654712546573_rp, -0.6852363130542333_rp, 0.6852363130542333_rp, &
            -0.7198818501716109_rp, 0.7198818501716109_rp, -0.7528199072605319_rp, 0.7528199072605319_rp, &
            -0.7839723589433414_rp, 0.7839723589433414_rp, -0.8132653151227975_rp, 0.8132653151227975_rp, &
            -0.8406292962525803_rp, 0.8406292962525803_rp, -0.8659993981540928_rp, 0.8659993981540928_rp, &
            -0.8893154459951141_rp, 0.8893154459951141_rp, -0.9105221370785028_rp, 0.9105221370785028_rp, &
            -0.9295691721319396_rp, 0.9295691721319396_rp, -0.9464113748584028_rp, 0.9464113748584028_rp, &
            -0.9610087996520538_rp, 0.9610087996520538_rp, -0.9733268277899110_rp, 0.9733268277899110_rp, &
            -0.9833362538846260_rp, 0.9833362538846260_rp, -0.9910133714767443_rp, 0.9910133714767443_rp, &
            -0.9963401167719553_rp, 0.9963401167719553_rp, -0.9993050417357722_rp, 0.9993050417357722_rp /

    ans = 0.0_rp
    do iAlpha = 1, numberOfGaussPoints
      alphai = (alpha2 - alpha1)*0.5_rp*x(iAlpha) + (alpha1 + alpha2)*0.5_rp
      do iR = 1, numberOfGaussPoints
        ri = (R2 - R1)*0.5_rp*x(iR) + (R2 + R1)*0.5_rp
        xs = ri*cos(alphai)
        ys = ri*sin(alphai)
        ans = ans + ri*f(k1, k2, b, xs, ys)*w(iAlpha)*w(iR)
      end do
    end do

    ans = 0.25_rp*(alpha2 - alpha1)*(R2 - R1)*ans
    if (abs(ans) <= 1.0d-08) then
      ans = 0.0_rp
      return
    end if
    return

  end subroutine RingIntegral


end module
