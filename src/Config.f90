module Config
  use precision_mod

  ! Module Config contains data that are used by the F.EN.I.A. subprocesses.

  !*******************************************************************************
  ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
  !*******************************************************************************

  ! Determine whether to pause on warnings or not:
  logical,parameter,public:: warnings_pause=.false.

  ! Default string length:
  integer, parameter, public :: str_len = 128

  ! Determine the precision for the calculations:
  !integer,parameter,public:: srk=selected_real_kind(p=6,r=37) ! single
  integer,parameter,public:: srk=selected_real_kind(p=15,r=307) ! double

  ! Define the dimensions of the SVG viewBox:
  real(rp), parameter, public :: viewBox_width=700.0_rp ! px
  real(rp), parameter, public :: viewBox_height=700.0_rp ! px
  real(rp), parameter, public :: viewBox_centerX = viewBox_width*0.5_rp
  real(rp), parameter, public :: viewBox_centerY = viewBox_height*0.5_rp

  ! Mathematical constants (with precision to spare):
  real(rp),parameter,public:: pi=3.141592653589793238462643383279502884197_rp

end module Config
