module VTKMod
  use precision_mod

  contains

  subroutine PlotFlux(output_file, sym, verx, very, ncells, kart, nzones, flux, resx, resy)
    use Config, only : str_len
    use GlobalAssemblyMod, only : assembly_type, nRow, nCol, dpitch
    implicit none

    character(str_len), intent(in) :: output_file
    real(rp), intent(in) :: verx(:,:)
    real(rp), intent(in) :: very(:,:)
    integer, intent(in) :: sym
    integer, intent(in) :: ncells
    integer, intent(in) :: kart(:)
    integer, intent(in) :: nzones(:)
    real(rp), intent(in) :: flux(:,:,:)
    integer, intent(in) :: resx
    integer, intent(in) :: resy

    real(rp), allocatable :: x(:,:), y(:,:), z(:,:)
    character(str_len) :: scalar_name(3)
    real(rp), allocatable :: scalar_value(:,:,:)

    real(rp) :: x0, y0, dx, dy, xx, yy, xlocal, ylocal
    integer :: ix, iy, ir, ic
    integer :: itype, icell

    open(1656, file=output_file)

    if (sym == -360 .or. sym == 360) then
      allocate(x(resx, resy))
      allocate(y(resx, resy))
      allocate(z(resx, resy))
      allocate(scalar_value(resx,resy,3))
      x = 0.0_rp
      y = 0.0_rp
      z = 0.0_rp
      scalar_value = 0.0_rp
      if (ncells == 1) then
        itype = kart(1)
        if (resx == resy) then
          x0 = min(minval(verx(itype,:)),minval(very(itype,:)))
          y0 = x0
          dx = (max(maxval(verx(itype,:)),maxval(very(itype,:))) - x0)/resx
          dy = dx
          x0 = x0 + 0.5_rp*dx
          y0 = x0
        else
          x0 = minval(verx(itype,:))
          y0 = minval(very(itype,:))
          dx = (maxval(verx(itype,:)) - x0)/resx
          dy = (maxval(very(itype,:)) - y0)/resy
        end if
        do ix = 1, resx
          xx = x0 + real(ix-1)*dx
          x(ix,:)= xx
          do iy = 1, resy
            yy = y0 + real(iy-1)*dy
            y(:,iy)= yy
            z(ix,iy) = fluxReconstruct(xx, yy, itype, 1, flux)
!            z(ix,iy) = fluxIntegrate2D(xx-0.5_rp*dx, xx+0.5_rp*dx, yy-0.5_rp*dy, yy+0.5_rp*dy, &
!                                       3, itype, 1, flux)/(dx*dy)
          end do
        end do
      else
        if (assembly_type == 'regular square') then
          x0 = 0.0d0
          y0 = 0.0d0
          dx = dpitch * nCol / resx
          dy = dpitch * nRow / resy
          x0 = x0 + 0.5d0 * dx
          y0 = y0 - 0.5d0 * dy
          do ix = 1, resx
            xx = x0 + real(ix-1)*dx
            x(ix,:)= xx
            do iy = 1, resy
              yy = y0 - real(iy-1)*dy
              y(:,iy)= yy
              icell = get_cell_number_regular_square_lattice(xx, -yy)
              itype = kart(icell)
              ic = int(xx/dpitch) + 1
              ir = int(-yy/dpitch) + 1
              if (icell == 1) then
                xlocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                ylocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 2) then
                ylocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                xlocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 3) then
                xlocal = xx - ((ic - 1) * dpitch + 0.5d0 * dpitch)
                ylocal = -yy - ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 4) then
                ylocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                xlocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 6) then
                ylocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                xlocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 7) then
                ylocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                xlocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 8) then
                ylocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                xlocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              elseif (icell == 9) then
                xlocal = -xx + ((ic - 1) * dpitch + 0.5d0 * dpitch)
                ylocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              else
                xlocal = xx - ((ic - 1) * dpitch + 0.5d0 * dpitch)
                ylocal = yy + ((ir - 1) * dpitch + 0.5d0 * dpitch)
              end if
!              xlocal = get_xlocal(assembly_type, xx)
!              ylocal = -get_ylocal(assembly_type, -yy)
              z(ix,iy) = fluxReconstruct(xlocal, ylocal, itype, icell, flux)
            end do
          end do
        else
          write(*,*) "The drawing of the output VTK files is not supported for this number of cells and geometry at the moment."
          stop
        end if
      end if
#ifdef __INTEL_COMPILER
      scalar_name(:)=(/'x','y','flux'/)
#else
      scalar_name(1)='x'
      scalar_name(2)='y'
      scalar_name(3)='flux'
#endif
      scalar_value(:,:,1)=x(:,:)
      scalar_value(:,:,2)=y(:,:)
      scalar_value(:,:,3)=z(:,:)
      call VTK_Initialize(output_file)
      call VTK_Surface(output_file,x,y,z,scalar_name,scalar_value)
      call VTK_Finalize(output_file)
    else
      write(*,*) "The drawing of the output VTK files is not supported for this symmetry at the moment."
      stop
    end if

  end subroutine PlotFlux

  subroutine WriteMeshFluxToCSV(output_file, sym, verx, very, ncells, kart, nzones, flux, resx, resy)
    use Config, only : str_len
    use ReadWriteMod, only : WritePointsToCSV
    implicit none

    character(str_len), intent(in) :: output_file
    real(rp), intent(in) :: verx(:,:)
    real(rp), intent(in) :: very(:,:)
    integer, intent(in) :: sym
    integer, intent(in) :: ncells
    integer, intent(in) :: kart(:)
    integer, intent(in) :: nzones(:)
    real(rp), intent(in) :: flux(:,:,:)
    integer, intent(in) :: resx
    integer, intent(in) :: resy

    real(rp), allocatable :: x(:), y(:), z(:), values(:)

    real(rp) :: x0, y0, dx, dy, xx, yy
    integer :: ix, iy, ipoint
    integer :: itype

    open(1656, file=output_file)

    if (sym == -360 .or. sym == 360) then
      allocate(x(resx*resy))
      allocate(y(resx*resy))
      allocate(z(resx*resy))
      allocate(values(resx*resy))
      x = 0.0_rp
      y = 0.0_rp
      z = 0.0_rp
      if (ncells == 1) then
        itype = kart(1)
        if (resx == resy) then
          x0 = min(minval(verx(itype,:)),minval(very(itype,:)))
          y0 = x0
          dx = (max(maxval(verx(itype,:)),maxval(very(itype,:))) - x0)/resx
          dy = dx
        else
          x0 = minval(verx(itype,:))
          y0 = minval(very(itype,:))
          dx = (maxval(verx(itype,:)) - x0)/resx
          dy = (maxval(very(itype,:)) - y0)/resy
        end if
        ipoint = 1
        do ix = 1, resx
          xx = x0 + real(ix-1)*dx
          do iy = 1, resy
            yy = y0 + real(iy-1)*dy
            x(ipoint) = xx
            y(ipoint) = yy
            z(ipoint) = 0.0_rp
            values(ipoint) = fluxReconstruct(xx, yy, itype, 1, flux)
            ! values(ipoint) = fluxIntegrate2D(xx, xx+dx, yy, yy+dy, 3, itype, 1, flux)
            ipoint = ipoint + 1
          end do
        end do
        call WritePointsToCSV(output_file, resx*resy, x, y, z, values)
      else
        write(*,*) "The drawing of the output VTK files is not supported for this number of cells at the moment."
        stop
      end if
    else
      write(*,*) "The drawing of the output VTK files is not supported for this symmetry at the moment."
      stop
    end if

  end subroutine

  real(rp) function fluxIntegrate2D(ax, bx, ay, by, ngp, itype, icell, flux)
    use GlobalMod, only : xGauss, wGauss
    implicit none

    real(rp), intent(in) :: ax
    real(rp), intent(in) :: bx
    real(rp), intent(in) :: ay
    real(rp), intent(in) :: by
    integer, intent(in) :: ngp
    integer, intent(in) :: itype
    integer, intent(in) :: icell
    real(rp), intent(in) :: flux(:,:,:)

    real(rp) :: ksi(ngp), eta(ngp)
    real(rp) :: wi(ngp), wj(ngp)

    integer :: i, j
    real(rp) :: xi, yj

    fluxIntegrate2D = 0.0_rp

    ksi(:) = xGauss(ngp,1:ngp)
    eta(:) = xGauss(ngp,1:ngp)
    wi(:)  = wGauss(ngp,1:ngp)
    wj(:)  = wGauss(ngp,1:ngp)

    do i = 1, ngp
      xi = (ax + bx)*0.5_rp + (bx - ax)*0.5_rp*ksi(i)
      do j = 1, ngp
        yj = (ay + by)*0.5_rp + (by - ay)*0.5_rp*eta(j)
        fluxIntegrate2D = fluxIntegrate2D + wi(i)*wj(j)*fluxReconstruct(xi, yj, itype, icell, flux)
      end do
    end do

    fluxIntegrate2D = fluxIntegrate2D*0.25_rp*(bx - ax)*(by - ay)

    return

  end function

  real(rp) function fluxReconstruct(x, y, itype, icell, flux)
    use GlobalMod, only : ort
    use MathMod, only : P
    use GlobalAssemblyMod, only : nharms, nzones
    implicit none

    real(rp), intent(in) :: x, y
    integer, intent(in) :: itype
    integer, intent(in) :: icell
    real(rp), intent(in) :: flux(:,:,:)

    integer :: iz, ih, k
    real(rp) :: pnorm

    fluxReconstruct = 0.0_rp

    iz = zoneNumber(x, y, itype)
    if (iz < 0) then
      fluxReconstruct = -5.0
      return
    end if

    do k = 1, nharms(itype)
      pnorm = 0.0_rp
      do ih = 1, k
        pnorm = pnorm + ort(itype,ih,k)*P(ih, x, y)
      end do
      fluxReconstruct = fluxReconstruct + flux(icell,iz,k)*pnorm
    end do

    if (fluxReconstruct < 0.) then
      fluxReconstruct = 0.0d0
    end if

    return

  end function fluxReconstruct

  integer function zoneNumber(x, y, itype)
    use GlobalAssemblyMod, only : cell_rad, nzones, nsides, cell_vertx, cell_verty

    implicit none

    real(rp), intent(in) :: x, y
    integer, intent(in) :: itype

    integer :: i, ir

    if (inPolygon(x, y, nsides(itype), cell_vertx(itype,:), cell_verty(itype,:))) then
      do ir = 1, nzones(itype) - 1
        if (x**2 + y**2 < cell_rad(itype,ir)**2) then
          zoneNumber = ir
          return
        end if
      end do
      zoneNumber = nzones(itype)
    else
      zoneNumber = -1
    end if

    return

  end function zoneNumber

  logical function inPolygon(x, y, nvert, verx, very)
    implicit none

    real(rp), intent(in) :: x
    real(rp), intent(in) :: y
    integer, intent(in) :: nvert
    real(rp), intent(in) :: verx(:)
    real(rp), intent(in) :: very(:)

    integer :: i
    real(rp) :: minX, minY
    real(rp) :: maxX, maxY
    integer :: inout_res

    minX = minval(verx)
    minY = minval(very)
    maxX = maxval(verx)
    maxY = maxval(very)

    if (x < minX .or. x > maxX .or. y < minY .or. y > maxY) then
      inPolygon = .false.
      return
    end if

    call PNPOLY(x, y, verx, very, nvert, inout_res)

    if (inout_res >= 0) then
      inPolygon = .true.
      return
    else
      inPolygon = .false.
      return
    end if

  end function

  SUBROUTINE PNPOLY(PX,PY,XX,YY,N,INOUT_RES)
    implicit none
    real(rp), intent(in) :: PX, PY
    REAL(rp), intent(in) :: XX(:),YY(:)
    integer, intent(in) :: N
    integer, intent(inout) :: INOUT_RES

    REAL(rp) :: X(N),Y(N)
    LOGICAL :: MX,MY,NX,NY
    integer :: maxdim

    integer :: i, j
    INTEGER :: O
!      OUTPUT UNIT FOR PRINTED MESSAGES
    DATA O/6/
    MAXDIM=200
    IF(N.LE.MAXDIM)GO TO 6
    WRITE(O,7)
7   FORMAT('0WARNING:',I5,' TOO GREAT FOR THIS VERSION OF PNPOLY. &
    1RESULTS INVALID')
    RETURN
6   DO 1 I=1,N
    X(I)=XX(I)-PX
1   Y(I)=YY(I)-PY
    INOUT_RES=-1
    DO 2 I=1,N
    J=1+MOD(I,N)
    MX=X(I).GE.0.0
    NX=X(J).GE.0.0
    MY=Y(I).GE.0.0
    NY=Y(J).GE.0.0
    IF(.NOT.((MY.OR.NY).AND.(MX.OR.NX)).OR.(MX.AND.NX)) GO TO 2
    IF(.NOT.(MY.AND.NY.AND.(MX.OR.NX).AND..NOT.(MX.AND.NX))) GO TO 3
    INOUT_RES=-INOUT_RES
    GO TO 2
3   IF((Y(I)*X(J)-X(I)*Y(J))/(X(J)-X(I))) 2,4,5
4   INOUT_RES=0
    RETURN
5   INOUT_RES=-INOUT_RES
2   CONTINUE
    RETURN
    END SUBROUTINE

  integer function FileManager(filename,connected)

  ! FileManager manages the file connections in a program and enables the
  ! reference of the I/O files by name. The function can be used exactly as the
  ! index number of an I/O file in an I/O command. FileManager and the OPEN or the
  ! CLOSE statements can be used together in a program without conflicts.

  ! SVG note:
  ! All I/O of the SVG library is streamed through the present function. To add
  ! extensions such as user interactivity upon file creation or overwriting, only
  ! the present function needs to be modified.

  !*******************************************************************************
  ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
  !*******************************************************************************

  ! INPUT (REQUIRED):
  ! filename: character, scalar. The I/O file's name.

  ! INPUT (OPTIONAL):
  ! connected: logical, scalar. Defines whether the file with the given filename
  !	has already been connected or not.

  ! OUTPUT (REQUIRED):
  ! FileManager: integer, scalar. The index number of the given filename.

  ! OUTPUT (OPTIONAL):
  ! No optional output arguments. All arguments are required.

  ! Example of usage:

  !interface
  !function FileManager(filename,connected)
  !implicit none
  !character,intent(in):: filename*(*)
  !logical,optional,intent(out):: connected
  !integer:: FileManager
  !end function FileManager
  !end interface

  !write(FileManager('test.txt'),'(F5.3)')1.234
  !rewind(FileManager('test.txt'))
  !read(FileManager('test.txt'),*)a

  ! On some compilers the line:
  !write(FileManager('test.txt'),*)'test.txt',FileManager('test.txt')
  ! may produce a "recursive I/O operation" error.

  ! ------------------------------------------------------------------------------

  implicit none

  ! Argument variables:
  character,intent(in):: filename*(*)
  logical,optional,intent(out):: connected

  ! Private variables:
  integer,save:: counter=0
  logical:: number_taken
  integer:: err

  ! ------------------------------------------------------------------------------

  ! Error control:

  if (len_trim(filename)==0) then
  write(*,*)"FileManager"
  write(*,*)"ERROR: blank filename."
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  ! ------------------------------------------------------------------------------

  ! Find out if the file is connected:
  inquire(file=filename,opened=number_taken,iostat=err)

  if (err/=0) then
  write(*,*)"FileManager"
  write(*,*)"ERROR: INQUIRE statement failed."
  write(*,*)"filename : ",trim(filename)
  write(*,*)"error flag : ",err
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if (present(connected)) connected=number_taken

  ! If the file is not connected, find an empty slot and open it there. If the
  ! file is already connected, obtain the connection number.
  already_connected: if (number_taken) then

  ! Get the connection number:
    inquire(file=filename,number=FileManager,iostat=err)

    if (err/=0) then
    write(*,*)"FileManager"
    write(*,*)"ERROR: INQUIRE statement failed."
    write(*,*)"filename : ",trim(filename)
    write(*,*)"error flag : ",err
    write(*,*)"Program terminated."
    read(*,*)
    stop
    end if

  else already_connected

    search_empty_slot: do

      counter=counter+1
      inquire(unit=counter,opened=number_taken,iostat=err)

      if (err/=0) then
      write(*,*)"FileManager"
      write(*,*)"ERROR: INQUIRE statement failed."
      write(*,*)"connection unit : ",counter
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
      end if

      slot_found: if (.not.number_taken) then

  ! If an empty slot is found, the requested file is opened.
        open(counter,file=filename,iostat=err)

        if (err/=0) then
        write(*,*)"FileManager"
        write(*,*)"ERROR: file could not be opened."
        write(*,*)"filename : ",trim(filename)
        write(*,*)"error flag : ",err
        write(*,*)"Program terminated."
        read(*,*)
        stop
        end if

        FileManager=counter
        exit search_empty_slot

      end if slot_found

    end do search_empty_slot

  end if already_connected

  end function FileManager

  function StringInteger(numerical_value)

  ! StringInteger converts a given number into an alphanumerical string.

  !*******************************************************************************
  ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
  !*******************************************************************************

  ! INPUT (REQUIRED):
  ! numerical_value: integer, scalar. The number that will be converted in a
  !	string.

  ! INPUT (OPTIONAL):
  ! No optional input arguments. All arguments are required.

  ! OUTPUT (REQUIRED):
  ! StringInteger: character, scalar. The resulting character string.

  ! OUTPUT (OPTIONAL):
  ! No optional output arguments. All arguments are required.

  ! Example of usage:

  !interface
  !function StringInteger(numerical_value)
  !use Config, only: str_len
  !implicit none
  !integer,intent(in):: numerical_value
  !character:: StringInteger*(str_len)
  !end function StringInteger
  !end interface

  !write(*,*)String(127)

  ! The length of the output StringInteger could have been empiricaly set equal to
  ! the amount of significant digits in the base of the numbering system used on
  ! the computer for the same type of integer variable as the input argument
  ! (using intrisic function DIGITS). This length is found to be enough for the
  ! representation of a number of the given precision. However, setting the string
  ! length manually is viewed as more portable.

  ! ------------------------------------------------------------------------------

  ! External variables:
  use Config, only: str_len

  implicit none

  ! Argument variables:
  integer,intent(in):: numerical_value
  character:: StringInteger*(str_len)

  ! Private variables:
  integer:: err

  ! ------------------------------------------------------------------------------

  ! Error control:

  if (numerical_value/=numerical_value) then
  write(*,*)"StringInteger"
  write(*,*)"ERROR: input is NaN."
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  ! ------------------------------------------------------------------------------

  StringInteger=''
  write(StringInteger,*,iostat=err)numerical_value

  if (err/=0) then
  write(*,*)"StringInteger"
  write(*,*)"ERROR: unable to convert number into string."
  write(*,*)"error flag : ",err
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  StringInteger=adjustl(StringInteger)

  ! This subprogram enables the transformation of a numerical value into a string
  ! inside a function rather than in a subroutine, thus saving lines and variables
  ! in the calling code.

  end function StringInteger

  subroutine VTK_Initialize(output_file)

  ! VTK_Initialize is called to initialize an output VTK PolyData file.

  !*******************************************************************************
  ! Evangelos Bertakis, F.EN.I.A. Project, 2009                                  *
  !*******************************************************************************

  ! INPUT (REQUIRED):
  ! output_file: character, scalar. The name of the output VTK file. It must have
  !	the ".vtp" extension (otherwise an error is generated).

  ! INPUT (OPTIONAL):
  ! No optional input arguments. All arguments are required.

  ! OUTPUT (REQUIRED):
  ! This subroutine returns no output.

  ! OUTPUT (OPTIONAL):
  ! This subroutine returns no output.

  ! Example of usage:

  !interface
  !subroutine VTK_Initialize(output_file)
  !implicit none
  !character,intent(in):: output_file*(*)
  !end subroutine VTK_Initialize
  !end interface

  !! Create an empty VTK document:
  !call VTK_Initialize(output_file='plot.vtp')
  !call VTK_Finalize(output_file='plot.vtp')

  ! ------------------------------------------------------------------------------

  ! External variables:
  use Config, only: warnings_pause

  implicit none

  ! Argument variables:
  character,intent(in):: output_file*(*)

  ! Private variables:
  integer:: err,io
  logical:: connected

  ! Variable initialization:
  io=FileManager(output_file,connected)

  ! ------------------------------------------------------------------------------

  ! Error control:

  if (connected) then
  write(*,*)"VTK_Initialize"
  write(*,*)"WARNING: VTK file has already been accessed."
  write(*,*)"Continue..."
  if (warnings_pause) read(*,*)
  end if

  if (len_trim(output_file)<5) then
  write(*,*)"VTK_Initialize"
  write(*,*)"ERROR: invalid filename."
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.vtp') then
  write(*,*)"VTK_Initialize"
  write(*,*)"ERROR: file must have the .vtp extension."
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  ! ------------------------------------------------------------------------------

  write(io,'(A21)',iostat=err)"<?xml version='1.0'?>"

  if (err/=0) then
  write(*,*)"VTK_Initialize"
  write(*,*)"ERROR: writing to file was not possible."
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"error flag : ",err
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  write(io,'(A39)')"<VTKFile type='PolyData' version='0.1'>"
  write(io,'(A10)')"<PolyData>"
  write(io,*)

  end subroutine VTK_Initialize

  subroutine VTK_Surface(output_file,x,y,z,scalar_name,scalar_value)

  ! VTK_Surface is called to draw a surface in an VTK file. Prior to
  ! the call to subroutine VTK_Surface, a call to subroutine
  ! InitializeVTK must take place. In addition, after VTK_Surface is
  ! called, subroutine FinalizeVTK must be called.

  !*******************************************************************************
  ! Evangelos Bertakis, F.EN.I.A. Project, 2009                                  *
  !*******************************************************************************

  ! INPUT (REQUIRED):
  ! output_file: character. The name of the output VTK file. It must have the
  !	".svg" extension (otherwise an error is generated).
  ! x: real, array (2D). The x coordinates of the points. The elements are in
  !	1-1 correspondance with the elements of the other coordinate arrays.
  ! y: real, array (2D). The y coordinates of the points. The elements are in
  !	1-1 correspondance with the elements of the other coordinate arrays.
  ! z: real, array (2D). The z coordinates of the points. The elements are in
  !	1-1 correspondance with the elements of the other coordinate arrays.

  ! INPUT (OPTIONAL):
  ! scalar_name: character, array (1D). The names of the variables that will be
  !	stored in the VTK object as XML variables. This variable may not
  !	be omitted when variable "scalar_value" is included in the arguments
  !	and vica versa. The size of the array must be equal to the size of
  !	variable "scalar_value" (dim=3) and their elements are 1-1
  !	corresponding.
  ! scalar_value: real, array (3D). The values of the variables that will be
  !	stored in the VTK object as XML variables. This variable may not
  !	be omitted when variable "scalar_name" is included in the arguments
  !	and vica versa. The size of the array (dim=3) must be equal to the size
  !	of variable "scalar_name" and their elements are 1-1 corresponding.
  !	The elements of the first two dimensions are in 1-1 correspondance with
  !	the lines of the coordinate array.

  ! OUTPUT (REQUIRED):
  ! This subroutine returns no output.

  ! OUTPUT (OPTIONAL):
  ! This subroutine returns no output.

  ! Example of usage:

  !interface
  !subroutine VTK_Surface(output_file,x,y,z,scalar_name,scalar_value)
  !use Config, only: srk
  !implicit none
  !character,intent(in):: output_file*(*)
  !real(rp),intent(in):: x(:,:),y(:,:),z(:,:)
  !character,optional,intent(in):: scalar_name(:)*(*)
  !real(rp),optional,intent(in):: scalar_value(:,:,:)
  !end subroutine VTK_Surface
  !end interface

  ! ----------------------------------------------------------------------------

  ! External dependencies:
  use Config, only: warnings_pause,srk

  implicit none

  ! Argument variables:
  character,intent(in):: output_file*(*)
  real(rp),intent(in):: x(:,:),y(:,:),z(:,:)
  character,optional,intent(in):: scalar_name(:)*(*)
  real(rp),optional,intent(in):: scalar_value(:,:,:)

  ! Private variables:
  integer:: err,i,j,k,m,io
  real(rp):: coordinate_array(3*size(x)),property_array(size(x))
  integer:: id(size(x,dim=1),size(x,dim=2))
  integer:: connectivity_array(4*((size(x,dim=1)-1)*(size(x,dim=2)-1)))
  integer:: connectivity_offsets((size(x,dim=1)-1)*(size(x,dim=2)-1))
  logical:: connected

  ! Variable initialization:
  io=FileManager(output_file,connected)

  ! ----------------------------------------------------------------------------

  ! Error control:

  if (.not.connected) then
  write(*,*)"VTK_Surface"
  write(*,*)"WARNING: uninitialized VTK file."
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"Continue..."
  if (warnings_pause) read(*,*)
  end if

  if (len_trim(output_file)<5) then
  write(*,*)"VTK_Path"
  write(*,*)"ERROR: invalid filename"
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.vtp') then
  write(*,*)"VTK_Path"
  write(*,*)"ERROR: file must have the .vtp extension"
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if ((size(x)/=size(y)).or.(size(x)/=size(z))) then
  write(*,*)"VTK_Surface"
  write(*,*)"ERROR: all coordinate arrays must have equal amount of elements"
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if ((all(x==0.0)).and.(all(y==0.0)).and.(all(z==0.0))) then
  write(*,*)"VTK_Surface"
  write(*,*)"WARNING: missing data"
  write(*,*)"Continue..."
  if (warnings_pause) read(*,*)
  end if

  if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
  write(*,*)"VTK_Surface"
  write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
  write(*,*)"without variable 'scalar_value'."
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
  write(*,*)"VTK_Surface"
  write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
  write(*,*)"without variable 'scalar_name'."
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if ((present(scalar_name)).and.(present(scalar_value))) then
  if (size(scalar_name)/=size(scalar_value,dim=3)) then
  write(*,*)"VTK_Surface"
  write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
  write(*,*)"same amount of columns."
  write(*,*)"size(scalar_name): ",size(scalar_name)
  write(*,*)"size(scalar_value,dim=2): ",size(scalar_value,dim=2)
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if
  end if

  if (present(scalar_value)) then
  if ((size(scalar_value,dim=1)/=size(x,dim=1)).or. &
    & (size(scalar_value,dim=2)/=size(x,dim=2))) then
  write(*,*)"VTK_Surface"
  write(*,*)"ERROR: variables 'scalar_value' and 'points' must have the"
  write(*,*)"same size."
  write(*,*)"size(scalar_value,dim=1): ",size(scalar_value,dim=1)
  write(*,*)"size(scalar_value,dim=2): ",size(scalar_value,dim=2)
  write(*,*)"size(points,dim=1): ",size(x,dim=1)
  write(*,*)"size(points,dim=2): ",size(x,dim=2)
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if
  end if

  ! ----------------------------------------------------------------------------

  ! Get the coordinates array.
  j=1
  m=0
  do i=1,size(x,dim=1)
    do k=1,size(x,dim=2)
      coordinate_array(j:j+2)=(/x(i,k),y(i,k),z(i,k)/)
      j=j+3
      id(i,k)=m
      m=m+1
    end do
  end do

  ! Get the connectivity array.
  k=1
  do i=1,size(x,dim=1)-1
    do j=1,size(x,dim=2)-1
      connectivity_array(k:k+3)= &
        & (/id(i,j),id(i,j+1),id(i+1,j+1),id(i+1,j)/)
      k=k+4
    end do
  end do

  ! Get the offsets array.
  j=0
  do i=1,size(connectivity_offsets)
    j=j+4
    connectivity_offsets(i)=j
  end do

  ! ----------------------------------------------------------------------------

  write(io,'(A6)',iostat=err)"<Piece"

  if (err/=0) then
  write(*,*)"VTK_Path"
  write(*,*)"ERROR: writing to file was not possible"
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"error flag : ",err
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  write(io,*)"NumberOfPoints='",size(x),"'"
  write(io,*)"NumberOfVerts='",0,"'"
  write(io,*)"NumberOfLines='",0,"'"
  write(io,*)"NumberOfStrips='",0,"'"
  write(io,*)"NumberOfPolys='",(size(x,dim=1)-1)*(size(x,dim=2)-1),"'"
  write(io,*)">"
  write(io,*)

  if (present(scalar_name)) then
    write(io,*)"<PointData Scalars='PointProperty'>"
    do m=1,size(scalar_name)
      write(io,*)"<DataArray type='Float32' Name='", &
        & trim(scalar_name(m)),"' format='ascii'>"
      k=1 ! Get the property array.
      do i=1,size(x,dim=1)
        do j=1,size(x,dim=2)
          property_array(k)=scalar_value(i,j,m)
          k=k+1
        end do
      end do
      write(io,*)property_array(:)
      write(io,*)"</DataArray>"
    end do
    write(io,*)"</PointData>"
    write(io,*)
  end if

  write(io,*)"<Points>"
  write(io,*)"<DataArray type='Float32' NumberOfComponents='3' format='ascii'>"
  write(io,*)coordinate_array
  write(io,*)"</DataArray>"
  write(io,*)"</Points>"
  write(io,*)
  write(io,*)"<Polys>"
  write(io,*)
  write(io,*)"<DataArray type='Int32' Name='connectivity' format='ascii'>"
  write(io,*)connectivity_array
  write(io,*)"</DataArray>"
  write(io,*)
  write(io,*)"<DataArray type='Int32' Name='offsets' format='ascii'>"
  write(io,*)connectivity_offsets
  write(io,*)"</DataArray>"
  write(io,*)
  write(io,*)"</Polys>"
  write(io,*)
  write(io,'(A8)')"</Piece>"
  write(io,*)

  end subroutine VTK_Surface

  subroutine VTK_Finalize(output_file)

  ! VTK_Finalize is called to finalize an output VTK PolyData file.

  !*******************************************************************************
  ! Evangelos Bertakis, F.EN.I.A. Project, 2009                                  *
  !*******************************************************************************

  ! INPUT (REQUIRED):
  ! output_file: character, scalar. The name of the output VTK file. It must have
  !	the ".vtp" extension (otherwise an error is generated).

  ! INPUT (OPTIONAL):
  ! No optional input arguments. All arguments are required.

  ! OUTPUT (REQUIRED):
  ! This subroutine returns no output.

  ! OUTPUT (OPTIONAL):
  ! This subroutine returns no output.

  ! Example of usage:

  !interface
  !subroutine VTK_Finalize(output_file)
  !implicit none
  !character,intent(in):: output_file*(*)
  !end subroutine VTK_Finalize
  !end interface

  !! Create an empty VTK document:
  !call VTK_Finalize(output_file='plot.vtp')
  !call VTK_Finalize(output_file='plot.vtp')

  ! ------------------------------------------------------------------------------

  ! External variables:
  use Config, only: warnings_pause

  implicit none

  ! Argument variables:
  character,intent(in):: output_file*(*)

  ! Private variables:
  integer:: err,io
  logical:: connected

  ! Variable initialization:
  io=FileManager(output_file,connected)

  ! ------------------------------------------------------------------------------

  ! Error control:

  if (.not.connected) then
  write(*,*)"VTK_Finalize"
  write(*,*)"WARNING: uninitialized VTK file."
  write(*,*)"Continue..."
  if (warnings_pause) read(*,*)
  end if

  if (len_trim(output_file)<5) then
  write(*,*)"VTK_Finalize"
  write(*,*)"ERROR: invalid filename."
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.vtp') then
  write(*,*)"VTK_Finalize"
  write(*,*)"ERROR: file must have the .vtp extension."
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  ! ------------------------------------------------------------------------------

  write(io,'(A10)',iostat=err)"<!--EOF-->"

  if (err/=0) then
  write(*,*)"VTK_Finalize"
  write(*,*)"ERROR: writing to file was not possible."
  write(*,*)"filename : ",trim(output_file)
  write(*,*)"error flag : ",err
  write(*,*)"Program terminated."
  read(*,*)
  stop
  end if

  write(io,'(A11)')"</PolyData>"
  write(io,'(A10)')"</VTKFile>"
  write(io,*)

  end subroutine VTK_Finalize

  integer function get_cell_number_regular_square_lattice(x, y)
    use GlobalAssemblyMod, only : dpitch, nRow, nCol
    implicit none

    real(rp), intent(in) :: x
    real(rp), intent(in) :: y

    integer :: ix, iy

    ix = int(x/dpitch) + 1
    iy = int(y/dpitch) + 1

    get_cell_number_regular_square_lattice = (iy - 1) * nCol + ix

    return

  end function get_cell_number_regular_square_lattice

  real(8) function get_xlocal(assembly_type, x)
    use GlobalAssemblyMod, only : dpitch, str_len

    character(str_len) :: assembly_type
    real(8), intent(in) :: x

    get_xlocal = x - dpitch * int(x/dpitch) - 0.5d0 * dpitch

  end function get_xlocal

  real(8) function get_ylocal(assembly_type, y)
    use GlobalAssemblyMod, only : dpitch, str_len

    character(str_len) :: assembly_type
    real(8), intent(in) :: y

    get_ylocal = y - dpitch * int(y/dpitch) - 0.5d0 * dpitch

  end function get_ylocal

end module
