module GlobalAssemblyMod
  use Config, only : str_len
  use precision_mod

  character(str_len) :: assembly_type

  logical :: flagren = .false.

  integer :: sym         ! Element of the symmetry which is used for the assembly calculations
  integer :: nCellTypes  ! Number of the different types of the cell in the assembly
  integer :: nMat        ! Number of different materials in the assembly
  integer :: nGeomTypes  ! Number of geometrical types of the cells in the assembly
  integer :: nRow        ! Number of rows
  integer :: nCol        ! Number of the columns for the case of the square assembly
  integer :: nRow_c      ! Number of the rows for corner corrections
  integer :: nCol_c      ! Number od the columns for corner corrections
  integer :: nCell       ! Total number of cells in the assembly
  integer :: nGroups     ! Number of energy groups
  integer :: ng_collaps = 2  ! Number of the groups for XS collapsing
  integer :: nPhi        ! Number of the azimuthal angles used for simulation
  integer :: nTheta      ! Number of the polar angles
  real(rp) :: dseg        ! Maximal length of the segment on the sides of the cell
  real(rp) :: dpitch      ! Pitch of the regular assembly (hexagonal or square), i.e. pitch of the cell
  real(rp) :: assmb_pitch ! Pitch of the assembly in the case of the regular assembly with the gap
  integer :: irun        ! Flag to run with different iteration schemes 0: traditional, 1: Helios
  integer :: idraw       ! Flag for draw/no-draw cells geometries
  integer :: icompare    ! Flag for comparison/no-comparison with OpenMC
  integer :: ivtk        ! Flag for print/no-print vtk file with flux distributions
  integer :: ixs         ! Flag for producing homogenised cross sections for the full assembly
  integer :: iout        ! Flag defining the output print
  logical :: withGap     ! With or without gap
  integer :: gap_mat_number ! Number of material which fills the gap
  real(rp) :: eps_f       ! convergence criteria for the fluxes
  real(rp) :: eps_k       ! convergence criteria for the fission sources
  integer :: iter_max    ! Maximal number of the internal iterations (for currents)
  integer :: kiter_max   ! Maximal number of the iuter interations on fission source
  real(8) :: keff        ! Effective multiplication factor

  character(str_len), allocatable :: path_to_material(:) ! Array containing paths to input materials files
  character(str_len), allocatable :: path_to_cell(:)     ! Array containing paths to input cells files
  character(str_len) :: path_to_reference ! Path to the file with the reference results which will be used for comparison

  integer, allocatable :: kart(:) ! Cartogram (material map) of the assembly
  integer, allocatable :: kart_corner(:) ! Material map of the corners for the correction of the albedos

  real(rp), allocatable :: STOT(:,:)   ! Total cross section STOT(material,group)
  real(rp), allocatable :: SSCA(:,:,:) ! Scattering cross sections SSCA(material,group,group)
  real(rp), allocatable :: SCHI(:,:)   ! Fission spectrum of the prompt neutrons
  real(rp), allocatable :: SFIS(:,:)   ! Fisssion cross section SFIS(material,group)
  real(rp), allocatable :: SFNF(:,:)   ! Nu-fission cross section SFNF(material,group)
  real(rp), allocatable :: SABS(:,:)   ! Absorbtion cross section SABS(material,group)
  real(rp), allocatable :: SOUR(:,:)   ! External source in given material SOUR(material,group)

  integer, allocatable  :: group_bounds_collapsed(:) ! array containing boundaries (group numbers) for the collapsed group structure
  real(rp), allocatable :: xs_tot(:,:)    ! Total cross sections collapsed
  real(rp), allocatable :: xs_abs(:,:)    ! Absorption cross sections collapsed
  real(rp), allocatable :: xs_sfnf(:,:)   ! Production cross sections collapsed
  real(rp), allocatable :: xs_scat(:,:,:) ! Scattering cross sections collapsed
  real(rp), allocatable :: xs_chi(:,:)    ! Fission spectrum collapsed
  real(rp), allocatable :: xs_sf(:,:)     ! Fission cross section collapsed
  integer, allocatable  :: merge_cells(:) ! Array with the numbers of the homogenisation regions

  character(str_len), allocatable :: cell_type(:) ! Cell types in the cartogram
  integer, allocatable  :: ncirc(:)               ! Numbers of circles in the cells ncirc(nCellTypes)
  real(rp), allocatable :: albedo(:,:,:)             ! Albedos for each side of the fuel assembly
  integer, allocatable  :: num_cell_edge(:,:)     ! Cell number on the edge of the assembly (changes from 1 to nCol/nRow)
  real(rp), allocatable :: cell_rad(:,:)          ! Radii of the circles in the cells Rad(nCellTypes,maxval(ncirc))
  real(rp), allocatable :: phiSect(:)             ! Sector boundaries of the azimuthal angles phi
  real(rp), allocatable :: thetaSect(:)           ! Sector boundaries of the polar angles theta
  integer, allocatable  :: nsides(:)              ! Number of sides in the cell nsides(nCellTypes)
  integer, allocatable  :: nharms(:)       ! Number of harmonics in the cell
  integer, allocatable  :: nzones(:)              ! Number of regions in the cell
  real(rp), allocatable :: chi(:)                 ! Weights of the sectors on azimuthal angle phi
  real(rp), allocatable :: eps(:)                 ! Weights of the sectors on polar angle theta

  integer, allocatable  :: cell_nbetaelem(:)      ! Number of non-zero beta elements in the Beta matrix cell_nbetaelem(icell)
  real(rp), allocatable :: cell_beta(:,:)         ! Beta elements for the cell cell_beta(icell,maxval(cell_nbetaelem))
  real(rp), allocatable :: cell_brc(:,:)
  integer, allocatable :: cell_insidebet(:,:)    ! input side of the beta elements
  integer, allocatable :: cell_insegbet(:,:)     ! input segment of the beta elements
  integer, allocatable :: cell_insectbet(:,:)    ! input sector of the beta elements
  integer, allocatable :: cell_outsidebet(:,:)   ! output side of the beta elements
  integer, allocatable :: cell_outsegbet(:,:)    ! output segment of the beta elements
  integer, allocatable :: cell_outsectbet(:,:)   ! output sector of the beta elements
  integer, allocatable :: cell_thetasectbet(:,:) ! theta sector of the beta elements

  integer, allocatable :: cell_nsegments(:,:) ! Number of segments on the sides of the cell cell_nsegments(nCellTypes,maxval(nsides))
  integer, allocatable :: cell_mat(:,:) ! material composition of the cells mat_cell(nCellTypes,maxval(ncirc)+1)
  integer, allocatable :: cell_nphib(:) ! nphib in the cells nphib_cell(nCellTypes)
  integer, allocatable :: cell_nphig(:) ! nphig in the cells nphig_cell(nCellTypes)
  integer, allocatable :: cell_nphiu(:) ! nphiu in the cells nphiu_cell(nCellTypes)
  real(rp), allocatable :: cell_dcb(:)   ! dcb for the cells dcb_cell(nCellTypes)
  real(rp), allocatable :: cell_dcg(:)   ! dcg for the cells dcg_cell(nCellTypes)
  real(rp), allocatable :: cell_dcu(:)   ! dcu for the cells dcu_cell(nCellTypes)
  real(rp), allocatable :: cell_vertx(:,:) ! x-coordinates of the outer polygon of the cell cell_vertx(nCellTypes,maxval(nsides))
  real(rp), allocatable :: cell_verty(:,:) ! y-coordinates of the outer polygon of the cell cell_verty(nCellTypes,maxval(nsides))
  real(rp), allocatable :: cell_vol(:,:)   ! volumes of regions in the cells cell_vol(nCellTypes,maxval(ncirc)+1)
  real(rp), allocatable :: cell_segboundx(:,:,:) ! x coordinates of the boundaries of the segments on each side of the cell cell_segboundx(nCellType,maxval(nsides),maxval(nsegments)+1)
  real(rp), allocatable :: cell_segboundy(:,:,:) ! y coordinates of the boundaries of the segments on each side of the cell cell_segboundx(nCellType,maxval(nsides),maxval(nsegments)+1)
  real(rp), allocatable :: cell_segsdarr(:,:,:)  ! Segment boundaries (one-dimensional, distributed from 0 to side's length) on the sides of the cells cell_segsdarr(nCellType,maxval(nsides),maxval(nsegments)+1)
  real(rp), allocatable :: cell_ort(:,:,:,:)     ! Orthogonal coefficients of the cell in each region cell_ort(nCellTypes,maxval(nzones),maxval(nharms),maxval(nharms))

  real(rp), allocatable :: cell_G(:,:,:,:,:,:,:)
  real(rp), allocatable :: cell_V(:,:,:,:,:,:,:)
  real(rp), allocatable :: cell_U(:,:,:,:,:)

  real(rp), allocatable :: cell_flux(:,:,:,:)
  real(rp), allocatable :: cell_curr(:,:,:,:,:,:)

  integer, allocatable :: neighCell(:,:)
  integer, allocatable :: neighSide(:,:)
  real(rp), allocatable :: fact(:)

  real(rp), allocatable :: flux_fuel_reference(:) ! reference 1-group flux in the fuel
  real(rp), allocatable :: flux_aver_reference(:) ! reference 1-group flux averaged over the cell

  integer :: verbosity ! Verbosity level

contains

  subroutine AbortProgram
    implicit none

    stop
  end subroutine AbortProgram

  logical function inMaterialMap(icell)

    integer, intent(in) :: icell ! type of the cell

    integer :: i

    do i = 1, nCell
      if (icell == kart(i)) then
        inMaterialMap = .true.
        return
      end if
    end do

    inMaterialMap = .false.

    return

  end function inMaterialMap

end module
