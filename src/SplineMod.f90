module SplineMod
    use precision_mod

    real(rp), allocatable :: b3_bickley(:,:)
    real(rp), allocatable :: c3_bickley(:,:)
    real(rp), allocatable :: d3_bickley(:,:)
    real(rp), allocatable :: bickley3_data(:,:)

    real(rp), allocatable :: b2_bickley(:,:)
    real(rp), allocatable :: c2_bickley(:,:)
    real(rp), allocatable :: d2_bickley(:,:)
    real(rp), allocatable :: bickley2_data(:,:)

    real(rp), allocatable :: b1_bickley(:,:)
    real(rp), allocatable :: c1_bickley(:,:)
    real(rp), allocatable :: d1_bickley(:,:)
    real(rp), allocatable :: bickley1_data(:,:)

    real(rp), allocatable :: tau_data(:)

    real(rp) :: tau_min, tau_max
    real(rp) :: dtau
    integer :: n_tau

contains

    subroutine allocate_bickley(n_theta, n_tau)
        implicit none

        integer, intent(in) :: n_theta
        integer, intent(in) :: n_tau

        allocate(b3_bickley(n_theta, n_tau))
        allocate(c3_bickley(n_theta, n_tau))
        allocate(d3_bickley(n_theta, n_tau))
        b3_bickley = 0.0_rp
        c3_bickley = 0.0_rp
        d3_bickley = 0.0_rp
        allocate(b2_bickley(n_theta, n_tau))
        allocate(c2_bickley(n_theta, n_tau))
        allocate(d2_bickley(n_theta, n_tau))
        b2_bickley = 0.0_rp
        c2_bickley = 0.0_rp
        d2_bickley = 0.0_rp
        allocate(b1_bickley(n_theta, n_tau))
        allocate(c1_bickley(n_theta, n_tau))
        allocate(d1_bickley(n_theta, n_tau))
        b1_bickley = 0.0_rp
        c1_bickley = 0.0_rp
        d1_bickley = 0.0_rp

        return

    end subroutine

    subroutine read_bickley_data(n_theta)
        use Config, only : str_len
        use GlobalAssemblyMod, only : eps
        implicit none

        integer, intent(in)                         :: n_theta

        integer :: i, i_theta, i_tau
        character(len=str_len) :: name_of_file1, name_of_file2, name_of_file3
        real(rp) :: theta_min, theta_max
        character(256) :: path_to_bickley
        integer :: statu

        call get_environment_variable("PYCCCPO_HOME", value=path_to_bickley, status=statu)
        if (statu == 1) then
          name_of_file1 = adjustl("data/bickley1_"//adjustl(str(n_theta)//".bick"))
          name_of_file2 = adjustl("data/bickley2_"//adjustl(str(n_theta)//".bick"))
          name_of_file3 = adjustl("data/bickley3_"//adjustl(str(n_theta)//".bick"))
        else
          name_of_file1 = adjustl(trim(path_to_bickley)//"/data/bickley1_"//adjustl(str(n_theta)//".bick"))
          name_of_file2 = adjustl(trim(path_to_bickley)//"/data/bickley2_"//adjustl(str(n_theta)//".bick"))
          name_of_file3 = adjustl(trim(path_to_bickley)//"/data/bickley3_"//adjustl(str(n_theta)//".bick"))
        end if
        open(10, file = name_of_file1)
        open(20, file = name_of_file2)
        open(30, file = name_of_file3)

        read(10,*) n_tau, tau_min, tau_max
        read(20,*) n_tau, tau_min, tau_max
        read(30,*) n_tau, tau_min, tau_max
        dtau = (tau_max - tau_min)/(n_tau - 1)

        allocate(tau_data(n_tau))
        tau_data = 0.0_rp
        do i_tau = 1, n_tau
            tau_data(i_tau) = (i_tau - 1) * dtau
        end do
        allocate(bickley1_data(n_theta,n_tau))
        allocate(bickley2_data(n_theta,n_tau))
        allocate(bickley3_data(n_theta,n_tau))
        bickley1_data = 0.0_rp
        bickley2_data = 0.0_rp
        bickley3_data = 0.0_rp

        do i = 1, n_theta
            read(10, *) i_theta, theta_min, theta_max
            read(20, *) i_theta, theta_min, theta_max
            read(30, *) i_theta, theta_min, theta_max
            do i_tau = 1, n_tau
                read(10,*) bickley1_data(i_theta,i_tau)
                read(20,*) bickley2_data(i_theta,i_tau)
                bickley2_data(i_theta,i_tau) = bickley2_data(i_theta,i_tau)/eps(i_theta)
                read(30,*) bickley3_data(i_theta,i_tau)
                bickley3_data(i_theta,i_tau) = bickley3_data(i_theta,i_tau)/eps(i_theta)
            end do
        end do

        close(10)
        close(20)
        close(30)

    end subroutine

    subroutine spline_coefficients_bickley(n_theta, n_tau)
        implicit none

        integer, intent(in) :: n_theta
        integer, intent(in) :: n_tau

        integer :: i_theta

        do i_theta = 1, n_theta
            call spline_coefficients(n_tau, tau_data(1:n_tau), bickley1_data(i_theta,1:n_tau), &
                                     b1_bickley(i_theta,1:n_tau), c1_bickley(i_theta,1:n_tau),  &
                                     d1_bickley(i_theta,1:n_tau))
            call spline_coefficients(n_tau, tau_data(1:n_tau), bickley2_data(i_theta,1:n_tau), &
                                     b2_bickley(i_theta,1:n_tau), c2_bickley(i_theta,1:n_tau),  &
                                     d2_bickley(i_theta,1:n_tau))
            call spline_coefficients(n_tau, tau_data(1:n_tau), bickley3_data(i_theta,1:n_tau), &
                                     b3_bickley(i_theta,1:n_tau), c3_bickley(i_theta,1:n_tau), &
                                     d3_bickley(i_theta,1:n_tau))
        end do

    end subroutine

    subroutine spline_coefficients(n, x, y, b, c, d)
        !======================================================================
        !  Calculate the coefficients b(i), c(i), and d(i), i=1,2,...,n
        !  for cubic spline interpolation
        !  s(x) = y(i) + b(i)*(x-x(i)) + c(i)*(x-x(i))**2 + d(i)*(x-x(i))**3
        !  for  x(i) <= x <= x(i+1)
        !  Alex G: January 2010
        !----------------------------------------------------------------------
        !  input..
        !  x = the arrays of data abscissas (in strictly increasing order)
        !  y = the arrays of data ordinates
        !  n = size of the arrays xi() and yi() (n>=2)
        !  output..
        !  b, c, d  = arrays of spline coefficients
        !  comments ...
        !  spline.f90 program is based on fortran version of program spline.f
        !  the accompanying function fspline can be used for interpolation
        !======================================================================
        implicit none
        integer, intent(in)  :: n
        real(rp), intent(in)  :: x(:)
        real(rp), intent(in)  :: y(:)
        real(rp), intent(out)  :: b(:), c(:), d(:)

        integer i, j, gap
        real(rp) h

        gap = n-1
        ! check input
        if ( n < 2 ) return
        if ( n < 3 ) then
            b(1) = (y(2)-y(1))/(x(2)-x(1))   ! linear interpolation
            c(1) = 0.
            d(1) = 0.
            b(2) = b(1)
            c(2) = 0.
            d(2) = 0.
            return
        end if
        !
        ! step 1: preparation
        !
        d(1) = x(2) - x(1)
        c(2) = (y(2) - y(1))/d(1)
        do i = 2, gap
            d(i) = x(i+1) - x(i)
            b(i) = 2.0*(d(i-1) + d(i))
            c(i+1) = (y(i+1) - y(i))/d(i)
            c(i) = c(i+1) - c(i)
        end do
        !
        ! step 2: end conditions
        !
        b(1) = -d(1)
        b(n) = -d(n-1)
        c(1) = 0.0
        c(n) = 0.0
        if(n /= 3) then
            c(1) = c(3)/(x(4)-x(2)) - c(2)/(x(3)-x(1))
            c(n) = c(n-1)/(x(n)-x(n-2)) - c(n-2)/(x(n-1)-x(n-3))
            c(1) = c(1)*d(1)**2/(x(4)-x(1))
            c(n) = -c(n)*d(n-1)**2/(x(n)-x(n-3))
        end if
        !
        ! step 3: forward elimination
        !
        do i = 2, n
            h = d(i-1)/b(i-1)
            b(i) = b(i) - h*d(i-1)
            c(i) = c(i) - h*c(i-1)
        end do
        !
        ! step 4: back substitution
        !
        c(n) = c(n)/b(n)
        do j = 1, gap
            i = n-j
            c(i) = (c(i) - d(i)*c(i+1))/b(i)
        end do
        !
        ! step 5: compute spline coefficients
        !
        b(n) = (y(n) - y(gap))/d(gap) + d(gap)*(c(gap) + 2.0*c(n))
        do i = 1, gap
            b(i) = (y(i+1) - y(i))/d(i) - d(i)*(c(i+1) + 2.0*c(i))
            d(i) = (c(i+1) - c(i))/d(i)
            c(i) = 3.*c(i)
        end do
        c(n) = 3.0*c(n)
        d(n) = d(n-1)
    end subroutine spline_coefficients

    real(rp) function ispline_bickley(ibick, i_theta, u)
        !======================================================================
        ! function ispline evaluates the cubic spline interpolation at point z
        ! ispline = y(i)+b(i)*(u-x(i))+c(i)*(u-x(i))**2+d(i)*(u-x(i))**3
        ! where  x(i) <= u <= x(i+1)
        !----------------------------------------------------------------------
        ! input..
        ! u       = the abscissa at which the spline is to be evaluated
        ! x, y    = the arrays of given data points
        ! b, c, d = arrays of spline coefficients computed by spline
        ! n       = the number of data points
        ! output:
        ! ispline = interpolated value at point u
        !=======================================================================
        implicit none
        integer, intent(in) :: ibick
        integer, intent(in) :: i_theta
        real(rp), intent(in) :: u
        integer i
        real(rp) dx

        ! if u is ouside the x() interval take a boundary value (left or right)
        if (u > tau_max) then
            ispline_bickley = 0.0_rp
            return
        elseif (u < tau_min) then
            write(*,*) "Function <ispline_bickley3>:"
            write(*,*) "optical path is less than minimal allowable value. Program is stopped."
            stop
        end if
        ispline_bickley = 0.0_rp

        ! Define interval i where spline should be evaluated
        i = int(u/dtau) + 1

        !*
        !  evaluate spline interpolation
        !*
        dx = u - tau_data(i)
        if (ibick == 1) then
          ispline_bickley = bickley1_data(i_theta,i) + dx*(b1_bickley(i_theta,i) + &
                            dx*(c1_bickley(i_theta,i) + dx*d1_bickley(i_theta,i)))
          return
        elseif (ibick == 2) then
          ispline_bickley = bickley2_data(i_theta,i) + dx*(b2_bickley(i_theta,i) + &
                            dx*(c2_bickley(i_theta,i) + dx*d2_bickley(i_theta,i)))
          return
        elseif (ibick == 3) then
          ispline_bickley = bickley3_data(i_theta,i) + dx*(b3_bickley(i_theta,i) + &
                            dx*(c3_bickley(i_theta,i) + dx*d3_bickley(i_theta,i)))
          return
        end if

    end function ispline_bickley

    character(len=20) function str(k)
    !   "Convert an integer to string."
      integer, intent(in) :: k
      write (str, *) k
      str = adjustr(str)
    end function str

    real(rp) function f3(tau, theta)
        implicit none
        real(rp), intent(in) :: tau
        real(rp), intent(in) :: theta

        f3 = sin(theta)*sin(theta)*exp(-tau/sin(theta))

    end function

!    subroutine integrate_bickley_riemann(f, theta1, theta2, tau, typ, N, ans)
!        implicit none
!        interface
!            real(rp) function f(tau, theta)
!              real(rp), intent(in) :: tau
!              real(rp), intent(in) :: theta
!            end function
!        end interface
!
!        real(rp), intent(in) :: theta1
!        real(rp), intent(in) :: theta2
!        real(rp), intent(in) :: tau
!        integer, intent(in) :: typ
!        integer, intent(in) :: N
!        real(rp), intent(out) :: ans
!
!        real(rp) :: theta, dtheta
!        integer :: i
!
!        ans = 0.0_rp
!
!        dtheta = (theta2 - theta1)/N
!
!        do i = 1, N
!            if (typ == 1) then
!                theta = theta1 + (i - 1) * dtheta
!            elseif (typ == 2) then
!                theta = theta1 + i * dtheta
!            elseif (typ == 3) then
!                theta = theta1 + (2 * i - 1)*dtheta*0.5_rp
!            end if
!            ans = ans + f(tau, theta)*dtheta
!        end do
!
!        return
!    end subroutine

    real(rp) function Ki3_partial(i_theta, tau)
      implicit none
      real(rp), intent(in) :: tau
      integer, intent(in) :: i_theta

      Ki3_partial = ispline_bickley(3, i_theta, tau)

    end function

    real(rp) function Ki2_partial(i_theta, tau)
      implicit none
      real(rp), intent(in) :: tau
      integer, intent(in) :: i_theta

      Ki2_partial = ispline_bickley(2, i_theta, tau)

    end function

    real(rp) function Ki1_partial(i_theta, tau)
      implicit none
      real(rp), intent(in) :: tau
      integer, intent(in) :: i_theta

      Ki1_partial = ispline_bickley(1, i_theta, tau)

    end function

end module SplineMod
