module ConstantsMod
  !---------------------------------------------
  ! Author : DL
  ! Date   : 06.09.2016
  ! Module contains different constants used by program
  !---------------------------------------------
  use precision_mod
  integer, parameter :: MAX_LINE_LENGTH = 128 ! Maximal number of characters in line
  integer, parameter :: MAX_MSG_LEN = 256     ! Maximal length of the message in terminal window
  integer, parameter :: N_GAUSS_POINTS = 200  ! Number of Gauss points for integration of the
  real(rp), parameter :: DELTA_MIN = 5.0E-6_rp
  real(rp), parameter :: PI  = 3.141592653589793238462643383279502884197_rp
  real(rp), parameter :: PI4 = 1.273239544735162764865776807710062712431_rp ! 4/PI
  real(rp), parameter :: PI2 = 0.636619772367581382432888403855031356215_rp ! 2/PI
end module ConstantsMod
