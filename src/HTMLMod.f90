module HTMLMod
  use precision_mod
  !------------------------------------
  ! This module contains functions and
  ! subroutines related to manipulation
  ! with HTML files.
  !------------------------------------
contains

  subroutine HTML_Initialize(output_file)

    ! HTML_Initialize is called to initialize an output HTML file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".html" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine HTML_Initialize(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine HTML_Initialize
    !end interface

    !call HTML_Initialize(output_file='output.html')
    !call HTML_Finalize(output_file='output.html')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (connected) then
      write(*,*)"HTML_Initialize"
      write(*,*)"WARNING: HTML file has already been accessed."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<6) then
      write(*,*)"HTML_Initialize"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-4:len_trim(output_file))/='.html') then
      write(*,*)"HTML_Initialize"
      write(*,*)"ERROR: file must have the .html extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A6)',iostat=err)"<html>"

    if (err/=0) then
      write(*,*)"HTML_Initialize"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,'(A6)')"<head>"
    write(io,'(A23)')"<style type='text/css'>"
    write(io,'(A6)')"body {"
    write(io,'(A34)')"margin-left: 3%; margin-right: 3%;"
    write(io,'(A34)')"margin-top: 3%; margin-bottom: 3%;"
    write(io,'(A21)')"font-family: verdana;"
    write(io,'(A1)')"}"
    write(io,'(A8)')"</style>"
    write(io,'(A7)')"</head>"
    write(io,'(A6)')"<body>"
    write(io,*)

  end subroutine HTML_Initialize

  subroutine HTML_Heading(output_file,string,level)

    ! HTML_Heading is called to write a heading in an output HTML file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output HTML file. It must have
    !	the ".html" extension (otherwise an error is generated).
    ! string: character, scalar. The string of the heading.
    ! level: integer, scalar. The level of the heading. There are 6 supported
    !	levels with level 1 being the most "important" and level 6 the less
    !	"important".

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine HTML_Heading(output_file,string,level)
    !implicit none
    !character,intent(in):: output_file*(*),string*(*)
    !integer,intent(in):: level
    !end subroutine HTML_Heading
    !end interface

    !call HTML_Initialize(output_file='output.html')
    !call HTML_Heading(output_file='output.html',string='Hello World',level=1)
    !call HTML_Finalize(output_file='output.html')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*),string*(*)
    integer,intent(in):: level

    ! Private variables:
    integer:: io,err
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"HTML_Heading"
      write(*,*)"WARNING: uninitialized HTML file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<6) then
      write(*,*)"HTML_Heading"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-4:len_trim(output_file))/='.html') then
      write(*,*)"HTML_Heading"
      write(*,*)"ERROR: file must have the .html extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((level<1).or.(level>6)) then
      write(*,*)"HTML_Heading"
      write(*,*)"ERROR: unsuported heading level"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (len_trim(string)==0) then
      write(*,*)"HTML_Heading"
      write(*,*)"ERROR: missing heading string"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A2,I1,A1)',iostat=err)"<h",level,">"

    if (err/=0) then
      write(*,*)"HTML_Heading"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! Write the heading string:
    write(io,*)trim(string)

    ! Note: some compilers may break a very long character string in multiple lines
    ! when printing it in a file. To avoid such behaviour, use the "advance=no"
    ! option in the write statement as follows. The code given will force the string
    ! to remain in one line.
    !do i=1,len_trim(string)-1
    !	write(io,'(A1)',advance='no')string(i:i)
    !end do
    !write(io,'(A1)')string(len_trim(string):len_trim(string))

    write(io,'(A3,I1,A1)')"</h",level,">"
    write(io,*)

  end subroutine HTML_Heading

  subroutine HTML_Table(output_file,numbers,headings,decimal_places,scientific)

    ! HTML_Table is called to create a two-dimensional table in an HTML file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".html" extension (otherwise an error is generated).
    ! numbers: real, array (2D). Contains the table's numerical data.
    ! headings: character, array (1D). Contains the headings of the table. The array
    !	must have as many elements as the amount of columns of input variable
    !	array "numbers".
    ! decimal_places: integer, array (1D). The amount of decimal places that the
    !	formatted form of every column is required to have. The array must
    !	have as many elements as the amount of columns of input variable array
    !	"numbers".
    ! scientific: logical, array (1D). Each element specifies whether the scientific
    !	notation will be used for the respective table column. The array must
    !	have as many elements as the amount of columns of input variable array
    !	"numbers".

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine HTML_Table(output_file,numbers,headings,decimal_places,scientific)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*),headings(:)*(*)
    !real(rp),intent(in):: numbers(:,:)
    !integer,intent(in):: decimal_places(:)
    !logical,intent(in):: scientific(:)
    !end subroutine HTML_Table
    !end interface

    !real(rp):: x(30),y(30),numbers(30,2)

    !x(1)=0.0 ; y(1)=0.000 ; x(11)=1.0 ; y(11)=0.841 ; x(21)=2.0 ; y(21)=0.909
    !x(2)=0.1 ; y(2)=0.100 ; x(12)=1.1 ; y(12)=0.891 ; x(22)=2.1 ; y(22)=0.863
    !x(3)=0.2 ; y(3)=0.199 ; x(13)=1.2 ; y(13)=0.932 ; x(23)=2.2 ; y(23)=0.808
    !x(4)=0.3 ; y(4)=0.296 ; x(14)=1.3 ; y(14)=0.964 ; x(24)=2.3 ; y(24)=0.746
    !x(5)=0.4 ; y(5)=0.389 ; x(15)=1.4 ; y(15)=0.985 ; x(25)=2.4 ; y(25)=0.675
    !x(6)=0.5 ; y(6)=0.479 ; x(16)=1.5 ; y(16)=0.997 ; x(26)=2.5 ; y(26)=0.598
    !x(7)=0.6 ; y(7)=0.565 ; x(17)=1.6 ; y(17)=1.000 ; x(27)=2.6 ; y(27)=0.516
    !x(8)=0.7 ; y(8)=0.644 ; x(18)=1.7 ; y(18)=0.992 ; x(28)=2.7 ; y(28)=0.427
    !x(9)=0.8 ; y(9)=0.717 ; x(19)=1.8 ; y(19)=0.974 ; x(29)=2.8 ; y(29)=0.335
    !x(10)=0.9 ; y(10)=0.783 ; x(20)=1.9 ; y(20)=0.946 ; x(30)=2.9 ; y(30)=0.239

    !numbers(:,1)=x
    !numbers(:,2)=y

    !call HTML_Initialize(output_file='output.html')
    !call HTML_Table(output_file='output.html',numbers=numbers, &
      !	& headings=(/'x-axis data','y-axis data'/),decimal_places=(/1,3/), &
      !	& scientific=(/.false.,.false./))
    !call HTML_Finalize(output_file='output.html')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause
    use ReadWriteMod, only : FileManager, FormatReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*),headings(:)*(*)
    real(rp),intent(in):: numbers(:,:)
    integer,intent(in):: decimal_places(:)
    logical,intent(in):: scientific(:)

    ! Private variables:
    integer:: i,j,io,err
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"HTML_Table"
      write(*,*)"WARNING: uninitialized HTML file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<6) then
      write(*,*)"HTML_Table"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-4:len_trim(output_file))/='.html') then
      write(*,*)"HTML_Table"
      write(*,*)"ERROR: file must have the .html extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (all(numbers==0)) then
      write(*,*)"HTML_Table"
      write(*,*)"WARNING: all table numbers are equal to zero"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (all(headings=='')) then
      write(*,*)"HTML_Table"
      write(*,*)"WARNING: missing table headings"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((size(numbers,dim=2)/=size(headings)).or. &
        & (size(numbers,dim=2)/=size(decimal_places)).or. &
        & (size(numbers,dim=2)/=size(scientific))) then
      write(*,*)"HTML_Table"
      write(*,*)"ERROR: unmatching table contents and miscellaneous table information"
      write(*,*)"size(numbers,dim=2)",size(numbers,dim=2)
      write(*,*)"size(headings)",size(headings)
      write(*,*)"size(decimal_places)",size(decimal_places)
      write(*,*)"size(scientific)",size(scientific)
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A50)',iostat=err)"<table border='0' cellspacing='0' cellpadding='5'>"

    if (err/=0) then
      write(*,*)"HTML_Table"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,'(A19)')"<tr align='center'>"
    do i=1,size(headings)
      write(io,*)"<th>"//trim(headings(i))//"</th>"
    end do
    write(io,'(A5)')"</tr>"

    do i=1,size(numbers,dim=1)
      write(io,'(A19)')"<tr align='center'>"
      do j=1,size(numbers,dim=2)
        write(io,*)"<td>"// &
          & trim(FormatReal(numbers(i,j),decimal_places(j), &
          & scientific(j)))//"</td>"
      end do
      write(io,'(A5)')"</tr>"
    end do

    write(io,'(A8)')"</table>"
    write(io,*)

  end subroutine HTML_Table

  subroutine HTML_Finalize(output_file)

    ! HTML_Finalize is called to finalize an output HTML file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".html" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine HTML_Finalize(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine HTML_Finalize
    !end interface

    !call HTML_Initialize(output_file='output.html')
    !call HTML_Finalize(output_file='output.html')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"HTML_Finalize"
      write(*,*)"WARNING: uninitialized HTML file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<6) then
      write(*,*)"HTML_Finalize"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-4:len_trim(output_file))/='.html') then
      write(*,*)"HTML_Finalize"
      write(*,*)"ERROR: file must have the .html extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A10)',iostat=err)"<!--EOF-->"

    if (err/=0) then
      write(*,*)"HTML_Finalize"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,'(A7)')"</body>"
    write(io,'(A7)')"</html>"

    close(io)

  end subroutine HTML_Finalize

end module
