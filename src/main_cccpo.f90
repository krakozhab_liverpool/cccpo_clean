program main_cccpo
  use Config, only : str_len
  implicit none

  character(len=str_len) :: inputFile, outputFile
  character(len=str_len), allocatable :: arg(:)

  integer :: i, n

  !---------------------------------------------
  ! Getting command line arguments
  !---------------------------------------------
  n = command_argument_count()
  if (n > 0) then
    allocate(arg(n))
    do i = 1, n
      call get_command_argument(i, value=arg(i))
    end do
    if (n == 1) then
      inputFile = arg(1)
      outputFile = 'results/output.out'
    elseif (n == 2) then
      inputFile = arg(1)
      outputFile = arg(2)
    end if
  else
    inputFile = 'input/input.in'
    outputFile = 'results/output.out'
  end if

  call CCCPO_SC(inputFile)

!  write(*,'(2f10.3,f10.2)') (flux_omc(i), flux_merged(i),(flux_merged(i)-flux_omc(i))*100.0_rp/flux_omc(i), i = 1, size(flux_merged))

!  open(10, file = outputFile)
!  write(10,'(2f10.3,f10.2)') (flux_omc(i), flux_merged(i), &
!                             (flux_merged(i)-flux_omc(i))*100.0_rp/flux_omc(i), &
!                              i = 1, size(flux_merged))
!  close(10)

end program
