module GlobalMod
  use ConstantsMod, only : N_GAUSS_POINTS
  use Config, only : str_len
  use ints_module
  use fhash_module__ints_double
  use precision_mod
  !---------------------------------------------
  ! Author : DL
  ! Date   : 06.09.2016
  ! Module contains different variables and subroutines which can be used by
  ! all other modules and subroutines in the project
  !---------------------------------------------
  character(str_len) :: cellType ! keyword defining type of the cell: regular - for regular polygon
                                         !                                    arbitrary - for arbitrary cell
  real(rp) :: xGauss(N_GAUSS_POINTS, N_GAUSS_POINTS) ! abscissas for Gauss integration scheme
  real(rp) :: wGauss(N_GAUSS_POINTS, N_GAUSS_POINTS) ! weights for Gauss integration scheme

  integer :: nSides ! Number of the sides in the cell
  integer :: nPhi   ! Number of the angles for discretization of the azimuthal angle
  integer :: nTheta ! Number of the angles for discretization of the polar angle
  integer :: nHarm  ! Number of the harmonics for flux expansion

  real(rp), allocatable :: phiSect(:)    ! Boundaries of sectors on phi angle
  real(rp), allocatable :: thetaSect(:)  ! Boundaries of sectors on theta (polar) angle
  real(rp), allocatable :: chi(:)        ! Weights of the sectors on phi angle = 0.5*(sin(phi(n+1)) - sin(phi(n)))
  real(rp), allocatable :: eps(:)        ! Weights of the sectors on phi angle
  real(rp), allocatable :: segSdArr(:,:) ! Array containing boundaries of the segments on the sides in one dimension i.e. from 0 to sideLength

  real(rp) :: dSeg   ! Maximal length of the segment on the side of the polygon
  real(rp) :: dPitch ! Pitch of the lattice

  integer :: nZone  ! Total number of zones in the cell
  integer :: nCirc  ! Number of circles in the cell1
  integer :: nMat   ! Number of the materials with different cross sections

  real(rp), allocatable :: Rad(:) ! Radii of the circles
  real(rp), allocatable :: cX(:)  ! x-coordinates of the circle's centres in the cell
  real(rp), allocatable :: cY(:)  ! y-coordinates of the circle's centres in the cell

  real(rp), allocatable :: Acoef(:)   ! Coefficients A for line equation describing side of the cell
  real(rp), allocatable :: Bcoef(:)   ! Coefficients B for line equation describing side of the cell
  real(rp), allocatable :: Ccoef(:)   ! Coefficients C for line equation describing side of the cell
  real(rp), allocatable :: zoneVol(:) ! Volumes of the zones in the cell
  real(rp), allocatable :: ort(:,:,:) ! Orthogonal coefficients of zones in the cell

  real(rp), allocatable :: verX(:) ! x-coordinates of the vertices of the polygon
  real(rp), allocatable :: verY(:) ! y-coordinates of the vertices of the polygon

  real(rp), allocatable :: normVecX(:) ! x-coordinates of the normal vector to the polygon sides
  real(rp), allocatable :: normVecY(:) ! y-coordinates of the normal vector to the polygon sides

  real(rp), allocatable :: Stot(:) ! Total cross sections
  real(rp), allocatable :: Ssca(:) ! Scattering cross sections
  real(rp), allocatable :: Sabs(:) ! Absorption cross sections
  real(rp), allocatable :: Qsrc(:) ! External source of the neutrons

  integer, allocatable :: Kart(:)      ! Material mapping inside cell from
                                       ! innermost to outermost regions
  integer, allocatable :: merge_arr(:) ! Array containing the data for merging fluxes

  integer :: nPhiB ! minimal number of the angles for integration of Beta
  integer :: nPhiG ! minimal number of the angles for integration of Gamma
  integer :: nPhiU ! minimal number of the angles for integration of U

  real(rp) :: dCb   ! Maximal length of the segment for integration of beta
  real(rp) :: dCg   ! Maximal length of the segment for integration of beta
  real(rp) :: dCu   ! Maximal length of the segment for integration of beta

  integer, allocatable :: nSegments(:)   ! Number of the segments on the side of the polygon
  real(rp), allocatable :: segBoundX(:,:) ! x coordinates of the segment's boundaries on the sides of the polygon
  real(rp), allocatable :: segBoundY(:,:) ! y coordinates of the segment's boundaries on the sides of the polygon

  integer, allocatable :: nAngBeta(:) ! Actual number of the angles for integration of beta (surface to surface probability)
  integer, allocatable :: nAngGamm(:) ! Actual number of the angles for integration of gamma (surface to region probability)
  integer, allocatable :: nAngU(:)    ! Actual number of the angles for integration of U (region to region probability)

  real(rp), allocatable :: phiBeta(:,:) ! Array contains boundaries of the angles for integration over phi for each side of polygon
  real(rp), allocatable :: phiGamm(:,:) ! Array contains boundaries of the angles for integration over phi
  real(rp), allocatable :: phiU(:,:)    ! Array contains boundaries of the angles for integration over phi

  integer              :: nChordBet         ! number of chords for integration of surface-to-surface CP (beta)
  integer, allocatable :: nSegChordBet(:)   ! nSegChordBet(numberOfBetaChords) number of segments along the chord for integration beta
  real(rp), allocatable :: lenChordBet(:,:)  ! length of segments along the chord
  real(rp), allocatable :: wghtChordBet(:)   ! weight of the chord
  integer, allocatable :: zoneChordBet(:,:) ! zones' numbers along the chord (for definition of the materials)
  real(rp), allocatable :: xChordBet(:,:)    ! x-coordinates of the segments along the chord
  real(rp), allocatable :: yChordBet(:,:)    ! y-coordinates of the segments along the chord
  integer, allocatable :: inSideBet(:)      ! number of input side for beta element
  integer, allocatable :: inSegBet(:)       ! number of input segment for beta element
  integer, allocatable :: inSectBet(:)      ! number of input sector for beta element
  integer, allocatable :: outSideBet(:)     ! number of output side for beta element
  integer, allocatable :: outSegBet(:)      ! number of output segment for beta element
  integer, allocatable :: outSectBet(:)     ! number of output segment for beta element
  integer, allocatable :: thetaSectBet(:)   ! number of polar sector
  real(rp), allocatable :: brc(:)            ! beta reciprocity coefficient for beta matrix calculation
  integer, allocatable :: nBeta(:)          ! nBeta(numberOfBetaChords) number of the element in beta array where chord contributes
  integer              :: nBetaElem         ! Total number of beta elements

  integer              :: nChordGam         ! number of chords for integration of surface-to-region CP (gamma)
  integer, allocatable :: nSegChordGam(:)   ! nSegChordGam(numberOfGammaChords) number of segments along the chord for integration gamma
  real(rp), allocatable :: lenChordGam(:,:)  ! length of segments along the chord
  real(rp), allocatable :: wghtChordGam(:)   ! weight of the chord
  integer, allocatable :: zoneChordGam(:,:) ! zones' numbers along the chord (for definition of the materials)
  real(rp), allocatable :: xChordGam(:,:)    ! x-coordinates of the segments along the chord
  real(rp), allocatable :: yChordGam(:,:)    ! y-coordinates of the segments along the chord
  integer, allocatable :: inSideGam(:)      ! number of input side for gamma element
  integer, allocatable :: inSegGam(:)       ! number of input segment for gamma element
  integer, allocatable :: inSectGam(:)      ! number of input sector for gamma element
  integer, allocatable :: inZoneGam(:)      ! number of region where collision takes place for gamma element
  integer, allocatable :: inHarmGam(:)      ! number of harmonic for gamma element
  integer, allocatable :: nGamma(:)         ! nGamma(numberOfGammaSegments) number of the element in gamma array where chord's segment contributes
  integer              :: nGammaElem        ! Total number of gamma elements

  integer              :: nChordU           ! number of chords for integration of region-to-region CP (U)
  integer, allocatable :: nSegChordU(:)     ! nSegChordU(numberOfUChords) number of segments along the chord for integration U
  real(rp), allocatable :: lenChordU(:,:)    ! lengths of segments along the chord
  real(rp), allocatable :: wghtChordU(:)     ! weight of the chord
  integer, allocatable :: zoneChordU(:,:)   ! regions numbers along the chord (for definition of the materials)
  real(rp), allocatable :: xChordU(:,:)      ! x-coordinates of the segments along the chord
  real(rp), allocatable :: yChordU(:,:)      ! y-coordinates of the segments along the chord
  integer, allocatable :: zoneFromU(:)      ! number of input side for gamma element
  integer, allocatable :: harmFromU(:)      ! number of input segment for gamma element
  integer, allocatable :: zoneToU(:)        ! number of input sector for gamma element
  integer, allocatable :: harmToU(:)        ! number of region where collision takes place for gamma element
  integer, allocatable :: nU(:)             ! nU(numberOfUSegments) number of the element in U array where chord's segment contributes
  integer              :: nUElem            ! Total number of U elements

  real(rp), allocatable :: Beta(:)           ! Surface to surface collision probability Beta
  real(rp), allocatable :: Gamm(:)           ! Surface to region collision probability Gamma
  real(rp), allocatable :: G(:,:,:,:,:,:)      ! Surface-to-region collision probability (Gamma)
  real(rp), allocatable :: V(:,:,:,:,:,:)      ! Region-to-surface collision probability
  real(rp), allocatable :: U(:,:,:,:)        ! Region to region collision probability U

  real(rp), allocatable :: flux(:)           ! Fluxes in the regions of the cell
  real(rp), allocatable :: flux_omc(:)       ! Fluxes for the comparison with OpenMC code
  real(rp), allocatable :: flux_merged(:)    ! Merged fluxes

  type(fhash_type__ints_double) :: h_beta        ! Beta hash
  type(ints_type)               :: key_beta      ! Beta indices
  integer, allocatable          :: nBeta_key(:,:)! Beta keys (inside,insect,inseg,outside,outseg,outsect,polar_sect) for each beta chord

  type(fhash_type__ints_double) :: h_gamma
  type(ints_type)               :: key_gamma
  integer, allocatable          :: nGamma_key(:,:)

contains

end module
