module ReadWriteMod
  !---------------------------------------------
  ! Author : DL
  ! Date   : 06.09.2016
  ! Module contains subroutines for reading and writing data
  !---------------------------------------------
  use precision_mod
  use Config, only : str_len
contains

  subroutine ReadInit(filename)
    !---------------------------------------------
    ! Author : DL
    ! Date   : 06.09.2016
    ! Subroutine reads initial data from input file
    !
    ! Input:
    ! filename - name of the input file
    !---------------------------------------------
    use ConstantsMod, only : PI, PI2
    use Config, only : str_len
    use GeometryMod, only : GetVerticesPolygon
    use SplineMod, only : read_bickley_data, allocate_bickley, spline_coefficients_bickley, &
                          n_tau
    use GlobalAssemblyMod
    implicit none

    character(len=str_len) :: filename
    character(len=str_len) :: path_to_bickley
    integer :: i
    real(rp) :: delta_phi, delta_theta, theta0, theta1

    real(rp), allocatable :: Stot_read(:), Ssca_read(:), Sabs_read(:), Qsrc_read(:)

    logical :: ok
    ! Check whether filename exist
    inquire( file = filename, exist=ok )
    if (ok) then
      call ReadAssembly(filename)
      call ReadMaterials()
      call ReadCells()
      if (withGap) then
        call AddGapCells()
      end if
    ! Reading Bickley data
!      path_to_bickley = "data"
      call read_bickley_data(nTheta)
      call allocate_bickley(nTheta, n_tau)
      call spline_coefficients_bickley(nTheta, n_tau)
      return
      ! If yes - open file for reading
!      open(10, file = filename)
    else
      ! If no - generate warning message and abort program
      call WriteMessage('File ' // filename // ' does not exist.')
      call AbortProgram
    end if

!    read(10, *) cellType
!    read(10, *) nSides, nPhi, nTheta, nHarm
!
!! Calculation of sectors boundaries on phi and theta angles
!    allocate(phiSect(nPhi+1))
!    allocate(thetaSect(nTheta+1))
!    phiSect = 0.0_rp
!    thetaSect = 0.0_rp
!    delta_phi = PI/nPhi
!    delta_theta = 0.5_rp*PI/nTheta
!    do i = 1, nPhi + 1
!      phiSect(i) = -0.5_rp*PI + (i-1)*delta_phi
!    end do
!    do i = 1, nTheta + 1
!      thetaSect(i) = (i-1)*delta_theta
!    end do
!
!! Calculations of the weights chi of the phi sectors
!    allocate(chi(nPhi))
!    allocate(eps(nTheta))
!    chi = 0.0_rp
!    eps = 0.0_rp
!    do i = 1, nPhi
!      chi(i) = 0.5_rp*(dsin(phiSect(i+1)) - dsin(phiSect(i)))
!    end do
!    do i = 1, nTheta
!      theta0 = thetaSect(i)
!      theta1 = thetaSect(i+1)
!      eps(i) = PI2*(theta1 - 0.5_rp*sin(2.0_rp*theta1) - theta0 + 0.5_rp*sin(2.0_rp*theta0))
!    end do
!
!    allocate(verX(nSides))
!    allocate(verY(nSides))
!    verX = 0.0_rp
!    verY = 0.0_rp
!
!    if (cellType == "regular") then
!      read(10, *) dSeg, dPitch
!      call GetVerticesPolygon(nSides, dPitch, verX, verY)
!    elseif (cellType == "arbitrary") then
!      read(10, *) dSeg
!      do i = 1, nSides
!        read(10, *) verX(i), verY(i)
!      end do
!    endif
!
!    read(10, *) nCirc
!    nZone = nCirc + 1
!    allocate(zoneVol(nZone))
!    zoneVol = 0.0_rp
!    ! Allocate and initialize array Rad(:)
!    if (nCirc > 0) then
!      allocate(Rad(nCirc))
!      Rad = 0.0_rp
!      allocate(cX(nCirc))
!      allocate(cY(nCirc))
!      cX = 0.0_rp
!      cY = 0.0_rp
!      ! Read radii values from input file
!      read(10, *) (Rad(i), i=1,nCirc)
!      do i = 1, nCirc
!        read(10,*) cX(i), cY(i)
!        if (cx(i) /= cx(1) .or. cy(i) /= cy(1)) then
!          write(*,*) "Only the circles with the centers at the same point are allowed in the current version of the prgram."
!          write(*,*) "Please, check the centers of the circles. They should be identical for all the circles."
!          stop
!        end if
!      end do
!    end if
!
!    ! The origin of the coordinate sysytem should be at the centers of the circles. Therefore, the coordinates
!    ! of the vertices will be modified.
!    if (nCirc > 0) then
!        do i = 1, nSides
!            verX(i) = verX(i) - cx(1)
!            verY(i) = verY(i) - cy(1)
!        end do
!        do i = 1, nCirc
!            cx(i) = 0.0_rp
!            cy(i) = 0.0_rp
!        end do
!    end if
!
!    read(10, *) nMat
!    ! Initialize arrays containing cross sections
!    allocate(Stot(nZone))
!    allocate(Ssca(nZone))
!    allocate(Sabs(nZone))
!    allocate(Qsrc(nZone))
!    Stot = 0.0_rp
!    Ssca = 0.0_rp
!    Sabs = 0.0_rp
!    Qsrc = 0.0_rp
!    allocate(Stot_read(nMat))
!    allocate(Ssca_read(nMat))
!    allocate(Sabs_read(nMat))
!    allocate(Qsrc_read(nMat))
!    Stot_read = 0.0_rp
!    Ssca_read = 0.0_rp
!    Sabs_read = 0.0_rp
!    Qsrc_read = 0.0_rp
!    ! Read the cross sections
!    do i = 1, nMat
!      read(10, *) Stot_read(i), Ssca_read(i), Qsrc_read(i)
!      Sabs_read(i) = Stot_read(i) - Ssca_read(i)
!    end do
!
!    allocate(Kart(nZone))
!    Kart = 0
!    read(10, *) (Kart(i), i = 1, nZone)
!
!    do i = 1, nZone
!      Stot(i) = Stot_read(Kart(i))
!      Ssca(i) = Ssca_read(Kart(i))
!      Sabs(i) = Sabs_read(Kart(i))
!      Qsrc(i) = Qsrc_read(Kart(i))
!    end do
!
!    deallocate(Stot_read)
!    deallocate(Ssca_read)
!    deallocate(Sabs_read)
!    deallocate(Qsrc_read)
!
!    allocate(merge_arr(nZone))
!    merge_arr = 0
!    read(10, *) (merge_arr(i), i = 1, nZone)
!
!    read(10, *) nPhiB, nPhiG, nPhiU
!    read(10, *) dCb, dCg, dCu
!
!    allocate(flux_omc(nZone))
!    read(10, *) (flux_omc(i), i = 1, nZone)
!
!    ! Reading Bickley data
!    path_to_bickley = "data"
!    call read_bickley_data(nTheta, path_to_bickley)
!    call allocate_bickley(nTheta, n_tau)
!    call spline_coefficients_bickley(nTheta, n_tau)
!
!    close(10)

  end subroutine ReadInit

  subroutine ReadAssembly(filename)
    !-------------------------------------
    ! Subroutine reads input assembly file
    !-------------------------------------
    use ConstantsMod, only : PI, PI2
    use Config, only : str_len
    use GlobalAssemblyMod

    implicit none

    character(str_len), intent(in) :: filename

    integer :: ig, i, j, icell, ir
    integer :: n_assmb_sides, n_cell_edge
    real(rp) :: delta_phi, delta_theta, theta0, theta1

    open(101, file=filename)

    read(101, '(a)') assembly_type
    call RemoveComments(assembly_type)
    if (assembly_type == "regular hexagonal with the gap") then
      read(101, *) nPhi, dseg, nTheta, assmb_pitch
      withGap = .true.
    else
      read(101, *) nPhi, dseg, nTheta
      withGap = .false.
    end if
    allocate(phiSect(nPhi+1))
    allocate(thetaSect(nTheta+1))
    phiSect = 0.0_rp
    thetaSect = 0.0_rp
    delta_phi = PI/nPhi
    do i = 1, nPhi + 1
      phiSect(i) = -0.5_rp*PI + (i-1)*delta_phi
    end do
    delta_theta = 0.5_rp*PI/nTheta
    do i = 1, nTheta + 1
      thetaSect(i) = (i-1)*delta_theta
    end do
! Calculations of the weights chi of the phi sectors
    allocate(chi(nPhi))
    allocate(eps(nTheta))
    chi = 0.0_rp
    eps = 0.0_rp
    do i = 1, nPhi
      chi(i) = 0.5_rp*(sin(phiSect(i+1)) - sin(phiSect(i)))
    end do
    do i = 1, nTheta
      theta0 = thetaSect(i)
      theta1 = thetaSect(i+1)
      eps(i) = PI2*(theta1 - 0.5_rp*sin(2.0_rp*theta1) - theta0 + 0.5_rp*sin(2.0_rp*theta0))
    end do
    read(101, *) nGroups
    nrow = 0
    if (assembly_type == "regular square" .or. assembly_type == "regular square with the gap") then
      read(101, *) nRow, nCol
    else
      read(101, *) nRow
    end if
    read(101, *) sym
    read(101, *) nMat
    allocate(path_to_material(nMat))
    do i = 1, nMat
#ifdef __INTEL_COMPILER
      read(101, '(1i,1a)') j, path_to_material(j)
#else
      read(101, '(i3,1a)') j, path_to_material(j)
#endif
      call RemoveComments(path_to_material(j))
    end do
    allocate(STOT(nMat,nGroups))
    STOT = 0.0_rp
    allocate(SSCA(nMat,nGroups,nGroups))
    SSCA = 0.0_rp
    allocate(SCHI(nMat,nGroups))
    SCHI = 0.0_rp
    allocate(SFIS(nMat,nGroups))
    SFIS = 0.0_rp
    allocate(SFNF(nMat,nGroups))
    SFNF = 0.0_rp
    allocate(SABS(nMat,nGroups))
    SABS = 0.0_rp
    allocate(SOUR(nMat,nGroups))
    SOUR = 0.0_rp
    read(101, *) nCellTypes
    if (assembly_type == "regular hexagonal with the gap") then
      if (sym == 360 .or. sym == -360) then
        nCellTypes = nCellTypes + 2
      else
        write(*,*) "This symmetry is not supported for the simulations with the gap at the moment"
        stop
      end if
    end if
    allocate(cell_type(nCellTypes))
    allocate(nsides(nCellTypes))
    nsides = 0
    allocate(ncirc(nCellTypes))
    ncirc = 0
    allocate(nharms(nCellTypes))
    nharms = 0
    allocate(nzones(nCellTypes))
    nzones = 0
    allocate(cell_dcb(nCellTypes))
    allocate(cell_dcg(nCellTypes))
    allocate(cell_dcu(nCellTypes))
    cell_dcb = 0.0_rp
    cell_dcg = 0.0_rp
    cell_dcu = 0.0_rp
    allocate(cell_nphib(nCellTypes))
    allocate(cell_nphig(nCellTypes))
    allocate(cell_nphiu(nCellTypes))
    cell_nphib = 0
    cell_nphig = 0
    cell_nphiu = 0
    allocate(path_to_cell(nCellTypes))
    if (withGap) nCellTypes = nCellTypes - 2
    do i = 1, nCellTypes
#ifdef __INTEL_COMPILER
      read(101, '(1i,1a)') j, path_to_cell(j)
#else
      read(101, '(i3,1a)') j, path_to_cell(j)
#endif
      call RemoveComments(path_to_cell(j))
    end do
    if (withGap) nCellTypes = nCellTypes + 2

    nCell = get_cell_tot_number(assembly_type, nRow, nCol, sym)

    if (nCell < 0) then
      write(*,*) "Current geometry or symmetry is not supported"
      stop
    end if

    allocate(kart(nCell))
    kart = 0

    if (assembly_type == "regular hexagonal" .or. assembly_type == "regular hexagonal with the gap") then
      read(101, *) (kart(i), i=1,nCell)
    elseif (assembly_type == "regular square") then
      if (sym == 360) then
        do ir = 1, nRow
          read(101, *) (kart(icell), icell = (ir-1)*nRow + 1, ir*nCol)
        end do
      elseif (sym == 45 .or. sym == 451) then
        i = 1
        do ir = 1, nRow
          read(101, *) (kart(icell), icell = i, i + ir - 1)
          i = i + ir
        end do
      end if
    end if
    if (assembly_type == "regular square") then
      n_assmb_sides = 4
      n_cell_edge = nCol
    elseif (assembly_type == "regular hexagonal" .or. assembly_type == "regular hexagonal with the gap") then
      n_assmb_sides = 6
      n_cell_edge = nRow
    end if
    allocate(albedo(nGroups,n_assmb_sides,n_cell_edge))
    allocate(num_cell_edge(n_assmb_sides,nCell))
    albedo = 0.0_rp
    do ig = 1, nGroups
      do i = 1, n_assmb_sides
        read(101, *) (albedo(ig,i,j), j = 1, n_cell_edge)
      end do
    end do
! Debug
!    albedo(1,1,1:17) = (/1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09, 1.10, &
!                         1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17/)
!    albedo(1,2,1:17) = (/2.01, 2.02, 2.03, 2.04, 2.05, 2.06, 2.07, 2.08, 2.09, 2.10, &
!                         2.11, 2.12, 2.13, 2.14, 2.15, 2.16, 2.17/)
!    albedo(1,3,1:17) = (/3.01, 3.02, 3.03, 3.04, 3.05, 3.06, 3.07, 3.08, 3.09, 3.10, &
!                         3.11, 3.12, 3.13, 3.14, 3.15, 3.16, 3.17/)
!    albedo(1,4,1:17) = (/4.01, 4.02, 4.03, 4.04, 4.05, 4.06, 4.07, 4.08, 4.09, 4.10, &
!                         4.11, 4.12, 4.13, 4.14, 4.15, 4.16, 4.17/)
    if (assembly_type == "regular hexagonal with the gap") then
      read(101, *) gap_mat_number
    end if
    read(101, *) irun, idraw, icompare, ivtk, ixs, iout
    if (icompare > 0) then
      read(101, '(1a)') path_to_reference
      if (icompare == 1) then
        open(102, file = path_to_reference)
        allocate(flux_fuel_reference(nCell))
        allocate(flux_aver_reference(nCell))
        flux_fuel_reference = 0.0_rp
        flux_aver_reference = 0.0_rp
        do icell = 1, nCell
          read(102, *) i, flux_fuel_reference(i), flux_aver_reference(i)
        end do
        close(102)
      end if
    end if

    if (ixs == 2 .or. ixs == 3 .or. ixs == 4) then
      read(101, *) ng_collaps
      allocate(group_bounds_collapsed(ng_collaps+1))
      read(101, *) (group_bounds_collapsed(i),i=1,ng_collaps+1)
      if (ixs == 4) then
        allocate(merge_cells(nCell))
        merge_cells = 0
        if (assembly_type == "regular square") then
          if (sym == 360) then
            do ir = 1, nRow
              read(101, *) (merge_cells(icell), icell = (ir-1)*nRow + 1, ir*nRow)
            end do
          elseif (sym == 45 .or. sym == 451) then
            i = 1
            do ir = 1, nRow
              read(101, *) (merge_cells(icell), icell = i, i + ir - 1)
              i = i + ir
            end do
          end if
        end if
      end if
    end if
!    if (irun == 2 .and. assembly_type == 'regular square') then ! Reading of the materila map for the correction of the corner albedos
!      read(101, *) nRow_c, nCol_c
!      allocate(kart_corner(nRow_c*nCol_c))
!      do ir = 1, nRow
!        read(101, *) (kart_corner(icell), icell = (ir-1)*nRow_c + 1, ir*nCol_c)
!      end do
!    elseif (irun == 2 .and. assembly_type /= 'regular square') then
!      write(*,*) "Corner correction is not supported for the " // trim(assembly_type)
!      write(*,*) "Program was stopeed"
!      stop
!    end if

! Reading 2D array for albedo printout (for corner corrections).
! Zero numbers in the array corresponds to the corner of the assembly
! where albedo corrections should be performed. Ones are for the
! corners of the 3 other surrounding assmblies
    if (iout == 4) then
      allocate(kart_corner(nRow*nCol))
      do ir = 1, nRow
        read(101, *) (kart_corner(icell), icell = (ir-1)*nRow + 1, ir*nCol)
      end do
    end if
    read(101, *) eps_f, eps_k, iter_max, kiter_max

    write(*, '(2a)')       "Assembly type                 : ", trim(assembly_type)
    write(*, '(1a,i5)')    "Number of azimuthal sectors   :", nPhi
    write(*, '(1a,1f8.3)') "Maximal length of segment     :", dseg
    write(*, '(1a,i5)')    "Number of polar sectors       :", nTheta
    write(*, '(1a,i5)')    "Number of energy groups       :", nGroups
    write(*, '(1a,i5)')    "Number of rows                :", nRow
    if (assembly_type == "regular square") then
      write(*, '(1a,i5)')    "Number of columns             :", nCol
    end if
    write(*, '(1a,i5)')    "Symmetry type                 :", sym
    write(*, '(1a,i5)')    "Number of differrent materials:", nMat
    do i = 1, nMat
      write(*,'(1i4,2a)') i, " -> ", trim(path_to_material(i))
    end do
    write(*, '(1a,i5)')    "Number of different cell types:", nCellTypes
    do i = 1, nCellTypes
      if (withGap) then
        if (i == nCellTypes - 1) then
          write(*,'(1i4,2a)') i, " -> added gap corner cell"
          cycle
        end if
        if (i == nCellTypes) then
          write(*,'(1i4,2a)') i, " -> added gap side cell"
          cycle
        end if
      end if
      write(*,'(1i4,2a)') i, " -> ", trim(path_to_cell(i))
    end do
    write(*, '(1a,i5)')    "Total number of the cells     :", nCell
    write(*, '(1a)')       "Distribution of the fuel cells in the assembly: "
#ifdef __INTEL_COMPILER
    write(*, '(<nCell>i3)') kart
#else
    write(*,*) kart
#endif
    write(*, '(1a,i5)')    "Drawing cells                 :", idraw
    write(*, '(1a,i5)')    "Comapring with OpenMC         :", icompare

    close(101)

    return

  end subroutine ReadAssembly

  subroutine ReadMaterials()
    use Config, only : str_len
    use GlobalAssemblyMod, only : nGroups, STOT, SSCA, SABS, SOUR, &
                                  SFIS, SFNF, SCHI, nMat, path_to_material

    integer :: ifile, nunit, ig, jg, imat

    do ifile = 1, nmat
      nunit = 1000 + ifile
      imat = ifile
      open(nunit, file=path_to_material(ifile))
      do ig = 1, nGroups
        read(nunit, *) STOT(imat,ig)
      end do
      do ig = 1, nGroups
        read(nunit, *) (SSCA(imat,ig,jg), jg = 1, nGroups)
      end do
      do ig = 1, nGroups
        read(nunit, *) SCHI(imat,ig)
      end do
      do ig = 1, nGroups
        read(nunit, *) SFIS(imat,ig)
      end do
      do ig = 1, nGroups
        read(nunit, *) SFNF(imat,ig)
      end do
      do ig = 1, nGroups
        read(nunit, *) SOUR(imat,ig)
      end do
      do ig = 1, nGroups
        SABS(imat,ig) = STOT(imat,ig) - sum(SSCA(imat,ig,1:nGroups))
      end do
      close(nunit)
    end do

  end subroutine

  subroutine ReadCells()
    use GlobalAssemblyMod
    use GeometryMod, only : GetVerticesPolygon
    use ArrayMod, only : SetArraySize2DReal, SetArraySize2DInt

    implicit none

    integer :: nunit, nsides_max, ncircs_max
    integer :: icell, j, n_cell_files
    real(rp) :: pitch

    nsides_max = -1.0
    ncircs_max = -1.0
    if (withGap) then
      n_cell_files = nCellTypes - 2
    else
      n_cell_files = nCellTypes
    end if
    do icell = 1, n_cell_files
      nunit = 1000 + icell
      open(nunit, file=path_to_cell(icell))
      read(nunit, '(a)') cell_type(icell)
      call RemoveComments(cell_type(icell))
      read(nunit, *) nsides(icell)
      read(nunit, *) nharms(icell)
      if (icell == 1) then
        nsides_max = nsides(icell)
        allocate(cell_vertx(nCellTypes,nsides(icell)))
        allocate(cell_verty(nCellTypes,nsides(icell)))
        cell_vertx = 0.0_rp
        cell_verty = 0.0_rp
      end if
      if (nsides(icell) > nsides_max .and. icell > 1) then
        nsides_max = nsides(icell)
        call SetArraySize2DReal(cell_vertx, nCellTypes, nsides_max)
        call SetArraySize2DReal(cell_verty, nCellTypes, nsides_max)
      end if
      if (cell_type(icell) == "regular") then
        read(nunit, *) pitch
        if (icell == 1) then
          dpitch = pitch
        else
          if (pitch .ne. dpitch) then
            write(*,*) "All pitches of the cells should be equal in the current version of the code."
            write(*,*) "Please, check the cell number ", icell
            stop
          end if
        end if
        call GetVerticesPolygon(nsides(icell), pitch, cell_vertx(icell,:), cell_verty(icell,:))
      else
        write(*,*) "This type of the cell is not supported at the moment. Please, choose another type of the cell"
        stop
      end if
      read(nunit, *) ncirc(icell)
      nzones(icell) = ncirc(icell) + 1
      if (icell == 1) then
        ncircs_max = ncirc(icell)
        if (ncirc(icell) > 0) then
          allocate(cell_rad(nCellTypes,ncircs_max))
        else
          allocate(cell_rad(nCellTypes,1))
          nzones(icell) = 1
        end if
        cell_rad = 0.0_rp
        allocate(cell_mat(nCellTypes,ncircs_max+1))
        cell_mat = 0
      end if
      if (ncirc(icell) > ncircs_max .and. icell > 1) then
        ncircs_max = ncirc(icell)
        call SetArraySize2DReal(cell_rad, nCellTypes, ncircs_max)
        call SetArraySize2DInt(cell_mat, nCellTypes, ncircs_max+1)
      end if
      if (ncirc(icell) > 0) then
        read(nunit, *) (cell_rad(icell,j), j = 1, ncirc(icell))
      end if
      read(nunit, *) (cell_mat(icell,j), j = 1, ncirc(icell) + 1)
      read(nunit, *) cell_nphib(icell), cell_nphig(icell), cell_nphiu(icell)
      read(nunit, *) cell_dcb(icell), cell_dcg(icell), cell_dcu(icell)
      close(nunit)
    end do
    return
  end subroutine

  subroutine AddGapCells()
    use GlobalAssemblyMod
    implicit none

    integer :: icell

    do icell = nCellTypes - 1, nCellTypes
      if (assembly_type == "regular hexagonal with the gap") then
        if (icell == nCellTypes - 1) cell_type(icell) = "gap corner hex"
        if (icell == nCellTypes)     cell_type(icell) = "gap side hex"
        nsides(icell) = 5
        nharms(icell) = maxval(nharms(1:nCellTypes-2))
        if (icell == nCellTypes - 1 .and. nharms(icell) > 3) nharms(icell) = 3
        if (icell == nCellTypes - 1) call GetVerticesCornerGapHex(nRow, assmb_pitch, dpitch, nsides(icell), &
                                          cell_vertx(nCellTypes - 1,:), cell_verty(nCellTypes - 1,:))
        if (icell == nCellTypes)     call GetVerticesSideHex(nRow, assmb_pitch, dpitch, nsides(icell), &
                                          cell_vertx(nCellTypes,:), cell_verty(nCellTypes,:))
        ncirc(icell) = 0
        nzones(icell) = 1
        cell_rad(icell,:) = 0.0_rp
        cell_mat(icell,1) = gap_mat_number
        cell_nphib(icell) = maxval(cell_nphib(1:nCellTypes-2))
        cell_nphig(icell) = maxval(cell_nphig(1:nCellTypes-2))
        cell_nphiu(icell) = maxval(cell_nphiu(1:nCellTypes-2))
        cell_dcb(icell) = minval(cell_dcb(1:nCellTypes-2))
        cell_dcg(icell) = minval(cell_dcg(1:nCellTypes-2))
        cell_dcu(icell) = minval(cell_dcu(1:nCellTypes-2))
      else
        write(*,*) "This assembly type is not supported at the moment."
        stop
      end if
    end do

    return

  end subroutine

  subroutine GetVerticesCornerGapHex(nrow, assmb_pitch, dpitch, nvert, verx, very)
    implicit none

    real(rp) :: r, h, d

    integer, intent(in) :: nrow
    real(rp), intent(in) :: assmb_pitch
    real(rp), intent(in) :: dpitch
    integer, intent(in) :: nvert
    real(rp), intent(inout) :: verx(:)
    real(rp), intent(inout) :: very(:)

    if (nvert > 5 .or. nvert < 5) then
      write(*,*) "Number of vertices for this cell type should be exactly 5."
      stop
    end if

    r = dpitch/sqrt(3.0_rp)
    h = dpitch
    ! d = 0.5_rp * (assmb_pitch - (nrow + 2.0_rp/3.0_rp) * h * sqrt(3.0_rp))
    d = 0.5_rp*(assmb_pitch - ((nrow - 0.5_rp) * h  + (h/6.0_rp))*sqrt(3.0_rp))
    !d = (assmb_pitch - ((nrow - 0.5_rp) * h  + (h/6.0_rp))*sqrt(3.0_rp))

    verx(1) = -0.5_rp * r
    very(1) = 0.0_rp

    verx(2) = 0.5_rp * r
    very(2) = 0.0_rp

    verx(3) = 0.5_rp * (d + r)
    very(3) = 0.5_rp * d * sqrt(3.0_rp)

    verx(4) = 0.0_rp
    very(4) = (assmb_pitch/sqrt(3.0_rp)) - (nrow - 0.5_rp) * h

    verx(5) = -0.5_rp * (d + r)
    very(5) = 0.5_rp * d * sqrt(3.0_rp)

    return

  end subroutine

  subroutine GetVerticesSideHex(nrow, assmb_pitch, dpitch, nvert, verx, very)
    implicit none

    real(rp) :: r, h, d

    integer, intent(in) :: nrow
    real(rp), intent(in) :: assmb_pitch
    real(rp), intent(in) :: dpitch
    integer, intent(in) :: nvert
    real(rp), intent(inout) :: verx(:)
    real(rp), intent(inout) :: very(:)

    if (nvert > 5 .or. nvert < 5) then
      write(*,*) "Number of vertices for this cell type should be exactly 5."
      stop
    end if

    r = dpitch/sqrt(3.0_rp)
    h = dpitch

    d = 0.5_rp*(assmb_pitch - ((nrow - 0.5_rp) * h  + (h/6.0_rp))*sqrt(3.0_rp))

    verx(1) = -0.5_rp * h
    very(1) = 0.0_rp

    verx(2) = 0.5_rp * h
    very(2) = 0.0_rp

    verx(3) = 0.5_rp * h
    very(3) = d

    verx(4) = 0.0_rp
    very(4) = d + 0.5_rp * r

    verx(5) = -0.5_rp * h
    very(5) = d

    return

  end subroutine

  integer function get_cell_tot_number(assembly_type, nRow, nCol, sym)
    use Config, only : str_len
    implicit none

    character(str_len), intent(in) :: assembly_type
    integer, intent(in) :: nRow
    integer, intent(in) :: nCol
    integer, intent(in) :: sym

    integer :: irow, s, ds

    if (assembly_type(1:len("regular hexagonal")) == "regular hexagonal") then
      if (sym == 30) then
        s = 0
        do irow = 1, nRow
          ds = int((irow-1)/2) + 1
          s = s + ds
        end do
        get_cell_tot_number = s
        return
      elseif (sym == 60) then
        s = 0
        do irow = 1, nRow
          s = s + irow
        end do
        get_cell_tot_number = s
        return
      elseif (sym == 180) then
        s = 1
        do irow = 2, nRow
          s = s + (irow - 1) * 3 + 1
        end do
        get_cell_tot_number = s
        return
      elseif (sym == -360 .or. sym == 360) then
        s = 1
        do irow = 2, nRow
          s = s + (irow - 1) * 6
        end do
        get_cell_tot_number = s
        return
      end if
    elseif (assembly_type(1:len("regular square")) == "regular square") then
      if (sym == 360) then
        get_cell_tot_number = nRow * nCol
        return
      end if
      if (sym == 45 .or. sym == 451) then ! 45 degree reflective symmetry
        if (nrow /= ncol) then
          write(*,*) "The number of the rows and columns should be equal for this type of the symmetry."
          stop
        end if
        s = 0
        do irow = 1, nRow
          s = s + irow
        end do
        get_cell_tot_number = s
        return
      end if
    end if

    get_cell_tot_number = -1
    return

  end function

  subroutine RemoveComments(line)
    use Config, only : str_len

    implicit none

    character(str_len), intent(inout) :: line

    character(str_len) :: line_temp
    integer :: i

    line_temp = ""
    do i = 1, len(line)
      if (line(i:i) == "!") then
        exit
      else
        line_temp(i:i) = line(i:i)
      end if
    end do

    line = trim(adjustl(line_temp))

    return

  end subroutine

  subroutine WriteMessage(msg)
    !---------------------------------------------
    ! Author : DL
    ! Date   : 06.09.2016
    ! Subroutine generate message msg in the terminal window
    !
    ! Input:
    ! msg - message which should be generated
    !---------------------------------------------
    implicit none
    character(len = *) :: msg

    write(*,*) msg

  end subroutine WriteMessage

  character(len=8) function intToChar(i)
    !-----------------------------------
    ! Function converts integer to character
    ! Input:
    !   i  - integer number
    ! Output:
    !   intToChar - character
    !-----------------------------------
    implicit none
    integer, intent(in) :: i

    write (intToChar, '(I5.5)') i

    return

  end function

  function FileManager(filename,connected)

    ! FileManager manages the file connections in a program and enables the
    ! reference of the I/O files by name. The function can be used exactly as the
    ! index number of an I/O file in an I/O command. FileManager and the OPEN or the
    ! CLOSE statements can be used together in a program without conflicts.

    ! SVG note:
    ! All I/O of the SVG library is streamed through the present function. To add
    ! extensions such as user interactivity upon file creation or overwriting, only
    ! the present function needs to be modified.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! filename: character, scalar. The I/O file's name.

    ! INPUT (OPTIONAL):
    ! connected: logical, scalar. Defines whether the file with the given filename
    !	has already been connected or not.

    ! OUTPUT (REQUIRED):
    ! FileManager: integer, scalar. The index number of the given filename.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !function FileManager(filename,connected)
    !implicit none
    !character,intent(in):: filename*(*)
    !logical,optional,intent(out):: connected
    !integer:: FileManager
    !end function FileManager
    !end interface

    !write(FileManager('test.txt'),'(F5.3)')1.234
    !rewind(FileManager('test.txt'))
    !read(FileManager('test.txt'),*)a

    ! On some compilers the line:
    !write(FileManager('test.txt'),*)'test.txt',FileManager('test.txt')
    ! may produce a "recursive I/O operation" error.

    ! ------------------------------------------------------------------------------

    implicit none

    ! Argument variables:
    character,intent(in):: filename*(*)
    logical,optional,intent(out):: connected
    integer:: FileManager

    ! Private variables:
    integer,save:: counter=0
    logical:: number_taken
    integer:: err

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (len_trim(filename)==0) then
      write(*,*)"FileManager"
      write(*,*)"ERROR: blank filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    ! Find out if the file is connected:
    inquire(file=filename,opened=number_taken,iostat=err)

    if (err/=0) then
      write(*,*)"FileManager"
      write(*,*)"ERROR: INQUIRE statement failed."
      write(*,*)"filename : ",trim(filename)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (present(connected)) connected=number_taken

    ! If the file is not connected, find an empty slot and open it there. If the
    ! file is already connected, obtain the connection number.
    already_connected: if (number_taken) then

      ! Get the connection number:
      inquire(file=filename,number=FileManager,iostat=err)

      if (err/=0) then
        write(*,*)"FileManager"
        write(*,*)"ERROR: INQUIRE statement failed."
        write(*,*)"filename : ",trim(filename)
        write(*,*)"error flag : ",err
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if

    else already_connected

      search_empty_slot: do

        counter=counter+1
        inquire(unit=counter,opened=number_taken,iostat=err)

        if (err/=0) then
          write(*,*)"FileManager"
          write(*,*)"ERROR: INQUIRE statement failed."
          write(*,*)"connection unit : ",counter
          write(*,*)"error flag : ",err
          write(*,*)"Program terminated."
          read(*,*)
          stop
        end if

        slot_found: if (.not.number_taken) then

          ! If an empty slot is found, the requested file is opened.
          open(counter,file=filename,iostat=err)

          if (err/=0) then
            write(*,*)"FileManager"
            write(*,*)"ERROR: file could not be opened."
            write(*,*)"filename : ",trim(filename)
            write(*,*)"error flag : ",err
            write(*,*)"Program terminated."
            read(*,*)
            stop
          end if

          FileManager=counter
          exit search_empty_slot

        end if slot_found

      end do search_empty_slot

    end if already_connected

  end function FileManager

  function StringReal(numerical_value)

    ! StringReal converts a given number into an alphanumerical string.

    ! SVG note:
    ! Some subsets of the SVG specification (e.g. the SVG Tiny 1.2 specification)
    ! specify certain limitations for the floating point numbers in the SVG file.
    ! In order to avoid truncation errors in the calculations, the accuracy of the
    ! floating points used should be high, but in order to comply to certain
    ! specifications the numbers should be manipulated before being printed in the
    ! SVG file (bound checking and truncating). This manipulation should be done in
    ! this subroutine, since it is used for the output of all floating point numbers
    ! in the SVG library.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! numerical_value: real, scalar. The number that will be converted in a string.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! StringReal: character, scalar. The resulting character string.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !function StringReal(numerical_value)
    !use Config, only: srk,str_len
    !implicit none
    !real(rp),intent(in):: numerical_value
    !character:: StringReal*(str_len)
    !end function StringReal
    !end interface

    !write(*,*)digits(1.0_rp),precision(1.0_rp),range(1.0_rp)
    !write(*,*)'1234567890123456789012345678901234567890'
    !write(*,*)StringReal(9.12345959458498594594859678123_rp)
    !write(*,'(F30.20)')9.12345959458498594594859678123_rp

    ! The length of the output StringInteger could have been empiricaly set equal to
    ! the amount of significant digits in the base of the numbering system used on
    ! the computer for the same type of integer variable as the input argument
    ! (using intrisic function DIGITS). This length is found to be enough for the
    ! representation of a number of the given precision. However, setting the string
    ! length manually is viewed as more portable.

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,str_len

    implicit none

    ! Argument variables:
    real(rp),intent(in):: numerical_value
    character:: StringReal*(str_len)

    ! Private variables:
    integer:: err

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (numerical_value/=numerical_value) then
      write(*,*)"StringReal"
      write(*,*)"ERROR: input is NaN."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    StringReal=''
    write(StringReal,*,iostat=err)numerical_value

    if (err/=0) then
      write(*,*)"StringReal"
      write(*,*)"ERROR: unable to convert number into string."
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    StringReal=adjustl(StringReal)
    if (StringReal(len_trim(StringReal):len_trim(StringReal))=='.') &
      & StringReal=trim(StringReal)//'0'

    ! This subprogram enables the transformation of a numerical value into a string
    ! inside a function rather than in a subroutine, thus saving lines and variables
    ! in the calling code.

  end function StringReal

  function StringInteger(numerical_value)

    ! StringInteger converts a given number into an alphanumerical string.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! numerical_value: integer, scalar. The number that will be converted in a
    !	string.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! StringInteger: character, scalar. The resulting character string.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !function StringInteger(numerical_value)
    !use Config, only: str_len
    !implicit none
    !integer,intent(in):: numerical_value
    !character:: StringInteger*(str_len)
    !end function StringInteger
    !end interface

    !write(*,*)String(127)

    ! The length of the output StringInteger could have been empiricaly set equal to
    ! the amount of significant digits in the base of the numbering system used on
    ! the computer for the same type of integer variable as the input argument
    ! (using intrisic function DIGITS). This length is found to be enough for the
    ! representation of a number of the given precision. However, setting the string
    ! length manually is viewed as more portable.

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: str_len

    implicit none

    ! Argument variables:
    integer,intent(in):: numerical_value
    character:: StringInteger*(str_len)

    ! Private variables:
    integer:: err

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (numerical_value/=numerical_value) then
      write(*,*)"StringInteger"
      write(*,*)"ERROR: input is NaN."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    StringInteger=''
    write(StringInteger,*,iostat=err)numerical_value

    if (err/=0) then
      write(*,*)"StringInteger"
      write(*,*)"ERROR: unable to convert number into string."
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    StringInteger=adjustl(StringInteger)

    ! This subprogram enables the transformation of a numerical value into a string
    ! inside a function rather than in a subroutine, thus saving lines and variables
    ! in the calling code.

  end function StringInteger

  function FormatReal(numerical_value,decimal_places,scientific)

    ! FormatReal converts a given numerical value into an alphanumerical string
    ! using an explicitly defined format.

    ! SVG note:
    ! This function is NOT intended to be used for the printing of SVG object
    ! variables in the SVG file (use function StringReal for that purpose). The
    ! present function is intended for the pretty-printing of numerical values as
    ! SVG Text objects.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! numerical_value: real, scalar. The number that will be converted into a
    !	string using the specified format. The user is responsible for
    !	specifying the most appropriate format for the given number.
    ! decimal_places: integer, scalar. The amount of decimal places that the
    !	formatted form of the given number is required to have.
    ! scientific: logical, scalar. Explicitly specifies whether the scientific
    !	notation will be used for formating the given number or not.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! FormatReal: character, scalar. The formatted numerical value as a character
    !	variable.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !function FormatReal(numerical_value,decimal_places,scientific)
    !use Config, only: srk,str_len
    !implicit none
    !real(rp),intent(in):: numerical_value
    !integer,intent(in):: decimal_places
    !logical,intent(in):: scientific
    !character:: FormatReal*(str_len)
    !end function FormatReal
    !end interface

    !real:: numerical_value

    !numerical_value=123456.145
    !write(*,*)numerical_value
    !write(*,*)trim(FormatReal(numerical_value,2,.false.))
    !write(*,*)trim(FormatReal(numerical_value,2,.true.))

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,str_len

    implicit none

    ! Argument variables:
    real(rp),intent(in):: numerical_value
    integer,intent(in):: decimal_places
    logical,intent(in):: scientific
    character:: FormatReal*(str_len)

    ! Private variables:
    character:: format_string*(str_len)
    integer:: err

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (numerical_value/=numerical_value) then
      write(*,*)"FormatReal"
      write(*,*)"ERROR: input is NaN."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (str_len<decimal_places+7) then
      write(*,*)"FormatReal"
      write(*,*)"ERROR: the specified decimal places amount is incompatible to the"
      write(*,*)"value of global variable 'str_len'"
      write(*,*)"str_len: ",str_len
      write(*,*)"decimal_places: ",decimal_places
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    if (scientific) then
      format_string='(ES'//trim(StringInteger(str_len))// &
        & '.'//trim(StringInteger(decimal_places))//')'
    else
      format_string='(F'//trim(StringInteger(str_len))// &
        & '.'//trim(StringInteger(decimal_places))//')'
    end if

    FormatReal=''
    write(FormatReal,format_string,iostat=err)numerical_value

    if (err/=0) then
      write(*,*)"FormatReal"
      write(*,*)"ERROR: unable to convert number into string."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! Delete the trailing decimal point if it is requested that the given number is
    ! printed as an integer:
    if ((decimal_places==0).and.(.not.scientific)) FormatReal &
      (len_trim(FormatReal):len_trim(FormatReal))=''

    FormatReal=adjustl(FormatReal)

  end function FormatReal

  subroutine WritePointsToCSV(output_file, npoints, xcoord, ycoord, zcoord, values)
    use Config, only : str_len
    implicit none

    character(str_len), intent(in) :: output_file
    integer, intent(in) :: npoints
    real(rp), intent(in) :: xcoord(:)
    real(rp), intent(in) :: ycoord(:)
    real(rp), intent(in) :: zcoord(:)
    real(rp), intent(in) :: values(:)

    integer :: i

    open(1657, file = output_file)

    write(1657,'(1a)') "xcoord, ycoord, zcoord, value"

    do i = 1, npoints
      write(1657,'(f12.8,a1,f12.8,a1,f12.8,a1,f12.8,a1)') xcoord(i), ",", ycoord(i), ",", zcoord(i), ",", values(i)
    end do

    close(1657)

  end subroutine

  subroutine Homogenise_XS(ixs, ngroups, ncell, flux)
    use GlobalAssemblyMod, only : kart, cell_vol, cell_mat, nzones,   &
                                  SFIS, SABS, STOT, SFIS, SFNF, SSCA, &
                                  SCHI, nrow, ncol, nMat, ng_collaps, &
                                  xs_tot, xs_abs, xs_chi, xs_sfnf,    &
                                  xs_sf, xs_scat, merge_cells
    implicit none
    integer, intent(in) :: ixs
    integer, intent(in) :: ngroups
    integer, intent(in) :: ncell
    real(rp), intent(in) :: flux(:,:,:,:)

    integer :: ig, ic, ir, it, imt, ig1
    real(rp) :: fv(ngroups), rabs(ngroups), rtot(ngroups),   &
                rsf(ngroups), rsfnf(ngroups), rchi(ngroups), &
                fv_fiss(ngroups)
    real(rp) :: rsf_pin(ngroups, ncell), rxn_pin(ncell)
    real(rp) :: rscat(ngroups,ngroups)
    real(rp) :: avrsf
    integer  :: icol, icol1, icol2, irow
    integer  :: nunit, n_hom_reg, ireg
    character(str_len) :: frmt
    character(str_len) :: filename

    open(2410, file = 'results/cross_sections.out')


    fv = 0.0_rp
    fv_fiss = 0.0_rp
    rsf = 0.0_rp
    rtot = 0.0_rp
    rabs = 0.0_rp
    rscat = 0.0_rp
    rsfnf = 0.0_rp
    rchi = 0.0_rp
    rsf_pin = 0.0_rp
    rxn_pin = 0.0_rp
    do ig = 1, ngroups
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
          imt = cell_mat(it,ir)
          fv(ig) = fv(ig) + flux(ig,ic,ir,1)*cell_vol(it,ir)
          rsf(ig) = rsf(ig) + flux(ig,ic,ir,1)*SFIS(imt,ig)*cell_vol(it,ir)
          rsf_pin(ig, ic) = rsf_pin(ig, ic) + flux(ig,ic,ir,1)*SFIS(imt,ig)*cell_vol(it,ir)
          rabs(ig) = rabs(ig) + flux(ig,ic,ir,1)*SABS(imt,ig)*cell_vol(it,ir)
          rtot(ig) = rtot(ig) + flux(ig,ic,ir,1)*STOT(imt,ig)*cell_vol(it,ir)
          rsfnf(ig) = rsfnf(ig) + flux(ig,ic,ir,1)*SFNF(imt,ig)*cell_vol(it,ir)
          if (SCHI(imt,ig) > 0.0_rp) then
            rchi(ig) = rchi(ig) + flux(ig,ic,ir,1)*SCHI(imt,ig)*cell_vol(it,ir)
            fv_fiss(ig) = fv_fiss(ig) + flux(ig,ic,ir,1)*cell_vol(it,ir)
          end if
          do ig1 = 1, ngroups
            rscat(ig,ig1) = rscat(ig,ig1) + flux(ig,ic,ir,1)*SSCA(imt,ig,ig1)*cell_vol(it,ir)
          end do
        end do
      end do
    end do

    do ig = 1, ngroups
      do ig1 = 1, ngroups
        rscat(ig,ig1) = rscat(ig,ig1)/fv(ig)
      end do
      rtot(ig) = rtot(ig)/fv(ig)
!      rabs(ig) = rabs(ig)/fv(ig)
      rabs(ig) = rtot(ig) - sum(rscat(ig,1:ngroups))
      rsfnf(ig) = rsfnf(ig)/fv(ig)
      if (fv_fiss(ig) > 0.0_rp) then
        rchi(ig) = rchi(ig)/fv_fiss(ig)
      else
        rchi(ig) = 0.0_rp
      end if
    end do

    if (ixs == 2) then
      ! Writing out homogenised cross sections in DYN3D IWQS=22 format
      call CollapseFullGeometry(ngroups, ng_collaps, ncell, flux)

      avrsf = sum(rsf_pin(1:ngroups,1:ncell))/ncell
      do ic = 1, ncell
        rxn_pin(ic) = sum(rsf_pin(1:ngroups,ic))/avrsf
      end do

      filename = 'xs_dyn3d.out'

      call WriteDYN3DLib(filename, ng_collaps, xs_tot(:,1), xs_abs(:,1), xs_scat(:,:,1), &
                         xs_sf(:,1), xs_sfnf(:,1), xs_chi(:,1), rxn_pin(:))
    elseif (ixs == 3) then
      ! writing out homogenised XSs for each material
      call CollapseByMaterial(ngroups, ng_collaps, ncell, flux)
      do imt = 1, nMat
        nunit = 2007
        filename = 'results/material' // intToChar(imt) // '.out'
        open(nunit, file=filename)
        do ig = 1, ng_collaps
          write(nunit, '(E15.5)') xs_tot(ig,imt)
        end do
        frmt = variable_format(ng_collaps, 'E15.5')
        do ig = 1, ng_collaps
          write(nunit, frmt) (xs_scat(ig,ig1,imt),ig1=1,ng_collaps)
        end do
        do ig = 1, ng_collaps
          write(nunit, '(E15.5)') xs_chi(ig,imt)
        end do
        do ig = 1, ng_collaps
          write(nunit, '(E15.5)') xs_sf(ig,imt)
        end do
        do ig = 1, ng_collaps
          write(nunit, '(E15.5)') xs_sfnf(ig,imt)
        end do
        do ig = 1, ng_collaps
          write(nunit, '(E15.5)') 0.0
        end do
        close(nunit)
      end do
    elseif (ixs == 4) then
      n_hom_reg = maxval(merge_cells)
      call CollapseByCells(ngroups, ng_collaps, ncell, merge_cells, flux)
      do ireg = 1, n_hom_reg
        filename = 'xs_dyn3d' // intToChar(ireg) // '.out'
        call WriteDYN3DLib(filename, ng_collaps, xs_tot(:,ireg), xs_abs(:,ireg), xs_scat(:,:,ireg), &
                           xs_sf(:,ireg), xs_sfnf(:,ireg), xs_chi(:,ireg))
      end do
    else
      do ig = 1, ngroups
        write(2410, '(a,i3)') 'GROUP ', ig
        write(2410, '(a,f12.8)') 'TOTAL      : ', rtot(ig)/fv(ig)
        write(2410, '(a,f12.8)') 'ABSORPTION : ', rabs(ig)/fv(ig)
        write(2410, '(a,f12.8)') 'PRODUCTION : ', rsfnf(ig)/fv(ig)
        write(2410, '(a,f12.8)') 'FISSION    : ', rsf(ig)/fv(ig)
      end do
      write(2410, '(a)') 'SCATTERING:'
      do ig = 1, ngroups
        do ig1 = 1, ngroups
          write(2410, '(f12.8)') rscat(ig,ig1)/fv(ig)
        end do
      end do
    end if
    if (ngroups == 2) then
      write(*,*) 'Kinf = ', (rsfnf(1) + (rscat(1,2)/rabs(2))*rsfnf(2))/(rabs(1) + rscat(1,2))
    elseif (ngroups == 1) then
      write(*,*) 'Kinf = ', rsfnf(1)/rabs(1)
    end if
    close(2410)

  end subroutine Homogenise_XS

  subroutine PrintOutput(iout, nGroups, nCell, flux)
    use GlobalAssemblyMod, only : kart, cell_vol, cell_mat, nzones,   &
                                  SFIS, SABS, STOT, SFIS, SFNF, SSCA, &
                                  SCHI, nCol, nRow, cell_curr, neighCell, &
                                  neighSide, cell_nsegments, nTheta, nPhi, &
                                  nsides, kart_corner
    implicit none
    integer, intent(in) :: iout
    integer, intent(in) :: ngroups
    integer, intent(in) :: ncell
    real(rp), intent(in) :: flux(:,:,:,:)

    integer :: i, ig, ic, ir, it, imt, ii
    integer :: irow, icol, icol1, icol2
    integer :: iside(2), nsd
    integer :: isd, iphi, itheta, iseg, nc, ns
    real(rp) :: jin, jout
    real(rp) :: jin_tot(ngroups,4), jout_tot(ngroups,4)
    integer :: nunit
    real(rp) :: rsf(ngroups, ncell)
    real(rp) :: avrsf
    real(rp), allocatable :: albedo_row(:, :)
    character(str_len) :: frmt

    nunit = 1355
    open(nunit, file = 'results/output.out')

    rsf = 0.0_rp

    if (iout == 1) then
      do ig = 1, ngroups
        do ic = 1, ncell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it, ir)
            if (SFIS(imt,ig) > 0.0_rp) then
              rsf(ig, ic) = rsf(ig,ic) + flux(ig,ic,ir,1)*SFIS(imt,ig)*cell_vol(it,ir)
            end if
          end do
        end do
      end do
      ! Printing fission rates (normalised and not normalised)
      frmt = variable_format(nCol, 'E15.5')
      write(nunit,'(a)') 'Absolute fission rates Flux*vol*SIGF (sum for all energy groups):'
      icol1 = 1
      icol2 = nCol
      do irow = 1, nRow
        write(nunit, frmt) (sum(rsf(1:ngroups,icol)),icol=icol1,icol2)
        icol1 = icol2 + 1
        icol2 = icol2 + ncol
      end do
      ! Averaged fission rates
      avrsf = sum(rsf(1:ngroups,1:ncell))/ncell
      write(nunit,'(a)') 'Normalised to average fission rates:'
      icol1 = 1
      icol2 = nCol
      do irow = 1, nRow
        write(nunit, frmt) (sum(rsf(1:ngroups,icol)/avrsf),icol=icol1,icol2)
        icol1 = icol2 + 1
        icol2 = icol2 + ncol
      end do
    end if

    if (iout == 2) then ! writing albedos
      jin_tot = 0.
      jout_tot = 0.
      do ic = 1, ncell
        if (ic == 100 .or. ic == 115 .or. ic == 130) then
          nsd = 1
          iside = 1
        elseif (ic == 82 .or. ic == 83 .or. ic == 84) then
          nsd = 1
          iside = 2
        elseif (ic == 96 .or. ic == 111 .or. ic == 126) then
          nsd = 1
          iside = 3
        elseif (ic == 142 .or. ic == 143 .or. ic == 144) then
          nsd = 1
          iside = 4
        elseif (ic == 81) then
          nsd = 2
          iside = (/2, 3/)
        elseif (ic == 85) then
          nsd = 2
          iside = (/1, 2/)
        elseif (ic == 145) then
          nsd = 2
          iside = (/1, 4/)
        elseif (ic == 141) then
          nsd = 2
          iside = (/3, 4/)
        else
          cycle
        end if
        do ig = 1, ngroups
          write(nunit, *) "Group ", ig
          do isd = 1, nsd
            write(nunit, *) "Side ", iside(isd)
            nc = neighCell(iside(isd),ic)
            it = kart(nc)
            ns = neighSide(iside(isd),ic)
            jin = 0.
            do iseg = 1, cell_nsegments(it,ns)
              do iphi = 1, nPhi
                do itheta = 1, nTheta
                  jin = jin + cell_curr(ig,nc,ns,iseg,iphi,itheta)
                end do
              end do
            end do
            jout = 0.
            it = kart(ic)
            do iseg = 1, cell_nsegments(it,iside(isd))
              do iphi = 1, nPhi
                do itheta = 1, nTheta
                  jout = jout + cell_curr(ig,ic,iside(isd),iseg,iphi,itheta)
                end do
              end do
            end do
            write(nunit, *) ic, jin/jout
            jin_tot(ig,iside(isd)) = jin_tot(ig,iside(isd)) + jin
            jout_tot(ig,iside(isd)) = jout_tot(ig,iside(isd)) + jout
          end do
        end do
      end do
      write(nunit, *) "Albedo = ", jin_tot/jout_tot
    end if

    if (iout == 3) then
      allocate(albedo_row(ngroups, nRow))
      albedo_row = 0.0_rp
      jin_tot = 0.
      jout_tot = 0.
      do ig = 1, ngroups
        write(nunit, *) "GROUP ", ig
        ii = 1
        do irow = nRow / 2 + 1, nRow
          do icol = 1, nCol / 2
            if (irow == nRow / 2 + 1) then
              ic = (irow - 1) * nCol + icol
            else
              if (icol == nCol / 2) then
                ic = (irow - 1) * nCol + nCol / 2
              else
                cycle
              end if
            end if
            if (irow == nRow / 2 + 1 .and. icol /= nCol / 2) then
              nsd = 1
              iside = (/2, 2/)
            elseif (irow == nRow / 2 + 1 .and. icol == nCol / 2) then ! Corner cell
              nsd = 2
              iside = (/2, 1/)
            else
              nsd = 1
              iside = (/1, 1/)
            end if
            do i = 1, nsd
              isd = iside(i)
              nc = neighCell(isd, ic)
              it = kart(nc)
              ns = neighSide(isd, ic)
              jin = 0.
              do iseg = 1, cell_nsegments(it,ns)
                do iphi = 1, nPhi
                  do itheta = 1, nTheta
                    jin = jin + cell_curr(ig,nc,ns,iseg,iphi,itheta)
                  end do
                end do
              end do
              jout = 0.
              it = kart(ic)
              do iseg = 1, cell_nsegments(it,isd)
                do iphi = 1, nPhi
                  do itheta = 1, nTheta
                    jout = jout + cell_curr(ig,ic,isd,iseg,iphi,itheta)
                  end do
                end do
              end do
              ! write(nunit, '(i4, E14.6)') ic, jin/jout
              albedo_row(ig, ii) = jin/jout
              ii = ii + 1
            end do
          end do
        end do
        frmt = variable_format(nRow/2, 'E14.6')
        write(nunit, frmt) (albedo_row(ig,ii), ii=nRow/2,1,-1)
        write(nunit, frmt) (albedo_row(ig,ii), ii=1,nRow/2)
      end do

    end if

    if (iout == 4) then
      jin_tot = 0.
      jout_tot = 0.
      allocate(albedo_row(ngroups, nRow))
      albedo_row = 0.0_rp
      do ig = 1, ngroups
        write(nunit, *) "GROUP ", ig
        ii = 1
        do ic = 1, ncell
          if (kart_corner(ic) == 0) then
            do isd = 1, nsides(kart(ic))
              nc = neighCell(isd, ic)
              if (kart_corner(nc) > 0) then
                it = kart(nc)
                ns = neighSide(isd, ic)
                jin = 0.
                do iseg = 1, cell_nsegments(it,ns)
                  do iphi = 1, nPhi
                    do itheta = 1, nTheta
                      jin = jin + cell_curr(ig,nc,ns,iseg,iphi,itheta)
                    end do
                  end do
                end do
                jout = 0.
                it = kart(ic)
                do iseg = 1, cell_nsegments(it,isd)
                  do iphi = 1, nPhi
                    do itheta = 1, nTheta
                      jout = jout + cell_curr(ig,ic,isd,iseg,iphi,itheta)
                    end do
                  end do
                end do
                ! write(nunit, '(i4, E14.6)') ic, jin/jout
                albedo_row(ig, ii) = jin/jout
                ii = ii + 1
              end if
            end do
          end if
        end do
        frmt = variable_format(nRow, 'E14.6')
        write(nunit, frmt) (albedo_row(ig,ii), ii=1,nRow)
      end do
    end if
    close(nunit)
  end subroutine

  character(len=str_len) function variable_format(n, frmt)
    implicit none
    integer, intent(in) :: n
    character(len=*), intent(in) :: frmt

    character(str_len) :: fmt_temp

    write(fmt_temp, *) n
    variable_format = '(' // trim(adjustl(fmt_temp)) // frmt // ')'

    return

  end function

  subroutine CollapseByMaterial(ngroups, ng_collaps, ncell, flux)
    use GlobalAssemblyMod, only : kart, cell_vol, cell_mat, nzones,   &
                                  SABS, STOT, SFIS, SFNF, SSCA, SCHI, &
                                  xs_abs, xs_tot, xs_sf, xs_sfnf,     &
                                  xs_scat, xs_chi, nMat,              &
                                  group_bounds_collapsed
    implicit none

    integer, intent(in) :: ngroups
    integer, intent(in) :: ng_collaps
    integer, intent(in) :: ncell
    real(rp), intent(in) :: flux(:,:,:,:)

    integer :: ig, ig1, ic, ir, it, imat, igc, igc1
    real(rp) :: flux_vol_mat_tot(ng_collaps,nMat)
    real(rp) :: vol, sum_chi(nMat)

    call AllocateCollapsedXSS(ng_collaps, nMat)
    flux_vol_mat_tot = 0.0_rp
    do ig = 1, ngroups
      igc = get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
      do ic = 1, ncell
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          flux_vol_mat_tot(igc,imat) = flux_vol_mat_tot(igc,imat) + flux(ig,ic,ir,1)*cell_vol(it,ir)
          xs_sf(igc,imat) = xs_sf(igc,imat) + flux(ig,ic,ir,1)*SFIS(imat,ig)*cell_vol(it,ir)
          xs_abs(igc,imat) = xs_abs(igc,imat) + flux(ig,ic,ir,1)*SABS(imat,ig)*cell_vol(it,ir)
          xs_tot(igc,imat) = xs_tot(igc,imat) + flux(ig,ic,ir,1)*STOT(imat,ig)*cell_vol(it,ir)
          xs_sfnf(igc,imat) = xs_sfnf(igc,imat) + flux(ig,ic,ir,1)*SFNF(imat,ig)*cell_vol(it,ir)
          do ig1 = 1, ngroups
            igc1 = get_collapsed_group(ig1, ng_collaps, group_bounds_collapsed)
            xs_scat(igc,igc1,imat) = xs_scat(igc,igc1,imat) + flux(ig,ic,ir,1)*SSCA(imat,ig,ig1)*cell_vol(it,ir)
          end do
        end do
      end do
    end do

    ! Calculation of homogenised cross sections
    do ig = 1, ng_collaps
      do imat = 1, nMat
        if (flux_vol_mat_tot(ig,imat) > 0.0_rp) then
          xs_sf(ig,imat) = xs_sf(ig,imat)/flux_vol_mat_tot(ig,imat)
          xs_abs(ig,imat) = xs_abs(ig,imat)/flux_vol_mat_tot(ig,imat)
          xs_tot(ig,imat) = xs_tot(ig,imat)/flux_vol_mat_tot(ig,imat)
          xs_sfnf(ig,imat) = xs_sfnf(ig,imat)/flux_vol_mat_tot(ig,imat)
          do ig1 = 1, ng_collaps
            xs_scat(ig,ig1,imat) = xs_scat(ig,ig1,imat)/flux_vol_mat_tot(ig,imat)
          end do
        end if
      end do
    end do

    ! Homogenisation of the chi
    sum_chi = 0.
    do ig = 1, ngroups
      igc = get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
      do ic = 1, ncell
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          do ig1 = 1, ngroups
            if (SFNF(imat,ig) > 0.0_rp) then
              sum_chi(imat) = sum_chi(imat) + SFNF(imat,ig)*flux(ig,ic,ir,1)*cell_vol(it,ir)*SCHI(imat,ig1)
            end if
            xs_chi(igc,imat) = xs_chi(igc,imat) + SFNF(imat,ig1)*flux(ig1,ic,ir,1)*cell_vol(it,ir)*SCHI(imat,ig)
          end do
        end do
      end do
    end do

    do imat = 1, nMat
      if (sum_chi(imat) > 0.0) then
        do igc = 1, ng_collaps
          xs_chi(igc,imat) = xs_chi(igc,imat)/sum_chi(imat)
        end do
      end if
    end do

    return

  end subroutine

  subroutine CollapseFullGeometry(ngroups, ng_collaps, ncell, flux)
    use GlobalAssemblyMod, only : kart, cell_vol, cell_mat, nzones,        &
                                  SABS, STOT, SFIS, SFNF, SSCA, SCHI,      &
                                  xs_tot, xs_abs, xs_sf, xs_sfnf, xs_scat, &
                                  xs_chi, group_bounds_collapsed
    implicit none

    integer, intent(in)  :: ngroups
    integer, intent(in)  :: ng_collaps
    integer, intent(in)  :: ncell
    real(rp), intent(in) :: flux(:,:,:,:)

    integer  :: ig, ig1, ic, ir, it, imat, igc, igc1

    real(rp) :: flux_vol(ng_collaps)
    real(rp) :: sum_chi

    call AllocateCollapsedXSS(ng_collaps, 1)

    xs_tot = 0.0_rp
    xs_abs = 0.0_rp
    xs_sf  = 0.0_rp
    xs_sfnf = 0.0_rp
    xs_scat = 0.0_rp
    xs_chi  = 0.0_rp

    do ig = 1, ngroups
      igc = get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
      do ic = 1, ncell
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          flux_vol(igc) = flux_vol(igc) + flux(ig,ic,ir,1)*cell_vol(it,ir)
          xs_sf(igc,1) = xs_sf(igc,1) + flux(ig,ic,ir,1)*SFIS(imat,ig)*cell_vol(it,ir)
          xs_abs(igc,1) = xs_abs(igc,1) + flux(ig,ic,ir,1)*SABS(imat,ig)*cell_vol(it,ir)
          xs_tot(igc,1) = xs_tot(igc,1) + flux(ig,ic,ir,1)*STOT(imat,ig)*cell_vol(it,ir)
          xs_sfnf(igc,1) = xs_sfnf(igc,1) + flux(ig,ic,ir,1)*SFNF(imat,ig)*cell_vol(it,ir)
          do ig1 = 1, ngroups
            igc1 = get_collapsed_group(ig1, ng_collaps, group_bounds_collapsed)
            xs_scat(igc,igc1,1) = xs_scat(igc,igc1,1) + flux(ig,ic,ir,1)*SSCA(imat,ig,ig1)*cell_vol(it,ir)
          end do
        end do
      end do
    end do

    ! Calculation of homogenised cross sections
    do ig = 1, ng_collaps
      if (flux_vol(ig) > 0.0_rp) then
        xs_sf(ig,1) = xs_sf(ig,1)/flux_vol(ig)
        xs_abs(ig,1) = xs_abs(ig,1)/flux_vol(ig)
        xs_tot(ig,1) = xs_tot(ig,1)/flux_vol(ig)
        xs_sfnf(ig,1) = xs_sfnf(ig,1)/flux_vol(ig)
        do ig1 = 1, ng_collaps
          xs_scat(ig,ig1,1) = xs_scat(ig,ig1,1)/flux_vol(ig)
        end do
      end if
    end do

    ! Homogenisation of the chi
    sum_chi = 0.
    do ig = 1, ngroups
      igc = get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
      do ic = 1, ncell
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          do ig1 = 1, ngroups
            if (SFNF(imat,ig) > 0.0_rp) then
              sum_chi = sum_chi + SFNF(imat,ig)*flux(ig,ic,ir,1)*cell_vol(it,ir)*SCHI(imat,ig1)
            end if
            xs_chi(igc,1) = xs_chi(igc,1) + SFNF(imat,ig1)*flux(ig1,ic,ir,1)*cell_vol(it,ir)*SCHI(imat,ig)
          end do
        end do
      end do
    end do

    if (sum_chi > 0.0_rp) then
      do igc = 1, ng_collaps
        xs_chi(igc,1) = xs_chi(igc,1)/sum_chi
      end do
    end if

    return

  end subroutine

  subroutine CollapseByCells(ngroups, ng_collaps, ncell, cell_numbers, flux)
    use GlobalAssemblyMod, only : kart, cell_vol, cell_mat, nzones,        &
                                  SABS, STOT, SFIS, SFNF, SSCA, SCHI,      &
                                  xs_tot, xs_abs, xs_sf, xs_sfnf, xs_scat, &
                                  xs_chi, group_bounds_collapsed
    implicit none

    integer, intent(in) :: ngroups
    integer, intent(in) :: ng_collaps
    integer, intent(in) :: ncell
    integer, intent(in) :: cell_numbers(:) ! Numbers of the cells for homogenisation. Cells with the same numbers will be treated as one region for homogenisation
    real(rp), intent(in) :: flux(:,:,:,:)

    integer :: ig, ig1, ic, ir, it, imat, igc, igc1
    integer :: n_hom_reg, ichom
    real(rp) :: vol

    real(rp), allocatable :: sum_chi(:)
    real(rp), allocatable :: flux_vol_mat_tot(:,:)

    n_hom_reg = maxval(cell_numbers)

    allocate(sum_chi(n_hom_reg))
    allocate(flux_vol_mat_tot(ng_collaps,n_hom_reg))
    sum_chi = 0.0_rp
    flux_vol_mat_tot = 0.0_rp

    call AllocateCollapsedXSS(ng_collaps, n_hom_reg)

    if (.not. allocated(group_bounds_collapsed)) then
      write(*, *) "groups_bounds_collapsed is not allocated."
      stop
    end if

    do ig = 1, ngroups
      igc = get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
      do ic = 1, ncell
        ichom = cell_numbers(ic)
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          flux_vol_mat_tot(igc,ichom) = flux_vol_mat_tot(igc,ichom) + flux(ig,ic,ir,1)*cell_vol(it,ir)
          xs_sf(igc,ichom) = xs_sf(igc,ichom) + flux(ig,ic,ir,1)*SFIS(imat,ig)*cell_vol(it,ir)
          xs_abs(igc,ichom) = xs_abs(igc,ichom) + flux(ig,ic,ir,1)*SABS(imat,ig)*cell_vol(it,ir)
          xs_tot(igc,ichom) = xs_tot(igc,ichom) + flux(ig,ic,ir,1)*STOT(imat,ig)*cell_vol(it,ir)
          xs_sfnf(igc,ichom) = xs_sfnf(igc,ichom) + flux(ig,ic,ir,1)*SFNF(imat,ig)*cell_vol(it,ir)
          do ig1 = 1, ngroups
            igc1 = get_collapsed_group(ig1, ng_collaps, group_bounds_collapsed)
            xs_scat(igc,igc1,ichom) = xs_scat(igc,igc1,ichom) + flux(ig,ic,ir,1)*SSCA(imat,ig,ig1)*cell_vol(it,ir)
          end do
        end do
      end do
    end do

    ! Calculation of homogenised cross sections
    do ig = 1, ng_collaps
      do ichom = 1, n_hom_reg
        if (flux_vol_mat_tot(ig,ichom) > 0.0_rp) then
          xs_sf(ig,ichom) = xs_sf(ig,ichom)/flux_vol_mat_tot(ig,ichom)
          xs_abs(ig,ichom) = xs_abs(ig,ichom)/flux_vol_mat_tot(ig,ichom)
          xs_tot(ig,ichom) = xs_tot(ig,ichom)/flux_vol_mat_tot(ig,ichom)
          xs_sfnf(ig,ichom) = xs_sfnf(ig,ichom)/flux_vol_mat_tot(ig,ichom)
          do ig1 = 1, ng_collaps
            xs_scat(ig,ig1,ichom) = xs_scat(ig,ig1,ichom)/flux_vol_mat_tot(ig,ichom)
          end do
        end if
      end do
    end do

    ! Homogenisation of the chi
    sum_chi = 0.
    do ig = 1, ngroups
      igc = get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
      do ic = 1, ncell
        ichom = cell_numbers(ic)
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          do ig1 = 1, ngroups
            if (SFNF(imat,ig) > 0.0_rp) then
              sum_chi(ichom) = sum_chi(ichom) + SFNF(imat,ig)*flux(ig,ic,ir,1)*cell_vol(it,ir)*SCHI(imat,ig1)
            end if
            xs_chi(igc,ichom) = xs_chi(igc,ichom) + SFNF(imat,ig1)*flux(ig1,ic,ir,1)*cell_vol(it,ir)*SCHI(imat,ig)
          end do
        end do
      end do
    end do

    do ichom = 1, n_hom_reg
      if (sum_chi(ichom) > 0.0_rp) then
        do igc = 1, ng_collaps
          xs_chi(igc,ichom) = xs_chi(igc,ichom)/sum_chi(ichom)
        end do
      end if
    end do

    return


  end subroutine

  integer function get_collapsed_group(ig, ng_collaps, group_bounds_collapsed)
    implicit none

    integer, intent(in) :: ig
    integer, intent(in) :: ng_collaps
    integer, intent(in) :: group_bounds_collapsed(:)

    integer :: ig1

    do ig1 = 1, ng_collaps
      if (ig <= group_bounds_collapsed(ig1+1)) then
        get_collapsed_group = ig1
        return
      end if
    end do

  end function

  subroutine AllocateCollapsedXSS(ngroups, nsets)
    use GlobalAssemblyMod, only : xs_abs, xs_tot, xs_sf, xs_sfnf, &
                                  xs_scat, xs_chi
    implicit none

    integer, intent(in) :: ngroups
    integer, intent(in) :: nsets

    if (allocated(xs_tot)) deallocate(xs_tot)
    allocate(xs_tot(ngroups,nsets))
    if (allocated(xs_abs)) deallocate(xs_abs)
    allocate(xs_abs(ngroups,nsets))
    if (allocated(xs_sfnf)) deallocate(xs_sfnf)
    allocate(xs_sfnf(ngroups,nsets))
    if (allocated(xs_scat)) deallocate(xs_scat)
    allocate(xs_scat(ngroups,ngroups,nsets))
    if (allocated(xs_chi)) deallocate(xs_chi)
    allocate(xs_chi(ngroups,nsets))
    if (allocated(xs_sf)) deallocate(xs_sf)
    allocate(xs_sf(ngroups,nsets))
    xs_abs = 0.0_rp
    xs_tot = 0.0_rp
    xs_sf  = 0.0_rp
    xs_sfnf = 0.0_rp
    xs_scat = 0.0_rp
    xs_chi = 0.0_rp

    return

  end subroutine

  subroutine WriteDYN3DLib(filename, ngroups, xs_tot, xs_abs, xs_scat, &
                           xs_sf, xs_sfnf, xs_chi, rsf_pin)
    use GlobalAssemblyMod, only : nRow, nCol, nCell
    implicit none

    character(str_len), intent(in) :: filename
    integer,  intent(in) :: ngroups
    real(rp), intent(in) :: xs_tot(:)
    real(rp), intent(in) :: xs_abs(:)
    real(rp), intent(in) :: xs_scat(:,:)
    real(rp), intent(in) :: xs_sf(:)
    real(rp), intent(in) :: xs_sfnf(:)
    real(rp), intent(in) :: xs_chi(:)

    real(rp), optional, intent(in) :: rsf_pin(:)

    character(str_len) :: frmt
    integer :: ig, ig1, icol, irow, icol1, icol2
    integer :: nunit

    nunit = 2410

    open(nunit, file='results/' // filename )

    write(nunit,'(a)') '*      Mod Dens      Boron ppm      Fuel Temp      Mod Temp'
    write(nunit,'(a)') '              1              1              1             1'
    write(nunit,'(a)') '         750.00'
    write(nunit,'(a)') '           0.00'
    write(nunit,'(a)') '         300.00'
    write(nunit,'(a)') '         300.00'
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* ----------------------------------------------------------'
    write(nunit,'(a)') '* BURNUP   0.0'
    write(nunit,'(a)') '* ----------------------------------------------------------'
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Transport XSEC Table'
    write(nunit,'(a)') '*'
    do ig = 1, ngroups
      write(nunit,'(a,i3)') '* GROUP       ', ig
      write(nunit,'(E15.5)') xs_tot(ig)
    end do
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Absorption XSEC Table'
    write(nunit,'(a)') '*'
    do ig = 1, ngroups
      write(nunit,'(a,i3)') '* GROUP       ', ig
      write(nunit,'(E15.5)') xs_abs(ig)
    end do
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Nu-Fission XSEC Table'
    write(nunit,'(a)') '*'
    do ig = 1, ngroups
      write(nunit,'(a,i3)') '* GROUP       ', ig
      write(nunit,'(E15.5)') xs_sfnf(ig)
    end do
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Kappa-Fission XSEC Table'
    write(nunit,'(a)') '*'
    do ig = 1, ngroups
      write(nunit,'(a,i3)') '* GROUP       ', ig
      write(nunit,'(E15.5)') 1.0e-13
    end do
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* P0 Scattering XSEC Table'
    write(nunit,'(a)') '*'
    do ig = 1, ngroups
      do ig1 = 1, ngroups
        write(nunit,'(a,i3,a,i3)') '* GROUP       ', ig, ' ->         ', ig1
        write(nunit,'(E15.5)') xs_scat(ig,ig1)
      end do
    end do
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* ADF Table'
    write(nunit,'(a)') '*'
    do ig = 1, ngroups
      write(nunit,'(a,i3)') '* GROUP       ', ig
      write(nunit,'(E15.5)') 1.0
    end do
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Fission Spectrum'
    write(nunit,'(a)') '*'
    write(nunit,'(a,12i3)') '* GROUP    ',(ig,ig=1,ngroups)
    write(nunit,'(12E15.5)') (xs_chi(ig),ig=1,ngroups)
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Inverse Velocity'
    write(nunit,'(a)') '*'
    write(nunit,'(a,12i3)') '* GROUP    ',(ig,ig=1,ngroups)
    write(nunit,'(12E15.5)') (1.0,ig=1,ngroups)
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Delayed-Neutron Decay Constant (Lambda)'
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* GROUP       1              2              3              4              5              6'
    write(nunit,'(a)') '    1.00000E+00    1.00000E+00    1.00000E+00    1.00000E+00    1.00000E+00    1.00000E+00'
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* Delayed-Neutron Fraction (Beta)'
    write(nunit,'(a)') '*'
    write(nunit,'(a)') '* GROUP       1              2              3              4              5              6'
    write(nunit,'(a)') '    1.00000E+00    1.00000E+00    1.00000E+00    1.00000E+00    1.00000E+00    1.00000E+00'
    if (present(rsf_pin)) then
      write(nunit,'(a)') '*'
      write(nunit,'(a)') '* Pin powers'
      write(nunit,'(a)') '*'
      frmt = variable_format(nCol, 'E15.5')
      write(nunit, '(i7)') ncell
      icol1 = 1
      icol2 = nCol
      do irow = 1, nRow
        write(nunit, frmt) (rsf_pin(icol),icol=icol1,icol2)
        icol1 = icol2 + 1
        icol2 = icol2 + ncol
      end do
    end if
    write(nunit,'(a)') '*'
    write(nunit,'(a)') 'END'

    close(nunit)

  end subroutine

end module
