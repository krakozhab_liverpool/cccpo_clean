module GeometryMod
  !-----------------------------------------------
  ! Author : DL
  ! Date   : 15.09.2016
  ! This module contains subroutines and functions
  ! for work with geometry such as calculations of the
  ! vertices coordinates, calculations of the
  ! lines cross-sections, line-circle cross sections, etc.
  !-----------------------------------------------
  use precision_mod
contains

  logical function line_imp_is_degenerate_2d ( a, b )

    !*****************************************************************************80
    !
    !! LINE_IMP_IS_DEGENERATE_2D finds if an implicit point is degenerate in 2D.
    !
    !  Discussion:
    !
    !    The implicit form of a line in 2D is:
    !
    !      A * X + B * Y + C = 0
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    06 May 2005
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, real ( rp ) A, B, C, the implicit line parameters.
    !
    !    Output, logical ( kind = 4 ) LINE_IMP_IS_DEGENERATE_2D, is true if the
    !    line is degenerate.
    !
    implicit none

    integer ( kind = 4 ), parameter :: dim_num = 2

    real ( rp ) a
    real ( rp ) b

    line_imp_is_degenerate_2d = ( a * a + b * b == 0.0D+00 )

    return
  end function line_imp_is_degenerate_2d

  integer function i4_modp ( i, j )

  !*****************************************************************************80
  !
  !! I4_MODP returns the nonnegative remainder of integer division.
  !
  !  Discussion:
  !
  !    If
  !      NREM = I4_MODP ( I, J )
  !      NMULT = ( I - NREM ) / J
  !    then
  !      I = J * NMULT + NREM
  !    where NREM is always nonnegative.
  !
  !    The MOD function computes a result with the same sign as the
  !    quantity being divided.  Thus, suppose you had an angle A,
  !    and you wanted to ensure that it was between 0 and 360.
  !    Then mod(A,360) would do, if A was positive, but if A
  !    was negative, your result would be between -360 and 0.
  !
  !    On the other hand, I4_MODP(A,360) is between 0 and 360, always.
  !
  !  Example:
  !
  !        I     J     MOD  I4_MODP    Factorization
  !
  !      107    50       7       7    107 =  2 *  50 + 7
  !      107   -50       7       7    107 = -2 * -50 + 7
  !     -107    50      -7      43   -107 = -3 *  50 + 43
  !     -107   -50      -7      43   -107 =  3 * -50 + 43
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    02 March 1999
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    Input, integer ( kind = 4 ) I, the number to be divided.
  !
  !    Input, integer ( kind = 4 ) J, the number that divides I.
  !
  !    Output, integer ( kind = 4 ) I4_MODP, the nonnegative remainder when I is
  !    divided by J.
  !
    implicit none

    integer ( kind = 4 ) i
    integer ( kind = 4 ) j

    if ( j == 0 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'I4_MODP - Fatal error!'
      write ( *, '(a,i8)' ) '  I4_MODP ( I, J ) called with J = ', j
      stop 1
    end if

    i4_modp = mod ( i, j )

    if ( i4_modp < 0 ) then
      i4_modp = i4_modp + abs ( j )
    end if

    return
  end function

  integer function i4_wrap ( ival, ilo, ihi )

  !*****************************************************************************80
  !
  !! I4_WRAP forces an I4 to lie between given limits by wrapping.
  !
  !  Example:
  !
  !    ILO = 4, IHI = 8
  !
  !    I  I4_WRAP
  !
  !    -2     8
  !    -1     4
  !     0     5
  !     1     6
  !     2     7
  !     3     8
  !     4     4
  !     5     5
  !     6     6
  !     7     7
  !     8     8
  !     9     4
  !    10     5
  !    11     6
  !    12     7
  !    13     8
  !    14     4
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    19 August 2003
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    Input, integer ( kind = 4 ) IVAL, an integer value.
  !
  !    Input, integer ( kind = 4 ) ILO, IHI, the desired bounds for the integer
  !    value.
  !
  !    Output, integer ( kind = 4 ) I4_WRAP, a "wrapped" version of IVAL.
  !
    implicit none

    integer ( kind = 4 ) ihi
    integer ( kind = 4 ) ilo
    integer ( kind = 4 ) ival
    integer ( kind = 4 ) jhi
    integer ( kind = 4 ) jlo
    integer ( kind = 4 ) wide

    jlo = min ( ilo, ihi )
    jhi = max ( ilo, ihi )

    wide = jhi - jlo + 1

    if ( wide == 1 ) then
      i4_wrap = jlo
    else
      i4_wrap = jlo + i4_modp ( ival - jlo, wide )
    end if

    return
  end function

  subroutine GetVerticesPolygon(nSide, pitch, verX, verY)
    !--------------------------------------------------
    ! Returns vertices of the regular polygon.
    ! At the moment the number of sides is limited
    ! by 4 and 6 sides
    ! Input:
    !   nSide - number of sides of the polygon1
    !   pitch - pitch of polygon
    ! Output:
    !   verX - x-coordinates of the polygon's vertices
    !   verY - y-coordinates of the polygon's vertices
    !--------------------------------------------------
    implicit none

    integer, intent(in) :: nSide
    real(rp), intent(in) :: pitch

    real(rp), intent(out) :: verX(:)
    real(rp), intent(out) :: verY(:)

    if (nSide == 4) then
      call GetVerticesSquare(pitch, verX, verY)
      return
    elseif (nSide == 6) then
      call GetVerticesHex(pitch, verX, verY)
      return
    end if

    write(*,*) "The number of side nSide = ", nSide, "currently is not supported."
    stop

  end subroutine GetVerticesPolygon

  subroutine GetNumberOfSegments(verX, verY, nSides, dSeg, nSegments)
    use GlobalAssemblyMod, only : verbosity
    !----------------------------------------------------------------
    ! The subroutine calculates the number of the segments on each
    ! side of the polygon
    !----------------------------------------------------------------
    implicit none

    real(rp), intent(in) :: verX(:)
    real(rp), intent(in) :: verY(:)
    integer, intent(in) :: nSides
    real(rp), intent(in) :: dSeg

    integer, intent(out) :: nSegments(:)

    integer :: i
    real(rp) :: length

    do i = 1, nSides - 1
      length = getSegmentLength(verX(i), verY(i), verX(i+1), verY(i+1))
      nSegments(i) = int(length/dSeg) + 1
    end do

    length = getSegmentLength(verX(nSides), verY(nSides), verX(1), verY(1))
    nSegments(nSides) = int(length/dSeg) + 1
    if (verbosity > 0) then
      write(*, *) "nsegments: ", nSegments
    end if

    return

  end subroutine GetNumberOfSegments

  subroutine GetSegmentBoundaries(verX, verY, nSide, nSegments, segBoundX, segBoundY, segSdArr)
    implicit none

    real(rp), intent(in) :: verX(:)
    real(rp), intent(in) :: verY(:)
    integer, intent(in) :: nSide
    integer, intent(in) :: nSegments(:)

    real(rp), intent(out) :: segBoundX(:,:)
    real(rp), intent(out) :: segBoundY(:,:)
    real(rp), intent(out) :: segSdArr(:,:)

    integer :: i, j
    real(rp) :: vecX, vecY
    real(rp) :: segLen, sideLen, fact
    integer :: iSide, iSeg
    real(rp) :: p1(2), p2(2)
    real(rp) :: sdLen

!    if (not(allocated(segBoundX))) then
!      allocate(segBoundX(nSide,maxval(nSegments)+1))
!    end if
!    if (not(allocated(segBoundY))) then
!      allocate(segBoundY(nSide,maxval(nSegments)+1))
!    end if
!
!    segBoundX = 0.0_rp
!    segBoundY = 0.0_rp

    do i = 1, nSide - 1
      sideLen = getSegmentLength(verX(i), verY(i), verX(i+1), verY(i+1))
      segLen = sideLen/nSegments(i)
      fact = segLen/sideLen
      vecX = verX(i+1) - verX(i)
      vecY = verY(i+1) - verY(i)
      if (nSegments(i) > 1) then
        segBoundX(i, 1) = verX(i)
        segBoundY(i, 1) = verY(i)
        do j = 1, nSegments(i) - 1
          segBoundX(i, j+1) = verX(i) + fact*j*vecX
          segBoundY(i, j+1) = verY(i) + fact*j*vecY
        end do
        segBoundX(i, nSegments(i) + 1) = verX(i+1)
        segBoundY(i, nSegments(i) + 1) = verY(i+1)
      else
        segBoundX(i, 1) = verX(i)
        segBoundY(i, 1) = verY(i)
        segBoundX(i, 2) = verX(i+1)
        segBoundY(i, 2) = verY(i+1)
      end if
    end do

    sideLen = getSegmentLength(verX(nSide), verY(nSide), verX(1), verY(1))
    segLen = sideLen/nSegments(nSide)
    fact = segLen/sideLen
    vecX = verX(1) - verX(nSide)
    vecY = verY(1) - verY(nSide)
    if (nSegments(nSide) > 1) then
      segBoundX(nSide, 1) = verX(nSide)
      segBoundY(nSide, 1) = verY(nSide)
      do j = 1, nSegments(i) - 1
        segBoundX(nSide, j+1) = verX(nSide) + fact*j*vecX
        segBoundY(nSide, j+1) = verY(nSide) + fact*j*vecY
      end do
      segBoundX(nSide, nSegments(i) + 1) = verX(1)
      segBoundY(nSide, nSegments(i) + 1) = verY(1)
    else
      segBoundX(nSide, 1) = verX(nSide)
      segBoundY(nSide, 1) = verY(nSide)
      segBoundX(nSide, 2) = verX(1)
      segBoundY(nSide, 2) = verY(1)
    end if

    ! Calculation of the segment boundaries on each side
!    allocate(segSdArr(nSide, maxval(nSegments) + 1))
!    segSdArr = 0.0_rp
    do iSide = 1, nSide
      if (iSide < nSide) then
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(iSide+1)
        p2(2) = verY(iSide+1)
      else
        p1(1) = verX(iSide)
        p1(2) = verY(iSide)
        p2(1) = verX(1)
        p2(2) = verY(1)
      end if
      sdLen = getSegmentLength(p1(1),p1(2),p2(1),p2(2)) ! Length of the side
      do iSeg = 2, nSegments(iSide)
        segSdArr(iSide,iSeg) = getSegmentLength(p1(1),p1(2),segBoundX(iSide,iSeg),segBoundY(iSide,iSeg))
      end do
      segSdArr(iSide,nSegments(iSide)+1) = sdLen
    end do

    return

  end subroutine GetSegmentBoundaries

  subroutine GetZoneVolumes(x,y,r,vol)
    use ConstantsMod, only : PI
    implicit none

    real(rp) :: x(:), y(:)
    real(rp), optional :: r(:)
    real(rp), allocatable :: vol(:)

    real(rp) :: r1, r2
    integer :: nZone
    integer :: i

    if (.not. present(r)) then
      nZone = 1
      if (.not.(allocated(vol))) allocate(vol(1))
      vol(1) = getPolygonArea(x,y)
      return
    else
      nZone = size(r) + 1
    end if

    r1 = 0.0_rp
    do i = 1, nZone - 1
      r2 = r(i)
      vol(i) = pi*(r2**2 - r1**2)
      r1 = r2
    end do

    r2 = r(nZone-1)
    vol(nZone) = getPolygonArea(x,y) - pi*r2**2
    return

  end subroutine GetZoneVolumes

  real(rp) function getPolygonArea(x,y)
    implicit none
    real(rp) :: x(:), y(:)
    real(rp) :: s
    integer :: nVert
    integer :: i

    nVert = size(x)
    s = 0.0_rp
    do i = 1, nVert - 1
        s = s + (x(i)+x(i+1))*(y(i)-y(i+1))
    end do
    s = s + (x(nVert)+x(1))*(y(nVert)-y(1))
    getPolygonArea = 0.5_rp*abs(s)
  end function getPolygonArea

  subroutine GetVerticesSquare(pitch, verX, verY)
    !---------------------------------------------
    ! Author : DL
    ! Date   : 15.09.2016
    !
    ! Calculates the x and y coordinates of the vertices
    ! of the regular square using the length of its side
    ! It is assumed that centre of the square is located
    ! in point (0,0)
    !
    ! Input :
    !  pitch - the length of the side
    ! Output :
    !  verX, verY - arrays with x and y coordinates of the
    !               vertices
    !----------------------------------------------
    implicit none
    real(rp), intent(in)  :: pitch   ! The side of the square
    real(rp), intent(out) :: verX(:) ! x coordinates of the vertices
    real(rp), intent(out) :: verY(:) ! y coordinates of the vertices

    real(rp) :: px, py

    px = 0.5_rp*pitch
    py = px

    verX(1) = -px
    verY(1) = -py

    verX(2) =  px
    verY(2) = -py

    verX(3) =  px
    verY(3) =  py

    verX(4) = -px
    verY(4) =  py

  end subroutine GetVerticesSquare

  subroutine GetVerticesHex(pitch, verX, verY)
    !---------------------------------------------
    ! Author : DL
    ! Date   : 15.09.2016
    !
    ! Calculates the x and y coordinates of the vertices
    ! of the regular hexagon using the its pitch.
    ! It is assumed that centre of the hexagon is located
    ! in point (0,0)
    !
    ! Input :
    !  pitch - the pitch of the hexagon
    ! Output :
    !  verX, verY - arrays with x and y coordinates of the
    !               vertices
    !----------------------------------------------
    implicit none
    real(rp), intent(in)  :: pitch   ! The side of the square
    real(rp), intent(out) :: verX(:) ! x coordinates of the vertices
    real(rp), intent(out) :: verY(:) ! y coordinates of the vertices

    real(rp) :: hp, hR, R

    hp = 0.5_rp*pitch
    R  = pitch/sqrt(3.0_rp)
    hR = 0.5_rp*R

!    verX(1) = -R
!    verY(1) = 0.0_rp
!
!    verX(2) = -hR
!    verY(2) = -hp
!
!    verX(3) =  hR
!    verY(3) = -hp
!
!    verX(4) =  R
!    verY(4) =  0.0_rp
!
!    verX(5) =  hR
!    verY(5) =  hp
!
!    verX(6) = -hR
!    verY(6) =  hp

    verX(1) = -hp
    verY(1) = -hR

    verX(2) = 0.0_rp
    verY(2) = -R

    verX(3) =  hp
    verY(3) = -hR

    verX(4) =  hp
    verY(4) =  hR

    verX(5) =  0.0_rp
    verY(5) =  R

    verX(6) = -hp
    verY(6) =  hR

  end subroutine GetVerticesHex

  subroutine GetCoeffA(y1, y2, A)
    !--------------------------------
    !Returns coefficient A of the line defined by 2 points
    ! Input:
    !    y1 - y-coordinate of the first point
    !    y2 - y-coordinate of the second point
    ! Output:
    !    A  - Coefficient A for the line equation
    !---------------------------------
    implicit none

    real(rp), intent(in) :: y1
    real(rp), intent(in) :: y2

    real(rp), intent(out) :: A

    A = y2 - y1

    return

  end subroutine GetCoeffA

  subroutine GetCoeffB(x1, x2, B)
    !--------------------------------
    !Returns coefficient B of the line defined by 2 points
    ! Input:
    !    x1 - x coordinate of the first point
    !    y1 - y-coordinate of the first point
    !    x2 - x-coordinate of the first point
    !    y2 - y-coordinate of the first point
    ! Output:
    !    B  - Coefficient B for the line equation
    !---------------------------------
    implicit none

    real(rp), intent(in) :: x1
    real(rp), intent(in) :: x2

    real(rp), intent(out) :: B

    B = x1 - x2

    return

  end subroutine GetCoeffB

  subroutine GetCoeffC(x1, y1, x2, y2, C)
    !--------------------------------
    !Returns coefficient C of the line defined by 2 points
    ! Input:
    !    x1 - x coordinate of the first point
    !    y1 - y-coordinate of the first point
    !    x2 - x-coordinate of the first point
    !    y2 - y-coordinate of the first point
    ! Output:
    !    C  - Coefficient C for the line equation
    !---------------------------------
    implicit none

    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1
    real(rp), intent(in) :: x2
    real(rp), intent(in) :: y2

    real(rp), intent(out) :: C

    C = y1*x2 - x1*y2

    return

  end subroutine GetCoeffC

  function getSegmentMiddle(x0, y0, x1, y1)
    !----------------------------------------
    ! Function returns array xy(2) containing
    ! x and y coordinates of the segment's
    ! middle point
    ! xy(1) - x coordinate
    ! xy(2) - y coordinate
    !----------------------------------------
    implicit none

    real(rp), intent(in) :: x0 ! x coordinate of the segment start point
    real(rp), intent(in) :: y0 ! y coordinate of the segment start point
    real(rp), intent(in) :: x1 ! x coordinate of the segment end point
    real(rp), intent(in) :: y1 ! y coordinate of the segment end point

    real(rp) :: getSegmentMiddle(2)

    getSegmentMiddle(1) = (x1 + x0)*0.5_rp
    getSegmentMiddle(2) = (y1 + y0)*0.5_rp

    return

  end function getSegmentMiddle

  subroutine GetNormVecSquare(verX, verY, normVecX, normVecY)
    !----------------------------------------------------------
    ! Subroutine calculates x and y coordinates for the normal
    ! vectors to the sides of the square.
    ! Input:
    !    verX - array with x coordinates of the square vertices
    !    verY - array with y coordinates of the square vertices
    !    pitch - pitch (side) of the square
    ! Output:
    !    normVecX - array with the x coordinates of the normal
    !               vectors
    !    normVecY - array with the y coordinates of the normal
    !               vectors
    !-----------------------------------------------------------
    implicit none

    real(rp), intent(in) :: verX(4)
    real(rp), intent(in) :: verY(4)

    real(rp), intent(out) :: normVecX(4)
    real(rp), intent(out) :: normVecY(4)

    real(rp) :: x, y
    integer :: i, i1, i2

    do i = 1, 4
      if (i == 4) then
        i1 = 4
        i2 = 1
      else
        i1 = i
        i2 = i + 1
      end if
      call GetCoeffA(verY(i1), verY(i2), x)
      call GetCoeffB(verX(i1), verX(i2), y)
      if (crossProduct(verX(i2)-verX(i1), verY(i2)-verY(i1), x, y) < 0) then
        normVecX(i) = x
        normVecY(i) = y
      else
        normVecX(i) = -x
        normVecY(i) = -y
      end if
    end do

    return

  end subroutine GetNormVecSquare

  subroutine GetNormVecHex(verX, verY, normVecX, normVecY)
    !----------------------------------------------------------
    ! Subroutine calculates x and y coordinates for the normal
    ! vectors to the sides of the hexagon.
    ! Input:
    !    verX - array with x coordinates of the hexagon vertices
    !    verY - array with y coordinates of the hexagon vertices
    !    pitch - pitch (side) of the square
    ! Output:
    !    normVecX - array with the x coordinates of the normal
    !               vectors
    !    normVecY - array with the y coordinates of the normal
    !               vectors
    !-----------------------------------------------------------
    implicit none

    real(rp), intent(in) :: verX(6)
    real(rp), intent(in) :: verY(6)

    real(rp), intent(out) :: normVecX(6)
    real(rp), intent(out) :: normVecY(6)

    real(rp) :: x, y
    integer :: i, i1, i2

    do i = 1, 6
      if (i == 6) then
        i1 = 6
        i2 = 1
      else
        i1 = i
        i2 = i + 1
      end if
      call GetCoeffA(verY(i1), verY(i2), x)
      call GetCoeffB(verX(i1), verX(i2), y)
      if (crossProduct(verX(i2)-verX(i1), verY(i2)-verY(i1), x, y) < 0) then
        normVecX(i) = x
        normVecY(i) = y
      else
        normVecX(i) = -x
        normVecY(i) = -y
      end if
    end do

    return

  end subroutine GetNormVecHex

  subroutine GetNormVecPolygon(verX, verY, nSides, normVecX, normVecY)
    !----------------------------------------------------------
    ! Subroutine calculates x and y coordinates for the normal
    ! vectors to the sides of the hexagon. Numbering of the
    ! polygon's vertices is clockwise.
    ! Input:
    !    verX - array with x coordinates of the hexagon vertices
    !    verY - array with y coordinates of the hexagon vertices
    !    nSides - number of sides of the polygon
    ! Output:
    !    normVecX - array with the x coordinates of the normal
    !               vectors
    !    normVecY - array with the y coordinates of the normal
    !               vectors
    !-----------------------------------------------------------
    implicit none

    real(rp), intent(in) :: verX(:)
    real(rp), intent(in) :: verY(:)
    integer, intent(in) :: nSides

    real(rp), intent(out) :: normVecX(nSides)
    real(rp), intent(out) :: normVecY(nSides)

    real(rp) :: x, y
    real(rp) :: nrmVec(2)
    integer :: i, i1, i2

    do i = 1, nSides
      if (i == nSides) then
        i1 = nSides
        i2 = 1
      else
        i1 = i
        i2 = i + 1
      end if
      call GetCoeffA(verY(i1), verY(i2), x)
      call GetCoeffB(verX(i1), verX(i2), y)
      if (crossProduct(verX(i2)-verX(i1), verY(i2)-verY(i1), x, y) > 0) then
        normVecX(i) = -x
        normVecY(i) = -y
      else
        normVecX(i) = x
        normVecY(i) = y
      end if
      ! Normalization of the vectors
      nrmVec = getNormVec(normVecX(i), normVecY(i))
      normVecX(i) = nrmVec(1)
      normVecY(i) = nrmVec(2)
    end do

    return

  end subroutine GetNormVecPolygon

  function getNormVec(x, y)
    !------------------------------------------
    ! Subroutine returns normalized unit vector
    !
    ! Input:
    !     x - the x coordinate of the input
    !         vector
    !     y - the y coordinate of the input
    !         vector
    ! Output:
    !     getNormVec(1) - x coordinate of the
    !                     normalized vector
    !     getNormVec(2) - y coordinate of the
    !                     normalized vector
    !-------------------------------------------
    implicit none

    real(rp), intent(in) :: x ! x coordinate of the input vector
    real(rp), intent(in) :: y ! y coordinate of the input vector

    real(rp) :: getNormVec(2) ! x and y coordinates of the normalized vector

    real(rp) :: lng

    lng = getVecLength(x, y)

    getNormVec(1) = x/lng
    getNormVec(2) = y/lng

    return

  end function getNormVec

  subroutine NormalizeVector(x, y)
  !-------------------------------
  ! Subroutine returns normalized
  ! to unity coordinates x and y
  ! of initial vector (x, y)
  !-------------------------------
    implicit none
    real(rp), intent(inout) :: x, y

    real(rp) :: lng

    lng = getVecLength(x,y)

    x = x/lng
    y = y/lng

    return

  end subroutine NormalizeVector

  real(rp) function getVecLength(x, y)
    !-----------------------------------
    ! Returns length of the vector with
    ! coordinates x and y
    !
    ! Input
    !    x - x coordinate of the vector
    !    y - y coordinate of the vector
    !
    ! Output
    !    getVecLength - length of vector
    !-----------------------------------
    implicit none

    real(rp), intent(in) :: x ! x coordinate of the vector
    real(rp), intent(in) :: y ! y coordinate of the vector

    getVecLength = sqrt(x*x + y*y)

    return

  end function getVecLength

  function setVecLength(x, y, vecLen)
    !-------------------------------------------------
    ! Set the vecLen for the vector with coordinates
    ! x, y. Returns array containing coordinates x and
    ! y of the new vector with length vecLen
    !-------------------------------------------------
    implicit none
    real(rp), intent(in) :: x ! x coordinate of the input vector
    real(rp), intent(in) :: y ! y coordinate of the input vector
    real(rp), intent(in) :: vecLen ! desirable length of the vector

    real(rp) :: setVecLength(2) ! x (setVecLength(1)) and y (setVecLength(2)) coordinates of the output vector with length vecLen

    real(rp) :: curLen

    curLen = getVecLength(x, y)

    setVecLength(1) = x*vecLen/curLen
    setVecLength(2) = y*vecLen/curLen

    return

  end function setVecLength

  real(rp) function crossProduct(x1, y1, x2, y2)
    !--------------------------------------------
    ! Function returns cross product of the two
    ! vectors with coordinates x1, y1 and x2, y2
    ! Input:
    !   x1 - x coordinate of the first vector
    !   y1 - y coordinate of the first vector
    !   x2 - x coordinate of the second vector
    !   y2 - y coordinate of the second vector
    ! Output:
    !   crossProduct - cross product of the vectors
    !--------------------------------------------
    implicit none

    real(rp), intent(in) :: x1 ! x coordinate of the first vector
    real(rp), intent(in) :: y1 ! y coordinate of the first vector
    real(rp), intent(in) :: x2 ! x coordinate of the second vector
    real(rp), intent(in) :: y2 ! y coordinate of the second vector

    crossProduct = x1*y2 - y1*x2

    return

  end function crossProduct

  real(rp) function getSegmentLength(x1, y1, x2, y2)
    implicit none
    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1
    real(rp), intent(in) :: x2
    real(rp), intent(in) :: y2

    getSegmentLength = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1))
    return

  end function getSegmentLength

  subroutine circles_imp_int_2d ( r1, pc1, r2, pc2, int_num, p )

    !*****************************************************************************80
    !
    !! CIRCLES_IMP_INT_2D: finds the intersection of two implicit circles in 2D.
    !
    !  Discussion:
    !
    !    Two circles can intersect in 0, 1, 2 or infinitely many points.
    !
    !    The 0 and 2 intersection cases are numerically robust; the 1 and
    !    infinite intersection cases are numerically fragile.  The routine
    !    uses a tolerance to try to detect the 1 and infinite cases.
    !
    !    Points P on an implicit circle in 2D satisfy the equation:
    !
    !      ( P(1) - PC(1) )^2 + ( P(2) - PC(2) )^2 = R^2
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    19 October 2004
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, real ( rp ) R1, the radius of the first circle.
    !
    !    Input, real ( rp ) PC1(2), the center of the first circle.
    !
    !    Input, real ( rp ) R2, the radius of the second circle.
    !
    !    Input, real ( rp ) PC2(2), the center of the second circle.
    !
    !    Output, integer ( kind = 4 ) INT_NUM, the number of intersecting points
    !    found.  INT_NUM will be 0, 1, 2 or 3.  3 indicates that there are an
    !    infinite number of intersection points.
    !
    !    Output, real ( rp ) P(2,2), if INT_NUM is 1 or 2,
    !    the coordinates of the intersecting points.
    !
    implicit none

    integer ( kind = 4 ), parameter :: dim_num = 2

    real ( rp ) distsq
    integer ( kind = 4 ) int_num
    real ( rp ) p(dim_num,2)
    real ( rp ) pc1(dim_num)
    real ( rp ) pc2(dim_num)
    real ( rp ) r1
    real ( rp ) r2
    real ( rp ) root
    real ( rp ) sc1
    real ( rp ) sc2
    real ( rp ) t1
    real ( rp ) t2
    real ( rp ) tol

    tol = epsilon ( tol )

    p(1:dim_num,1:2) = 0.0D+00
    !
    !  Take care of the case in which the circles have the same center.
    !
    t1 = ( abs ( pc1(1) - pc2(1) ) &
      + abs ( pc1(2) - pc2(2) ) ) / 2.0_rp

    t2 = ( abs ( pc1(1) ) + abs ( pc2(1) ) &
      + abs ( pc1(2) ) + abs ( pc2(2) ) + 1.0_rp ) / 5.0_rp

    if ( t1 <= tol * t2 ) then

      t1 = abs ( r1 - r2 )
      t2 = ( abs ( r1 ) + abs ( r2 ) + 1.0_rp) / 3.0_rp

      if ( t1 <= tol * t2 ) then
        int_num = 3
      else
        int_num = 0
      end if

      return

    end if

    distsq = ( pc1(1) - pc2(1) )**2 + ( pc1(2) - pc2(2) )**2

    root = 2.0_rp * ( r1**2 + r2**2 ) * distsq - distsq**2 &
      - ( r1 - r2 )**2 * ( r1 + r2 )**2

    if ( root < -tol ) then
      int_num = 0
      return
    end if

    sc1 = ( distsq - ( r2**2 - r1**2 ) ) / distsq

    if ( root < tol ) then
      int_num = 1
      p(1:dim_num,1) = pc1(1:dim_num) &
        + 0.5_rp * sc1 * ( pc2(1:dim_num) - pc1(1:dim_num) )
      return
    end if

    sc2 = sqrt ( root ) / distsq

    int_num = 2

    p(1,1) = pc1(1) + 0.5_rp * sc1 * ( pc2(1) - pc1(1) ) &
      - 0.5_rp * sc2 * ( pc2(2) - pc1(2) )
    p(2,1) = pc1(2) + 0.5_rp * sc1 * ( pc2(2) - pc1(2) ) &
      + 0.5_rp * sc2 * ( pc2(1) - pc1(1) )

    p(1,2) = pc1(1) + 0.5_rp * sc1 * ( pc2(1) - pc1(1) ) &
      + 0.5_rp * sc2 * ( pc2(2) - pc1(2) )
    p(2,2) = pc1(2) + 0.5_rp * sc1 * ( pc2(2) - pc1(2) ) &
      - 0.5_rp * sc2 * ( pc2(1) - pc1(1) )

    return
  end subroutine circles_imp_int_2d

  subroutine GetTangentPoints(x0, y0, xc, yc, r, xt1, yt1, xt2, yt2)
    implicit none
    real(rp) :: x0
    real(rp) :: y0
    real(rp) :: xc
    real(rp) :: yc
    real(rp) :: r
    real(rp) :: xt1
    real(rp) :: yt1
    real(rp) :: xt2
    real(rp) :: yt2

    real(rp) :: r1, d
    real(rp) :: p0(2), pc(2), pt(2,2)
    integer :: int_num

    d = getSegmentLength(x0, y0, xc, yc)
    r1 = sqrt(d*d - r*r)

    p0 = (/x0, y0/)
    pc = (/xc, yc/)
    call circles_imp_int_2d(r1, p0, r, pc, int_num, pt)

    xt1 = pt(1,1)
    yt1 = pt(2,1)
    xt2 = pt(1,2)
    yt2 = pt(2,2)

    return

  end subroutine GetTangentPoints

  real(rp) function getAngle(x0, y0, x1, y1)
    !----------------------------------------------
    ! Function returns angle between vectors with
    ! coordinates (x0,y0) and (x1,y1). The angle is
    ! in interval (-PI,PI).
    !----------------------------------------------
    use ConstantsMod, only : PI
    implicit none

    real(rp), intent(in) :: x0
    real(rp), intent(in) :: y0
    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1

    real(rp) :: a, b

    if (x0 == 0.0_rp .and. y0 == 0.0_rp) then
      getAngle = 0.0_rp
      return
    end if

    if (x1 == 0.0_rp .and. y1 == 0.0_rp) then
      getAngle = 0.0_rp
      return
    end if

    a = x0*x1 + y0*y1
    b = getVecLength(x0,y0)*getVecLength(x1,y1)

    if (abs(a) > b) b = abs(a)
    if (crossProduct(x0,y0,x1,y1) < 0) then
      getAngle = -acos(a/b)
      return
    else
      getAngle = acos(a/b)
      return
    end if

    return

  end function getAngle

  real(rp) function getAngle2PI(x0, y0, x1, y1)
    !----------------------------------------------
    ! Function returns angle between vectors with
    ! coordinates (x0,y0) and (x1,y1). The angle is
    ! in interval (0,2*PI).
    !----------------------------------------------
    use ConstantsMod, only : PI
    implicit none

    real(rp), intent(in) :: x0
    real(rp), intent(in) :: y0
    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1

    real(rp) :: ang

    ang = getAngle(x0,y0,x1,y1)
    if (ang >= 0.0_rp) then
      getAngle2PI = ang
      return
    else
      getAngle2PI = ang + 2.0_rp*PI
    end if

    return

  end function getAngle2PI

  subroutine GetTangentPointsVec(vecX, vecY, xc, yc, r, xt1, yt1, xt2, yt2)
    !-----------------------------------------------------------------------------------------
    ! Subroutine returns the tangent points for circle with radius r, with centre located in (xc,yc).
    ! Tangent lines at these points coincide with directional vector (vecX,vecY).
    !-----------------------------------------------------------------------------------------
    ! The idea of algorithm is simple and can be described in two steps.
    ! 1. Get line which is orthogonal to the vector (vecX,vecY) at the centre of the circle
    ! 2. Find cross sections of that line with circumference.
    !-----------------------------------------------------------------------------------------
    implicit none

    real(rp), intent(in) :: vecX ! x coordinate of the directional vector
    real(rp), intent(in) :: vecY ! y coordinate of the directional vector
    real(rp), intent(in) :: xc   ! x coordinate of the centre of circle
    real(rp), intent(in) :: yc   ! y coordinate of the centre of circle
    real(rp), intent(in) :: r    ! radius of the circle

    real(rp), intent(out) :: xt1  ! x coordinate of the first tangential point
    real(rp), intent(out) :: yt1  ! y coordinate of the first tangential point
    real(rp), intent(out) :: xt2  ! x coordinate of the second tangential point
    real(rp), intent(out) :: yt2  ! y coordinate of the second tangential point

    real(rp) :: A, B, C ! coefficient of the line
    integer :: flag

    ! Get coefficients of the line orthogonal to the vector (vecX, vecY) at point (xc,yc)

    A = vecX
    B = vecY
    C = -vecY*yc - vecX*xc

    call LineCircleIntersection(A,B,C,xc,yc,r,xt1,yt1,xt2,yt2,flag)

    return

  end subroutine GetTangentPointsVec

  subroutine LineCircleIntersection(A, B, C, xc, yc, r, x1, y1, x2, y2, flag)
    !---------------------------------------------------------------------------------
    ! Subroutine returns points of intersection for line given in general form (A,B,C)
    ! and circle with radius r and centre in (xc, yc)
    ! If flag = 1 then 2 intersection points
    !    flag = 0 then 1 intersection (tangential line)
    !    flag = -1 then there is no intersections
    ! Algorithm is based on geometrical approach and is taken from:
    ! http://e-maxx.ru/algo/circle_line_intersection
    !----------------------------------------------------------------------------------
    use ConstantsMod, only : EPS => DELTA_MIN
    implicit none

    real(rp), intent(in) :: A  ! A coefficient of the line
    real(rp), intent(in) :: B  ! B coefficient of the line
    real(rp), intent(in) :: C  ! C coefficient of the line
    real(rp), intent(in) :: xc ! x coordinate of the centre of the circle
    real(rp), intent(in) :: yc ! y coordinate of the centre of the circle
    real(rp), intent(in) :: r  ! radius of the circle

    real(rp), intent(out) :: x1   ! x coordinate of the 1 intersection
    real(rp), intent(out) :: y1   ! y coordinate of the 1 intersection
    real(rp), intent(out) :: x2   ! x coordinate of the 2 intersection
    real(rp), intent(out) :: y2   ! y coordinate of the 2 intersection
    integer, intent(out) :: flag ! flag

    real(rp) :: C1, x0, y0, d, mult, ax, ay, bx, by

    ! Move the origin into the point (0,0). C coefficient will is changed
    C1 = C + A*xc + B*yc

    x0 = -A*C1/(A*A+B*B)
    y0 = -B*C1/(A*A+B*B)

    if (C1*C1 > r*r*(A*A+B*B)+EPS) then
      flag = -1
      return
    elseif (abs(C1*C1 - r*r*(A*A+B*B)) < EPS) then
      flag = 0
      x1 = x0 + xc
      y1 = y0 + yc
      x2 = x0 + xc
      y2 = y0 + yc
      return
    end if

    d = r*r - C1*C1/(A*A+B*B);
    mult = sqrt (d / (A*A+B*B));
    ax = x0 + B * mult;
    bx = x0 - B * mult;
    ay = y0 - A * mult;
    by = y0 + A * mult;

    flag = 1
    x1 = ax + xc
    y1 = ay + yc
    x2 = bx + xc
    y2 = by + yc

    return

  end subroutine LineCircleIntersection

  subroutine lines_imp_int_2d ( a1, b1, c1, a2, b2, c2, ival, p )

    !*****************************************************************************80
    !
    !! LINES_IMP_INT_2D determines where two implicit lines intersect in 2D.
    !
    !  Discussion:
    !
    !    The implicit form of a line in 2D is:
    !
    !      A * X + B * Y + C = 0
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    25 February 2005
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, real ( rp ) A1, B1, C1, define the first line.
    !    At least one of A1 and B1 must be nonzero.
    !
    !    Input, real ( rp ) A2, B2, C2, define the second line.
    !    At least one of A2 and B2 must be nonzero.
    !
    !    Output, integer ( kind = 4 ) IVAL, reports on the intersection.
    !
    !    -1, both A1 and B1 were zero.
    !    -2, both A2 and B2 were zero.
    !     0, no intersection, the lines are parallel.
    !     1, one intersection point, returned in P.
    !     2, infinitely many intersections, the lines are identical.
    !
    !    Output, real ( rp ) P(2), if IVAL = 1, then P is
    !    the intersection point.  Otherwise, P = 0.
    !
    implicit none

    integer ( kind = 4 ), parameter :: dim_num = 2

    real ( rp ) a(dim_num,dim_num+1)
    real ( rp ) a1
    real ( rp ) a2
    real ( rp ) b1
    real ( rp ) b2
    real ( rp ) c1
    real ( rp ) c2
    integer ( kind = 4 ) info
    integer ( kind = 4 ) ival
    real ( rp ) p(dim_num)

    p(1:dim_num) = 0.0D+00
    !
    !  Refuse to handle degenerate lines.
    !
    if ( line_imp_is_degenerate_2d ( a1, b1 ) ) then
      ival = -1
      return
    end if

    if ( line_imp_is_degenerate_2d ( a2, b2 ) ) then
      ival = -2
      return
    end if
    !
    !  Set up and solve a linear system.
    !
    a(1,1) = a1
    a(1,2) = b1
    a(1,3) = -c1

    a(2,1) = a2
    a(2,2) = b2
    a(2,3) = -c2

    call r8mat_solve ( 2, 1, a, info )
    !
    !  If the inverse exists, then the lines intersect at the solution point.
    !
    if ( info == 0 ) then

      ival = 1
      p(1:dim_num) = a(1:dim_num,3)
      !
      !  If the inverse does not exist, then the lines are parallel
      !  or coincident.  Check for parallelism by seeing if the
      !  C entries are in the same ratio as the A or B entries.
      !
    else

      ival = 0

      if ( a1 == 0.0D+00 ) then
        if ( b2 * c1 == c2 * b1 ) then
          ival = 2
        end if
      else
        if ( a2 * c1 == c2 * a1 ) then
          ival = 2
        end if
      end if

    end if

    return
  end subroutine lines_imp_int_2d

  subroutine r8mat_solve ( n, rhs_num, a, info )

    !*****************************************************************************80
    !
    !! R8MAT_SOLVE uses Gauss-Jordan elimination to solve an N by N linear system.
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    29 August 2003
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, integer ( kind = 4 ) N, the order of the matrix.
    !
    !    Input, integer ( kind = 4 ) RHS_NUM, the number of right hand sides.
    !    RHS_NUM must be at least 0.
    !
    !    Input/output, real ( rp ) A(N,N+rhs_num), contains in rows and
    !    columns 1 to N the coefficient matrix, and in columns N+1 through
    !    N+rhs_num, the right hand sides.  On output, the coefficient matrix
    !    area has been destroyed, while the right hand sides have
    !    been overwritten with the corresponding solutions.
    !
    !    Output, integer ( kind = 4 ) INFO, singularity flag.
    !    0, the matrix was not singular, the solutions were computed;
    !    J, factorization failed on step J, and the solutions could not
    !    be computed.
    !
    implicit none

    integer ( kind = 4 ) n
    integer ( kind = 4 ) rhs_num

    real ( rp ) a(n,n+rhs_num)
    real ( rp ) apivot
    real ( rp ) factor
    integer ( kind = 4 ) i
    integer ( kind = 4 ) info
    integer ( kind = 4 ) ipivot
    integer ( kind = 4 ) j

    info = 0

    do j = 1, n
      !
      !  Choose a pivot row.
      !
      ipivot = j
      apivot = a(j,j)

      do i = j+1, n
        if ( abs ( apivot ) < abs ( a(i,j) ) ) then
          apivot = a(i,j)
          ipivot = i
        end if
      end do

      if ( apivot == 0.0D+00 ) then
        info = j
        return
      end if
      !
      !  Interchange.
      !
      do i = 1, n + rhs_num
        call r8_swap ( a(ipivot,i), a(j,i) )
      end do
      !
      !  A(J,J) becomes 1.
      !
      a(j,j) = 1.0D+00
      a(j,j+1:n+rhs_num) = a(j,j+1:n+rhs_num) / apivot
      !
      !  A(I,J) becomes 0.
      !
      do i = 1, n

        if ( i /= j ) then

          factor = a(i,j)
          a(i,j) = 0.0D+00
          a(i,j+1:n+rhs_num) = a(i,j+1:n+rhs_num) - factor * a(j,j+1:n+rhs_num)

        end if

      end do

    end do

    return
  end subroutine

  subroutine r8_swap ( x, y )

    !*****************************************************************************80
    !
    !! R8_SWAP switches two R8's.
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    01 May 2000
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input/output, real ( rp ) X, Y.  On output, the values of X and
    !    Y have been interchanged.
    !
    implicit none

    real ( rp ) x
    real ( rp ) y
    real ( rp ) z

    z = x
    x = y
    y = z

    return
  end subroutine

  subroutine segment_contains_point_2d ( p1, p2, p, u )

    !*****************************************************************************80
    !
    !! SEGMENT_CONTAINS_POINT_2D reports if a line segment contains a point in 2D.
    !
    !  Discussion:
    !
    !    A line segment is the finite portion of a line that lies between
    !    two points P1 and P2.
    !
    !    In exact arithmetic, point P is on the line segment between
    !    P1 and P2 if and only if 0 <= U <= 1 and V = 0.
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    17 August 2005
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, real ( rp ) P1(2), P2(2), the endpoints of the line segment.
    !
    !    Input, real ( rp ) P(2), a point to be tested.
    !
    !    Output, real ( rp ) U(2), the components of P, with the first
    !    component measured along the axis with origin at P1 and unit at P2,
    !    and second component the magnitude of the off-axis portion of the
    !    vector P-P1, measured in units of (P2-P1).
    !
    implicit none

    integer ( kind = 4 ), parameter :: dim_num = 2

    real ( rp ) normsq
    real ( rp ) p(dim_num)
    real ( rp ) p1(dim_num)
    real ( rp ) p2(dim_num)
    real ( rp ) u(dim_num)

    normsq = sum ( ( p2(1:dim_num) - p1(1:dim_num) )**2 )

    if ( normsq == 0.0D+00 ) then

      if ( all ( p(1:dim_num) == p1(1:dim_num) ) ) then
        u(1) = 0.5D+00
        u(2) = 0.0D+00
      else
        u(1) = 0.5D+00
        u(2) = huge ( u(2) )
      end if

    else

      u(1) = sum ( ( p(1:dim_num)  - p1(1:dim_num) ) &
        * ( p2(1:dim_num) - p1(1:dim_num) ) ) / normsq

      u(2) = sqrt ( ( ( u(1) - 1.0_rp ) * p1(1) - u(1) * p2(1) + p(1) )**2 &
        + ( ( u(1) - 1.0_rp ) * p1(2) - u(1) * p2(2) + p(2) )**2 ) &
        / sqrt ( normsq )

    end if

    return
  end subroutine

  subroutine RotateVec(vecX, vecY, phi)
  !--------------------------------------------
  ! Rotate vector with coordinates (vecX, vecY)
  ! on angle phi clockwise
  !--------------------------------------------
    implicit none
    real(rp), intent(inout) :: vecX
    real(rp), intent(inout) :: vecY
    real(rp), intent(in) :: phi

    real(rp) :: vecX1, vecY1

    vecX1 = vecX*cos(phi) - vecY*sin(phi)
    vecY1 = vecX*sin(phi) + vecY*cos(phi)
    vecX = vecX1
    vecY = vecY1

    return

  end subroutine

  function rotateVector(vecX, vecY, phi) result (vec)
    implicit none
    real(rp), intent(in) :: vecX
    real(rp), intent(in) :: vecY
    real(rp), intent(in) :: phi

    real(rp) :: vec(2)

    vec(1) = vecX*cos(phi) - vecY*sin(phi)
    vec(2) = vecX*sin(phi) + vecY*cos(phi)

    return

  end function

  logical function isOnSegment(px, py, x1, y1, x2, y2)
  !----------------------------------------------------
  ! Function returns true if point (px,py) belongs to
  ! segment with vertices (x1,y1) and (x2,y2) and false
  ! in opposite case
  !----------------------------------------------------
    use ConstantsMod, only : EPS => DELTA_MIN
    use MathMod, only : scalarProduct, crossProduct
    implicit none
    real(rp), intent(in) :: px
    real(rp), intent(in) :: py
    real(rp), intent(in) :: x1
    real(rp), intent(in) :: y1
    real(rp), intent(in) :: x2
    real(rp), intent(in) :: y2

    isOnSegment = .false.

    if (abs(crossProduct(x2-x1,y2-y1,px-x1,py-y1)) <= EPS) then
      if (scalarProduct(x1-px,y1-py,x2-px,y2-py) <= 0.0_rp) then
        isOnSegment = .true.
        return
      else
        isOnSegment = .false.
        return
      end if
    end if

  end function

  real(rp) function getMinDistanceToCircle(x0, y0, vecX, vecY, xc, yc, r)
  !-------------------------------------------------------------------
  ! Function returns minimal distance from point x0, y0 in direction
  ! (vecX,vecY) to circle with centre in (xc,yc) and radius r
  ! If there is no intersections - returns -1.0_rp
  !-------------------------------------------------------------------
    use ConstantsMod
    implicit none

    real(rp), intent(in) :: x0, y0
    real(rp), intent(in) :: vecX, vecY
    real(rp), intent(in) :: xc, yc, r

    real(rp) :: xx, yy
    real(rp) :: a, k, c, D, t
    real(rp) :: d1, d2

    xx = x0 - xc
    yy = y0 - yc

    a = vecX*vecX + vecY*vecY
    k = xx*vecX + yy*vecY
    c = xx*xx + yy*yy - r*r
    D = k*k - a*c

    if (D < 0) then
      getMinDistanceToCircle = -1.0_rp
      return
    end if

    d1 = (-k + sqrt(D))/a
    d2 = (-k - sqrt(D))/a
    if (d1 <= 0.0_rp .and. d2 <= 0.0_rp) then
      getMinDistanceToCircle = -1.0_rp
      return
    end if
    if (d1 > 0.0_rp .and. d2 <= 0.0_rp) then
      if (abs(d1) < DELTA_MIN) then
        getMinDistanceToCircle = -1.0_rp
        return
      end if
      getMinDistanceToCircle = d1
      return
    end if
    if (d2 > 0.0_rp .and. d1 <= 0.0_rp) then
      if (abs(d2) < DELTA_MIN) then
        getMinDistanceToCircle = -1.0_rp
        return
      end if
      getMinDistanceToCircle = d2
      return
    end if
    if (d1 > 0.0_rp .and. d2 > 0.0_rp) then
      if (d1 < DELTA_MIN) d1 = huge(d1)
      if (d2 < DELTA_MIN) d2 = huge(d2)
      getMinDistanceToCircle = min(d1,d2)
      return
    end if


    if (c < 0) then
      d1 = (-k + sqrt(D))/a
      if (abs(d1) < DELTA_MIN) getMinDistanceToCircle = -1.0_rp
      return
    else
      t = -k - sqrt(D)
      if (t > 0) then
        getMinDistanceToCircle = t/a
        if (getMinDistanceToCircle < DELTA_MIN) getMinDistanceToCircle = -1.0_rp
        return
      else
        getMinDistanceToCircle = -1.0_rp
        return
      end if
    end if

  end function

  real(rp) function getMinDistanceToLine(x0, y0, vecX, VecY, A, B, C)
  !-----------------------------------------------------------------
  ! Function returns minimal distance from point (x0,y0) in direction
  ! (vecX,vecY) to line with coefficients A, B, C
  ! The result is positive in the case of cross section
  ! and negative in the case vector is parallel to particle or
  ! there is no cross section with line in this direction
  !------------------------------------------------------------------
    use ConstantsMod
    implicit none

    real(rp), intent(in) :: x0
    real(rp), intent(in) :: y0
    real(rp), intent(in) :: vecX
    real(rp), intent(in) :: vecY
    real(rp), intent(in) :: A
    real(rp), intent(in) :: B
    real(rp), intent(in) :: C

    real(rp) :: denom

    denom = A*vecX + B*vecY

    if (abs(denom) < DELTA_MIN) then
      getMinDistanceToLine = -1.0_rp
      return
    end if

    getMinDistanceToLine = (-C - A*x0 - B*y0)/denom

    return

  end function

  integer function getZoneNumber(px, py, nCirc, cx, cy, Rad)
  !------------------------------------------------------------------------------------
  ! Function returns number of zone which point with coordinates (px, py) belongs to
  ! Geometry is outer polygon with inscribed into the outer polygon circles with centers
  ! in cx() and cy() and radii Rad(). It is assumed that point (px,py) is inside polygon
  ! Zone numbering starts from innermost to outermost zone
  !-------------------------------------------------------------------------------------
    use ConstantsMod
    implicit none
    real(rp), intent(in) :: px, py
    integer, intent(in) :: nCirc
    real(rp), intent(in) :: cx(:), cy(:), Rad(:)

    integer :: i

    do i = 1, nCirc
      if ( (px - cx(i))**2 + (py - cy(i))**2 - Rad(i)**2 < 0) then
        getZoneNumber = i
        return
      end if
    end do

    getZoneNumber = nCirc + 1
    return

  end function

  function r8_epsilon()

!*********************************************************************72
!
!c R8_EPSILON returns the R8 roundoff unit.
!
!  Discussion:
!
!    The roundoff unit is a number R which is a power of 2 with the
!    property that, to the precision of the computer's arithmetic,
!      1 .lt. 1 + R
!    but
!      1 = ( 1 + R / 2 )
!
!    FORTRAN90 provides the superior library routine
!
!      EPSILON ( X )
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    01 September 2012
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Output, real(rp) R8_EPSILON, the R8 roundoff unit.
!
      implicit none

      real(rp) r8_epsilon

      r8_epsilon = 2.220446049250313D-016

      return
  end function r8_epsilon

  function lrline ( xu, yu, xv1, yv1, xv2, yv2, dv )

!*********************************************************************72
!
!c LRLINE determines if a point is left of, right or, or on a directed line.
!
!  Discussion:
!
!    The directed line is parallel to, and at a signed distance DV from
!    a directed base line from (XV1,YV1) to (XV2,YV2).
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    03 June 2009
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    This FORTRAN77 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, real(rp) XU, YU, the coordinates of the point whose
!    position relative to the directed line is to be determined.
!
!    Input, real(rp) XV1, YV1, XV2, YV2, the coordinates of two points
!    that determine the directed base line.
!
!    Input, real(rp) DV, the signed distance of the directed line
!    from the directed base line through the points (XV1,YV1) and (XV2,YV2).
!    DV is positive for a line to the left of the base line.
!
!    Output, integer ( kind = 4 ) LRLINE, the result:
!    +1, the point is to the right of the directed line;
!     0, the point is on the directed line;
!    -1, the point is to the left of the directed line.
!
      implicit none

      real(rp) dv
      real(rp) dx
      real(rp) dxu
      real(rp) dy
      real(rp) dyu
      integer ( kind = 4 ) lrline
!      real(rp) r8_epsilon
      real(rp) t
      real(rp) tol
      real(rp) tolabs
      real(rp) xu
      real(rp) xv1
      real(rp) xv2
      real(rp) yu
      real(rp) yv1
      real(rp) yv2

      tol = 100.0_rp * r8_epsilon()

      dx = xv2 - xv1
      dy = yv2 - yv1
      dxu = xu - xv1
      dyu = yu - yv1

      tolabs = tol * max ( abs ( dx ), abs ( dy ), abs ( dxu ), &
       abs ( dyu ), abs ( dv ) )

      t = dy * dxu - dx * dyu + dv * sqrt ( dx * dx + dy * dy )

      if ( tolabs .lt. t ) then
        lrline = 1
      else if ( - tolabs .le. t ) then
        lrline = 0
      else
        lrline = -1
      end if

      return
  end function lrline

  subroutine r8tris2 ( node_num, node_xy, triangle_num, triangle_node, triangle_neighbor )

!*********************************************************************72
!
!c R8TRIS2 constructs a Delaunay triangulation of 2D vertices.
!
!  Discussion:
!
!    The routine constructs the Delaunay triangulation of a set of 2D vertices
!    using an incremental approach and diagonal edge swaps.  Vertices are
!    first sorted in lexicographically increasing (X,Y) order, and
!    then are inserted one at a time from outside the convex hull.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    08 June 2009
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, integer NODE_NUM, the number of nodes.
!
!    Input/output, real ( rp ) NODE_XY(2,NODE_NUM), the coordinates
!    of the nodes.  On output, the vertices have been sorted into
!    dictionary order.
!
!    Output, integer TRIANGLE_NUM, the number of triangles in the
!    triangulation;  TRIANGLE_NUM is equal to 2*NODE_NUM - NB - 2, where NB is
!    the number of boundary vertices.
!
!    Output, integer TRIANGLE_NODE(3,TRIANGLE_NUM), the nodes that
!    make up each triangle.  The elements are indices of P.  The vertices of
!    the triangles are in counter clockwise order.
!
!    Output, integer TRIANGLE_NEIGHBOR(3,TRIANGLE_NUM), the
!    triangle neighbor list.  Positive elements are indices of TIL; negative
!    elements are used for links of a counter clockwise linked list of boundary
!    edges;  LINK = -(3*I + J-1) where I, J = triangle, edge index;
!    TRIANGLE_NEIGHBOR(J,I) refers to the neighbor along edge from vertex J
!    to J+1 (mod 3).
!
      implicit none

      integer dim_num
      parameter ( dim_num = 2 )
      integer node_num

      real(rp) cmax
      integer dim
      integer e
      integer i
      integer ierr
      integer indx(node_num)
      integer j
      integer k
      integer l
      integer ledg
      integer lr
!      integer lrline
      integer ltri
      integer m
      integer m1
      integer m2
      integer n
      real(rp) node_xy(dim_num,node_num)
!      real(rp) r8_epsilon
      integer redg
      integer rtri
      integer stack(node_num)
      integer t
      real(rp) tol
      integer top
      integer triangle_neighbor(3,node_num*2)
      integer triangle_num
      integer triangle_node(3,node_num*2)

      tol = 100.0_rp * r8_epsilon()

      ierr = 0
!
!  Sort the vertices by increasing (x,y).
!
      call r82vec_sort_heap_index_a ( node_num, node_xy, indx )

      call r82vec_permute ( node_num, indx, node_xy )
!
!  Make sure that the data nodes are "reasonably" distinct.
!
      m1 = 1

      do i = 2, node_num

        m = m1
        m1 = i

        k = 0

        do j = 1, dim_num

          cmax = max ( abs ( node_xy(j,m) ), abs ( node_xy(j,m1) ) )

          if ( tol * ( cmax + 1.0D+00 ) &
              .lt. abs ( node_xy(j,m) - node_xy(j,m1) ) ) then
            k = j
            go to 10
          end if

        end do

10      continue

        if ( k .eq. 0 ) then
          write ( *, '(a)' ) ' '
          write ( *, '(a)' ) 'R8TRIS2 - Fatal error!'
          write ( *, '(a,i8)' ) '  Fails for point number I = ', i
          write ( *, '(a,i8)' ) '  M = ', m
          write ( *, '(a,i8)' ) '  M1 = ', m1
          write ( *, '(a,2g14.6)' ) &
           '  NODE_XY(M)  = ', ( node_xy(dim,m), dim = 1, dim_num )
          write ( *, '(a,2g14.6)' ) &
           '  NODE_XY(M1) = ', ( node_xy(dim,m1), dim = 1, dim_num )
          ierr = 224
          stop
        end if

      end do
!
!  Starting from nodes M1 and M2, search for a third point M that
!  makes a "healthy" triangle (M1,M2,M)
!
      m1 = 1
      m2 = 2
      j = 3

20    continue

        if ( node_num .lt. j ) then
          write ( *, '(a)' ) ' '
          write ( *, '(a)' ) 'R8TRIS2 - Fatal error!'
          ierr = 225
          stop
        end if

        m = j

        lr = lrline ( node_xy(1,m), node_xy(2,m), node_xy(1,m1), &
         node_xy(2,m1), node_xy(1,m2), node_xy(2,m2), 0.0_rp )

        if ( lr .ne. 0 ) then
          go to 30
        end if

        j = j + 1

      go to 20

30    continue
!
!  Set up the triangle information for (M1,M2,M), and for any other
!  triangles you created because points were collinear with M1, M2.
!
      triangle_num = j - 2

      if ( lr .eq. -1 ) then

        triangle_node(1,1) = m1
        triangle_node(2,1) = m2
        triangle_node(3,1) = m
        triangle_neighbor(3,1) = -3

        do i = 2, triangle_num

          m1 = m2
          m2 = i+1

          triangle_node(1,i) = m1
          triangle_node(2,i) = m2
          triangle_node(3,i) = m

          triangle_neighbor(1,i-1) = -3 * i
          triangle_neighbor(2,i-1) = i
          triangle_neighbor(3,i) = i - 1

        end do

        triangle_neighbor(1,triangle_num) = -3 * triangle_num - 1
        triangle_neighbor(2,triangle_num) = -5
        ledg = 2
        ltri = triangle_num

      else

        triangle_node(1,1) = m2
        triangle_node(2,1) = m1
        triangle_node(3,1) = m

        triangle_neighbor(1,1) = -4

        do i = 2, triangle_num

          m1 = m2
          m2 = i+1

          triangle_node(1,i) = m2
          triangle_node(2,i) = m1
          triangle_node(3,i) = m

          triangle_neighbor(3,i-1) = i
          triangle_neighbor(1,i) = -3 * i - 3
          triangle_neighbor(2,i) = i - 1

        end do

        triangle_neighbor(3,triangle_num) = -3 * triangle_num
        triangle_neighbor(2,1) = -3 * triangle_num - 2
        ledg = 2
        ltri = 1

      end if
!
!  Insert the vertices one at a time from outside the convex hull,
!  determine visible boundary edges, and apply diagonal edge swaps until
!  Delaunay triangulation of vertices (so far) is obtained.
!
      top = 0

      do i = j+1, node_num

        m = i
        m1 = triangle_node(ledg,ltri)

        if ( ledg <= 2 ) then
          m2 = triangle_node(ledg+1,ltri)
        else
          m2 = triangle_node(1,ltri)
        end if

        lr = lrline ( node_xy(1,m), node_xy(2,m), node_xy(1,m1), &
         node_xy(2,m1), node_xy(1,m2), node_xy(2,m2), 0.0_rp )

        if ( 0 .lt. lr ) then
          rtri = ltri
          redg = ledg
          ltri = 0
        else
          l = -triangle_neighbor(ledg,ltri)
          rtri = l / 3
          redg = mod ( l, 3 ) + 1
        end if

        call vbedg ( node_xy(1,m), node_xy(2,m), node_num, node_xy, &
         triangle_num, triangle_node, triangle_neighbor, ltri, ledg, &
         rtri, redg )

        n = triangle_num + 1
        l = -triangle_neighbor(ledg,ltri)

40      continue

          t = l / 3
          e = mod ( l, 3 ) + 1
          l = -triangle_neighbor(e,t)
          m2 = triangle_node(e,t)

          if ( e <= 2 ) then
            m1 = triangle_node(e+1,t)
          else
            m1 = triangle_node(1,t)
          end if

          triangle_num = triangle_num + 1
          triangle_neighbor(e,t) = triangle_num

          triangle_node(1,triangle_num) = m1
          triangle_node(2,triangle_num) = m2
          triangle_node(3,triangle_num) = m

          triangle_neighbor(1,triangle_num) = t
          triangle_neighbor(2,triangle_num) = triangle_num - 1
          triangle_neighbor(3,triangle_num) = triangle_num + 1

          top = top + 1

          if ( node_num .lt. top ) then
            ierr = 8
            write ( *, '(a)' ) ' '
            write ( *, '(a)' ) 'R8TRIS2 - Fatal error!'
            write ( *, '(a)' ) '  Stack overflow.'
            stop
          end if

          stack(top) = triangle_num

          if ( t .eq. rtri .and. e .eq. redg ) then
            go to 50
          end if

        go to 40

50      continue

        triangle_neighbor(ledg,ltri) = -3 * n - 1
        triangle_neighbor(2,n) = -3 * triangle_num - 2
        triangle_neighbor(3,triangle_num) = -l

        ltri = n
        ledg = 2

        call swapec ( m, top, ltri, ledg, node_num, node_xy, &
         triangle_num, triangle_node, triangle_neighbor, stack, &
         ierr )

        if ( ierr .ne. 0 ) then
          write ( *, '(a)' ) ' '
          write ( *, '(a)' ) 'R8TRIS2 - Fatal error!'
          write ( *, '(a)' ) '  Error return from SWAPEC.'
          stop
        end if

      end do
!
!  Now account for the sorting that we did.
!
      do i = 1, 3
        do j = 1, triangle_num
          triangle_node(i,j) = indx ( triangle_node(i,j) )
        end do
      end do

      call perm_inverse ( node_num, indx )

      call r82vec_permute ( node_num, indx, node_xy )

      return
  end subroutine r8tris2

  subroutine vbedg ( x, y, node_num, node_xy, triangle_num, &
       triangle_node, triangle_neighbor, ltri, ledg, rtri, redg )

!*********************************************************************72
!
!c VBEDG determines which boundary edges are visible to a point.
!
!  Discussion:
!
!    The point (X,Y) is assumed to be outside the convex hull of the
!    region covered by the 2D triangulation.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    04 June 2009
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    This FORTRAN77 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, real(rp) X, Y, the coordinates of a point outside the
!    convex hull of the current triangulation.
!
!    Input, integer NODE_NUM, the number of nodes.
!
!    Input, real(rp) NODE_XY(2,NODE_NUM), the coordinates of the nodes.
!
!    Input, integer TRIANGLE_NUM, the number of triangles.
!
!    Input, integer TRIANGLE_NODE(3,TRIANGLE_NUM), the triangle
!    incidence list.
!
!    Input, integer TRIANGLE_NEIGHBOR(3,TRIANGLE_NUM), the
!    triangle neighbor list; negative values are used for links of a
!    counter clockwise linked list of boundary edges;
!      LINK = -(3*I + J-1) where I, J = triangle, edge index.
!
!    Input/output, integer LTRI, LEDG.  If LTRI /= 0 then these
!    values are assumed to be already computed and are not changed, else they
!    are updated.  On output, LTRI is the index of boundary triangle to the
!    left of the leftmost boundary triangle visible from (X,Y), and LEDG is
!    the boundary edge of triangle LTRI to the left of the leftmost boundary
!    edge visible from (X,Y).  1 <= LEDG <= 3.
!
!    Input/output, integer RTRI.  On input, the index of the
!    boundary triangle to begin the search at.  On output, the index of the
!    rightmost boundary triangle visible from (X,Y).
!
!    Input/output, integer REDG, the edge of triangle RTRI that
!    is visible from (X,Y).  1 <= REDG <= 3.
!
      implicit none

      integer dim_num
      parameter ( dim_num = 2 )
      integer node_num
      integer triangle_num

      integer a
      integer b
      integer e
      integer l
      logical ldone
      integer ledg
      integer lr
!      integer lrline
      integer ltri
      real(rp) node_xy(2,node_num)
      integer redg
      integer rtri
      integer t
      integer triangle_node(3,triangle_num)
      integer triangle_neighbor(3,triangle_num)
      real(rp) x
      real(rp) y
!
!  Find the rightmost visible boundary edge using links, then possibly
!  leftmost visible boundary edge using triangle neighbor information.
!
      if ( ltri .eq. 0 ) then
        ldone = .false.
        ltri = rtri
        ledg = redg
      else
        ldone = .true.
      end if

10    continue

        l = - triangle_neighbor(redg,rtri)
        t = l / 3
        e = mod ( l, 3 ) + 1
        a = triangle_node(e,t)

        if ( e .le. 2 ) then
          b = triangle_node(e+1,t)
        else
          b = triangle_node(1,t)
        end if

        lr = lrline ( x, y, node_xy(1,a), node_xy(2,a), node_xy(1,b), &
         node_xy(2,b), 0.0_rp )

        if ( lr .le. 0 ) then
          go to 20
        end if

        rtri = t
        redg = e

      go to 10

20    continue

      if ( ldone ) then
        return
      end if

      t = ltri
      e = ledg

30    continue

        b = triangle_node(e,t)
        e = e - 1
        e = i4_wrap ( e, 1, 3 )

40      continue

        if ( 0 .lt. triangle_neighbor(e,t) ) then

          t = triangle_neighbor(e,t)

          if ( triangle_node(1,t) .eq. b ) then
            e = 3
          else if ( triangle_node(2,t) .eq. b ) then
            e = 1
          else
            e = 2
          end if

          go to 40

        end if

        a = triangle_node(e,t)

        lr = lrline ( x, y, node_xy(1,a), node_xy(2,a), node_xy(1,b), &
         node_xy(2,b), 0.0_rp )

        if ( lr .le. 0 ) then
          go to 50
        end if

      go to 30

50    continue

      ltri = t
      ledg = e

      return
  end subroutine vbedg

  function diaedg ( x0, y0, x1, y1, x2, y2, x3, y3 )
!*****************************************************************************80
!
!c DIAEDG chooses a diagonal edge.
!
!  Discussion:
!
!    The routine determines whether 0--2 or 1--3 is the diagonal edge
!    that should be chosen, based on the circumcircle criterion, where
!    (X0,Y0), (X1,Y1), (X2,Y2), (X3,Y3) are the vertices of a simple
!    quadrilateral in counterclockwise order.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    03 June 2009
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    This FORTRAN77 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, real(rp) X0, Y0, X1, Y1, X2, Y2, X3, Y3, the
!    coordinates of the vertices of a quadrilateral, given in
!    counter clockwise order.
!
!    Output, integer DIAEDG, chooses a diagonal:
!    +1, if diagonal edge 02 is chosen;
!    -1, if diagonal edge 13 is chosen;
!     0, if the four vertices are cocircular.
!
      implicit none

      real(rp) ca
      real(rp) cb
      integer diaedg
      real(rp) dx10
      real(rp) dx12
      real(rp) dx30
      real(rp) dx32
      real(rp) dy10
      real(rp) dy12
      real(rp) dy30
      real(rp) dy32
!      real(rp) r8_epsilon
      real(rp) s
      real(rp) tol
      real(rp) tola
      real(rp) tolb
      real(rp) x0
      real(rp) x1
      real(rp) x2
      real(rp) x3
      real(rp) y0
      real(rp) y1
      real(rp) y2
      real(rp) y3

      tol = 100.0_rp * r8_epsilon ( )

      dx10 = x1 - x0
      dy10 = y1 - y0
      dx12 = x1 - x2
      dy12 = y1 - y2
      dx30 = x3 - x0
      dy30 = y3 - y0
      dx32 = x3 - x2
      dy32 = y3 - y2

      tola = tol * max ( abs ( dx10 ), abs ( dy10 ), &
       abs ( dx30 ), abs ( dy30 ) )

      tolb = tol * max ( abs ( dx12 ), abs ( dy12 ), &
       abs ( dx32 ), abs ( dy32 ) )

      ca = dx10 * dx30 + dy10 * dy30
      cb = dx12 * dx32 + dy12 * dy32

      if ( tola .lt. ca .and. tolb .lt. cb ) then

        diaedg = - 1

      else if ( ca .lt. - tola .and. cb .lt. - tolb ) then

        diaedg = 1

      else

        tola = max ( tola, tolb )
        s = ( dx10 * dy30 - dx30 * dy10 ) * cb &
         + ( dx32 * dy12 - dx12 * dy32 ) * ca

        if ( tola .lt. s ) then
          diaedg = - 1
        else if ( s .lt. - tola ) then
          diaedg = 1
        else
          diaedg = 0
        end if

      end if

      return
  end function diaedg

  subroutine swapec ( i, top, btri, bedg, node_num, node_xy, &
       triangle_num, triangle_node, triangle_neighbor, stack, ierr )

!*********************************************************************72
!
!c SWAPEC swaps diagonal edges until all triangles are Delaunay.
!
!  Discussion:
!
!    The routine swaps diagonal edges in a 2D triangulation, based on
!    the empty circumcircle criterion, until all triangles are Delaunay,
!    given that I is the index of the new vertex added to the triangulation.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    03 June 2009
!
!  Author:
!
!    Original FORTRAN77 version by Barry Joe.
!    This FORTRAN77 version by John Burkardt.
!
!  Reference:
!
!    Barry Joe,
!    GEOMPACK - a software package for the generation of meshes
!    using geometric algorithms,
!    Advances in Engineering Software,
!    Volume 13, pages 325-331, 1991.
!
!  Parameters:
!
!    Input, integer I, the index of the new vertex.
!
!    Input/output, integer TOP, the index of the top of the stack.
!    On output, TOP is zero.
!
!    Input/output, integer BTRI, BEDG; on input, if positive, are
!    the triangle and edge indices of a boundary edge whose updated indices
!    must be recorded.  On output, these may be updated because of swaps.
!
!    Input, integer NODE_NUM, the number of nodes.
!
!    Input, real(rp) NODE_XY(2,NODE_NUM), the coordinates of the nodes.
!
!    Input, integer TRIANGLE_NUM, the number of triangles.
!
!    Input/output, integer TRIANGLE_NODE(3,TRIANGLE_NUM), the
!    triangle incidence list.  May be updated on output because of swaps.
!
!    Input/output, integer TRIANGLE_NEIGHBOR(3,TRIANGLE_NUM), the
!    triangle neighbor list; negative values are used for links of the
!    counter-clockwise linked list of boundary edges;  May be updated on output
!    because of swaps.
!      LINK = -(3*I + J-1) where I, J = triangle, edge index.
!
!    Workspace, integer STACK(MAXST); on input, entries 1 through TOP
!    contain the indices of initial triangles (involving vertex I)
!    put in stack; the edges opposite I should be in interior;  entries
!    TOP+1 through MAXST are used as a stack.
!
!    Output, integer IERR is set to 8 for abnormal return.
!
      implicit none

      integer dim_num
      parameter ( dim_num = 2 )
      integer node_num
      integer triangle_num

      integer a
      integer b
      integer bedg
      integer btri
      integer c
!      integer diaedg
      integer e
      integer ee
      integer em1
      integer ep1
      integer f
      integer fm1
      integer fp1
      integer i
      integer ierr
      integer l
      real(rp) node_xy(dim_num,node_num)
      integer r
      integer s
      integer stack(node_num)
      integer swap
      integer t
      integer top
      integer triangle_node(3,triangle_num)
      integer triangle_neighbor(3,triangle_num)
      integer tt
      integer u
      real(rp) x
      real(rp) y
!
!  Determine whether triangles in stack are Delaunay, and swap
!  diagonal edge of convex quadrilateral if not.
!
      x = node_xy(1,i)
      y = node_xy(2,i)

10    continue

        if ( top .le. 0 ) then
          go to 40
        end if

        t = stack(top)
        top = top - 1

        if ( triangle_node(1,t) .eq. i ) then
          e = 2
          b = triangle_node(3,t)
        else if ( triangle_node(2,t) .eq. i ) then
          e = 3
          b = triangle_node(1,t)
        else
          e = 1
          b = triangle_node(2,t)
        end if

        a = triangle_node(e,t)
        u = triangle_neighbor(e,t)

        if ( triangle_neighbor(1,u) .eq. t ) then
          f = 1
          c = triangle_node(3,u)
        else if ( triangle_neighbor(2,u) .eq. t ) then
          f = 2
          c = triangle_node(1,u)
        else
          f = 3
          c = triangle_node(2,u)
        end if

        swap = diaedg ( x, y, node_xy(1,a), node_xy(2,a), node_xy(1,c), &
         node_xy(2,c), node_xy(1,b), node_xy(2,b) )

        if ( swap .eq. 1 ) then

          em1 = i4_wrap ( e - 1, 1, 3 )
          ep1 = i4_wrap ( e + 1, 1, 3 )
          fm1 = i4_wrap ( f - 1, 1, 3 )
          fp1 = i4_wrap ( f + 1, 1, 3 )

          triangle_node(ep1,t) = c
          triangle_node(fp1,u) = i

          r = triangle_neighbor(ep1,t)
          s = triangle_neighbor(fp1,u)

          triangle_neighbor(ep1,t) = u
          triangle_neighbor(fp1,u) = t
          triangle_neighbor(e,t) = s
          triangle_neighbor(f,u) = r

          if ( 0 .lt. triangle_neighbor(fm1,u) ) then
            top = top + 1
            stack(top) = u
          end if

          if ( 0 .lt. s ) then

            if ( triangle_neighbor(1,s) .eq. u ) then
              triangle_neighbor(1,s) = t
            else if ( triangle_neighbor(2,s) .eq. u ) then
              triangle_neighbor(2,s) = t
            else
              triangle_neighbor(3,s) = t
            end if

            top = top + 1

            if ( node_num .lt. top ) then
              ierr = 8
              return
            end if

            stack(top) = t

          else

            if ( u .eq. btri .and. fp1 .eq. bedg ) then
              btri = t
              bedg = e
            end if

            l = - ( 3 * t + e - 1 )
            tt = t
            ee = em1

20          continue

            if ( 0 < triangle_neighbor(ee,tt) ) then

              tt = triangle_neighbor(ee,tt)

              if ( triangle_node(1,tt) .eq. a ) then
                ee = 3
              else if ( triangle_node(2,tt) .eq. a ) then
                ee = 1
              else
                ee = 2
              end if

              go to 20

            end if

            triangle_neighbor(ee,tt) = l

          end if

          if ( 0 .lt. r ) then

            if ( triangle_neighbor(1,r) .eq. t ) then
              triangle_neighbor(1,r) = u
            else if ( triangle_neighbor(2,r) .eq. t ) then
              triangle_neighbor(2,r) = u
            else
              triangle_neighbor(3,r) = u
            end if

          else

            if ( t .eq. btri .and. ep1 .eq. bedg ) then
              btri = u
              bedg = f
            end if

            l = - ( 3 * u + f - 1 )
            tt = u
            ee = fm1

30          continue

            if ( 0 .lt. triangle_neighbor(ee,tt) ) then

              tt = triangle_neighbor(ee,tt)

              if ( triangle_node(1,tt) .eq. b ) then
                ee = 3
              else if ( triangle_node(2,tt) .eq. b ) then
                ee = 1
              else
                ee = 2
              end if

              go to 30

            end if

            triangle_neighbor(ee,tt) = l

          end if

        end if

      go to 10

40    continue

      return
  end subroutine swapec

  subroutine triangulate_polygon(nVert, vertX, vertY, nTriang, triangles)
  !
  ! Wrapper function for calling Delaunay triangulation subroutine
  !
    implicit none

    integer :: nVert
    real(rp) :: vertX(:)
    real(rp) :: vertY(:)
    real(rp), allocatable :: triangles(:,:,:) !Triangles (2,3,nTriang) (x,y;1:3,1:nTriang)
    integer :: nTriang


    integer :: node_num
    real(rp) :: node_xy(2,nVert)
    integer :: triangle_num
    integer :: triangle_node(3,2*nVert)
    integer :: triangle_neighbor(3,2*nVert)
    integer :: i

    node_num = nVert
    do i = 1, node_num
        node_xy(1,i) = vertX(i)
        node_xy(2,i) = vertY(i)
    end do

    call r8tris2(node_num, node_xy, triangle_num, triangle_node, triangle_neighbor)

    nTriang = triangle_num

    allocate(triangles(2,3,nTriang))
    triangles = 0.0_rp

    do i = 1, triangle_num
        triangles(1,1,i) = node_xy(1,triangle_node(1,i))
        triangles(2,1,i) = node_xy(2,triangle_node(1,i))
        triangles(1,2,i) = node_xy(1,triangle_node(2,i))
        triangles(2,2,i) = node_xy(2,triangle_node(2,i))
        triangles(1,3,i) = node_xy(1,triangle_node(3,i))
        triangles(2,3,i) = node_xy(2,triangle_node(3,i))
    end do

    return

  end subroutine

  subroutine r82vec_sort_heap_index_a ( n, a, indx )

  !*****************************************************************************80
  !
  !! R82VEC_SORT_HEAP_INDEX_A does an indexed heap ascending sort of an R82VEC.
  !
  !  Discussion:
  !
  !    The sorting is not actually carried out.  Rather an index array is
  !    created which defines the sorting.  This array may be used to sort
  !    or index the array, or to sort or index related arrays keyed on the
  !    original array.
  !
  !    Once the index array is computed, the sorting can be carried out
  !    "implicitly:
  !
  !      A(1:2,INDX(I)), I = 1 to N is sorted,
  !
  !    or explicitly, by the call
  !
  !      call R82VEC_PERMUTE ( N, A, INDX )
  !
  !    after which A(1:2,I), I = 1 to N is sorted.
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    11 January 2004
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    Input, integer ( kind = 4 ) N, the number of entries in the array.
  !
  !    Input, real ( rp ) A(2,N), an array to be index-sorted.
  !
  !    Output, integer ( kind = 4 ) INDX(N), the sort index.  The
  !    I-th element of the sorted array is A(1:2,INDX(I)).
  !
    implicit none
    integer ( kind = 4 ) n

    real ( rp ) a(2,n)
    real ( rp ) aval(2)
    integer ( kind = 4 ) i
    integer ( kind = 4 ) indx(n)
    integer ( kind = 4 ) indxt
    integer ( kind = 4 ) ir
    integer ( kind = 4 ) j
    integer ( kind = 4 ) l

    if ( n < 1 ) then
      return
    end if

    if ( n == 1 ) then
      indx(1) = 1
      return
    end if

    call i4vec_indicator ( n, indx )

    l = n / 2 + 1
    ir = n

    do

      if ( 1 < l ) then

        l = l - 1
        indxt = indx(l)
        aval(1:2) = a(1:2,indxt)

      else

        indxt = indx(ir)
        aval(1:2) = a(1:2,indxt)
        indx(ir) = indx(1)
        ir = ir - 1

        if ( ir == 1 ) then
          indx(1) = indxt
          exit
        end if
      end if

      i = l
      j = l + l

      do while ( j <= ir )

        if ( j < ir ) then
          if (   a(1,indx(j)) <  a(1,indx(j+1)) .or. &
               ( a(1,indx(j)) == a(1,indx(j+1)) .and. &
                 a(2,indx(j)) <  a(2,indx(j+1)) ) ) then
            j = j + 1
          end if
        end if

        if (   aval(1) <  a(1,indx(j)) .or. &
             ( aval(1) == a(1,indx(j)) .and. &
               aval(2) <  a(2,indx(j)) ) ) then
          indx(i) = indx(j)
          i = j
          j = j + j
        else
          j = ir + 1
        end if

      end do

      indx(i) = indxt
    end do

    return
  end subroutine

  subroutine r82vec_permute ( n, p, a )

  !*****************************************************************************80
  !
  !! R82VEC_PERMUTE permutes a R82 vector in place.
  !
  !  Discussion:
  !
  !    This routine permutes an array of real "objects", but the same
  !    logic can be used to permute an array of objects of any arithmetic
  !    type, or an array of objects of any complexity.  The only temporary
  !    storage required is enough to store a single object.  The number
  !    of data movements made is N + the number of cycles of order 2 or more,
  !    which is never more than N + N/2.
  !
  !  Example:
  !
  !    Input:
  !
  !      N = 5
  !      P = (   2,    4,    5,    1,    3 )
  !      A = ( 1.0,  2.0,  3.0,  4.0,  5.0 )
  !          (11.0, 22.0, 33.0, 44.0, 55.0 )
  !
  !    Output:
  !
  !      A    = (  2.0,  4.0,  5.0,  1.0,  3.0 )
  !             ( 22.0, 44.0, 55.0, 11.0, 33.0 ).
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    28 March 2011
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    Input, integer ( kind = 4 ) N, the number of objects.
  !
  !    Input, integer ( kind = 4 ) P(N), the permutation.  P(I) = J means
  !    that the I-th element of the output array should be the J-th
  !    element of the input array.  P must be a legal permutation
  !    of the integers from 1 to N, otherwise the algorithm will
  !    fail catastrophically.
  !
  !    Input/output, real ( rp ) A(2,N), the array to be permuted.
  !
    implicit none

    integer ( kind = 4 ) n

    real ( rp ) a(2,n)
    real ( rp ) a_temp(2)
    integer ( kind = 4 ) iget
    integer ( kind = 4 ) iput
    integer ( kind = 4 ) istart
    integer ( kind = 4 ) p(n)
  !
  !  Search for the next element of the permutation that has not been used.
  !
    do istart = 1, n

      if ( p(istart) < 0 ) then

        cycle

      else if ( p(istart) == istart ) then

        p(istart) = - p(istart)
        cycle

      else

        a_temp(1:2) = a(1:2,istart)
        iget = istart
  !
  !  Copy the new value into the vacated entry.
  !
        do

          iput = iget
          iget = p(iget)

          p(iput) = - p(iput)

          if ( iget < 1 .or. n < iget ) then
            write ( *, '(a)' ) ' '
            write ( *, '(a)' ) 'R82VEC_PERMUTE - Fatal error!'
            stop 1
          end if

          if ( iget == istart ) then
            a(1:2,iput) = a_temp(1:2)
            exit
          end if

          a(1:2,iput) = a(1:2,iget)

        end do

      end if

    end do
  !
  !  Restore the signs of the entries.
  !
    p(1:n) = -p(1:n)

    return
  end subroutine

  subroutine perm_inverse ( n, p )

  !*****************************************************************************80
  !
  !! PERM_INVERSE inverts a permutation "in place".
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    25 July 2000
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    Input, integer ( kind = 4 ) N, the number of objects being permuted.
  !
  !    Input/output, integer ( kind = 4 ) P(N), the permutation, in standard
  !    index form.  On output, P describes the inverse permutation
  !
    implicit none

    integer ( kind = 4 ) n

    integer ( kind = 4 ) i
    integer ( kind = 4 ) i0
    integer ( kind = 4 ) i1
    integer ( kind = 4 ) i2
    integer ( kind = 4 ) is
    integer ( kind = 4 ) p(n)

    if ( n <= 0 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'PERM_INVERSE - Fatal error!'
      write ( *, '(a,i8)' ) '  Input value of N = ', n
      stop 1
    end if

    is = 1

    do i = 1, n

      i1 = p(i)

      do while ( i < i1 )
        i2 = p(i1)
        p(i1) = -i2
        i1 = i2
      end do

      is = -sign ( 1, p(i) )
      p(i) = sign ( p(i), is )

    end do

    do i = 1, n

      i1 = -p(i)

      if ( 0 <= i1 ) then

        i0 = i

        do

          i2 = p(i1)
          p(i1) = i0

          if ( i2 < 0 ) then
            exit
          end if

          i0 = i1
          i1 = i2

        end do

      end if

    end do

    return
  end subroutine

  subroutine i4vec_indicator ( n, a )

  !*****************************************************************************80
  !
  !! I4VEC_INDICATOR sets an integer vector to the indicator vector.
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    09 November 2000
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    Input, integer ( kind = 4 ) N, the number of elements of A.
  !
  !    Output, integer ( kind = 4 ) A(N), the array to be initialized.
  !
    implicit none

    integer ( kind = 4 ) n

    integer ( kind = 4 ) a(n)
    integer ( kind = 4 ) i

    do i = 1, n
      a(i) = i
    end do

    return
  end subroutine

end module GeometryMod
