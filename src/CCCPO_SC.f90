subroutine CCCPO_SC(inputFile)
  !---------------------------------------------
  ! Author: Dzianis Litskevich
  ! Date  : 06.09.2016
  ! Main subroutine, entry point of the program.
  !---------------------------------------------
!  use pyinterfacemod, only : calculate_collision_probabilities, print_beta, print_gamma, print_u, print_v
  use ReadWriteMod, only : ReadInit, intToChar, Homogenise_XS, PrintOutput
  use ConstantsMod, only : PI
  use Config, only : str_len
  use GeometryMod,  only : GetVerticesPolygon, GetNormVecPolygon, GetNumberOfSegments, GetSegmentBoundaries, &
    GetTangentPoints, triangulate_polygon
  use DrawMod
  use Config, only : str_len, viewBox_centerX, viewBox_centerY
  use MathMod, only : readGaussPoints, GetZoneVolumes
  use CPMod, only : PhiBoundariesBeta, PhiBoundariesGamma, PhiBoundariesU, ChordsGamma, ChordsU, ChordsBetaHash, &
                    Beta_to_array, V_to_array, G_to_array, b_mg, v_mg, g_mg, u_mg, beta_cp, g_cp, n_beta_cp,     &
                    n_g_cp, n_v_cp, v_cp
  use IntegrateCPMod, only : IntegrateGamma, IntegrateU, NormaliseGamma, &
                             IntegrateV, NormalizeU, OrthogonalizeU,     &
                             OrthogonaliseGamma, IntegrateBetaHash
  use OrtMod, only : GetOrthogonalCoefficients
  use GlobalMod, only : nChordBet, nSegChordBet, lenChordBet, wghtChordBet, zoneChordBet, nBeta_key, nBetaElem, &
                        inSideBet, inSegBet, inSectBet, outSideBet, outSectBet, outSegBet, thetaSectBet,        &
                        nSegChordGam, lenChordGam, wghtChordGam, zoneChordGam, xChordGam, yChordGam, inSideGam, &
                        inSegGam, inSectGam, inZoneGam, inHarmGam, nChordGam, nGamma, nGammaElem, nChordU,      &
                        nSegChordU, lenChordU, wghtChordU, xChordU, yChordU, zoneFromU, zoneToU, harmFromU,     &
                        harmToU, nU, nUElem, zoneChordU, Gamm, G, V, segSdArr, ort, U
  use GlobalAssemblyMod
  use NeighboursMod, only : GetNeighbours, AddNeighboursGap, Get_cell_edge_numbers
  use FluxMod, only : GetFluxAssembly, MergeFlux, GetFluxAssemblyMG, GetFluxAssemblyMG_new, TransportSweepMG, &
                      TransportSweepHelios
  use DebugMod
  use VTKMod, only : PlotFlux, WriteMeshFluxToCSV
  use MathMod, only : PnormPnorm, RingIntegral, PolygonRingIntegral
  use ArrayMod, only : SetArraySize2DReal, SetArraySize2DInt

  implicit none

  character(str_len), intent(in) :: inputFile

  character(str_len) :: nameOfSVG

  integer :: i, i1, j, s, n, m, k, k1, l, ir, imt, it
  integer :: cr, it1, it2
  real(rp) :: x0, y0, x1, y1, faver
  real(rp) :: fr
  character(str_len) :: vtk_output_file
  character(str_len) :: clr
  real(rp), allocatable :: lenSeg(:,:)
  real(rp), allocatable :: vol_merged(:)
  real(rp), allocatable :: normVecX(:), normVecY(:)
  integer, allocatable :: nAngBeta(:), nAngGamm(:), nAngU(:)
  real(rp), allocatable :: phiBeta(:,:), phiGamm(:,:), phiU(:,:)
  real(rp), allocatable :: cx(:), cy(:)
  real(rp), allocatable :: Beta(:), brc(:)

!  type(bmg), allocatable :: b_mg(:)
!  type(gmg), allocatable :: g_mg(:)
!  type(vmg), allocatable :: v_mg(:)
!  type(umg), allocatable :: u_mg(:)
!  integer :: nChordU         ! number of chords for integration of region-to-region CP (U)
!  integer :: nSegChordU(:)   ! number of segments along the chord for integration U
!  real(rp) :: lenChordU(:,:)  ! length of segments along the chord
!  real(rp), allocatable, intent(out) :: wghtChordU(:)   ! weight of the U chord
!  integer, allocatable, intent(out) :: zoneChordU(:,:) ! zones' numbers along the chord (for definition of the materials)
!  real(rp), allocatable, intent(out) :: xChordU(:,:)    ! x-coordinates of the segments along the chord
!  real(rp), allocatable, intent(out) :: yChordU(:,:)    ! y-coordinates of the segments along the chord
!  integer, allocatable, intent(out) :: zoneFromU(:)    ! number of zone from where neutron going out
!  integer, allocatable, intent(out) :: zoneToU(:)      ! number of zone where neutron goes
!  integer, allocatable, intent(out) :: harmFromU(:)    ! number of harmonic from
!  integer, allocatable, intent(out) :: harmToU(:)      ! number of harmonic to
!  integer, allocatable, intent(out) :: nU(:)           ! number of U element where chord contributes
!  integer, intent(out)              :: nUElem          ! total number of U elements
  real(rp), allocatable :: flux(:)
  integer :: nbetaelem_max
  integer :: nZoneMerged
  integer :: icell, icell1, ig, ig1

!  real(rp), allocatable :: flux_fuel_openmc(:)
!  real(rp), allocatable :: flux_aver_openmc(:)

  real(rp), allocatable :: triangles(:,:,:), b(:,:)
  real(rp) :: R1, R2, ans, err_max_fuel, err_max_aver
  integer :: nTriang

  call ReadInit(inputFile)

  allocate(neighCell(maxval(nsides),nCell))
  allocate(neighSide(maxval(nsides),nCell))
  allocate(fact(nCell))
  neighCell = 0
  neighSide = 0
  fact = 0.0_rp
  call GetNeighbours(nCell, assembly_type, sym, nRow, nCol, neighCell, neighSide, fact)
  call Get_cell_edge_numbers()

  if (withGap) then
    call AddNeighboursGap(assembly_type, nrow, sym, nCellTypes, nCell, neighCell, neighSide, fact, kart)
  end if

  !---------------------------------------------
  ! Reading Gauss points
  !---------------------------------------------
  call readGaussPoints()

  !---------------------------------------------
  ! Ray tracing and CP evaluation for different
  ! cell types
  !---------------------------------------------
  allocate(cell_vol(nCellTypes,maxval(ncirc)+1))
  cell_vol = 0.0_rp
  allocate(normVecX(maxval(nsides)))
  allocate(normVecY(maxval(nsides)))
  normVecX = 0.0_rp
  normVecY = 0.0_rp
  allocate(cell_nsegments(nCellTypes,maxval(nsides)))
  cell_nsegments = 0
  do icell = 1, nCellTypes
    call GetNumberOfSegments(cell_vertx(icell,:), cell_verty(icell,:), nsides(icell), dseg, cell_nsegments(icell,:))
  end do
  allocate(cell_segboundx(nCellTypes,maxval(nsides),maxval(cell_nsegments)+1))
  allocate(cell_segboundy(nCellTypes,maxval(nsides),maxval(cell_nsegments)+1))
  allocate(cell_segsdarr(nCellTypes,maxval(nsides),maxval(cell_nsegments)+1))
  allocate(cell_ort(nCellTypes,maxval(nzones),maxval(nharms),maxval(nharms)))
  allocate(ort(maxval(nzones),maxval(nharms),maxval(nharms)))
  cell_segboundx = 0.0_rp
  cell_segboundy = 0.0_rp
  cell_segsdarr = 0.0_rp
  cell_ort = 0.0_rp
  ort = 0.0_rp
  allocate(cx(maxval(ncirc)))
  allocate(cy(maxval(ncirc)))
  cx = 0.0_rp
  cy = 0.0_rp
  ig = 1

  if (idraw > 0) then
    nameOfSVG = "images/Assembly.svg"
    call DrawAssembly(nameOfSVG, assembly_type, sym, nRow, nCol, kart, neighCell, neighSide, &
                      nzones, nsides, cell_vertx, cell_verty, cell_rad, dpitch, assmb_pitch)
  end if

  open(1329, file = "results/Ort_debug.out")

  do icell = 1, nCellTypes
    if (ncirc(icell) <= 0) then
      call GetZoneVolumes(nvert=nsides(icell), ncirc=ncirc(icell), x=cell_vertx(icell,:), &
                          y=cell_verty(icell,:), vol=cell_vol(icell,:))
    else
      call GetZoneVolumes(nvert=nsides(icell), ncirc=ncirc(icell), x=cell_vertx(icell,:), &
                          y=cell_verty(icell,:), r=cell_rad(icell,:), vol=cell_vol(icell,:))
    end if
    call GetOrthogonalCoefficients(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), &
                      ncirc(icell), cell_rad(icell,:), nzones(icell), nharms(icell),        &
                      cell_vol(icell,:), cell_ort(icell,:,:,:))

    do i = 1, nzones(icell)
      write(*,*) "Zone ", i
      write(1329, *) "Zone ", i
      do j = 1, nharms(icell)
        write(*,'(6F8.3)') (cell_ort(icell,i,j,k),k=1,nharms(icell))
        write(1329,'(6F23.16)') (cell_ort(icell,i,j,k),k=1,nharms(icell))
      end do
    end do

! Check orthogonal polynomials
    call triangulate_polygon(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), nTriang, triangles)
    allocate(b(nharms(icell),nharms(icell)))
    R1 = 0.0_rp
    R2 = 0.0_rp
    do i = 1, nzones(icell)
      write(*,*) "Zone ", i
      write(1329, *) "Zone ", i
      if (ncirc(icell) > 0) then
        if (i /= nzones(icell)) then
          R1 = R2
          R2 = cell_rad(icell,i)
        else
          R1 = cell_rad(icell,i-1)
          R2 = R1
        end if
      end if
      b(1:nharms(icell),1:nharms(icell)) = cell_ort(icell,i,1:nHarms(icell),1:nHarms(icell))
      do j = 1, nharms(icell)
        do k = 1, nharms(icell)
          if (i < nzones(icell)) then
            call RingIntegral(R1, R2, 0.0_rp, 2.0_rp*PI, PnormPnorm, j, k, b, ans)
          else
            call PolygonRingIntegral(nTriang, triangles, R1, PnormPnorm, j, k, b, ans)
          end if
          write(*,*) j, k, ans
          write(1329,'(2I4,1F8.3)') j, k, ans
        end do
      end do
    end do
    deallocate(b)
    deallocate(triangles)
  end do

  close(1329)

!  call calculate_collision_probabilities()
!  call print_beta(1, 1)
!
!
!  stop

  allocate(b_mg(nGroups))
  allocate(g_mg(nGroups))
  allocate(v_mg(nGroups))
  allocate(u_mg(nGroups))

  write(*,*) "*****************Starting simulations********************"
  do icell = 1, nCellTypes
!    if (.not. inMaterialMap(icell)) then
!      cycle
!    end if
    write(*, '(1a,1i3)') "Evaluating the CPs for cell type ", icell
    write(*, '(1a,1i3)') "    Number of regions      : ", nzones(icell)
    write(*, '(1a,1i3)') "    Number of harmonics    : ", nharms(icell)
    write(*, '(1a,1i3)') "    Number of sides        : ", nsides(icell)
    write(*, '(1a,1i3)') "    Max. number of segments: ", maxval(cell_nsegments(icell,:))
    write(*, '(1a,1i3)') "    Number of azim. sectors: ", nPhi
    write(*, '(1a,1i3)') "    Number of polar sectors: ", nTheta
    if (ncirc(icell) <= 0) then
      call GetZoneVolumes(nvert=nsides(icell), ncirc=ncirc(icell), x=cell_vertx(icell,:), &
                          y=cell_verty(icell,:), vol=cell_vol(icell,:))
    else
      call GetZoneVolumes(nvert=nsides(icell), ncirc=ncirc(icell), x=cell_vertx(icell,:), &
                          y=cell_verty(icell,:), r=cell_rad(icell,:), vol=cell_vol(icell,:))
    end if
    call GetNormVecPolygon(cell_vertx(icell,:), cell_verty(icell,:), nsides(icell), normVecX, normVecY)
    call GetSegmentBoundaries(cell_vertx(icell,:), cell_verty(icell,:), nsides(icell), cell_nsegments(icell,:), &
                              cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), cell_segsdarr(icell,:,:))

    if (idraw > 0) then
    !---------------------------------------
    ! Draw individual cells
    !---------------------------------------
      nameOfSVG = "images/Image" // trim(intToChar(icell)) // ".svg"
      call InitializeSVG(nameOfSVG)
      svgImageFactorX = max(getImageFactorX(cell_vertx(icell,1:nsides(icell))),getImageFactorY(cell_verty(icell,1:nsides(icell))))
      svgImageFactorY = svgImageFactorX
      call TransformImageSvg(nameOfSVG, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY)
      ! Draw outer polygon
      if (cell_mat(icell,nzones(icell)) <= 12) then
        clr = colours(cell_mat(icell,nzones(icell)))
      else
        clr = "random_colour"
      end if
      call DrawPolygon(nameOfSVG, nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), 1.0_rp, 1.0_rp, "black", clr)
      ! Draw circles
      do i = ncirc(icell), 1, -1
        if (cell_mat(icell,i) <= 12) then
          clr = colours(cell_mat(icell,i))
        else
          clr = "random_colour"
        end if
        call DrawCircle(nameOfSVG, 0.0_rp, 0.0_rp, cell_rad(icell,i), 1.0_rp, 1.0_rp, "black", clr)
      end do
      ! Draw segment's boundaries
      do i = 1, nsides(icell)
        call DrawPoint(nameOfSVG, cell_vertx(icell,i), cell_verty(icell,i), 3.0_rp, "black")
        do j = 2, cell_nsegments(icell,i)
          call DrawPoint(nameOfSVG, cell_segboundx(icell,i,j), cell_segboundy(icell,i,j), 2.0_rp,"red")
        end do
      end do
      call FinalizeSVG(nameOfSVG)
    end if

    call GetOrthogonalCoefficients(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), &
                      ncirc(icell), cell_rad(icell,:), nzones(icell), nharms(icell),        &
                      cell_vol(icell,:), cell_ort(icell,:,:,:))
    call PhiBoundariesBeta(nsides(icell), cell_nsegments(icell,:), cell_segboundx(icell,:,:), &
                           cell_segboundy(icell,:,:), normVecX, normVecY, ncirc(icell),       &
                           cell_rad(icell,:), cx, cy, phiSect, nPhi, cell_nphib(icell), &
                           nAngBeta, phiBeta)
    call PhiBoundariesGamma(nsides(icell), cell_nsegments(icell,:), cell_segboundx(icell,:,:), &
                            cell_segboundy(icell,:,:), normVecX, normVecY, ncirc(icell),       &
                            cell_rad(icell,:), cx, cy, phiSect, nPhi, cell_nphig(icell), &
                            nAngGamm, phiGamm)
    call PhiBoundariesU(nsides(icell), cell_nsegments(icell,:), cell_segboundx(icell,:,:), &
                        cell_segboundy(icell,:,:), normVecX, normVecY, ncirc(icell),       &
                        cell_rad(icell,:), cx, cy, cell_nphiu(icell), nAngU, phiU)
! Set of chords for following evaluation of beta probabilities
!    segSdArr = cell_segsdarr
    call ChordsBetaHash(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), cell_nsegments(icell,:), &
                        cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), cell_segsdarr(icell,:,:),   &
                        nPhi, phiSect, normVecX, normVecY, ncirc(icell), cell_rad(icell,:), cx, cy,       &
                        nAngBeta, phiBeta, cell_dcb(icell))
! Calculaton of the gamma chords
    call ChordsGamma(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), cell_nsegments(icell,:),     &
                     cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), cell_segsdarr(icell,:,:), nPhi, &
                     phiSect, normVecX, normVecY, ncirc(icell), cell_rad(icell,:), cx, cy, nAngGamm,       &
                     phiGamm, cell_dcg(icell), nChordGam, nSegChordGam, lenChordGam, wghtChordGam,         &
                     zoneChordGam, xChordGam, yChordGam, inSideGam, inSegGam, inSectGam, inZoneGam,        &
                     inHarmGam, nGamma, nGammaElem)
! Evaluation of the chords for U calculations
    call ChordsU(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), cell_nsegments(icell,:),           &
                 cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), nPhi, phiSect, normVecX, normVecY,    &
                 ncirc(icell), cell_rad(icell,:), cx, cy, nAngU, phiU, cell_dcu(icell), nChordU, nSegChordU, &
                 lenChordU, wghtChordU, zoneChordU, xChordU, yChordU, zoneFromU, zoneToU, harmFromU,         &
                 harmToU, nU, nUElem)
! Allocation and filling of the array lenSeg containing segment lengths on the sides of the cells
    if (.not. allocated(lenSeg)) allocate(lenSeg(nsides(icell),maxval(cell_nsegments)))
    lenSeg = 0.0_rp
    do s = 1, nsides(icell)
      do n = 1, cell_nsegments(icell,s)
        x0 = cell_segboundx(icell,s,n)
        y0 = cell_segboundy(icell,s,n)
        x1 = cell_segboundx(icell,s,n+1)
        y1 = cell_segboundy(icell,s,n+1)
        lenSeg(s,n) = sqrt((x1-x0)**2 + (y1-y0)**2)
      end do
    end do
! Calculation of the collision probabilities for each energy group
    do ig = 1, nGroups
      write(*,*) "  Group ", ig
! Integration of the Beta elements using previously calculated chords
      call IntegrateBetaHash(nChordBet, nSegChordBet, lenChordBet, wghtChordBet, zoneChordBet, cell_segsdarr(icell,:,:), &
                             cell_mat(icell,:), nBeta_key, STOT(:,ig), maxval(cell_nsegments), cell_type(icell), nBetaElem, &
                             Beta, brc)
      if (icell == 1 .and. ig == 1) then
        allocate(cell_nbetaelem(nCellTypes))
        allocate(cell_beta(nCellTypes,nBetaElem))
        allocate(cell_brc(nCellTypes,nBetaElem))
        allocate(cell_insidebet(nCellTypes,nBetaElem))
        allocate(cell_insegbet(nCellTypes,nBetaElem))
        allocate(cell_insectbet(nCellTypes,nBetaElem))
        allocate(cell_outsidebet(nCellTypes,nBetaElem))
        allocate(cell_outsegbet(nCellTypes,nBetaElem))
        allocate(cell_outsectbet(nCellTypes,nBetaElem))
        allocate(cell_thetasectbet(nCellTypes,nBetaElem))
      end if
      if (nBetaElem > size(cell_beta,2)) then
        call SetArraySize2DReal(cell_beta, nCellTypes, nBetaElem)
        call SetArraySize2DReal(cell_brc, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_insidebet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_insegbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_insectbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_outsidebet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_outsegbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_outsectbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_thetasectbet, nCellTypes, nBetaElem)
      end if

      cell_nbetaelem(icell) = nBetaElem
      cell_beta(icell,1:nBetaElem) = Beta(1:nBetaElem)
      cell_brc(icell,1:nBetaElem) = brc(1:nBetaElem)
      cell_insidebet(icell,1:nBetaElem) = inSideBet(1:nBetaElem)
      cell_insegbet(icell,1:nBetaElem) = inSegBet(1:nBetaElem)
      cell_insectbet(icell,1:nBetaElem) = inSectBet(1:nBetaElem)
      cell_outsidebet(icell,1:nBetaElem) = outSideBet(1:nBetaElem)
      cell_outsegbet(icell,1:nBetaElem) = outSegBet(1:nBetaElem)
      cell_outsectbet(icell,1:nBetaElem) = outSectBet(1:nBetaElem)
      cell_thetasectbet(icell,1:nBetaElem) = thetaSectBet(1:nBetaElem)
      if (allocated(Beta))         deallocate(Beta)
      if (allocated(inSideBet))    deallocate(inSideBet)
      if (allocated(inSegBet))     deallocate(inSegBet)
      if (allocated(inSectBet))    deallocate(inSectBet)
      if (allocated(outSideBet))   deallocate(outSideBet)
      if (allocated(outSegBet))    deallocate(outSegBet)
      if (allocated(outSectBet))   deallocate(outSectBet)
      if (allocated(thetaSectBet)) deallocate(thetaSectBet)

! Allocation of the Gamma array
      if (icell == 1 .and. ig == 1) then
        allocate(Gamm(nGammaElem))
      end if
! Integration of the elements of the gamma matrix
      if (icell == 1 .and. ig == 1) then
        allocate(cell_G(nCellTypes,maxval(nzones),maxval(nharms),maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
        cell_G = 0.0_rp
      end if
      Gamm = 0.0_rp
      if (.not. allocated(G)) allocate(G(maxval(nzones),maxval(nharms),maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
      ort = cell_ort(icell,:,:,:)
      call IntegrateGamma(nChordGam, nSegChordGam, lenChordGam, wghtChordGam, zoneChordGam, xChordGam, yChordGam, &
                          nsides(icell), cell_nsegments(icell,:), nzones(icell), cell_mat(icell,:), STOT(:,ig),   &
                          nharms(icell))
      cell_G(icell,:,:,:,:,:,:) = G
!      call NormaliseGamma(cell_G(icell,:,:,:,:,:,:), cell_beta(icell,:), cell_outsegbet(icell,:),         &
!                          cell_outsectbet(icell,:), cell_outsidebet(icell,:), cell_thetasectbet(icell,:), &
!                          cell_nbetaelem(icell), nsides(icell), cell_nsegments(icell,:), nPhi, nTheta,    &
!                          nzones(icell))
! Allocation and initialization of the V matrix
      if (icell == 1 .and. ig == 1) then
        allocate(cell_V(nCellTypes,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
        cell_V = 0.0_rp
        allocate(V(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
      end if
      V = 0.0_rp
! Evaluation of the elements of the V matrix
      call IntegrateV(nzones(icell), nharms(icell), nsides(icell), cell_nsegments(icell,:), nPhi, nTheta, &
                      cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), chi, eps, cell_mat(icell,:),  &
                      cell_vol(icell,:), STOT(:,ig), G, V)
      cell_V(icell,:,:,:,:,:,:) = V
! Evaluation of the elements of the U matrix
      if (icell == 1 .and. ig == 1) then
        allocate(cell_U(nCellTypes,maxval(nzones),maxval(nharms),maxval(nzones),maxval(nharms)))
        cell_U = 0.0_rp
        allocate(U(maxval(nzones),maxval(nharms),maxval(nzones),maxval(nharms)))
      end if
      U = 0.0_rp
      call IntegrateU(nChordU, nSegChordU, lenChordU, wghtChordU, zoneChordU, xChordU, yChordU, &
                      cell_mat(icell,:), cell_vol(icell,:), STOT(:,ig),  nzones(icell), nharms(icell), U)
! Orthogonalization of the U matrix
      call OrthogonalizeU(nzones(icell), nharms(icell), U)
! Normalization of the U matrix
      call NormalizeU(nzones(icell), nharms(icell), nsides(icell), cell_nsegments(icell,:), nPhi, nTheta, &
                      V, U)
      cell_U(icell,:,:,:,:) = U
! Allocation of the memory for collision probabilities for each group
! Beta
      if (icell == 1) then
        allocate(b_mg(ig)%nbetaelem(nCellTypes))
        allocate(b_mg(ig)%beta(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%brc(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%insidebet(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%insegbet(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%insectbet(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%outsidebet(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%outsegbet(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%outsectbet(nCellTypes,nBetaElem))
        allocate(b_mg(ig)%thetasectbet(nCellTypes,nBetaElem))
! Gamma
        allocate(g_mg(ig)%g(nCellTypes,maxval(nzones),maxval(nharms),maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
! V
        allocate(v_mg(ig)%v(nCellTypes,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
! U
        allocate(u_mg(ig)%u(nCellTypes,maxval(nzones),maxval(nharms),maxval(nzones),maxval(nharms)))
      end if
      if (nBetaElem > size(b_mg(ig)%beta,2)) then
        call SetArraySize2DReal(b_mg(ig)%beta, nCellTypes, nBetaElem)
        call SetArraySize2DReal(b_mg(ig)%brc, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%insidebet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%insegbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%insectbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%outsidebet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%outsegbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%outsectbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(b_mg(ig)%thetasectbet, nCellTypes, nBetaElem)
      end if
! In the case of the regular cell ensure symmetric properties
!      do ibet = 1, cell_nbetaelem(icell)
!        bb1 =
!        bb2 =
!      end do
! Passing the data into the multigroup collision probabilities
      b_mg(ig)%nbetaelem(icell) = cell_nbetaelem(icell)
      b_mg(ig)%beta(icell,1:nBetaElem) = cell_beta(icell,1:nBetaElem)
      b_mg(ig)%brc(icell,1:nBetaElem) = cell_brc(icell,1:nBetaElem)
      b_mg(ig)%insidebet(icell,1:nBetaElem) = cell_insidebet(icell,1:nBetaElem)
      b_mg(ig)%insegbet(icell,1:nBetaElem) = cell_insegbet(icell,1:nBetaElem)
      b_mg(ig)%insectbet(icell,1:nBetaElem) = cell_insectbet(icell,1:nBetaElem)
      b_mg(ig)%outsidebet(icell,1:nBetaElem) = cell_outsidebet(icell,1:nBetaElem)
      b_mg(ig)%outsegbet(icell,1:nBetaElem) = cell_outsegbet(icell,1:nBetaElem)
      b_mg(ig)%outsectbet(icell,1:nBetaElem) = cell_outsectbet(icell,1:nBetaElem)
      b_mg(ig)%thetasectbet(icell,1:nBetaElem) = cell_thetasectbet(icell,1:nBetaElem)

      g_mg(ig)%g(icell,:,:,:,:,:,:) = cell_G(icell,:,:,:,:,:,:)

      v_mg(ig)%v(icell,:,:,:,:,:,:) = cell_V(icell,:,:,:,:,:,:)

      u_mg(ig)%u(icell,:,:,:,:) = cell_U(icell,:,:,:,:)
    end do

  end do

!  call print_beta(1, 1)
!  call print_gamma(1, 1)
!  call print_v(1, 1)
!  call print_u(1, 1)
!
!  stop

!  do ig = 1, nGroups
!    write(*,*) "Group ", ig
!    write(*,*) "  Beta:"
!    write(*,*) b_mg(ig)%beta
!    write(*,*) "  Gamma:"
!    write(*,*) g_mg(ig)%g
!    write(*,*) "  V:"
!    write(*,*) v_mg(ig)%v
!    write(*,*) "  U:"
!    write(*,*) u_mg(ig)%u
!  end do
!
!  stop
!  open(1654, file = "results/matrix_cccpo_xs_fuel.out")
!  open(1655, file = "results/matrix_cccpo_xs_clad.out")
!  open(1656, file = "results/matrix_cccpo_xs_moder.out")
!  do ig = 1, nGroups
!    write(1654, '(1e15.8)') STOT(1,ig)
!    do ig1 = 1, nGroups
!      write(1654, '(1e15.8)') SSCA(1,ig,ig1)
!    end do
!    write(1654, '(1e15.8)') SCHI(1,ig)
!    write(1654, '(1e15.8)') SFIS(1,ig)
!    write(1654, '(1e15.8)') SFNF(1,ig)
!    write(1654, '(1e15.8)') SOUR(1,ig)
!
!    write(1655, '(1e15.8)') STOT(2,ig)
!    do ig1 = 1, nGroups
!      write(1655, '(1e15.8)') SSCA(2,ig,ig1)
!    end do
!    write(1655, '(1e15.8)') SCHI(2,ig)
!    write(1655, '(1e15.8)') SFIS(2,ig)
!    write(1655, '(1e15.8)') SFNF(2,ig)
!    write(1655, '(1e15.8)') SOUR(2,ig)
!
!    write(1656, '(1e15.8)') STOT(3,ig)
!    do ig1 = 1, nGroups
!      write(1656, '(1e15.8)') SSCA(3,ig,ig1)
!    end do
!    write(1656, '(1e15.8)') SCHI(3,ig)
!    write(1656, '(1e15.8)') SFIS(3,ig)
!    write(1656, '(1e15.8)') SFNF(3,ig)
!    write(1656, '(1e15.8)') SOUR(3,ig)
!  end do
!  close(1654)
!  close(1655)
!  close(1656)
!
!  open(1654, file = "results/matrix_cccpo_b.out")
!  open(1655, file = "results/matrix_cccpo_v.out")
!  open(1656, file = "results/matrix_cccpo_g.out")
!  open(1657, file = "results/matrix_cccpo_u.out")
!  do ig = 1, nGroups
!    do i = 1, b_mg(ig)%nbetaelem(1)
!      write(1654, '(8i3,1e19.12)') ig, b_mg(ig)%insidebet(1,i), b_mg(ig)%insegbet(1,i), b_mg(ig)%insectbet(1,i), &
!                                   b_mg(ig)%outsidebet(1,i), b_mg(ig)%outsegbet(1,i), b_mg(ig)%outsectbet(1,i), &
!                                   b_mg(ig)%thetasectbet(1,i), b_mg(ig)%beta(1,i)
!    end do
!    do s = 1, nsides(1)
!      do n = 1, cell_nsegments(1,s)
!        do m = 1, nPhi
!          do l = 1, nTheta
!            do i = 1, nzones(1)
!              do k = 1, nharms(1)
!                write(1655, '(7i3,1e20.12)') ig, s, n, m, l, i, k, v_mg(ig)%v(1,s,n,m,l,i,k)
!              end do
!            end do
!          end do
!        end do
!      end do
!    end do
!    do i = 1, nzones(1)
!      do k = 1, nharms(1)
!        do s = 1, nsides(1)
!          do n = 1, cell_nsegments(1,s)
!            do m = 1, nPhi
!              do l = 1, nTheta
!                write(1656, '(7i3,1e20.12)') ig, i, k, s, n, m, l, g_mg(ig)%g(1,i,k,s,n,m,l)
!              end do
!            end do
!          end do
!        end do
!      end do
!    end do
!    do i1 = 1, nzones(1)
!      do k1 = 1, nharms(1)
!        do i = 1, nzones(1)
!          do k = 1, nharms(1)
!            write(1657, '(5i3,1e20.12)') ig, i1, k1, i, k, u_mg(ig)%u(1,i1,k1,i,k)
!          end do
!        end do
!      end do
!    end do
!  end do
!  close(1654)
!  close(1655)
!  close(1656)
!  close(1657)

  if (irun == 1) then
    call Beta_to_array(b_mg, nGroups, nCellTypes, nTheta, beta_cp, n_beta_cp)
    call V_to_array(v_mg, nGroups, nCellTypes, nTheta, nzones, nharms, nsides, cell_nsegments, nPhi, v_cp, n_v_cp)
    call G_to_array(g_mg, nGroups, nCellTypes, nTheta, nzones, nharms, nsides, cell_nsegments, nPhi, g_cp, n_g_cp)
  end if

  write(*,*) "Calculation of the fluxes..."

!  call GetFluxAssembly
  call system_clock(count_rate=cr)
  call system_clock(it1)
  if (irun == 0) then
    call GetFluxAssemblyMG()
  end if
!  call GetFluxAssemblyMG_new(kiter_max, eps_k)
!  call TransportSweepMG(eps_k, kiter_max)
  if (irun == 1) then
    call TransportSweepHelios(eps_k, kiter_max)
  end if

!  if (irun == 2) then ! Flux calculation mode with additional corner corrected albedos
!    call GetFluxCorrectedAlbedos(filename_corner, eps_f, eps_k, kiter_max)
!  end if
  call system_clock(it2)

! Writing output fission rates
  open(1737, file = 'results/fission_rates.out')
  do icell = 1, nCell
    fr = 0.0_rp
    it = kart(icell)
    do ir = 1, nzones(it)
      imt = cell_mat(it,ir)
      do ig = 1, nGroups
        fr = fr + cell_flux(ig,icell,ir,1)*SFIS(imt,ig)*cell_vol(it,ir)
      end do
    end do
    write(1737, '(1i5,1f8.5)') icell, fr
  end do
  close(1737)

  write(*,*) "Time of the flux simulation, s: ", (it2 -it1)/cr

  open(1201, file = 'results/keff.out')
  write(1201, *) keff
  close(1201)

  if (ivtk > 0) then
    do ig = 1, nGroups
      vtk_output_file = "flux_points_cccpo" // intToChar(ig) // ".vtp"
      call PlotFlux(vtk_output_file, sym, cell_vertx, cell_verty, nCell, kart, nzones, cell_flux(ig,:,:,:), 900, 900)
    end do
  end if

  if (ixs > 0) then
    call Homogenise_XS(ixs, nGroups, nCell, cell_flux)
  end if

  if (iout > 0) then
    call PrintOutput(iout, nGroups, nCell, cell_flux)
  end if

  stop

! Output printing
!  if (icompare > 0) then
!    allocate(flux_fuel_openmc(nCell))
!    allocate(flux_aver_openmc(nCell))
!  end if

! Low density of moderator, 60 degree, 6 rows, absorber (19th cell), without gap
!  flux_fuel_openmc = (/4.712,4.697,4.679,4.669,4.696,4.695,4.687,4.660,4.636,4.651,4.680,4.579, &
!                       4.523,4.523,4.579,4.261,4.188,4.330,0.633,3.836,3.542/)
!  flux_aver_openmc = (/5.161,5.148,5.137,5.127,5.147,5.148,5.138,5.114,5.093,5.107,5.133,5.034, &
!                       4.974,4.975,5.034,4.698,4.615,4.779,2.817,4.236,3.928/)
! Normal density of moderator, 60 degree, 6 rows, absorber (19th cell), without gap
!  flux_fuel_openmc = (/3.576,3.568,3.554,3.559,3.571,3.580,3.556,3.532,3.524,3.539,3.564,3.458, &
!                       3.405,3.420,3.489,3.128,3.053,3.274,0.409,2.699,2.412/)
!  flux_aver_openmc = (/3.850,3.840,3.827,3.832,3.846,3.856,3.829,3.804,3.796,3.813,3.838,3.727, &
!                       3.669,3.687,3.759,3.375,3.298,3.530,1.781,2.922,2.615/)
! Normal density of moderator, 60 degree, 6 rows fuel cell only, without gap
!  flux_fuel_openmc = (/3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657, &
!                       3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657,3.657/)
!  flux_aver_openmc = (/3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936, &
!                       3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936,3.936/)
! Normal density of moderator 30 degree 4 rows absorber in the center, without gap
!  flux_fuel_openmc = (/3.485,3.478,3.442,3.416,3.169,0.4454/)
!  flux_aver_openmc = (/3.756,3.747,3.710,3.680,3.418,1.933/)
! Low density of moderator 30 degree 4 rows absorber in the center, without gap
!  flux_fuel_openmc = (/4.638,4.583,4.564,4.500,4.307,0.6857/)
!  flux_aver_openmc = (/5.079,5.035,5.013,4.956,4.742,3.022/)
! Normal density of moderator, 360 degree, 4 rows, absorber central cell with gap
!  flux_fuel_openmc = (/5.193, 5.162, 5.114, 5.161, 5.193, 5.158, 5.107, 5.159, 5.188, 5.164, &
!                       5.112, 5.161, 5.191, 5.163, 5.118, 5.163, 5.192, 5.161, 5.112, 5.161, &
!                       5.187, 5.159, 5.107, 5.158, 4.334, 4.220, 4.220, 4.339, 4.215, 4.214, &
!                       4.339, 4.218, 4.217, 4.337, 4.219, 4.219, 4.338, 4.217, 4.216, 4.335, &
!                       4.216, 4.215, 3.945, 3.862, 3.942, 3.858, 3.941, 3.864, 3.941, 3.861, &
!                       3.942, 3.861, 3.944, 3.862, 3.490, 3.490, 3.488, 3.489, 3.488, 3.488, &
!                       0.483/)
!  flux_aver_openmc = (/5.193, 5.162, 5.114, 5.161, 5.193, 5.158, 5.107, 5.159, 5.188, 5.164, &
!                       5.112, 5.161, 5.191, 5.163, 5.118, 5.163, 5.192, 5.161, 5.112, 5.161, &
!                       5.187, 5.159, 5.107, 5.158, 4.661, 4.537, 4.537, 4.664, 4.533, 4.532, &
!                       4.664, 4.537, 4.535, 4.663, 4.536, 4.536, 4.663, 4.534, 4.534, 4.660, &
!                       4.533, 4.532, 4.242, 4.153, 4.242, 4.150, 4.241, 4.155, 4.241, 4.151, &
!                       4.240, 4.152, 4.242, 4.153, 3.757, 3.758, 3.756, 3.757, 3.755, 3.755, &
!                       2.100/)
! Normal density of moderator, 360 degree, 11 rows, UGD low enriched benchmark, guide tubes, central tube,
! no absorber, with the gap
!  flux_fuel_openmc = (/5.072, 5.082, 5.013, 4.996, 4.995, 5.012, 5.010, 4.999, 5.000, 5.016, &
!                       5.076, 5.098, 5.089, 5.036, 5.013, 4.988, 5.004, 4.990, 4.982, 4.986, &
!                       5.029, 5.086, 5.076, 5.086, 5.023, 4.988, 4.999, 4.996, 4.993, 4.991, &
!                       4.991, 5.021, 5.087, 5.078, 5.079, 5.012, 4.983, 4.993, 4.999, 5.000, &
!                       5.002, 5.001, 5.015, 5.076, 5.085, 5.080, 5.022, 5.005, 5.008, 5.010, &
!                       5.020, 5.000, 5.017, 5.023, 5.082, 5.094, 5.073, 5.020, 5.012, 5.009, &
!                       4.997, 4.985, 4.993, 4.996, 5.022, 5.087, 4.294, 4.203, 4.172, 4.158, &
!                       4.179, 4.174, 4.169, 4.171, 4.184, 4.214, 4.305, 4.210, 4.174, 4.170, &
!                       4.165, 4.171, 4.166, 4.155, 4.164, 4.211, 4.310, 4.210, 4.177, 4.159, &
!                       4.163, 4.171, 4.162, 4.164, 4.172, 4.204, 4.298, 4.203, 4.164, 4.161, &
!                       4.160, 4.173, 4.166, 4.165, 4.175, 4.200, 4.294, 4.204, 4.175, 4.167, &
!                       4.170, 4.174, 4.169, 4.163, 4.178, 4.207, 4.304, 4.207, 4.171, 4.171, &
!                       4.171, 4.162, 4.157, 4.158, 4.168, 4.204, 4.021, 3.971, 3.965, 3.963, &
!                       3.983, 3.994, 3.976, 3.978, 3.981, 4.034, 3.981, 3.964, 3.970, 3.990, &
!                       3.992, 3.967, 3.960, 3.975, 4.033, 3.974, 3.963, 3.969, 3.992, 4.000, &
!                       3.967, 3.955, 3.967, 4.028, 3.977, 3.957, 3.965, 3.988, 3.991, 3.967, &
!                       3.967, 3.976, 4.026, 3.971, 3.970, 3.973, 3.988, 3.992, 3.969, 3.963, &
!                       3.976, 4.035, 3.977, 3.968, 3.976, 4.003, 3.984, 3.969, 3.956, 3.966, &
!                       3.901, 3.887, 3.903, 3.947, 4.086, 3.941, 3.902, 3.898, 3.907, 3.889, &
!                       3.909, 3.947, 4.077, 3.949, 3.911, 3.896, 3.903, 3.896, 3.907, 3.950, &
!                       4.074, 3.950, 3.906, 3.892, 3.908, 3.890, 3.908, 3.943, 4.078, 3.951, &
!                       3.907, 3.900, 3.906, 3.883, 3.901, 3.945, 4.083, 3.952, 3.916, 3.889, &
!                       3.905, 3.892, 3.906, 3.942, 4.077, 3.947, 3.908, 3.892, 3.888, 3.914, &
!                       3.952, 4.068, 4.076, 3.952, 3.919, 3.897, 3.920, 3.955, 4.071, 4.070, &
!                       3.957, 3.918, 3.897, 3.925, 3.954, 4.067, 4.070, 3.955, 3.914, 3.891, &
!                       3.918, 3.950, 4.074, 4.074, 3.953, 3.929, 3.899, 3.913, 3.958, 4.075, &
!                       4.079, 3.966, 3.924, 3.904, 3.921, 3.957, 4.066, 4.070, 3.961, 3.920, &
!                       4.042, 4.081, 4.112, 5.094, 4.109, 4.082, 4.044, 4.079, 4.116, 5.099, &
!                       4.103, 4.077, 4.047, 4.090, 4.115, 5.098, 4.109, 4.078, 4.044, 4.076, &
!                       4.115, 5.096, 4.111, 4.078, 4.042, 4.085, 4.116, 5.106, 4.108, 4.080, &
!                       4.046, 4.082, 4.107, 5.094, 4.112, 4.084, 5.091, 4.161, 4.176, 4.168, &
!                       4.157, 5.095, 4.153, 4.180, 4.163, 4.158, 5.100, 4.154, 4.170, 4.170, &
!                       4.156, 5.096, 4.153, 4.184, 4.168, 4.152, 5.094, 4.160, 4.176, 4.171, &
!                       4.154, 5.087, 4.147, 4.173, 4.168, 4.145, 4.177, 4.238, 4.186, 4.186, &
!                       4.183, 4.242, 4.190, 4.196, 4.179, 4.232, 4.195, 4.193, 4.184, 4.240, &
!                       4.183, 4.193, 4.170, 4.235, 4.182, 4.183, 4.177, 4.234, 4.194, 4.183, &
!                       4.203, 4.205, 5.175, 4.212, 4.208, 5.184, 4.208, 4.206, 5.182, 4.215, &
!                       4.199, 5.177, 4.208, 4.196, 5.168, 4.202, 4.202, 5.177, 4.206, 4.202, &
!                       4.216, 4.217, 4.216, 4.217, 4.216, 4.212, 4.217, 4.219, 4.222, 4.214, &
!                       4.225, 4.226, 4.225, 4.220, 4.220, 4.222, 5.238/)
!  flux_aver_openmc = (/5.072, 5.082, 5.013, 4.996, 4.995, 5.012, 5.010, 4.999, 5.000, 5.016, &
!                       5.076, 5.098, 5.089, 5.036, 5.013, 4.988, 5.004, 4.990, 4.982, 4.986, &
!                       5.029, 5.086, 5.076, 5.086, 5.023, 4.988, 4.999, 4.996, 4.993, 4.991, &
!                       4.991, 5.021, 5.087, 5.078, 5.079, 5.012, 4.983, 4.993, 4.999, 5.000, &
!                       5.002, 5.001, 5.015, 5.076, 5.085, 5.080, 5.022, 5.005, 5.008, 5.010, &
!                       5.020, 5.000, 5.017, 5.023, 5.082, 5.094, 5.073, 5.020, 5.012, 5.009, &
!                       4.997, 4.985, 4.993, 4.996, 5.022, 5.087, 4.615, 4.517, 4.485, 4.476, &
!                       4.490, 4.490, 4.483, 4.483, 4.492, 4.528, 4.628, 4.525, 4.489, 4.482, &
!                       4.479, 4.485, 4.479, 4.468, 4.480, 4.530, 4.632, 4.526, 4.490, 4.473, &
!                       4.477, 4.486, 4.480, 4.475, 4.483, 4.519, 4.624, 4.520, 4.478, 4.475, &
!                       4.475, 4.488, 4.481, 4.479, 4.490, 4.516, 4.618, 4.519, 4.487, 4.481, &
!                       4.488, 4.489, 4.484, 4.477, 4.492, 4.527, 4.625, 4.522, 4.485, 4.486, &
!                       4.485, 4.477, 4.474, 4.471, 4.481, 4.521, 4.322, 4.268, 4.262, 4.264, &
!                       4.288, 4.294, 4.275, 4.275, 4.278, 4.336, 4.279, 4.260, 4.266, 4.291, &
!                       4.293, 4.266, 4.257, 4.276, 4.339, 4.275, 4.262, 4.270, 4.295, 4.299, &
!                       4.269, 4.257, 4.272, 4.333, 4.277, 4.258, 4.263, 4.288, 4.292, 4.271, &
!                       4.266, 4.274, 4.329, 4.270, 4.264, 4.273, 4.293, 4.291, 4.268, 4.263, &
!                       4.277, 4.338, 4.274, 4.265, 4.275, 4.297, 4.286, 4.266, 4.254, 4.267, &
!                       4.195, 4.182, 4.197, 4.243, 4.337, 4.243, 4.197, 4.194, 4.201, 4.185, &
!                       4.205, 4.242, 4.333, 4.245, 4.205, 4.189, 4.199, 4.188, 4.201, 4.245, &
!                       4.334, 4.245, 4.202, 4.189, 4.203, 4.186, 4.201, 4.239, 4.329, 4.246, &
!                       4.203, 4.191, 4.202, 4.180, 4.195, 4.244, 4.335, 4.250, 4.212, 4.187, &
!                       4.200, 4.185, 4.199, 4.240, 4.329, 4.242, 4.202, 4.183, 4.181, 4.209, &
!                       4.250, 4.373, 4.379, 4.253, 4.215, 4.193, 4.214, 4.252, 4.380, 4.378, &
!                       4.255, 4.215, 4.188, 4.223, 4.254, 4.373, 4.377, 4.255, 4.213, 4.186, &
!                       4.214, 4.251, 4.380, 4.377, 4.253, 4.221, 4.191, 4.212, 4.257, 4.382, &
!                       4.383, 4.265, 4.221, 4.194, 4.213, 4.256, 4.373, 4.376, 4.256, 4.216, &
!                       4.347, 4.383, 4.421, 5.029, 4.419, 4.388, 4.345, 4.385, 4.423, 5.034, &
!                       4.417, 4.384, 4.349, 4.395, 4.426, 5.031, 4.417, 4.384, 4.346, 4.384, &
!                       4.423, 5.032, 4.419, 4.385, 4.347, 4.394, 4.429, 5.040, 4.422, 4.386, &
!                       4.350, 4.389, 4.417, 5.029, 4.418, 4.387, 5.025, 4.469, 4.486, 4.479, &
!                       4.466, 5.030, 4.465, 4.489, 4.477, 4.469, 5.034, 4.467, 4.479, 4.481, &
!                       4.465, 5.029, 4.469, 4.492, 4.477, 4.463, 5.030, 4.470, 4.485, 4.482, &
!                       4.462, 5.023, 4.459, 4.484, 4.482, 4.459, 4.490, 4.496, 4.501, 4.500, &
!                       4.491, 4.499, 4.506, 4.510, 4.491, 4.495, 4.505, 4.507, 4.497, 4.498, &
!                       4.499, 4.500, 4.490, 4.493, 4.496, 4.498, 4.486, 4.492, 4.506, 4.498, &
!                       4.516, 4.521, 5.112, 4.522, 4.528, 5.119, 4.523, 4.520, 5.118, 4.526, &
!                       4.514, 5.112, 4.519, 4.514, 5.104, 4.514, 4.519, 5.113, 4.519, 4.521, &
!                       4.528, 4.533, 4.532, 4.534, 4.529, 4.526, 4.529, 4.531, 4.535, 4.528, &
!                       4.539, 4.540, 4.543, 4.537, 4.536, 4.537, 5.157/)
! Normal density of moderator, 360 degree, 11 rows, UGD low enriched benchmark, guide tubes, central tube,
! with absorber, with the gap
!  flux_fuel_openmc = (/5.010, 5.001, 4.920, 4.871, 4.849, 4.839, 4.843, 4.848, 4.885, 4.924, &
!                       5.004, 5.010, 5.006, 4.937, 4.891, 4.847, 4.840, 4.836, 4.850, 4.869, &
!                       4.930, 5.011, 5.006, 5.003, 4.928, 4.873, 4.853, 4.839, 4.833, 4.851, &
!                       4.870, 4.925, 5.009, 5.012, 5.005, 4.923, 4.869, 4.854, 4.837, 4.838, &
!                       4.857, 4.878, 4.928, 5.007, 5.018, 5.001, 4.927, 4.880, 4.851, 4.842, &
!                       4.844, 4.856, 4.887, 4.922, 5.001, 5.009, 4.992, 4.921, 4.879, 4.861, &
!                       4.828, 4.832, 4.847, 4.875, 4.931, 5.013, 4.230, 4.119, 4.065, 4.024, &
!                       4.010, 4.003, 4.009, 4.034, 4.072, 4.125, 4.229, 4.124, 4.069, 4.033, &
!                       4.011, 4.002, 4.011, 4.026, 4.060, 4.123, 4.233, 4.123, 4.066, 4.029, &
!                       4.010, 4.003, 4.003, 4.024, 4.063, 4.121, 4.228, 4.117, 4.057, 4.027, &
!                       4.006, 3.999, 4.009, 4.030, 4.063, 4.122, 4.229, 4.125, 4.067, 4.028, &
!                       4.009, 4.000, 4.007, 4.028, 4.068, 4.121, 4.225, 4.123, 4.064, 4.030, &
!                       4.008, 3.997, 4.004, 4.028, 4.065, 4.121, 3.920, 3.838, 3.787, 3.736, &
!                       3.718, 3.722, 3.738, 3.785, 3.837, 3.929, 3.841, 3.785, 3.740, 3.723, &
!                       3.722, 3.735, 3.781, 3.839, 3.926, 3.839, 3.782, 3.734, 3.716, 3.722, &
!                       3.733, 3.774, 3.830, 3.923, 3.840, 3.780, 3.736, 3.715, 3.720, 3.736, &
!                       3.784, 3.839, 3.926, 3.838, 3.788, 3.746, 3.719, 3.721, 3.742, 3.784, &
!                       3.836, 3.927, 3.841, 3.785, 3.742, 3.728, 3.718, 3.737, 3.777, 3.831, &
!                       3.701, 3.608, 3.532, 3.445, 3.513, 3.442, 3.530, 3.612, 3.705, 3.607, &
!                       3.535, 3.445, 3.514, 3.444, 3.532, 3.605, 3.699, 3.611, 3.531, 3.445, &
!                       3.504, 3.443, 3.531, 3.608, 3.703, 3.608, 3.532, 3.442, 3.506, 3.445, &
!                       3.532, 3.613, 3.706, 3.607, 3.531, 3.445, 3.509, 3.446, 3.536, 3.607, &
!                       3.703, 3.609, 3.536, 3.446, 3.511, 3.446, 3.533, 3.612, 3.422, 3.283, &
!                       3.156, 2.819, 2.817, 3.154, 3.280, 3.429, 3.281, 3.153, 2.822, 2.818, &
!                       3.150, 3.279, 3.425, 3.283, 3.158, 2.823, 2.820, 3.152, 3.273, 3.425, &
!                       3.283, 3.151, 2.824, 2.820, 3.150, 3.280, 3.425, 3.283, 3.156, 2.822, &
!                       2.821, 3.152, 3.282, 3.428, 3.283, 3.158, 2.820, 2.821, 3.155, 3.280, &
!                       2.789, 2.644, 2.540, 0.262, 2.526, 2.627, 2.792, 2.641, 2.541, 0.262, &
!                       2.524, 2.622, 2.793, 2.643, 2.545, 0.262, 2.524, 2.625, 2.789, 2.639, &
!                       2.545, 0.262, 2.526, 2.623, 2.789, 2.648, 2.542, 0.263, 2.525, 2.624, &
!                       2.792, 2.644, 2.542, 0.262, 2.530, 2.627, 0.259, 2.418, 2.355, 2.313, &
!                       2.352, 0.259, 2.419, 2.354, 2.313, 2.351, 0.259, 2.417, 2.354, 2.313, &
!                       2.352, 0.259, 2.414, 2.352, 2.314, 2.353, 0.259, 2.421, 2.354, 2.313, &
!                       2.352, 0.259, 2.417, 2.350, 2.311, 2.353, 2.345, 2.664, 2.332, 2.282, &
!                       2.347, 2.663, 2.332, 2.284, 2.344, 2.665, 2.334, 2.285, 2.346, 2.666, &
!                       2.333, 2.285, 2.342, 2.665, 2.329, 2.281, 2.344, 2.662, 2.336, 2.283, &
!                       2.323, 2.365, 0.244, 2.326, 2.362, 0.244, 2.318, 2.360, 0.244, 2.324, &
!                       2.363, 0.244, 2.324, 2.362, 0.244, 2.320, 2.361, 0.244, 2.421, 2.478, &
!                       2.423, 2.476, 2.419, 2.480, 2.419, 2.478, 2.423, 2.479, 2.422, 2.481, &
!                       2.972, 2.976, 2.972, 2.970, 2.969, 2.968, 4.023/)
!  flux_aver_openmc = (/5.010, 5.001, 4.920, 4.871, 4.849, 4.839, 4.843, 4.848, 4.885, 4.924, &
!                       5.004, 5.010, 5.006, 4.937, 4.891, 4.847, 4.840, 4.836, 4.850, 4.869, &
!                       4.930, 5.011, 5.006, 5.003, 4.928, 4.873, 4.853, 4.839, 4.833, 4.851, &
!                       4.870, 4.925, 5.009, 5.012, 5.005, 4.923, 4.869, 4.854, 4.837, 4.838, &
!                       4.857, 4.878, 4.928, 5.007, 5.018, 5.001, 4.927, 4.880, 4.851, 4.842, &
!                       4.844, 4.856, 4.887, 4.922, 5.001, 5.009, 4.992, 4.921, 4.879, 4.861, &
!                       4.828, 4.832, 4.847, 4.875, 4.931, 5.013, 4.547, 4.430, 4.372, 4.334, &
!                       4.315, 4.309, 4.313, 4.338, 4.379, 4.435, 4.550, 4.436, 4.377, 4.340, &
!                       4.316, 4.308, 4.316, 4.333, 4.369, 4.437, 4.553, 4.435, 4.374, 4.334, &
!                       4.316, 4.308, 4.311, 4.330, 4.370, 4.432, 4.549, 4.429, 4.367, 4.334, &
!                       4.311, 4.304, 4.316, 4.335, 4.371, 4.432, 4.550, 4.436, 4.376, 4.335, &
!                       4.315, 4.306, 4.314, 4.334, 4.375, 4.434, 4.546, 4.434, 4.372, 4.338, &
!                       4.314, 4.300, 4.310, 4.332, 4.371, 4.432, 4.217, 4.128, 4.074, 4.022, &
!                       4.006, 4.007, 4.026, 4.075, 4.127, 4.226, 4.133, 4.072, 4.026, 4.008, &
!                       4.008, 4.022, 4.069, 4.131, 4.226, 4.131, 4.070, 4.024, 4.005, 4.006, &
!                       4.021, 4.065, 4.124, 4.222, 4.133, 4.069, 4.022, 4.001, 4.005, 4.026, &
!                       4.072, 4.129, 4.224, 4.130, 4.074, 4.031, 4.008, 4.007, 4.029, 4.073, &
!                       4.129, 4.223, 4.130, 4.074, 4.030, 4.009, 4.003, 4.022, 4.066, 4.124, &
!                       3.984, 3.887, 3.806, 3.717, 3.737, 3.716, 3.804, 3.889, 3.988, 3.886, &
!                       3.808, 3.718, 3.738, 3.718, 3.807, 3.884, 3.985, 3.888, 3.805, 3.717, &
!                       3.733, 3.716, 3.803, 3.886, 3.988, 3.888, 3.806, 3.715, 3.730, 3.718, &
!                       3.807, 3.888, 3.989, 3.885, 3.804, 3.719, 3.733, 3.720, 3.810, 3.887, &
!                       3.985, 3.888, 3.809, 3.717, 3.734, 3.716, 3.806, 3.886, 3.693, 3.542, &
!                       3.408, 3.044, 3.042, 3.406, 3.538, 3.699, 3.540, 3.406, 3.048, 3.044, &
!                       3.404, 3.538, 3.695, 3.543, 3.412, 3.048, 3.045, 3.406, 3.534, 3.696, &
!                       3.542, 3.406, 3.049, 3.043, 3.404, 3.538, 3.694, 3.542, 3.410, 3.048, &
!                       3.045, 3.407, 3.541, 3.697, 3.542, 3.412, 3.046, 3.045, 3.407, 3.540, &
!                       3.014, 2.859, 2.749, 0.792, 2.733, 2.843, 3.016, 2.858, 2.749, 0.793, &
!                       2.733, 2.840, 3.016, 2.861, 2.753, 0.793, 2.733, 2.842, 3.013, 2.858, &
!                       2.753, 0.793, 2.733, 2.842, 3.013, 2.865, 2.753, 0.794, 2.734, 2.842, &
!                       3.017, 2.861, 2.751, 0.794, 2.737, 2.844, 0.784, 2.618, 2.558, 2.512, &
!                       2.551, 0.784, 2.618, 2.559, 2.512, 2.552, 0.784, 2.619, 2.558, 2.512, &
!                       2.552, 0.785, 2.618, 2.557, 2.511, 2.553, 0.784, 2.621, 2.557, 2.512, &
!                       2.552, 0.784, 2.618, 2.557, 2.512, 2.554, 2.541, 2.856, 2.528, 2.480, &
!                       2.541, 2.856, 2.530, 2.483, 2.540, 2.857, 2.528, 2.483, 2.545, 2.857, &
!                       2.529, 2.481, 2.542, 2.856, 2.525, 2.480, 2.540, 2.854, 2.530, 2.480, &
!                       2.527, 2.565, 0.742, 2.529, 2.564, 0.741, 2.525, 2.560, 0.741, 2.529, &
!                       2.564, 0.741, 2.527, 2.562, 0.740, 2.524, 2.561, 0.741, 2.623, 2.689, &
!                       2.624, 2.689, 2.623, 2.691, 2.624, 2.689, 2.623, 2.688, 2.623, 2.690, &
!                       3.217, 3.221, 3.220, 3.217, 3.214, 3.214, 3.935/)
! Normal density of moderator, sym = 360, square assembly 7x7, no gap, absorber in the center
!  flux_fuel_openmc = (/4.196, 4.177, 4.173, 4.154, 4.170, 4.191, 4.213, 4.181, 4.164, 4.095, &
!                       4.048, 4.108, 4.163, 4.199, 4.157, 4.102, 3.923, 3.693, 3.927, 4.106, &
!                       4.172, 4.145, 4.052, 3.698, 0.518, 3.694, 4.054, 4.149, 4.155, 4.107, &
!                       3.930, 3.696, 3.944, 4.092, 4.167, 4.175, 4.149, 4.095, 4.051, 4.112, &
!                       4.153, 4.184, 4.207, 4.188, 4.168, 4.151, 4.166, 4.193, 4.198/)
!  flux_aver_openmc = (/4.552, 4.537, 4.524, 4.506, 4.525, 4.551, 4.571, 4.539, 4.514, 4.447, &
!                       4.403, 4.459, 4.521, 4.552, 4.515, 4.455, 4.263, 4.020, 4.263, 4.460, &
!                       4.527, 4.502, 4.406, 4.022, 2.428, 4.018, 4.404, 4.505, 4.513, 4.453, &
!                       4.266, 4.019, 4.274, 4.446, 4.522, 4.532, 4.506, 4.448, 4.405, 4.459, &
!                       4.510, 4.545, 4.561, 4.543, 4.518, 4.507, 4.522, 4.544, 4.554/)
! Normal density of moderator, sym = 360, square assembly 11x11, no gap, 4 absorber cells in the center
!  flux_fuel_openmc = (/4.232, 4.232, 4.217, 4.206, 4.193, 4.186, 4.191, 4.207, 4.227, 4.235, &
!                       4.233, 4.231, 4.221, 4.201, 4.181, 4.150, 4.136, 4.151, 4.179, 4.204, &
!                       4.224, 4.231, 4.219, 4.202, 4.162, 4.106, 4.031, 3.982, 4.036, 4.110, &
!                       4.171, 4.204, 4.223, 4.203, 4.177, 4.105, 3.969, 3.741, 3.539, 3.735, &
!                       3.971, 4.108, 4.178, 4.206, 4.193, 4.156, 4.036, 3.740, 3.101, 0.432, &
!                       3.101, 3.736, 4.030, 4.149, 4.189, 4.187, 4.134, 3.984, 3.536, 0.432, &
!                       2.314, 0.432, 3.537, 3.984, 4.134, 4.189, 4.195, 4.155, 4.031, 3.737, &
!                       3.100, 0.433, 3.097, 3.734, 4.026, 4.145, 4.195, 4.206, 4.177, 4.108, &
!                       3.971, 3.736, 3.537, 3.739, 3.969, 4.106, 4.178, 4.208, 4.221, 4.207, &
!                       4.170, 4.109, 4.029, 3.983, 4.034, 4.107, 4.167, 4.207, 4.220, 4.229, &
!                       4.221, 4.207, 4.177, 4.147, 4.136, 4.152, 4.178, 4.204, 4.224, 4.235, &
!                       4.234, 4.230, 4.221, 4.207, 4.197, 4.189, 4.195, 4.203, 4.222, 4.234, &
!                       4.232/)
!  flux_aver_openmc = (/4.592, 4.589, 4.576, 4.565, 4.549, 4.544, 4.549, 4.565, 4.582, 4.593, &
!                       4.596, 4.592, 4.581, 4.560, 4.536, 4.506, 4.493, 4.506, 4.536, 4.562, &
!                       4.583, 4.592, 4.580, 4.561, 4.518, 4.458, 4.378, 4.333, 4.384, 4.462, &
!                       4.527, 4.562, 4.579, 4.561, 4.534, 4.457, 4.312, 4.067, 3.854, 4.065, &
!                       4.313, 4.459, 4.536, 4.562, 4.550, 4.508, 4.381, 4.064, 3.386, 2.042, &
!                       3.385, 4.063, 4.378, 4.505, 4.548, 4.547, 4.491, 4.329, 3.850, 2.044, &
!                       2.551, 2.042, 3.850, 4.330, 4.489, 4.545, 4.551, 4.509, 4.378, 4.064, &
!                       3.388, 2.043, 3.383, 4.060, 4.374, 4.503, 4.551, 4.562, 4.533, 4.460, &
!                       4.312, 4.064, 3.851, 4.067, 4.309, 4.457, 4.534, 4.565, 4.579, 4.563, &
!                       4.524, 4.460, 4.377, 4.329, 4.380, 4.458, 4.521, 4.561, 4.579, 4.587, &
!                       4.580, 4.562, 4.533, 4.504, 4.493, 4.507, 4.533, 4.562, 4.583, 4.592, &
!                       4.595, 4.590, 4.579, 4.562, 4.552, 4.548, 4.553, 4.560, 4.579, 4.592, &
!                       4.594/)
! C5G7 UO2 assembly 17x17 thermal group only, source in the moderator region only
!  flux_fuel_openmc = (/2.349, 2.353, 2.364, 2.376, 2.395, 2.421, 2.404, 2.407, 2.419, 2.402, 2.405, &
!                       2.423, 2.395, 2.380, 2.359, 2.348, 2.341, 2.352, 2.362, 2.385, 2.420, 2.461, &
!                       2.553, 2.466, 2.462, 2.548, 2.463, 2.466, 2.555, 2.456, 2.424, 2.382, 2.354, &
!                       2.346, 2.361, 2.387, 2.452, 2.580, 2.620, 3.190, 2.589, 2.585, 3.175, 2.588, &
!                       2.593, 3.196, 2.622, 2.576, 2.447, 2.382, 2.363, 2.377, 2.425, 2.579, 3.213, &
!                       2.649, 2.629, 2.512, 2.503, 2.597, 2.506, 2.509, 2.634, 2.649, 3.209, 2.582, &
!                       2.421, 2.375, 2.394, 2.458, 2.619, 2.650, 2.564, 2.618, 2.511, 2.506, 2.606, &
!                       2.505, 2.509, 2.626, 2.571, 2.651, 2.620, 2.459, 2.393, 2.415, 2.551, 3.195, &
!                       2.634, 2.624, 3.214, 2.603, 2.601, 3.200, 2.604, 2.605, 3.211, 2.628, 2.629, &
!                       3.194, 2.546, 2.416, 2.398, 2.457, 2.592, 2.507, 2.512, 2.605, 2.504, 2.503, &
!                       2.601, 2.505, 2.501, 2.608, 2.509, 2.509, 2.591, 2.465, 2.398, 2.400, 2.461, &
!                       2.583, 2.497, 2.503, 2.596, 2.502, 2.505, 2.600, 2.503, 2.504, 2.604, 2.507, &
!                       2.505, 2.588, 2.461, 2.399, 2.416, 2.546, 3.173, 2.592, 2.596, 3.194, 2.596, &
!                       2.600, 3.193, 2.599, 2.598, 3.197, 2.597, 2.592, 3.172, 2.543, 2.414, 2.401, &
!                       2.461, 2.585, 2.498, 2.500, 2.600, 2.507, 2.499, 2.595, 2.503, 2.505, 2.598, &
!                       2.503, 2.497, 2.582, 2.460, 2.394, 2.400, 2.463, 2.585, 2.509, 2.506, 2.603, &
!                       2.504, 2.503, 2.594, 2.501, 2.506, 2.607, 2.510, 2.508, 2.589, 2.459, 2.401, &
!                       2.410, 2.549, 3.198, 2.635, 2.626, 3.211, 2.607, 2.597, 3.190, 2.603, 2.604, &
!                       3.212, 2.621, 2.631, 3.189, 2.549, 2.417, 2.396, 2.461, 2.621, 2.652, 2.572, &
!                       2.624, 2.512, 2.503, 2.601, 2.505, 2.510, 2.625, 2.572, 2.647, 2.620, 2.461, &
!                       2.398, 2.374, 2.421, 2.580, 3.211, 2.645, 2.635, 2.510, 2.501, 2.599, 2.503, &
!                       2.512, 2.633, 2.651, 3.206, 2.584, 2.423, 2.376, 2.358, 2.378, 2.444, 2.580, &
!                       2.616, 3.195, 2.588, 2.585, 3.183, 2.590, 2.589, 3.190, 2.618, 2.574, 2.448, &
!                       2.382, 2.357, 2.352, 2.358, 2.382, 2.422, 2.459, 2.551, 2.466, 2.465, 2.548, &
!                       2.464, 2.467, 2.549, 2.459, 2.423, 2.383, 2.360, 2.346, 2.348, 2.354, 2.362, &
!                       2.378, 2.394, 2.418, 2.400, 2.404, 2.417, 2.405, 2.401, 2.413, 2.392, 2.378, &
!                       2.360, 2.351, 2.343/)
!  flux_aver_openmc = (/2.522, 2.527, 2.538, 2.551, 2.571, 2.596, 2.581, 2.583, 2.596, 2.580, 2.582, &
!                       2.598, 2.571, 2.556, 2.534, 2.522, 2.515, 2.525, 2.537, 2.563, 2.598, 2.643, &
!                       2.735, 2.647, 2.644, 2.731, 2.645, 2.648, 2.738, 2.639, 2.601, 2.558, 2.529, &
!                       2.520, 2.535, 2.564, 2.633, 2.765, 2.809, 3.252, 2.773, 2.769, 3.237, 2.772, &
!                       2.778, 3.258, 2.810, 2.760, 2.628, 2.558, 2.536, 2.553, 2.603, 2.763, 3.275, &
!                       2.839, 2.815, 2.697, 2.688, 2.781, 2.690, 2.696, 2.820, 2.840, 3.271, 2.765, &
!                       2.599, 2.551, 2.571, 2.642, 2.808, 2.841, 2.754, 2.806, 2.697, 2.691, 2.791, &
!                       2.691, 2.696, 2.811, 2.761, 2.843, 2.809, 2.641, 2.570, 2.591, 2.734, 3.257, &
!                       2.820, 2.811, 3.275, 2.789, 2.786, 3.263, 2.790, 2.791, 3.274, 2.815, 2.816, &
!                       3.255, 2.730, 2.592, 2.576, 2.639, 2.776, 2.692, 2.697, 2.791, 2.690, 2.689, &
!                       2.787, 2.691, 2.686, 2.793, 2.696, 2.694, 2.776, 2.646, 2.576, 2.578, 2.644, &
!                       2.768, 2.681, 2.689, 2.783, 2.688, 2.690, 2.785, 2.689, 2.690, 2.789, 2.693, &
!                       2.690, 2.772, 2.644, 2.576, 2.593, 2.729, 3.235, 2.776, 2.782, 3.257, 2.783, &
!                       2.786, 3.258, 2.785, 2.786, 3.258, 2.784, 2.778, 3.234, 2.727, 2.592, 2.577, &
!                       2.644, 2.771, 2.684, 2.685, 2.786, 2.693, 2.685, 2.782, 2.689, 2.691, 2.784, &
!                       2.688, 2.681, 2.768, 2.643, 2.573, 2.576, 2.647, 2.771, 2.693, 2.692, 2.791, &
!                       2.691, 2.689, 2.780, 2.688, 2.691, 2.792, 2.696, 2.692, 2.773, 2.643, 2.578, &
!                       2.586, 2.733, 3.260, 2.821, 2.812, 3.273, 2.793, 2.784, 3.252, 2.787, 2.790, &
!                       3.274, 2.809, 2.817, 3.253, 2.733, 2.594, 2.571, 2.643, 2.810, 2.842, 2.762, &
!                       2.810, 2.698, 2.689, 2.786, 2.690, 2.697, 2.813, 2.762, 2.838, 2.809, 2.645, &
!                       2.574, 2.551, 2.599, 2.765, 3.272, 2.836, 2.820, 2.695, 2.687, 2.784, 2.687, &
!                       2.698, 2.820, 2.842, 3.271, 2.767, 2.600, 2.551, 2.533, 2.554, 2.625, 2.762, &
!                       2.806, 3.258, 2.775, 2.772, 3.245, 2.775, 2.775, 3.252, 2.806, 2.759, 2.628, &
!                       2.558, 2.532, 2.527, 2.532, 2.558, 2.599, 2.641, 2.734, 2.649, 2.648, 2.732, &
!                       2.647, 2.649, 2.732, 2.642, 2.601, 2.560, 2.535, 2.520, 2.522, 2.528, 2.538, &
!                       2.554, 2.570, 2.593, 2.577, 2.581, 2.595, 2.582, 2.579, 2.589, 2.568, 2.553, &
!                       2.535, 2.525, 2.516/)
! C5G7 MOX assembly 17x17 thermal group only, source in the moderator region only
!  flux_fuel_openmc = (/1.601, 1.555, 1.534, 1.531, 1.540, 1.554, 1.540, 1.546, 1.554, 1.543, 1.546, &
!                       1.556, 1.541, 1.538, 1.533, 1.552, 1.594, 1.555, 1.325, 1.287, 1.295, 1.321, &
!                       1.396, 1.317, 1.315, 1.394, 1.317, 1.318, 1.400, 1.320, 1.299, 1.287, 1.321, &
!                       1.549, 1.535, 1.287, 1.272, 1.357, 1.375, 2.012, 1.326, 1.324, 1.990, 1.324, &
!                       1.326, 2.018, 1.377, 1.355, 1.271, 1.289, 1.535, 1.533, 1.298, 1.359, 2.032, &
!                       1.340, 1.218, 1.114, 1.111, 1.184, 1.109, 1.115, 1.223, 1.343, 2.033, 1.362, &
!                       1.295, 1.532, 1.540, 1.319, 1.374, 1.342, 1.157, 1.179, 1.085, 1.084, 1.163, &
!                       1.083, 1.086, 1.182, 1.160, 1.342, 1.376, 1.321, 1.538, 1.554, 1.394, 2.016, &
!                       1.219, 1.178, 1.869, 1.155, 1.155, 1.856, 1.156, 1.159, 1.868, 1.180, 1.218, &
!                       2.014, 1.395, 1.552, 1.540, 1.310, 1.325, 1.110, 1.082, 1.158, 1.074, 1.074, &
!                       1.155, 1.075, 1.073, 1.157, 1.084, 1.114, 1.327, 1.315, 1.538, 1.538, 1.312, &
!                       1.320, 1.106, 1.080, 1.152, 1.072, 1.075, 1.152, 1.073, 1.075, 1.158, 1.082, &
!                       1.109, 1.321, 1.312, 1.538, 1.546, 1.388, 1.987, 1.180, 1.157, 1.852, 1.152, &
!                       1.153, 1.849, 1.151, 1.153, 1.855, 1.162, 1.182, 1.984, 1.386, 1.548, 1.537, &
!                       1.309, 1.322, 1.105, 1.078, 1.156, 1.073, 1.070, 1.151, 1.073, 1.073, 1.156, &
!                       1.081, 1.105, 1.320, 1.310, 1.537, 1.538, 1.312, 1.325, 1.112, 1.080, 1.157, &
!                       1.074, 1.073, 1.150, 1.072, 1.074, 1.159, 1.083, 1.112, 1.326, 1.312, 1.539, &
!                       1.548, 1.394, 2.014, 1.218, 1.177, 1.862, 1.159, 1.154, 1.848, 1.156, 1.156, &
!                       1.867, 1.180, 1.219, 2.009, 1.393, 1.549, 1.539, 1.318, 1.372, 1.343, 1.159, &
!                       1.179, 1.086, 1.080, 1.160, 1.081, 1.084, 1.181, 1.160, 1.341, 1.374, 1.318, &
!                       1.540, 1.527, 1.295, 1.359, 2.031, 1.341, 1.221, 1.114, 1.108, 1.181, 1.109, &
!                       1.116, 1.220, 1.341, 2.030, 1.358, 1.296, 1.532, 1.534, 1.285, 1.270, 1.355, &
!                       1.374, 2.013, 1.324, 1.321, 1.993, 1.325, 1.326, 2.011, 1.376, 1.354, 1.271, &
!                       1.288, 1.535, 1.554, 1.324, 1.287, 1.296, 1.318, 1.394, 1.316, 1.315, 1.391, &
!                       1.316, 1.318, 1.395, 1.321, 1.298, 1.288, 1.325, 1.551, 1.601, 1.554, 1.535, &
!                       1.533, 1.537, 1.551, 1.541, 1.543, 1.554, 1.544, 1.543, 1.553, 1.539, 1.533, &
!                       1.538, 1.555, 1.596/)
!  flux_aver_openmc = (/1.776, 1.727, 1.704, 1.701, 1.710, 1.725, 1.712, 1.716, 1.725, 1.714, 1.716, &
!                       1.726, 1.712, 1.707, 1.703, 1.724, 1.768, 1.725, 1.529, 1.489, 1.495, 1.526, &
!                       1.606, 1.522, 1.520, 1.604, 1.522, 1.523, 1.610, 1.526, 1.499, 1.486, 1.525, &
!                       1.721, 1.705, 1.488, 1.471, 1.562, 1.584, 2.071, 1.527, 1.524, 2.048, 1.524, &
!                       1.528, 2.076, 1.587, 1.560, 1.470, 1.487, 1.705, 1.704, 1.499, 1.564, 2.091, &
!                       1.545, 1.435, 1.321, 1.317, 1.397, 1.314, 1.322, 1.440, 1.548, 2.091, 1.566, &
!                       1.496, 1.702, 1.712, 1.526, 1.584, 1.549, 1.372, 1.391, 1.288, 1.286, 1.372, &
!                       1.285, 1.290, 1.393, 1.375, 1.548, 1.585, 1.527, 1.709, 1.724, 1.605, 2.075, &
!                       1.436, 1.390, 1.928, 1.365, 1.364, 1.916, 1.366, 1.369, 1.927, 1.393, 1.435, &
!                       2.071, 1.606, 1.722, 1.711, 1.515, 1.527, 1.318, 1.287, 1.367, 1.276, 1.276, &
!                       1.363, 1.277, 1.274, 1.366, 1.287, 1.321, 1.529, 1.520, 1.710, 1.709, 1.518, &
!                       1.521, 1.312, 1.283, 1.361, 1.274, 1.277, 1.361, 1.275, 1.277, 1.366, 1.286, &
!                       1.316, 1.523, 1.518, 1.709, 1.717, 1.599, 2.047, 1.392, 1.367, 1.911, 1.361, &
!                       1.362, 1.909, 1.360, 1.362, 1.914, 1.371, 1.395, 2.043, 1.597, 1.720, 1.707, &
!                       1.514, 1.524, 1.311, 1.280, 1.364, 1.276, 1.273, 1.359, 1.274, 1.275, 1.364, &
!                       1.283, 1.311, 1.521, 1.515, 1.708, 1.708, 1.518, 1.527, 1.319, 1.283, 1.367, &
!                       1.277, 1.274, 1.358, 1.275, 1.277, 1.368, 1.287, 1.319, 1.527, 1.518, 1.710, &
!                       1.718, 1.605, 2.074, 1.434, 1.389, 1.922, 1.367, 1.363, 1.907, 1.363, 1.364, &
!                       1.926, 1.393, 1.437, 2.069, 1.605, 1.720, 1.709, 1.523, 1.584, 1.548, 1.374, &
!                       1.390, 1.289, 1.283, 1.369, 1.284, 1.288, 1.393, 1.375, 1.547, 1.583, 1.525, &
!                       1.710, 1.699, 1.496, 1.565, 2.090, 1.548, 1.437, 1.322, 1.314, 1.394, 1.315, &
!                       1.323, 1.437, 1.548, 2.090, 1.563, 1.496, 1.701, 1.704, 1.484, 1.469, 1.560, &
!                       1.584, 2.074, 1.526, 1.523, 2.052, 1.527, 1.529, 2.070, 1.585, 1.560, 1.470, &
!                       1.488, 1.705, 1.727, 1.528, 1.487, 1.496, 1.523, 1.604, 1.521, 1.521, 1.602, &
!                       1.521, 1.523, 1.605, 1.527, 1.498, 1.488, 1.529, 1.724, 1.775, 1.726, 1.706, &
!                       1.702, 1.707, 1.721, 1.711, 1.713, 1.725, 1.714, 1.714, 1.723, 1.708, 1.703, &
!                       1.707, 1.727, 1.770/)

  if (nGroups >= 1) then
    write(*,*) "Cell ", icell
    open(1529, file = 'results/outputFlux.out')
    err_max_fuel = 0.0_rp
    err_max_aver = 0.0_rp
    do icell = 1, nCell
      faver = 0.0_rp
      do i = 1, nzones(kart(icell))
        faver = faver + cell_flux(1,icell,i,1)*cell_vol(kart(icell),i)
      end do
      faver = faver/sum(cell_vol(kart(icell),:))
      if (icompare > 0) then
        if (.not. withGap) then
          if (abs((cell_flux(1,icell,1,1)/flux_fuel_reference(icell) - 1.0_rp)*100.0_rp) > abs(err_max_fuel)) then
            err_max_fuel = (cell_flux(1,icell,1,1)/flux_fuel_reference(icell) - 1.0_rp)*100.0_rp
          end if
          if (abs((faver/flux_aver_reference(icell) - 1.0_rp)*100.0_rp) > abs(err_max_aver)) then
            err_max_aver = (faver/flux_aver_reference(icell) - 1.0_rp)*100.0_rp
          end if
          write(*,'(i5,4f10.3,2f10.2)') icell,cell_flux(1,icell,1,1),faver,flux_fuel_reference(icell),flux_aver_reference(icell), &
                               (cell_flux(1,icell,1,1)/flux_fuel_reference(icell) - 1.0_rp)*100.0_rp, &
                               (faver/flux_aver_reference(icell) - 1.0_rp)*100.0_rp
          write(1529,'(i5,4f10.3,2f10.2)') icell,cell_flux(1,icell,1,1),faver,flux_fuel_reference(icell),&
                                           flux_aver_reference(icell), &
                               (cell_flux(1,icell,1,1)/flux_fuel_reference(icell) - 1.0_rp)*100.0_rp, &
                               (faver/flux_aver_reference(icell) - 1.0_rp)*100.0_rp
        else
  !        if (icell > nRow * 6) then
  !          icell1 = icell - nRow * 6
            icell1 = icell
          if (abs((cell_flux(1,icell,1,1)/flux_fuel_reference(icell) - 1.0_rp)*100.0_rp) > abs(err_max_fuel)) then
            err_max_fuel = (cell_flux(1,icell,1,1)/flux_fuel_reference(icell) - 1.0_rp)*100.0_rp
          end if
          if (abs((faver/flux_aver_reference(icell) - 1.0_rp)*100.0_rp) > abs(err_max_aver)) then
            err_max_aver = (faver/flux_aver_reference(icell) - 1.0_rp)*100.0_rp
          end if
          write(*,'(i5,4f10.3,2f10.2)') icell1,cell_flux(1,icell,1,1),faver,flux_fuel_reference(icell1),&
                                        flux_aver_reference(icell1), &
                                 (cell_flux(1,icell,1,1)/flux_fuel_reference(icell1) - 1.0_rp)*100.0_rp, &
                                 (faver/flux_aver_reference(icell1) - 1.0_rp)*100.0_rp
          write(1529,'(i5,4f10.3,2f10.2)') icell1,cell_flux(1,icell,1,1),faver,flux_fuel_reference(icell1),&
                                           flux_aver_reference(icell1), &
                                 (cell_flux(1,icell,1,1)/flux_fuel_reference(icell1) - 1.0_rp)*100.0_rp, &
                                 (faver/flux_aver_reference(icell1) - 1.0_rp)*100.0_rp
  !        end if
        end if
      else
        write(*,'(i5,10f10.3)') icell,(cell_flux(1,icell,i,1),i=1,nzones(icell)),faver
        write(1529,'(i5,10f10.3)') icell,(cell_flux(1,icell,i,1),i=1,nzones(icell)),faver
      end if
    end do
    write(*,'(1a,2f10.2)') 'Maximal difference in fuel flux is   : ', err_max_fuel
    write(*,'(1a,2f10.2)') 'Maximal difference in average flux is: ', err_max_aver
    write(1529,'(1a,2f10.2)') 'Maximal difference in fuel flux is   : ', err_max_fuel
    write(1529,'(1a,2f10.2)') 'Maximal difference in average flux is: ', err_max_aver
    close(1529)
    if (ivtk > 0) then
  !    vtk_output_file = "flux_plot.vtu"
      vtk_output_file = "flux_points_cccpo.vtp"
      call PlotFlux(vtk_output_file, sym, cell_vertx, cell_verty, nCell, kart, nzones, cell_flux(1,:,:,:), 500, 500)
  !    call WriteMeshFluxToCSV(vtk_output_file, sym, cell_vertx, cell_verty, nCell, kart, nzones, cell_flux, 10, 10)
    end if
  end if

end subroutine CCCPO_SC

