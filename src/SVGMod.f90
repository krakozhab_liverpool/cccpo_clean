module SVGMod
  use precision_mod

contains

  subroutine SVG_ActionButton(output_file,script,target_id,x,y,width,height, &
      & stroke_width,button_fill_opacity,stroke_rgb,button_fill_rgb, &
      & string,font,font_size,text_fill_opacity,text_fill_rgb)

    ! SVG_ActionButton is called to place an ActionButton in an SVG file. The
    ! respective ECMAscript function must be present in the SVG file

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! script: character, scalar. Specifies the script that defines the action that
    !	will be taken upon the onclick event of the button.
    ! target_id: character, scalar. The id of the object that will be affected by
    !	the predefined action.
    ! x: real, scalar. The x-axis coordinate of the button center. (px).
    ! y: real, scalar. The y-axis coordinate of the button center. (px).
    ! width: real, scalar. The button width. (px).
    ! height: real, scalar. he button height. (px).
    ! stroke_width: real, scalar. The thickness of the button line (px).
    ! button_fill_opacity: real, scalar. The opacity of the button fill
    !	(dimensionless, 0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! button_fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB
    !	channel values (dimensionless, 0<=fill_rgb(:)<=255).
    ! string: character, scalar. The string that will be drawn.
    ! font: character, scalar. The font that will be used for the rendering. The
    !	selected font must be supported in the system that will be used to
    !	render the SVG file.
    ! font_size: real, scalar. The size of the font.
    ! text_fill_opacity: real, scalar. The opacity of the text fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! text_fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB
    !	channel values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_ActionButton(output_file,script,target_id,x,y,width,height, &
      !	& stroke_width,button_fill_opacity,stroke_rgb,button_fill_rgb, &
      !	& string,font,font_size,text_fill_opacity,text_fill_rgb)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*),script*(*),string*(*),font*(*)
    !character,intent(in):: target_id*(*)
    !real(rp),intent(in):: x,y,width,height,stroke_width
    !real(rp),intent(in):: button_fill_opacity,text_fill_opacity,font_size
    !integer,intent(in):: stroke_rgb(3),button_fill_rgb(3),text_fill_rgb(3)
    !end subroutine SVG_ActionButton
    !end interface

    !character*(str_len):: id='1'
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Vanish(output_file='plot.svg')
    !call SVG_ActionButton(output_file='plot.svg',script='vanish',target_id=id, &
      !	& x=200.0_rp,y=500.0_rp,width=200.0_rp,height=50.0_rp, &
      !	& stroke_width=5.0_rp,button_fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,0,255/),button_fill_rgb=(/255,255,255/), &
      !	& string='hide circle',font='verdana',font_size=20.0_rp, &
      !	& text_fill_opacity=1.0_rp,text_fill_rgb=(/0,0,0/))
    !call SVG_Circle(output_file='plot.svg', &
      !	& x=200.0_rp,y=100.0_rp,radius=50.0_rp, &
      !	& stroke_width=10.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/255,255,255/), &
      !	& rotate=(/0.0_rp,100.0_rp,100.0_rp/),id=id)
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*),script*(*),string*(*),font*(*)
    character,intent(in):: target_id*(*)
    real(rp),intent(in):: x,y,width,height,stroke_width
    real(rp),intent(in):: button_fill_opacity,text_fill_opacity,font_size
    integer,intent(in):: stroke_rgb(3),button_fill_rgb(3),text_fill_rgb(3)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    !Edit this IF block in order to support more scripts.
    if (all(trim(script)/=(/'vanish'/))) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: unsupported script."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (any((/width,height/)<0.0)) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: invalid shape parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/width,height/)==0.0)) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"WARNING: SVG object will not be viewable."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<button_fill_opacity).or.(button_fill_opacity<0.0).or. &
        & (1.0<text_fill_opacity).or.(text_fill_opacity<0.0)) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: invalid paint parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((button_fill_opacity==0.0).and.(text_fill_opacity==0.0).and. &
        & (stroke_width==0.0)) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(text_fill_rgb<0)).or.(any(button_fill_rgb<0)).or. &
        & (any(stroke_rgb<0)).or.(any(stroke_rgb>255)).or. &
        & (any(text_fill_rgb>255)).or.(any(button_fill_rgb>255))) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    ! Group the rounded rectangle and its text.
    write(io,'(A2)',iostat=err)"<g"

    if (err/=0) then
      write(*,*)"SVG_ActionButton"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! Refer to the ECMAscript function.
    ! Edit this SELECT CASE block in order to support more scripts.
    select case (script)
      case("vanish")
        write(io,*)"onclick='vanish(evt,"//'"'// &
          & trim(target_id)//'"'//")'"
    end select

    write(io,'(A1)')">"
    write(io,*)

    ! Print the rounded rectangle.
    write(io,'(A5)',iostat=err)"<rect"
    write(io,*)"x='"//trim(StringReal(x-width/2.0_rp))//"'"
    write(io,*)"y='"//trim(StringReal(viewBox_height-y-height/2.0_rp))//"'"
    write(io,*)"rx='"//trim(StringReal(height/5.0_rp))//"'"
    write(io,*)"ry='"//trim(StringReal(height/5.0_rp))//"'"
    write(io,*)"width='"//trim(StringReal(width))//"'"
    write(io,*)"height='"//trim(StringReal(height))//"'"
    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(button_fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(button_fill_rgb(1)))//","// &
      & trim(StringInteger(button_fill_rgb(2)))//","// &
      & trim(StringInteger(button_fill_rgb(3)))//")'"
    write(io,'(A2)')"/>"
    write(io,*)

    ! Print the button text.
    write(io,'(A5)')"<text"
    write(io,*)"x='"//trim(StringReal(x))//"'"
    write(io,*)"y='"//trim(StringReal(viewBox_height-y+font_size/3.0_rp))//"'"
    write(io,*)"font-family='"//trim(font)//"'"
    write(io,*)"font-size='"//trim(StringReal(font_size))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(text_fill_opacity))//"'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(text_fill_rgb(1)))//","// &
      & trim(StringInteger(text_fill_rgb(2)))//","// &
      & trim(StringInteger(text_fill_rgb(3)))//")'"
    write(io,*)"text-anchor='middle'"
    write(io,*)"font-weight='normal'"
    !write(io,*)"font-style='italic'"
    write(io,'(A1)')">"
    write(io,*)trim(string)
    write(io,'(A7)')"</text>"
    write(io,*)

    ! End the grouping
    write(io,'(A4)',iostat=err)"</g>"
    write(io,*)

  end subroutine SVG_ActionButton

  subroutine SVG_BezierQuadratic(output_file,x,y,stroke_width,fill_opacity,stroke_rgb,fill_rgb,rotate,scalar_name,scalar_value,id)
    ! SVG_BezierQuadratic is called to draw a quadratic Bezier curve in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, array (1D). The x-axis coordinates of the bezier curve control
    !	points. (px).
    ! y: real, array (1D). The y-axis coordinates of the bezier curve control
    !	points. (px).
    ! stroke_width: real, scalar. The thickness of the bezier curve (px).
    ! fill_opacity: real, scalar. The opacity of the fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_BezierQuadratic(output_file,x,y, &
      !	& stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x(:),y(:),stroke_width,fill_opacity
    !integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_BezierQuadratic
    !end interface

    !real(rp):: x(15),y(15)
    !x(1)=50.0    ; y(1)=375.0
    !x(2)=150.0   ; y(2)=375.0
    !x(3)=150.0   ; y(3)=425.0
    !x(4)=250.0   ; y(4)=425.0
    !x(5)=250.0   ; y(5)=375.0
    !x(6)=350.0   ; y(6)=375.0
    !x(7)=350.0   ; y(7)=500.0
    !x(8)=450.0   ; y(8)=500.0
    !x(9)=450.0   ; y(9)=375.0
    !x(10)=550.0  ; y(10)=375.0
    !x(11)=550.0  ; y(11)=575.0
    !x(12)=650.0  ; y(12)=575.0
    !x(13)=650.0  ; y(13)=375.0
    !x(14)=750.0  ; y(14)=375.0
    !x(15)=750.0  ; y(15)=650.0
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_BezierQuadratic(output_file='plot.svg',x=x(1:15),y=y(1:15)-330.0, &
      !	& stroke_width=5.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/255,255,255/), &
      !	& rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/))
    !call SVG_BezierQuadratic(output_file='plot.svg',x=x(1:14),y=y(1:14)+100.0, &
      !	& stroke_width=5.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,255,0/),fill_rgb=(/255,255,255/), &
      !	& rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringInteger, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x(:),y(:),stroke_width,fill_opacity
    integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: file must have the .svg extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"WARNING: negative coordinates"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(y>viewBox_height)).or.(any(x>viewBox_width))) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (size(x)/=size(y)) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: coordinate arrays do not match"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: invalid stroke parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: invalid paint parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: invalid RGB channel value"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (size(x)<2) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"WARNING: can not plot a line with less than two points"
      write(*,*)"No line will be plotted."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((all(x==0)).or.(all(y==0))) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"WARNING: all array points are equal to zero"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_BezierQuadratic"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A9)',iostat=err)"<path d='"

    if (err/=0) then
      write(*,*)"SVG_BezierQuadratic"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename: ",trim(output_file)
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"M "//trim(StringReal(x(1)))//" "// &
      & trim(StringReal(viewBox_height-y(1)))

    ! Draw a quadratic Bezier ("Q") starting from the first point of the array
    ! ("M") to point "i+1" using point "i" as the control point.

    if (size(x)>2) then
      do i=2,size(x)-1,2
        if ((i+1)/=size(x)) then
          write(io,*)"Q "//trim(StringReal(x(i)))//","// &
            & trim(StringReal(viewBox_height-y(i)))//" "// &
            & trim(StringReal(x(i+1)))//","// &
            & trim(StringReal(viewBox_height-y(i+1)))
        else
          write(io,*)"Q "//trim(StringReal(x(i)))//","// &
            & trim(StringReal(viewBox_height-y(i)))//" "// &
            & trim(StringReal(x(i+1)))//","// &
            & trim(StringReal(viewBox_height-y(i+1)))//"'"
        end if
      end do
      if (mod(size(x),2)==0) write(io,*)"T "// &
        & trim(StringReal(x(size(x))))//" "// &
        & trim(StringReal(viewBox_height-y(size(y))))//"'"
    end if

    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_BezierQuadratic

  subroutine SVG_CartesianPlot(output_file, &
      & x_points,y_points,x_title,y_title, &
      & x_start,x_end,y_start,y_end, &
      & x_decimal_places,y_decimal_places,scientific_x,scientific_y, &
      & x_tick_interval,y_tick_interval, &
      & draw_ticks,tick_length,tick_width, &
      & diagram_title,data_title,title_diagram_font_size, &
      & title_axis_font_size,numbers_axis_font_size, &
      & font_rgb,axis_stroke_rgb,axis_stroke_width, &
      & draw_grid,grid_stroke_width,grid_stroke_rgb, &
      & shape_type,shape_size,shape_stroke_width, &
      & shape_fill_rgb,shape_stroke_rgb, &
      & line_type,line_stroke_width,line_stroke_rgb)

    ! SVG_CartesianPlot draws a cartesian diagram in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x_points: real, array (1D). The cartesian coordinates of the x-component of
    !	the points that will be plotted. Arrays x_points and y_points must have
    !	the same amount of elements and a 1-1 correspondance.
    ! y_points: real, array (1D). The cartesian coordinates of the y-component of
    !	the points that will be plotted. Arrays x_points and y_points must have
    !	the same amount of elements and a 1-1 correspondance.
    ! x_title: character, scalar. The title of the x-component.
    ! y_title: character, scalar. The title of the y-component.
    ! x_start: real, scalar. The initial value of the x-axis.
    ! x_end: real, scalar. The ending value of the x-axis.
    ! y_start: real, scalar. The initial value of the y-axis.
    ! y_end: real, scalar. The ending value of the y-axis.
    ! x_decimal_places: integer, scalar. The amount of decimal places of the numbers
    !	on the x-axis.
    ! y_decimal_places: integer, scalar. The amount of decimal places of the numbers
    !	on the y-axis.
    ! scientific_x: logical, scalar. Specifies whether the scientific notation will
    !	be used for the numbers on the x-axis.
    ! scientific_y: logical, scalar. Specifies whether the scientific notation will
    !	be used for the numbers on the y-axis.
    ! x_tick_interval: real, scalar. The interval between the ticks for the x axis.
    ! y_tick_interval: real, scalar. The interval between the ticks for the y axis.
    ! draw_ticks: logical, scalar. Specifies whether ticks will be drawn.
    ! tick_length: real, scalar. The length of the axes' tick.
    ! tick_width: real, scalar. The width of the axes' tick.
    ! diagram_title: character, scalar. The title of the graph.
    ! data_title: character, scalar. The title of the data series (for the legend).
    ! title_diagram_font_size: integer, scalar. The font size of the diagram title.
    ! title_axis_font_size: integer, scalar. The font size of the axes titles.
    ! number_axis_font_size: integer, scalar. The font size of the axes numbers.
    ! font_rgb: integer, array (1D) with 3 elements. Contains the RGB channel
    !	values of the color of all the text in the graph.
    ! axis_stroke_rgb: integer, array (1D) with 3 elements. Contains the RGB
    !	channel values of the color with which the line will be drawn.
    ! axis_stroke_width: integer, scalar. The thickness of the axes' line (px).
    ! draw_grid: logical, scalar. Specifies whether gridlines will be drawn.
    ! grid_stroke_width: integer, scalar. The width of the gridlines.
    ! grid_stroke_rgb: integer, array (1D) with 3 elements. Contains the RGB
    !	channel values of the color with which the gridlines will be drawn.
    ! shape_type: character, scalar. Specifies which shape will be used to plot the
    !	data.
    ! shape_size: integer, scalar. The shape's diameter (in pixels).
    ! shape_stroke_width: integer, scalar. The thickness of the line used to draw
    !	the circle (in pixels).
    ! shape_fill_rgb: integer, array (1D) with 3 elements. Contains the RGB channel
    !	values of the color with which the shape will be filled.
    ! shape_stroke_rgb: integer, array (1D) with 3 elements. Contains the RGB
    !	channel values of the color with which the line of the circle will be
    !	drawn.
    ! line_type: character, scalar. Specifies the kind of line that connects the
    !	diagram points.
    ! line_stroke_width: integer, scalar. The width of the line that connects the
    !	diagram points.
    ! line_stroke_rgb: integer, array (1D) with 3 elements. Contains the RGB
    !	channel values of the color with which the line that connects the
    !	diagram points will be drawn.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_CartesianPlot(output_file, &
      !	& x_points,y_points,x_title,y_title, &
      !	& x_start,x_end,y_start,y_end, &
      !	& x_decimal_places,y_decimal_places,scientific_x,scientific_y, &
      !	& x_tick_interval,y_tick_interval, &
      !	& draw_ticks,tick_length,tick_width, &
      !	& diagram_title,data_title,title_diagram_font_size, &
      !	& title_axis_font_size,numbers_axis_font_size, &
      !	& font_rgb,axis_stroke_rgb,axis_stroke_width, &
      !	& draw_grid,grid_stroke_width,grid_stroke_rgb, &
      !	& shape_type,shape_size,shape_stroke_width, &
      !	& shape_fill_rgb,shape_stroke_rgb, &
      !	& line_type,line_stroke_width,line_stroke_rgb)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*),shape_type*(*),line_type*(*)
    !character,intent(in):: diagram_title*(*),x_title*(*),y_title*(*)
    !character,intent(in):: data_title*(*)
    !real(rp),intent(in):: x_points(:),y_points(:),x_start,x_end,y_start,y_end
    !real(rp),intent(in):: x_tick_interval,y_tick_interval,tick_length,tick_width
    !real(rp),intent(in):: axis_stroke_width,grid_stroke_width
    !real(rp),intent(in):: shape_size,shape_stroke_width,line_stroke_width
    !real(rp),intent(in):: title_axis_font_size,numbers_axis_font_size
    !real(rp),intent(in):: title_diagram_font_size
    !integer,intent(in):: x_decimal_places,y_decimal_places
    !integer,intent(in):: axis_stroke_rgb(3),grid_stroke_rgb(3),font_rgb(3)
    !integer,intent(in):: shape_fill_rgb(3),shape_stroke_rgb(3),line_stroke_rgb(3)
    !logical,intent(in):: draw_ticks,draw_grid,scientific_x,scientific_y
    !end subroutine SVG_CartesianPlot
    !end interface

    !real(rp):: x(30),y(30)

    !x(1)=0.0 ; y(1)=0.000 ; x(11)=1.0 ; y(11)=0.841 ; x(21)=2.0 ; y(21)=0.909
    !x(2)=0.1 ; y(2)=0.100 ; x(12)=1.1 ; y(12)=0.891 ; x(22)=2.1 ; y(22)=0.863
    !x(3)=0.2 ; y(3)=0.199 ; x(13)=1.2 ; y(13)=0.932 ; x(23)=2.2 ; y(23)=0.808
    !x(4)=0.3 ; y(4)=0.296 ; x(14)=1.3 ; y(14)=0.964 ; x(24)=2.3 ; y(24)=0.746
    !x(5)=0.4 ; y(5)=0.389 ; x(15)=1.4 ; y(15)=0.985 ; x(25)=2.4 ; y(25)=0.675
    !x(6)=0.5 ; y(6)=0.479 ; x(16)=1.5 ; y(16)=0.997 ; x(26)=2.5 ; y(26)=0.598
    !x(7)=0.6 ; y(7)=0.565 ; x(17)=1.6 ; y(17)=1.000 ; x(27)=2.6 ; y(27)=0.516
    !x(8)=0.7 ; y(8)=0.644 ; x(18)=1.7 ; y(18)=0.992 ; x(28)=2.7 ; y(28)=0.427
    !x(9)=0.8 ; y(9)=0.717 ; x(19)=1.8 ; y(19)=0.974 ; x(29)=2.8 ; y(29)=0.335
    !x(10)=0.9 ; y(10)=0.783 ; x(20)=1.9 ; y(20)=0.946 ; x(30)=2.9 ; y(30)=0.239

    !call SVG_CartesianPlot( &
      !output_file='plot.svg', &
      !x_points=x, &
      !y_points=y, &
      !x_title='x_axis', &
      !y_title='y_axis', &
      !x_start=0.0_rp, &
      !x_end=3.0_rp, &
      !y_start=0.0_rp, &
      !y_end=1.1_rp, &
      !x_decimal_places=1, &
      !y_decimal_places=1, &
      !scientific_x=.false., &
      !scientific_y=.false., &
      !x_tick_interval=0.3_rp, &
      !y_tick_interval=0.2_rp, &
      !draw_ticks=.false., &
      !tick_length=10.0_rp, &
      !tick_width=3.0_rp, &
      !diagram_title='cartesian_diagram', &
      !data_title='data_series', &
      !title_diagram_font_size=40.0_rp, &
      !title_axis_font_size=30.0_rp, &
      !numbers_axis_font_size=25.0_rp, &
      !font_rgb=(/0,0,0/), &
      !axis_stroke_rgb=(/0,0,0/), &
      !axis_stroke_width=3.0_rp, &
      !draw_grid=.true., &
      !grid_stroke_width=2.0_rp, &
      !grid_stroke_rgb=(/127,127,127/), &
      !shape_type='square', &
      !shape_size=10.0_rp, &
      !shape_stroke_width=1.0_rp, &
      !shape_fill_rgb=(/255,0,0/), &
      !shape_stroke_rgb=(/255,0,0/), &
      !line_type='crooked', &
      !line_stroke_width=2.0_rp, &
      !line_stroke_rgb=(/0,0,255/) &
      !)

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,str_len,warnings_pause, &
      & viewBox_width,viewBox_height
    use ArrayMod, only : ArrayExpandCharacter, ArrayExpandReal
    use MathMod, only : LinearIntExp, BezierQuadraticControl
    use ReadWriteMod, only : FormatReal, StringInteger
    use HTMLMod, only : HTML_Initialize, HTML_Heading, HTML_Table, HTML_Finalize

    implicit none

    ! Parameters:
    character,parameter:: font*(7)='verdana'
    logical,parameter:: create_toggle_buttons=.true.,create_data_links=.true.

    ! Argument variables:
    character,intent(in):: output_file*(*),shape_type*(*),line_type*(*)
    character,intent(in):: diagram_title*(*),x_title*(*),y_title*(*)
    character,intent(in):: data_title*(*)
    real(rp),intent(in):: x_points(:),y_points(:),x_start,x_end,y_start,y_end
    real(rp),intent(in):: x_tick_interval,y_tick_interval,tick_length,tick_width
    real(rp),intent(in):: axis_stroke_width,grid_stroke_width
    real(rp),intent(in):: shape_size,shape_stroke_width,line_stroke_width
    real(rp),intent(in):: title_axis_font_size,numbers_axis_font_size
    real(rp),intent(in):: title_diagram_font_size
    integer,intent(in):: x_decimal_places,y_decimal_places
    integer,intent(in):: axis_stroke_rgb(3),grid_stroke_rgb(3),font_rgb(3)
    integer,intent(in):: shape_fill_rgb(3),shape_stroke_rgb(3),line_stroke_rgb(3)
    logical,intent(in):: draw_ticks,draw_grid,scientific_x,scientific_y

    ! Private variables:
    real(rp):: rectangle_width,rectangle_height,diagram_width,diagram_height
    real(rp):: legend_position_x,legend_position_y
    real(rp):: real_i,points(size(x_points),2),axis_start
    real(rp),allocatable:: bezier_points(:,:)
    integer:: i

    character*(str_len),allocatable,target,save:: file_list(:)
    character*(str_len),pointer:: character_pointer_1d(:)
    real(rp),allocatable,target,save:: legend_spacing(:)
    real(rp),pointer:: real_pointer_1d(:)
    logical:: plot_axes

    character*(str_len):: characters(2)
    real(rp):: numbers(size(x_points),2),tick_number,tick_coordinate

    ! Variable initialization
    ! axis_start: real, scalar. A value that specifies the margin left between the
    !	border of the canvas and the lower left tip of the diagram (px).
    diagram_width=viewBox_height
    diagram_height=viewBox_height
    axis_start=2.0_rp*title_diagram_font_size
    rectangle_width=diagram_width-2.0_rp*axis_start
    rectangle_height=rectangle_width
    plot_axes=.false.
    if (allocated(bezier_points)) deallocate(bezier_points)
    if (.not.(allocated(file_list))) allocate(file_list(0))
    if (.not.(allocated(legend_spacing))) allocate(legend_spacing(0))
    legend_position_y=diagram_height-axis_start
    legend_position_x=diagram_width-axis_start/2.0_rp

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (viewBox_height>=viewBox_width) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"ERROR: canvas height may not be bigger than canvas width"
      write(*,*)"for this type of graph"
      write(*,*)"canvas width: ",viewBox_width
      write(*,*)"canvas height: ",viewBox_height
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((diagram_width-2.0_rp*axis_start<=0).or. &
        & (diagram_height-2.0_rp*axis_start<=0)) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"ERROR: invalid axis geometry parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (size(x_points)/=size(y_points)) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"ERROR: x and y coordinates do not match"
      write(*,*)"size(x): ",size(x_points)
      write(*,*)"size(y): ",size(y_points)
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((all(x_points==0.0)).or.(all(y_points==0.0))) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"WARNING: all array points are equal to zero"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (all(shape_type/=(/'none    ','circle  ','square  ','cross   ', &
        & 'ex      ','triangle','diamond '/))) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"ERROR: incorrectly specified point shape"
      write(*,*)"shape_type: ",shape_type
      write(*,*)"supported values: 'none', 'circle', 'square',"
      write(*,*)"'cross', 'ex', 'triangle', 'diamond'"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (all(line_type/=(/'none   ','crooked','bezier '/))) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"ERROR: incorrectly specified line type"
      write(*,*)"line_type: ",line_type
      write(*,*)"supported values: 'none', 'crooked', 'bezier'"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((shape_type=='none').and.(line_type=='none')) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"WARNING: no data will be plotted"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((len_trim(x_title)>str_len).or. &
        & (len_trim(y_title)>str_len)) then
      write(*,*)"SVG_CartesianPlot"
      write(*,*)"ERROR: component title too long"
      write(*,*)"len_trim(x_title): ",len_trim(x_title)
      write(*,*)"len_trim(y_title): ",len_trim(y_title)
      write(*,*)"str_len: ",str_len
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    add_to_diagram: if (any(file_list==output_file)) then

      ! Scan the filelist:
      do i=1,size(file_list)
        if (file_list(i)==output_file) then

          ! Resurrect the SVG document:
          call SVG_Resurrect(output_file)
          ! Regulate spacing for the graph's legend:
          if (len_trim(data_title)/=0) legend_spacing(i)= &
            & legend_spacing(i)-1.5_rp*numbers_axis_font_size
          exit

        end if
      end do

    else add_to_diagram

      ! Initialize the diagram:
      plot_axes=.true.
      call SVG_Initialize(output_file)

      ! Add the filename to the list of initialized files:
      character_pointer_1d=>file_list
      call ArrayExpandCharacter(character_pointer_1d,output_file)
      deallocate(file_list)
      allocate(file_list(size(character_pointer_1d)))
      file_list=character_pointer_1d

      ! Initialize a legend:
      real_pointer_1d=>legend_spacing
      call ArrayExpandReal(real_pointer_1d,legend_position_y)
      deallocate(legend_spacing)
      allocate(legend_spacing(size(real_pointer_1d)))
      legend_spacing=real_pointer_1d

    end if add_to_diagram

    ! ------------------------------------------------------------------------------

    axes_plot: if (plot_axes) then

      ! Diagram title:
      call SVG_Text(output_file=output_file,string=diagram_title, &
        & x=diagram_width/2.0_rp,y=diagram_height-axis_start/2.0_rp, &
        & font=font,font_size=title_diagram_font_size,fill_opacity=1.0_rp, &
        & fill_rgb=font_rgb,anchor='middle')

      ! Draw the axes.

      ! The coordinates of the four square points are:
      ! x_a=axis_start                    (lower left square corner)
      ! y_a=axis_start                    (lower left square corner)
      ! x_b=axis_start                    (upper left square corner)
      ! y_b=axis_start+rectangle_height   (upper left square corner)
      ! x_c=axis_start+rectangle_width    (upper right square corner)
      ! y_c=axis_start+rectangle_height   (upper right square corner)
      ! x_d=axis_start+rectangle_width    (lower right square corner)
      ! y_d=axis_start                    (lower right square corner)

      call SVG_GroupStart(output_file)

      ! Left (AB) Vertical axis:
      call SVG_Line(output_file=output_file, &
        & x=(/axis_start,axis_start/), &
        & y=(/axis_start,axis_start+rectangle_height/), &
        stroke_width=axis_stroke_width,stroke_rgb=axis_stroke_rgb)

      ! Upper (BC) Horizontal axis:
      call SVG_Line(output_file=output_file, &
        & x=(/axis_start,axis_start+rectangle_width/), &
        & y=(/axis_start+rectangle_height,axis_start+rectangle_height/), &
        stroke_width=axis_stroke_width,stroke_rgb=axis_stroke_rgb)

      ! Right (CD) Vertical axis:
      call SVG_Line(output_file=output_file, &
        & x=(/axis_start+rectangle_width,axis_start+rectangle_width/), &
        & y=(/axis_start+rectangle_height,axis_start/), &
        stroke_width=axis_stroke_width,stroke_rgb=axis_stroke_rgb)

      ! Lower (AD) Horizontal axis:
      call SVG_Line(output_file=output_file, &
        & x=(/axis_start,axis_start+rectangle_width/), &
        & y=(/axis_start,axis_start/), &
        stroke_width=axis_stroke_width,stroke_rgb=axis_stroke_rgb)

      ! Axes numbers.

      ! Horizontal axis numbering:
      tick_number=x_start
      real_i=0.0_rp
      do while (tick_number<=x_end)
        if ((x_start<tick_number).and.(tick_number<x_end)) then
          tick_coordinate=LinearIntExp(x_start,x_end, &
            & axis_start,axis_start+rectangle_width,tick_number)
        else if (tick_number==x_start) then
          tick_coordinate=axis_start
        else if (tick_number>=x_end) then
          tick_coordinate=axis_start+rectangle_width
        end if
        call SVG_Text(output_file=output_file, &
          & string=trim(FormatReal( &
          & tick_number,x_decimal_places,scientific_x)), &
          & x=tick_coordinate, &
          & y=axis_start-5.0_rp*numbers_axis_font_size/4.0_rp, &
          & font=font,font_size=numbers_axis_font_size, &
          & fill_opacity=1.0_rp,fill_rgb=axis_stroke_rgb,anchor='middle')
        tick_number=x_start+real_i*x_tick_interval
        real_i=real_i+1.0_rp
      end do

      ! Vertical axis numbering:
      tick_number=y_start
      real_i=0.0_rp
      do while (tick_number<=y_end)
        if ((y_start<tick_number).and.(tick_number<y_end)) then
          tick_coordinate=LinearIntExp(y_start,y_end, &
            & axis_start,axis_start+rectangle_height,tick_number)
        else if (tick_number==y_start) then
          tick_coordinate=axis_start
        else if (tick_number>=y_end) then
          tick_coordinate=axis_start+rectangle_height
        end if
        call SVG_Text(output_file=output_file, &
          & string=trim(FormatReal( &
          & tick_number,y_decimal_places,scientific_y)), &
          & x=axis_start-numbers_axis_font_size/2.0_rp, &
          & y=tick_coordinate-numbers_axis_font_size/3.0_rp, &
          & font=font,font_size=numbers_axis_font_size, &
          & fill_opacity=1.0_rp,fill_rgb=axis_stroke_rgb,anchor='end')
        tick_number=y_start+real_i*y_tick_interval
        real_i=real_i+1.0_rp
      end do

      ! Draw the ticks:

      tick_draw: if (draw_ticks) then

        ! Ticks x-axis:
        tick_number=x_start
        real_i=0.0_rp
        do while (tick_number<x_end)
          if ((x_start<tick_number).and.(tick_number<x_end)) then
            tick_coordinate=LinearIntExp(x_start,x_end, &
              & axis_start,axis_start+rectangle_width,tick_number)
            call SVG_Line(output_file=output_file, &
              & x=(/tick_coordinate,tick_coordinate/), &
              & y=(/axis_start,axis_start+tick_length/), &
              & stroke_width=tick_width, &
              & stroke_rgb=axis_stroke_rgb)
          end if
          tick_number=x_start+real_i*x_tick_interval
          real_i=real_i+1.0_rp
        end do

        ! Ticks y-axis:
        tick_number=y_start
        real_i=0.0_rp
        do while (tick_number<y_end)
          if ((y_start<tick_number).and.(tick_number<y_end)) then
            tick_coordinate=LinearIntExp(y_start,y_end, &
              & axis_start,axis_start+rectangle_height,tick_number)
            call SVG_Line(output_file=output_file, &
              & x=(/axis_start,axis_start+tick_length/), &
              & y=(/tick_coordinate,tick_coordinate/), &
              & stroke_width=tick_width, &
              & stroke_rgb=axis_stroke_rgb)
          end if
          tick_number=y_start+real_i*y_tick_interval
          real_i=real_i+1.0_rp
        end do

      end if tick_draw

      ! Axes titles.

      ! Title x-axis:
      call SVG_Text(output_file=output_file,string=x_title, &
        & x=diagram_width/2.0_rp, &
        & y=title_axis_font_size/2.0_rp, &
        & font=font,font_size=title_axis_font_size, &
        & fill_opacity=1.0_rp,fill_rgb=font_rgb,anchor='middle')

      ! Title y-axis:
      call SVG_Text(output_file=output_file,string=y_title, &
        & x=title_axis_font_size/4.0_rp, &
        & y=diagram_height/2.0_rp, &
        & font=font,font_size=title_axis_font_size, &
        & fill_opacity=1.0_rp,fill_rgb=font_rgb,anchor='middle', &
        & rotate=(/-90.0_rp,title_axis_font_size/4.0_rp, &
        & diagram_height/2.0_rp/))

      call SVG_GroupEnd(output_file)

      ! Draw the gridlines:

      grid_draw: if (draw_grid) then

        call SVG_GroupStart(output_file)

        tick_number=x_start
        real_i=0.0_rp
        do while (tick_number<x_end)
          if ((x_start<tick_number).and.(tick_number<x_end)) then
            tick_coordinate=LinearIntExp(x_start,x_end, &
              & axis_start,axis_start+rectangle_width,tick_number)
            call SVG_Line(output_file=output_file, &
              & x=(/tick_coordinate,tick_coordinate/), &
              & y=(/axis_start,axis_start+rectangle_height/), &
              & stroke_width=grid_stroke_width, &
              & stroke_rgb=grid_stroke_rgb)
          end if
          tick_number=x_start+real_i*x_tick_interval
          real_i=real_i+1.0_rp
        end do

        tick_number=y_start
        real_i=0.0_rp
        do while (tick_number<y_end)
          if ((y_start<tick_number).and.(tick_number<y_end)) then
            tick_coordinate=LinearIntExp(y_start,y_end, &
              & axis_start,axis_start+rectangle_height,tick_number)
            call SVG_Line(output_file=output_file, &
              & x=(/axis_start,axis_start+rectangle_width/), &
              & y=(/tick_coordinate,tick_coordinate/), &
              & stroke_width=grid_stroke_width, &
              & stroke_rgb=grid_stroke_rgb)
          end if
          tick_number=y_start+real_i*y_tick_interval
          real_i=real_i+1.0_rp
        end do

        call SVG_GroupEnd(output_file)

      end if grid_draw

    end if axes_plot

    ! ------------------------------------------------------------------------------

    coordinate_conversion_loop: do i=1,size(x_points)

      points(i,1)=LinearIntExp(x_start,x_end, &
        axis_start,axis_start+rectangle_width,x_points(i))

      points(i,2)=LinearIntExp(y_start,y_end, &
        axis_start,axis_start+rectangle_height,y_points(i))

    end do coordinate_conversion_loop

    ! ------------------------------------------------------------------------------

    call SVG_GroupStart(output_file=output_file,id=trim(data_title))

    ! Diagram line:

    select case(trim(line_type))

      case('crooked')

        call SVG_Polyline(output_file=output_file,x=points(:,1),y=points(:,2), &
          & stroke_width=line_stroke_width,fill_opacity=0.0_rp, &
          & stroke_rgb=line_stroke_rgb,fill_rgb=(/255,255,255/), &
          & id=trim(data_title))

      case('bezier')

        ! In order for the Bezier curve to be drawn correctly, the array with the
        ! Bezier curve and control points needs to have an odd amount of elements.
        if (mod(size(x_points),2)==0) then
          allocate(bezier_points(size(x_points)+1,2))
          bezier_points(size(x_points)+1,1)=points(size(x_points),1)
          bezier_points(size(x_points)+1,2)=points(size(x_points),2)
        else
          allocate(bezier_points(size(x_points),2))
        end if

        do i=1,size(x_points)
          if ((mod(i,2)==0).and.(i/=1).and.(i/=size(x_points))) then

            ! The Bezier control points are calculated:
            bezier_points(i,:)=BezierQuadraticControl( &
              (/points(i-1,1),points(i-1,2)/), &
              (/points(i+1,1),points(i+1,2)/), &
              (/points(i,1),points(i,2)/))

          else

            ! The rest of the Bezier curve points are the same as the diagram points.
            bezier_points(i,:)=points(i,:)

          end if
        end do

        call SVG_BezierQuadratic(output_file=output_file, &
          & x=bezier_points(:,1),y=bezier_points(:,2), &
          & stroke_width=line_stroke_width,fill_opacity=0.0_rp, &
          & stroke_rgb=line_stroke_rgb,fill_rgb=(/255,255,255/), &
          & id=trim(data_title))

    end select

    ! Diagram points:

    call SVG_GroupStart(output_file=output_file,id=(trim(data_title)//'_points'))

    characters(1)=trim(x_title)//'_coordinate'
    characters(2)=trim(y_title)//'_coordinate'

    diagram_point_loop: do i=1,size(x_points)

      select case(trim(shape_type))

        case('circle')
          call SVG_Circle(output_file=output_file, &
            & x=points(i,1),y=points(i,2),radius=shape_size, &
            & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
            & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
            & scalar_name=characters, &
            & scalar_value=(/x_points(i),y_points(i)/), &
            & id=(trim(data_title)//'_'//trim(StringInteger(i))))

        case('square')
          call SVG_Square(output_file=output_file, &
            & x=points(i,1),y=points(i,2),side=shape_size, &
            & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
            & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
            & scalar_name=characters, &
            & scalar_value=(/x_points(i),y_points(i)/), &
            & id=(trim(data_title)//'_'//trim(StringInteger(i))))

        case('diamond')
          call SVG_Square(output_file=output_file, &
            & x=points(i,1),y=points(i,2),side=shape_size, &
            & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
            & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
            & rotate=(/45.0_rp,points(i,1),points(i,2)/), &
            & scalar_name=characters, &
            & scalar_value=(/x_points(i),y_points(i)/), &
            & id=(trim(data_title)//'_'//trim(StringInteger(i))))

        case('cross')
          call SVG_Cross(output_file=output_file, &
            & x=points(i,1),y=points(i,2),side=shape_size, &
            & stroke_width=shape_stroke_width,stroke_rgb=shape_stroke_rgb, &
            & scalar_name=characters, &
            & scalar_value=(/x_points(i),y_points(i)/), &
            & id=(trim(data_title)//'_'//trim(StringInteger(i))))

        case('ex')
          call SVG_Cross(output_file=output_file, &
            & x=points(i,1),y=points(i,2),side=shape_size, &
            & stroke_width=shape_stroke_width,stroke_rgb=shape_stroke_rgb, &
            & rotate=(/45.0_rp,points(i,1),points(i,2)/), &
            & scalar_name=characters, &
            & scalar_value=(/x_points(i),y_points(i)/), &
            & id=(trim(data_title)//'_'//trim(StringInteger(i))))

        case('triangle')
          call SVG_Triangle(output_file=output_file, &
            & x=points(i,1),y=points(i,2),side=shape_size, &
            & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
            & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
            & scalar_name=characters, &
            & scalar_value=(/x_points(i),y_points(i)/), &
            & id=(trim(data_title)//'_'//trim(StringInteger(i))))

      end select

    end do diagram_point_loop

    call SVG_GroupEnd(output_file)

    call SVG_GroupEnd(output_file)

    ! ------------------------------------------------------------------------------

    ! Make a legend:
    create_legend: if (len_trim(data_title)/=0) then

      call SVG_GroupStart(output_file)

      do i=1,size(file_list)
        if (file_list(i)==output_file) then

          ! Legend lines:
          if (trim(line_type)/='none') call SVG_Line(output_file=output_file, &
            & x=(/legend_position_x-numbers_axis_font_size, &
            & legend_position_x+numbers_axis_font_size/), &
            & y=(/legend_spacing(i),legend_spacing(i)/), &
            & stroke_width=line_stroke_width,stroke_rgb=line_stroke_rgb, &
            & id=(trim(data_title)//'_legend_line'))

          ! Legend points:
          legend_points: if (trim(shape_type)/='none') then

            select case(trim(shape_type))

              case('circle')
                call SVG_Circle(output_file=output_file, &
                  & x=legend_position_x,y=legend_spacing(i),radius=shape_size, &
                  & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
                  & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
                  & id=(trim(data_title)//'_legend_point'))

              case('square')
                call SVG_Square(output_file=output_file, &
                  & x=legend_position_x,y=legend_spacing(i),side=shape_size, &
                  & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
                  & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
                  & id=(trim(data_title)//'_legend_point'))

              case('diamond')
                call SVG_Square(output_file=output_file, &
                  & x=legend_position_x,y=legend_spacing(i),side=shape_size, &
                  & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
                  & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
                  & rotate=(/45.0_rp,legend_position_x,legend_spacing(i)/), &
                  & id=(trim(data_title)//'_legend_point'))

              case('cross')
                call SVG_Cross(output_file=output_file, &
                  & x=legend_position_x,y=legend_spacing(i),side=shape_size, &
                  & stroke_width=shape_stroke_width,stroke_rgb=shape_stroke_rgb, &
                  & id=(trim(data_title)//'_legend_point'))

              case('ex')
                call SVG_Cross(output_file=output_file, &
                  & x=legend_position_x,y=legend_spacing(i),side=shape_size, &
                  & stroke_width=shape_stroke_width,stroke_rgb=shape_stroke_rgb, &
                  & rotate=(/45.0_rp,legend_position_x,legend_spacing(i)/), &
                  & id=(trim(data_title)//'_legend_point'))

              case('triangle')
                call SVG_Triangle(output_file=output_file, &
                  & x=legend_position_x,y=legend_spacing(i),side=shape_size, &
                  & stroke_width=shape_stroke_width,fill_opacity=1.0_rp, &
                  & stroke_rgb=shape_stroke_rgb,fill_rgb=shape_fill_rgb, &
                  & id=(trim(data_title)//'_legend_point'))

            end select

          end if legend_points

          call SVG_GroupEnd(output_file)

          ! Add text to the graph legend:
          call SVG_Text(output_file=output_file,string=data_title, &
            & x=legend_position_x+2.0_rp*numbers_axis_font_size, &
            & y=legend_spacing(i)-numbers_axis_font_size/3.0_rp, &
            & font=font,font_size=numbers_axis_font_size, &
            & fill_opacity=1.0_rp,fill_rgb=font_rgb)

          ! ------------------------------------------------------------------------------

          ! Create a toggle button.
          if (create_toggle_buttons) then
            call SVG_Vanish(output_file=output_file)
            call SVG_ActionButton(output_file=output_file, &
              & script='vanish',target_id=trim(data_title), &
              & x=viewBox_width-3.0_rp*numbers_axis_font_size, &
              & y=legend_spacing(i), &
              & width=4.0_rp*numbers_axis_font_size, &
              & height=numbers_axis_font_size, &
              & stroke_width=grid_stroke_width,button_fill_opacity=1.0_rp, &
              & stroke_rgb=axis_stroke_rgb,button_fill_rgb=(/255,255,255/), &
              & string='toggle',font='verdana', &
              & font_size=2.0_rp*numbers_axis_font_size/3.0_rp, &
              & text_fill_opacity=1.0_rp,text_fill_rgb=font_rgb)
          end if

          ! Create a link button to the raw data.
          if (create_data_links) then
            ! Create the HTML table:
            characters(1)=trim(x_title)
            characters(2)=trim(y_title)
            numbers(:,1)=x_points
            numbers(:,2)=y_points
            call HTML_Initialize(output_file= &
              & trim(output_file)//'_'//trim(data_title)//'.html')
            call HTML_Heading(output_file= &
              & trim(output_file)//'_'//trim(data_title)//'.html', &
              & string=trim(output_file),level=3)
            call HTML_Heading(output_file= &
              & trim(output_file)//'_'//trim(data_title)//'.html', &
              & string=trim(diagram_title)//": "//trim(data_title),level=4)
            call HTML_Table(output_file= &
              & trim(output_file)//'_'//trim(data_title)//'.html', &
              & numbers=numbers,headings=characters, &
              & decimal_places=(/x_decimal_places+3,y_decimal_places+3/), &
              & scientific=(/.false.,.false./))
            call HTML_Finalize(output_file= &
              & trim(output_file)//'_'//trim(data_title)//'.html')
            ! Create the link:
            call SVG_LinkButton(output_file=output_file,link_string= &
              & trim(output_file)//'_'//trim(data_title)//'.html', &
              & x=viewBox_width-7.0_rp*numbers_axis_font_size, &
              & y=legend_spacing(i), &
              & width=3.0_rp*numbers_axis_font_size, &
              & height=numbers_axis_font_size, &
              & stroke_width=grid_stroke_width,button_fill_opacity=1.0_rp, &
              & stroke_rgb=axis_stroke_rgb,button_fill_rgb=(/255,255,255/), &
              & string='data',font='verdana', &
              & font_size=2.0_rp*numbers_axis_font_size/3.0_rp, &
              & text_fill_opacity=1.0_rp,text_fill_rgb=font_rgb)
          end if

        end if
      end do

    end if create_legend

    call SVG_Finalize(output_file)

  end subroutine SVG_CartesianPlot

  subroutine SVG_Resurrect(output_file)

    ! SVG_Resurrect is called to change an SVG file so that more data can be added
    ! to it. After resurrection, the SVG file will have to be re-finalized in order
    ! to be usable.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Resurrect(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine SVG_Resurrect
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Rect(output_file='plot.svg', &
      !	& x=200.0_rp,y=300.0_rp,width=100.0_rp,height=400.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/0.0_rp,200.0_rp,200.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')
    !write(*,*)"view the file and press enter to continue"
    !read(*,*)
    !call SVG_Resurrect(output_file='plot.svg')
    !call SVG_Rect(output_file='plot.svg', &
      !	& x=500.0_rp,y=300.0_rp,width=50.0_rp,height=200.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/0.0_rp,200.0_rp,200.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='2')
    !call SVG_Finalize(output_file='plot.svg')

    ! ------------------------------------------------------------------------------

    ! External dependencies:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    character:: a,key*(10),lock*(10)
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (connected) then
      write(*,*)"SVG_Resurrect"
      write(*,*)"WARNING: SVG file is open for editing."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Resurrect"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Resurrect"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    rewind(io)

    lock='<!--EOF-->'
    key=''

    do

      read(io,'(A1)',advance='no',iostat=err)a

      if (err==-1) then
        write(*,*)"SVG_Resurrect"
        write(*,*)"ERROR: SVG file is not finalized."
        write(*,*)"Data can be added without alterations."
        write(*,*)"filename : ",trim(output_file)
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
        return
      end if

      if (err>0) then
        write(*,*)err
        write(*,*)"SVG_Resurrect"
        write(*,*)"ERROR: reading from file was not possible"
        write(*,*)"filename : ",trim(output_file)
        write(*,*)"error flag : ",err
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if

      ! blank spaces and tabs are being ignored.
      if (a=='	'.or.a==' ') cycle

      key=key(2:len(key))//a

      if (key==lock) exit

    end do

    backspace(io)
    endfile(io)

    return

  end subroutine SVG_Resurrect

  subroutine SVG_Initialize(output_file)

    ! SVG_Initialize is called to initialize an output SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Initialize(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine SVG_Initialize
    !end interface

    !! Create an empty SVG document:
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Finalize(output_file='plot.svg')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (connected) then
      write(*,*)"SVG_Initialize"
      write(*,*)"WARNING: SVG file has already been accessed."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Initialize"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Initialize"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/viewBox_width,viewBox_height/)<0)) then
      write(*,*)"SVG_Initialize"
      write(*,*)"ERROR: viewBox may not have negative dimensions."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/viewBox_width,viewBox_height/)==0)) then
      write(*,*)"SVG_Initialize"
      write(*,*)"WARNING: SVG viewBox will not be viewable."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A37)',iostat=err)"<?xml version='1.0' standalone='no'?>"

    if (err/=0) then
      write(*,*)"SVG_Initialize"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,'(A46)')"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN'"
    write(io,'(A51)')"'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>"
    write(io,*)"<svg preserveAspectRatio='xMidYMid meet' viewBox='0,0,"// &
      & trim(StringReal(viewBox_width))//","// &
      & trim(StringReal(viewBox_height))//"'"
    write(io,*)"xmlns='http://www.w3.org/2000/svg'"
    write(io,*)"xmlns:xlink='http://www.w3.org/1999/xlink'"
    write(io,*)"version='1.1'>"

    write(io,*)

  end subroutine SVG_Initialize

  subroutine SVG_Text(output_file,string,x,y, &
      & font,font_size,fill_opacity,fill_rgb, &
      & bold,italic,anchor,rotate,scalar_name,scalar_value,id)

    ! SVG_Text is called to draw a text string in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! string: character, scalar. The string that will be drawn.
    ! x: real, scalar. The x-axis coordinate of the text position (px). See also
    !	optional variable "anchor" for the text position definition.
    ! y: real, scalar. The y-axis coordinate of the text position (px). See also
    !	optional variable "anchor" for the text position definition.
    ! font: character, scalar. The font that will be used for the rendering. The
    !	selected font must be supported in the system that will be used to
    !	render the SVG file.
    ! font_size: real, scalar. The size of the font.
    ! fill_opacity: real, scalar. The opacity of the text fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! bold: logical, scalar. When .true. the boldface type of the specified font
    !	will be used for the rendering.
    ! italic: logical, scalar. When .true. the italic type of the specified font
    !	will be used for the rendering.
    ! anchor: character, scalar. Specifies the alignment of the text respective to
    !	the provided text position. Supported values: "start", "middle", "end".
    !	If ommitted, the default value is "start".
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Text(output_file,string,x,y, &
      !	& font,font_size,fill_opacity,fill_rgb, &
      !	& bold,italic,anchor,rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*),string*(*),font*(*)
    !real(rp),intent(in):: x,y,fill_opacity,font_size
    !integer,intent(in):: fill_rgb(3)
    !logical,optional,intent(in):: bold,italic
    !character,optional,intent(in):: scalar_name(:)*(*),anchor*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Text
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Text(output_file='plot.svg',string='hello world', &
      !	& x=100.0_rp,y=200.0_rp,font='verdana',font_size=45.0_rp, &
      !	& fill_opacity=1.0_rp,fill_rgb=(/255,0,0/), &
      !	& bold=.false.,italic=.true.,anchor='middle', &
      !	& rotate=(/-25.0_rp,100.0_rp,200.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringInteger, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*),string*(*),font*(*)
    real(rp),intent(in):: x,y,fill_opacity,font_size
    integer,intent(in):: fill_rgb(3)
    logical,optional,intent(in):: bold,italic
    character,optional,intent(in):: scalar_name(:)*(*),anchor*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected,present_anchor

    ! Variable initialization:
    io=FileManager(output_file,connected)
    present_anchor=present(anchor)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Text"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (len_trim(string)==0) then
      write(*,*)"SVG_Text"
      write(*,*)"WARNING: missing string, nothing will be printed."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Text"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_Text"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: invalid paint parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (fill_opacity==0.0) then
      write(*,*)"SVG_Text"
      write(*,*)"WARNING: this fill-opacity will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(fill_rgb>255))) then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (present_anchor) then
      if (all(anchor/=(/"start ", "middle", "end   "/))) then
        write(*,*)"SVG_Text"
        write(*,*)"WARNING: anchor value not supported."
        write(*,*)"This setting will be ignored."
        write(*,*)"Continue..."
        present_anchor=.false.
        if (warnings_pause) read(*,*)
      end if
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Text"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_Text"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    ! In the SVG 1.1 specification there is no special treatment of superscripts and
    ! subscripts. Moreover, at the moment of this writing, the various SVG viewers
    ! exhibited different interpretations of the SVG 1.1 specification concerning
    ! the rendering of the "tspan" element. Calculating the positions of all
    ! superscripts and subscripts "by hand" in the fortran code would implicitly
    ! mean the authoring of a typesetting tool, which is not the purpose of this
    ! library. Sadly therefore, there is no support for suprescripts, subscripts and
    ! other mathematic notation for the time being.

    write(io,'(A5)',iostat=err)"<text"

    if (err/=0) then
      write(*,*)"SVG_Text"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"x='"//trim(StringReal(x))//"'"
    write(io,*)"y='"//trim(StringReal(viewBox_height-y))//"'"
    write(io,*)"font-family='"//trim(font)//"'"
    write(io,*)"font-size='"//trim(StringReal(font_size))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present_anchor) write(io,*)"text-anchor='"//trim(anchor)//"'"

    if (present(bold)) then
      if (bold) write(io,*)"font-weight='bold'"
    end if

    if (present(italic)) then
      if (italic) write(io,*)"font-style='italic'"
    end if

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A1)')">"

    write(io,*)trim(string)

    write(io,'(A7)')"</text>"
    write(io,*)

  end subroutine SVG_Text

  subroutine SVG_Line(output_file,x,y,stroke_width,stroke_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Line is called to draw a line in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, array (1D) with 2 elements. The x-axis coordinates of the line
    !	beginning (1st element) and the line end (2nd element) (px).
    ! y: real, array (1D) with 2 elements. The y-axis coordinates of the line
    !	beginning (1st element) and the line end (2nd element) (px).
    ! stroke_width: real, scalar. The thickness of the line (px).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Line(output_file,x,y,stroke_width,stroke_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x(2),y(2),stroke_width
    !integer,intent(in):: stroke_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Line
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Line(output_file='plot.svg', &
      !	& x=(/100.0_rp,500.0_rp/),y=(/100.0_rp,500.0_rp/), &
      !	& stroke_width=3.0_rp,stroke_rgb=(/255,0,0/), &
      !	& rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringInteger, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x(2),y(2),stroke_width
    integer,intent(in):: stroke_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Line"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Line"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(y>viewBox_height)).or.(any(x>viewBox_width))) then
      write(*,*)"SVG_Line"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (stroke_width==0.0) then
      write(*,*)"SVG_Line"
      write(*,*)"WARNING: this stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(stroke_rgb<0)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Line"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_Line"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A5)',iostat=err)"<line"

    if (err/=0) then
      write(*,*)"SVG_Line"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename: ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"x1='"//trim(StringReal(x(1)))//"'"
    write(io,*)"y1='"//trim(StringReal(y(1)))//"'"
    write(io,*)"x2='"//trim(StringReal(x(2)))//"'"
    write(io,*)"y2='"//trim(StringReal(y(2)))//"'"
    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Line

  subroutine SVG_GroupStart(output_file,rotate,scalar_name,scalar_value,id)

    ! SVG_GroupStart is called to initialize a group of SVG objects.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the group is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_GroupStart(output_file,rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_GroupStart
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_GroupStart(output_file='plot.svg', &
      !	& rotate=(/45.0_rp,200.0_rp,200.0_rp/), &
      !	& scalar_name=(/'ellipse1_x','ellipse1_y', &
      !	& 'square1_x ','square1_y '/), &
      !	& scalar_value=(/100.0_rp,100.0_rp,300.0_rp,300.0_rp/),id='1')
    !call SVG_Ellipse(output_file='plot.svg', &
      !	& x=100.0_rp,y=100.0_rp,radius_x=100.0_rp,radius_y=200.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/45.0_rp,100.0_rp,100.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/))
    !call SVG_Square(output_file='plot.svg', &
      !	& x=300.0_rp,y=300.0_rp,side=100.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/45.0_rp,300.0_rp,300.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/5.14_rp/))
    !call SVG_GroupEnd(output_file='plot.svg')
    !call SVG_Finalize(output_file='plot.svg')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_GroupStart"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_GroupStart"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_GroupStart"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_GroupStart"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_GroupStart"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_GroupStart"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_GroupStart"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    if ((present(rotate)).or.(present(id)).or. &
        & ((present(scalar_name)).and.(present(scalar_value)))) then

      write(io,'(A2)',iostat=err)"<g"

      if (err/=0) then
        write(*,*)"SVG_GroupStart"
        write(*,*)"ERROR: writing to file was not possible."
        write(*,*)"filename : ",trim(output_file)
        write(*,*)"error flag : ",err
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if

      if (present(rotate)) then
        write(io,*)"transform='rotate("// &
          & trim(StringReal(rotate(1)))//","// &
          & trim(StringReal(rotate(2)))//","// &
          & trim(StringReal(viewBox_height-rotate(3)))//")'"
      end if

      if ((present(scalar_name)).and.(present(scalar_value))) then
        do i=1,size(scalar_name)
          write(io,*)trim(scalar_name(i))//"='"// &
            & trim(StringReal(scalar_value(i)))//"'"
        end do
      end if

      if (present(id)) write(io,*)"id='"//trim(id)//"'"

      write(io,'(A1)')">"

    else

      write(io,'(A3)',iostat=err)"<g>"

      if (err/=0) then
        write(*,*)"SVG_GroupStart"
        write(*,*)"ERROR: writing to file was not possible."
        write(*,*)"filename : ",trim(output_file)
        write(*,*)"error flag : ",err
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if

    end if

    write(io,*)

  end subroutine SVG_GroupStart

  subroutine SVG_Polyline(output_file,x,y, &
      & stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Polyline is called to draw a crooked line in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, array (1D). The x-axis coordinates of the line points. (px).
    ! y: real, array (1D). The y-axis coordinates of the line points. (px).
    ! stroke_width: real, scalar. The thickness of the line (px).
    ! fill_opacity: real, scalar. The opacity of the fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Polyline(output_file,x,y, &
      !	& stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x(:),y(:),stroke_width,fill_opacity
    !integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Polyline
    !end interface

    !real(rp):: x(15),y(15)
    !x(1)=50.0    ; y(1)=375.0
    !x(2)=150.0   ; y(2)=375.0
    !x(3)=150.0   ; y(3)=425.0
    !x(4)=250.0   ; y(4)=425.0
    !x(5)=250.0   ; y(5)=375.0
    !x(6)=350.0   ; y(6)=375.0
    !x(7)=350.0   ; y(7)=500.0
    !x(8)=450.0   ; y(8)=500.0
    !x(9)=450.0   ; y(9)=375.0
    !x(10)=550.0  ; y(10)=375.0
    !x(11)=550.0  ; y(11)=575.0
    !x(12)=650.0  ; y(12)=575.0
    !x(13)=650.0  ; y(13)=375.0
    !x(14)=750.0  ; y(14)=375.0
    !x(15)=750.0  ; y(15)=650.0
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Polyline(output_file='plot.svg',x=x,y=y, &
      !	& stroke_width=5.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,0,0/),fill_rgb=(/255,0,0/), &
      !	& rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x(:),y(:),stroke_width,fill_opacity
    integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Polyline"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: file must have the .svg extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Polyline"
      write(*,*)"WARNING: negative coordinates"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(y>viewBox_height)).or.(any(x>viewBox_width))) then
      write(*,*)"SVG_Polyline"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (size(x)/=size(y)) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: coordinate arrays do not match"
      write(*,*)"size(x): ",size(x)
      write(*,*)"size(y): ",size(y)
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: invalid stroke parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: invalid paint parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
      write(*,*)"SVG_Polyline"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: invalid RGB channel value"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Polyline"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements"
        write(*,*)"size(scalar_name): ",size(scalar_name)
        write(*,*)"size(scalar_value): ",size(scalar_value)
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (size(x)<2) then
      write(*,*)"SVG_Polyline"
      write(*,*)"WARNING: can not plot a line with less than two points"
      write(*,*)"No line will be plotted."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((all(x==0)).or.(all(y==0))) then
      write(*,*)"SVG_Polyline"
      write(*,*)"WARNING: all array points are equal to zero"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_Polyline"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A9)',iostat=err)"<polyline"

    if (err/=0) then
      write(*,*)"SVG_Polyline"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"points='"
    do i=1,size(x)-1
      write(io,*)trim(StringReal(x(i)))//","// &
        & trim(StringReal(viewBox_height-y(i)))
    end do
    write(io,*)trim(StringReal(x(size(x))))//","// &
      & trim(StringReal(viewBox_height-y(size(y))))//"'"

    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Polyline

  subroutine SVG_Circle(output_file,x,y,radius, &
      & stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Circle is called to draw an ellipse in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, scalar. The x-axis coordinate of the ellipse center. (px).
    ! y: real, scalar. The y-axis coordinate of the ellipse center. (px).
    ! radius: real, scalar. The circle radius (px).
    ! stroke_width: real, scalar. The thickness of the ellipse line (px).
    ! fill_opacity: real, scalar. The opacity of the ellipse fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Circle(output_file,x,y,radius, &
      !	& stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x,y,radius,stroke_width,fill_opacity
    !integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Circle
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Circle(output_file='plot.svg', &
      !	& x=200.0_rp,y=200.0_rp,radius=100.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/15.0_rp,100.0_rp,100.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x,y,radius,stroke_width,fill_opacity
    integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Circle"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

!    if (any((/x,y/)<0.0)) then
!      write(*,*)"SVG_Circle"
!      write(*,*)"WARNING: negative coordinates."
!      write(*,*)"Continue..."
!      if (warnings_pause) read(*,*)
!    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_Circle"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (radius<0.0) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: invalid shape parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (radius==0.0) then
      write(*,*)"SVG_Circle"
      write(*,*)"WARNING: SVG object will not be viewable."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: invalid paint parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
      write(*,*)"SVG_Circle"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Circle"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_Circle"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    ! Rotating a circle may sound vain, but this is not true when a rotation centre
    ! other than its radius is selected.

    write(io,'(A7)',iostat=err)"<circle"

    if (err/=0) then
      write(*,*)"SVG_Circle"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename: ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"cx='"//trim(StringReal(x))//"'"
    write(io,*)"cy='"//trim(StringReal(y))//"'"
    write(io,*)"r='"//trim(StringReal(radius))//"'"
    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Circle

  subroutine SVG_Square(output_file,x,y,side, &
      & stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Square is called to draw a rectangle in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, scalar. The x-axis coordinate of the rectangle center. (px).
    ! y: real, scalar. The y-axis coordinate of the rectangle center. (px).
    ! side: real, scalar. The length of the square side. (px).
    ! stroke_width: real, scalar. The thickness of the rectangle line (px).
    ! fill_opacity: real, scalar. The opacity of the rectangle fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Square(output_file,x,y,side, &
      !	& stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x,y,side,stroke_width,fill_opacity
    !integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Square
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Square(output_file='plot.svg', &
      !	& x=200.0_rp,y=200.0_rp,side=100.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/45.0_rp,200.0_rp,200.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x,y,side,stroke_width,fill_opacity
    integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Square"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: invalid filename"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: file must have the .svg extension"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Square"
      write(*,*)"WARNING: negative coordinates"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_Square"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (side<0.0) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: invalid shape parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (side==0.0) then
      write(*,*)"SVG_Square"
      write(*,*)"WARNING: SVG object will not be viewable."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: invalid stroke parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: invalid paint parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
      write(*,*)"SVG_Square"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: invalid RGB channel value"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Square"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements"
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    ! ------------------------------------------------------------------------------

    ! In the SVG 1.1 specification, there is no actual "Square" basic shape. Here,
    ! the square is represented by an SVG Rectangle basic shape.

    write(io,'(A5)',iostat=err)"<rect"

    if (err/=0) then
      write(*,*)"SVG_Square"
      write(*,*)"ERROR: writing to file was not possible"
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"x='"//trim(StringReal(x-side/2.0_rp))//"'"
    write(io,*)"y='"//trim(StringReal(viewBox_height-y-side/2.0_rp))//"'"
    write(io,*)"width='"//trim(StringReal(side))//"'"
    write(io,*)"height='"//trim(StringReal(side))//"'"
    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Square

  subroutine SVG_Cross(output_file,x,y,side,stroke_width,stroke_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Cross is called to draw a cross in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, scalar. The x-axis coordinates of the cross center (px).
    ! y: real, scalar. The y-axis coordinates of the cross center (px).
    ! side: real, scalar. The length of the cross side. (px).
    ! stroke_width: real, scalar. The thickness of the cross lines (px).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Cross(output_file,x,y,side,stroke_width,stroke_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x,y,side,stroke_width
    !integer,intent(in):: stroke_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Cross
    !end interface

    !call SVG_Initialize('plot.svg')
    !call SVG_Cross(output_file='plot.svg', &
      !	& x=300.0_rp,y=300.0_rp,side=100.0_rp, &
      !	& stroke_width=3.0_rp,stroke_rgb=(/255,0,0/), &
      !	& rotate=(/45.0_rp,300.0_rp,300.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize('plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x,y,side,stroke_width
    integer,intent(in):: stroke_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Cross"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Cross"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_Cross"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (stroke_width==0.0) then
      write(*,*)"SVG_Cross"
      write(*,*)"WARNING: this stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(stroke_rgb<0)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Cross"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_Cross"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    ! Group the line elements. The group defines the stroke width and rgb values as
    ! well as the roation and scalar attributes.

    write(io,'(A2)',iostat=err)"<g"

    if (err/=0) then
      write(*,*)"SVG_Cross"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A1)')">"
    write(io,*)

    ! ------------------------------------------------------------------------------

    ! Type the lines.
    write(io,'(A5)')"<line"
    write(io,*)"x1='"//trim(StringReal(x))//"'"
    write(io,*)"y1='"//trim(StringReal(viewBox_height-y-side/2.0_rp))//"'"
    write(io,*)"x2='"//trim(StringReal(x))//"'"
    write(io,*)"y2='"//trim(StringReal(viewBox_height-y+side/2.0_rp))//"'"
    write(io,'(A2)')"/>"
    write(io,*)
    write(io,'(A5)')"<line"
    write(io,*)"x1='"//trim(StringReal(x-side/2.0_rp))//"'"
    write(io,*)"y1='"//trim(StringReal(viewBox_height-y))//"'"
    write(io,*)"x2='"//trim(StringReal(x+side/2.0_rp))//"'"
    write(io,*)"y2='"//trim(StringReal(viewBox_height-y))//"'"
    write(io,'(A2)')"/>"
    write(io,*)

    ! End the group.
    write(io,'(A4)')"</g>"
    write(io,*)

  end subroutine SVG_Cross

  subroutine SVG_Triangle(output_file,x,y,side, &
      & stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Triangle is called to draw a triangle in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, scalar. The x-axis coordinate of the triangle center. (px).
    ! y: real, scalar. The y-axis coordinate of the triangle center. (px).
    ! side: real, scalar. The length of the triangle side. (px).
    ! stroke_width: real, scalar. The thickness of the triangle line (px).
    ! fill_opacity: real, scalar. The opacity of the fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Triangle(output_file,x,y,side, &
      !	& stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x,y,side,stroke_width,fill_opacity
    !integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Triangle
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Triangle(output_file='plot.svg', &
      !	& x=350.0_rp,y=350.0_rp,side=50.0_rp, &
      !	& stroke_width=5.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,0,0/),fill_rgb=(/255,0,0/), &
      !	& rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height,pi
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x,y,side,stroke_width,fill_opacity
    integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    real(rp):: x_points(3),y_points(3),variable
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! The code below defines an equilateral triangle whose user-defined center is
    ! its barycenter.
    !variable=(side/2.0_rp)/cos(pi/6.0_rp)
    !x_points(1)=x
    !y_points(1)=y+variable
    !x_points(2)=x+side/2.0_rp
    !y_points(2)=y-variable*sin(pi/6.0_rp)
    !x_points(3)=x-side/2.0_rp
    !y_points(3)=y-variable*sin(pi/6.0_rp)

    ! The code below defines a triangle whose user-defined side is its bottom side
    ! that is equal to its height.
    variable=side/2.0_rp
    x_points(1)=x
    y_points(1)=y+variable
    x_points(2)=x+variable
    y_points(2)=y-variable
    x_points(3)=x-variable
    y_points(3)=y-variable

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Triangle"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Triangle"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_Triangle"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (side<0.0) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: invalid shape parameters"
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (side==0.0) then
      write(*,*)"SVG_Triangle"
      write(*,*)"WARNING: SVG object will not be viewable."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: invalid paint parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
      write(*,*)"SVG_Triangle"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Triangle"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"size(scalar_name): ",size(scalar_name)
        write(*,*)"size(scalar_value): ",size(scalar_value)
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A8)',iostat=err)"<polygon"

    if (err/=0) then
      write(*,*)"SVG_Triangle"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"points='"
    do i=1,size(x_points)-1
      write(io,*)trim(StringReal(x_points(i)))//","// &
        & trim(StringReal(viewBox_height-y_points(i)))
    end do
    write(io,*)trim(StringReal(x_points(size(x_points))))//","// &
      & trim(StringReal(viewBox_height-y_points(size(y_points))))//"'"

    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Triangle

  subroutine SVG_Vanish(output_file)

    ! SVG_Vanish is called to print the "vanish" ECMAScript function in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Vanish(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine SVG_Vanish
    !end interface

    !real(rp):: id=1.0
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Vanish(output_file='plot.svg')
    !call SVG_HideButton(output_file='plot.svg',id=id, &
      !	& x=200.0_rp,y=500.0_rp,width=200.0_rp,height=50.0_rp, &
      !	& stroke_width=5.0_rp,button_fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,0,255/),button_fill_rgb=(/255,255,255/), &
      !	& string='toggle circle',font='verdana',font_size=20.0_rp, &
      !	& text_fill_opacity=1.0_rp,text_fill_rgb=(/0,0,0/))
    !call SVG_Circle(output_file='plot.svg', &
      !	& x=200.0_rp,y=100.0_rp,radius=50.0_rp, &
      !	& stroke_width=10.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/255,255,255/), &
      !	& rotate=(/0.0_rp,100.0_rp,100.0_rp/), &
      !	& scalar_name=(/'id'/),scalar_value=(/id/))
    !call SVG_Finalize(output_file='plot.svg')

    ! Example of SVG application:

    !<svg viewBox='0 0 800 800'
    !xmlns='http://www.w3.org/2000/svg' version='1.1'>
    !
    !<script type='text/ecmascript'> <![CDATA[
    !function vanish(evt,id) {
    !	var document = evt.target.ownerDocument;
    !	var object = document.getElementById(id);
    !	var CurrentOpacity = object.getAttribute('opacity');
    !	if (CurrentOpacity == '0')
    !		object.setAttribute('opacity', '1');
    !	else
    !		object.setAttribute('opacity', '0');
    !}
    !]]> </script>
    !
    !<circle onclick='vanish(evt,"1.0")' cx='10' cy='20' r='10' fill='black' />
    !<circle onclick='vanish(evt,"2.0")' cx='50' cy='20' r='10' fill='green' />
    !
    !<g id="1.0">
    !	<circle id="2.0" cx='200' cy='225' r='100' fill='red' />
    !	<circle cx='500' cy='225' r='100' fill='blue' />
    !</g>
    !
    !</svg>

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Vanish"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Vanish"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Vanish"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A41)',iostat=err)"<script type='text/ecmascript'> <![CDATA["

    if (err/=0) then
      write(*,*)"SVG_Vanish"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"function vanish(evt,id) {"
    write(io,*)"    var document=evt.target.ownerDocument;"
    write(io,*)"    var object=document.getElementById(id);"
    write(io,*)"    var CurrentOpacity=object.getAttribute('opacity');"
    write(io,*)"if (CurrentOpacity=='0')"
    write(io,*)"    object.setAttribute('opacity','1');"
    write(io,*)"else"
    write(io,*)"    object.setAttribute('opacity','0');"
    write(io,*)"}"
    write(io,*)"]]> </script>"
    write(io,*)

  end subroutine SVG_Vanish

  subroutine SVG_LinkButton(output_file,link_string,x,y,width,height, &
      & stroke_width,button_fill_opacity,stroke_rgb,button_fill_rgb, &
      & string,font,font_size,text_fill_opacity,text_fill_rgb)

    ! SVG_LinkButton is called to place a LinkButton in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! link_string: character, scalar. The string specifying the link. It can be e.g.
    !	a web adress, a local file, etc. The validity of the link is not being
    !	checked by the present subroutine. Web adresses must be specified in
    !	full, e.g. "http://www.w3.org/". Linking with "www.w3.org" will result
    !	in an -obviously broken- link to local file named "www.w3.org".
    ! x: real, scalar. The x-axis coordinate of the button center. (px).
    ! y: real, scalar. The y-axis coordinate of the button center. (px).
    ! width: real, scalar. The button width. (px).
    ! height: real, scalar. he button height. (px).
    ! stroke_width: real, scalar. The thickness of the button line (px).
    ! button_fill_opacity: real, scalar. The opacity of the button fill
    !	(dimensionless, 0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! button_fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB
    !	channel values (dimensionless, 0<=fill_rgb(:)<=255).
    ! string: character, scalar. The string that will be drawn.
    ! font: character, scalar. The font that will be used for the rendering. The
    !	selected font must be supported in the system that will be used to
    !	render the SVG file.
    ! font_size: real, scalar. The size of the font.
    ! text_fill_opacity: real, scalar. The opacity of the text fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! text_fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB
    !	channel values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_LinkButton(output_file,link_string,x,y,width,height, &
      !	& stroke_width,button_fill_opacity,stroke_rgb,button_fill_rgb, &
      !	& string,font,font_size,text_fill_opacity,text_fill_rgb)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*),link_string*(*),string*(*),font*(*)
    !real(rp),intent(in):: x,y,width,height,stroke_width
    !real(rp),intent(in):: button_fill_opacity,text_fill_opacity,font_size
    !integer,intent(in):: stroke_rgb(3),button_fill_rgb(3),text_fill_rgb(3)
    !end subroutine SVG_LinkButton
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_LinkButton(output_file='plot.svg',link_string='http://www.w3.org/', &
      !	& x=200.0_rp,y=500.0_rp,width=200.0_rp,height=50.0_rp, &
      !	& stroke_width=5.0_rp,button_fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,0,255/),button_fill_rgb=(/255,255,255/), &
      !	& string='go home',font='verdana',font_size=20.0_rp, &
      !	& text_fill_opacity=1.0_rp,text_fill_rgb=(/0,0,0/))
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringInteger, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*),link_string*(*),string*(*),font*(*)
    real(rp),intent(in):: x,y,width,height,stroke_width
    real(rp),intent(in):: button_fill_opacity,text_fill_opacity,font_size
    integer,intent(in):: stroke_rgb(3),button_fill_rgb(3),text_fill_rgb(3)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((y>viewBox_height).or.(x>viewBox_width)) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (any((/width,height/)<0.0)) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: invalid shape parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/width,height/)==0.0)) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"WARNING: SVG object will not be viewable."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<button_fill_opacity).or.(button_fill_opacity<0.0).or. &
        & (1.0<text_fill_opacity).or.(text_fill_opacity<0.0)) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: invalid paint parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((button_fill_opacity==0.0).and.(text_fill_opacity==0.0).and. &
        & (stroke_width==0.0)) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(text_fill_rgb<0)).or.(any(button_fill_rgb<0)).or. &
        & (any(stroke_rgb<0)).or.(any(stroke_rgb>255)).or. &
        & (any(text_fill_rgb>255)).or.(any(button_fill_rgb>255))) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    ! Create the link.
    write(io,'(A2)',iostat=err)"<a"

    if (err/=0) then
      write(*,*)"SVG_LinkButton"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"xlink:href='"//trim(link_string)//"'>"
    write(io,*)

    ! Group the rounded rectangle and its text.
    write(io,'(A3)')"<g>"
    write(io,*)

    ! Print the rounded rectangle.
    write(io,'(A5)',iostat=err)"<rect"
    write(io,*)"x='"//trim(StringReal(x-width/2.0_rp))//"'"
    write(io,*)"y='"//trim(StringReal(viewBox_height-y-height/2.0_rp))//"'"
    write(io,*)"rx='"//trim(StringReal(height/5.0_rp))//"'"
    write(io,*)"ry='"//trim(StringReal(height/5.0_rp))//"'"
    write(io,*)"width='"//trim(StringReal(width))//"'"
    write(io,*)"height='"//trim(StringReal(height))//"'"
    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(button_fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(button_fill_rgb(1)))//","// &
      & trim(StringInteger(button_fill_rgb(2)))//","// &
      & trim(StringInteger(button_fill_rgb(3)))//")'"
    write(io,'(A2)')"/>"
    write(io,*)

    ! Print the button text.
    write(io,'(A5)')"<text"
    write(io,*)"x='"//trim(StringReal(x))//"'"
    write(io,*)"y='"//trim(StringReal(viewBox_height-y+font_size/3.0_rp))//"'"
    write(io,*)"font-family='"//trim(font)//"'"
    write(io,*)"font-size='"//trim(StringReal(font_size))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(text_fill_opacity))//"'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(text_fill_rgb(1)))//","// &
      & trim(StringInteger(text_fill_rgb(2)))//","// &
      & trim(StringInteger(text_fill_rgb(3)))//")'"
    write(io,*)"text-anchor='middle'"
    write(io,*)"font-weight='normal'"
    !write(io,*)"font-style='italic'"
    write(io,'(A1)')">"
    write(io,*)trim(string)
    write(io,'(A7)')"</text>"
    write(io,*)

    ! End the grouping
    write(io,'(A4)',iostat=err)"</g>"
    write(io,*)

    ! End the link.
    write(io,'(A4)')"</a>"
    write(io,*)

  end subroutine SVG_LinkButton

  subroutine SVG_GroupEnd(output_file)

    ! SVG_GroupEnd is called to finalize a group of SVG objects.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_GroupEnd(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine SVG_GroupEnd
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_GroupStart(output_file='plot.svg', &
      !	& rotate=(/45.0_rp,200.0_rp,200.0_rp/), &
      !	& scalar_name=(/'ellipse1_x','ellipse1_y', &
      !	& 'square1_x ','square1_y '/), &
      !	& scalar_value=(/100.0_rp,100.0_rp,300.0_rp,300.0_rp/))
    !call SVG_Ellipse(output_file='plot.svg', &
      !	& x=100.0_rp,y=100.0_rp,radius_x=100.0_rp,radius_y=200.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/45.0_rp,100.0_rp,100.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/))
    !call SVG_Square(output_file='plot.svg', &
      !	& x=300.0_rp,y=300.0_rp,side=100.0_rp, &
      !	& stroke_width=30.0_rp,fill_opacity=0.5_rp, &
      !	& stroke_rgb=(/255,0,0/),fill_rgb=(/0,0,255/), &
      !	& rotate=(/45.0_rp,300.0_rp,300.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/5.14_rp/))
    !call SVG_GroupEnd(output_file='plot.svg')
    !call SVG_Finalize(output_file='plot.svg')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_GroupEnd"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_GroupEnd"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_GroupEnd"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A4)',iostat=err)"</g>"

    if (err/=0) then
      write(*,*)"SVG_GroupEnd"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)

  end subroutine SVG_GroupEnd

  subroutine SVG_Finalize(output_file)

    ! SVG_Finalize is called to finalize an output SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Finalize(output_file)
    !implicit none
    !character,intent(in):: output_file*(*)
    !end subroutine SVG_Finalize
    !end interface

    !! Create an empty SVG document:
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Finalize(output_file='plot.svg')

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: warnings_pause
    use ReadWriteMod, only : FileManager

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)

    ! Private variables:
    integer:: err,io
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Finalize"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Finalize"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Finalize"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A10)',iostat=err)"<!--EOF-->"

    if (err/=0) then
      write(*,*)"SVG_Finalize"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

!DL+
    write(io,*) "</g>"
!DL-

    write(io,'(A6)')"</svg>"

    close(io)

  end subroutine SVG_Finalize

  subroutine SVG_Polygon(output_file,x,y, &
      & stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      & rotate,scalar_name,scalar_value,id)

    ! SVG_Polygon is called to draw a crooked line in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, array (1D). The x-axis coordinates of the line points. (px).
    ! y: real, array (1D). The y-axis coordinates of the line points. (px).
    ! stroke_width: real, scalar. The thickness of the line (px).
    ! fill_opacity: real, scalar. The opacity of the fill (dimensionless,
    !	0.0<=fill_opacity<=1.0).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).
    ! fill_rgb: integer, array (1D) with 3 elements. Contains the fill RGB channel
    !	values (dimensionless, 0<=fill_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Polygon(output_file,x,y, &
      !	& stroke_width,fill_opacity,stroke_rgb,fill_rgb, &
      !	& rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x(:),y(:),stroke_width,fill_opacity
    !integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Polygon
    !end interface

    !real(rp):: x(10),y(10)
    !x(1)=350.0  ; y(1)=75.0
    !x(2)=379.0  ; y(2)=161.0
    !x(3)=469.0  ; y(3)=161.0
    !x(4)=397.0  ; y(4)=215.0
    !x(5)=423.0  ; y(5)=301.0
    !x(6)=350.0  ; y(6)=250.0
    !x(7)=277.0  ; y(7)=301.0
    !x(8)=303.0  ; y(8)=215.0
    !x(9)=231.0  ; y(9)=161.0
    !x(10)=321.0 ; y(10)=161.0
    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Polygon(output_file='plot.svg',x=x,y=y, &
      !	& stroke_width=5.0_rp,fill_opacity=1.0_rp, &
      !	& stroke_rgb=(/0,0,0/),fill_rgb=(/255,0,0/), &
      !	& rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringReal, StringInteger

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x(:),y(:),stroke_width,fill_opacity
    integer,intent(in):: stroke_rgb(3),fill_rgb(3)
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Polygon"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

!    if (any((/x,y/)<0.0)) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"WARNING: negative coordinates."
!      write(*,*)"Continue..."
!      if (warnings_pause) read(*,*)
!    end if

    if ((any(y>viewBox_height)).or.(any(x>viewBox_width))) then
      write(*,*)"SVG_Polygon"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (size(x)/=size(y)) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: coordinate arrays do not match."
      write(*,*)"size(x): ",size(x)
      write(*,*)"size(y): ",size(y)
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (stroke_width<0.0) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: invalid paint parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
      write(*,*)"SVG_Polygon"
      write(*,*)"WARNING: this combination of fill-opacity and"
      write(*,*)"stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Polygon"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"size(scalar_name): ",size(scalar_name)
        write(*,*)"size(scalar_value): ",size(scalar_value)
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    if (size(x)<3) then
      write(*,*)"SVG_Polygon"
      write(*,*)"WARNING: can not plot a polygon with less than three points"
      write(*,*)"No polygon will be plotted."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((all(x==0)).or.(all(y==0))) then
      write(*,*)"SVG_Polygon"
      write(*,*)"WARNING: all array points are equal to zero"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (present(id)) then
      if (len_trim(id)==0) then
        write(*,*)"SVG_Polygon"
        write(*,*)"WARNING: invalid id."
        write(*,*)"Continue..."
        if (warnings_pause) read(*,*)
      end if
    end if

    ! ------------------------------------------------------------------------------

    write(io,'(A8)',iostat=err)"<polygon"

    if (err/=0) then
      write(*,*)"SVG_Polygon"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"points='"
    do i=1,size(x)-1
      !      write(io,*)trim(StringReal(x(i)))//","// &
        !        & trim(StringReal(viewBox_height-y(i)))
      write(io,*)trim(StringReal(x(i)))//","// &
        & trim(StringReal(y(i)))
    end do
    !    write(io,*)trim(StringReal(x(size(x))))//","// &
      !      & trim(StringReal(viewBox_height-y(size(y))))//"'"
    write(io,*)trim(StringReal(x(size(x))))//","// &
      & trim(StringReal(y(size(y))))//"'"

    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(stroke_rgb(1)))//","// &
      & trim(StringInteger(stroke_rgb(2)))//","// &
      & trim(StringInteger(stroke_rgb(3)))//")'"
    write(io,*)"fill='rgb("// &
      & trim(StringInteger(fill_rgb(1)))//","// &
      & trim(StringInteger(fill_rgb(2)))//","// &
      & trim(StringInteger(fill_rgb(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Polygon

  subroutine SVG_Vector(output_file,x,y,stroke_width,stroke_rgb,head_size, &
      & change_marker,rotate,scalar_name,scalar_value,id)

    ! SVG_Vector is called to draw a vector in an SVG file.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! output_file: character, scalar. The name of the output SVG file. It must have
    !	the ".svg" extension (otherwise an error is generated).
    ! x: real, array (1D) with 2 elements. The x-axis coordinates of the vector
    !	beginning (1st element) and the vector end (2nd element) (px).
    ! y: real, array (1D) with 2 elements. The y-axis coordinates of the vector
    !	beginning (1st element) and the vector end (2nd element) (px).
    ! head_size: real, scalar. The size of the arrow head, respective to the
    !	specified stroke width (dimensionless).
    ! stroke_width: real, scalar. The thickness of the vector line (px).
    ! stroke_rgb: integer, array (1D) with 3 elements. Contains the stroke RGB
    !	channel values (dimensionless, 0<=stroke_rgb(:)<=255).

    ! INPUT (OPTIONAL):
    ! change_marker: logical, scalar. If present and .true., it defines that
    !	the arrowhead properties will be set according to input variables
    !	"head_size" and "stroke_rgb". If abscent or .false. variables
    !	"head_size" and "stroke_rgb" will be ignored and the arrowhead used in
    !	the last call will be recycled. If variable "change_marker" is abscent
    !	or .false. upon the first call of the subroutine, then the arrowhead
    !	will be defined by the values of variables "head_size" and "stroke_rgb"
    !	in that call.
    ! rotate: real, array (1D) with 3 elements. The first element is the relative to
    !	the horizontal level angle in degrees that the object is going to be
    !	rotated by. The second and the third elements are the x- and
    !	y-coordinates of the rotation center respectively.
    ! scalar_name: character, array (1D). The names of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_value" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_value" and their elements are 1-1 corresponding.
    ! scalar_value: real, array (1D). The values of the non-SVG variables that
    !	will be stored in the SVG object as XML variables. This variable may not
    !	be omitted when variable "scalar_name" is included in the arguments
    !	and vica versa. The size of the array must be equal to the size of
    !	variable "scalar_name" and their elements are 1-1 corresponding.
    ! id: character, scalar. The id of the SVG object. The user is responsible for
    !	the consistency of the ids within an SVG document.

    ! OUTPUT (REQUIRED):
    ! This subroutine returns no output.

    ! OUTPUT (OPTIONAL):
    ! This subroutine returns no output.

    ! Example of usage:

    !interface
    !subroutine SVG_Vector(output_file,x,y,stroke_width,stroke_rgb,head_size, &
      !	& change_marker,rotate,scalar_name,scalar_value,id)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: output_file*(*)
    !real(rp),intent(in):: x(2),y(2),head_size,stroke_width
    !integer,intent(in):: stroke_rgb(3)
    !logical,optional,intent(in):: change_marker
    !character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    !real(rp),optional,intent(in):: rotate(3),scalar_value(:)
    !end subroutine SVG_Vector
    !end interface

    !call SVG_Initialize(output_file='plot.svg')
    !call SVG_Vector(output_file='plot.svg', &
      !	& x=(/100.0_rp,500.0_rp/),y=(/100.0_rp,500.0_rp/), &
      !	& head_size=10.0_rp,stroke_width=3.0_rp,stroke_rgb=(/255,0,0/), &
      !	& change_marker=.true.,rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='1')
    !! The second vector is different than the first, but the changes are not
    !! applied: change_marker=.false.
    !call SVG_Vector(output_file='plot.svg', &
      !	& x=(/100.0_rp,200.0_rp/),y=(/100.0_rp,500.0_rp/), &
      !	& head_size=10.0_rp,stroke_width=3.0_rp,stroke_rgb=(/0,0,255/), &
      !	& change_marker=.false.,rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='2')
    !! The third vector is the same as the second, but now the changes are
    !! applied: change_marker=.true.
    !call SVG_Vector(output_file='plot.svg', &
      !	& x=(/100.0_rp,100.0_rp/),y=(/100.0_rp,500.0_rp/), &
      !	& head_size=10.0_rp,stroke_width=3.0_rp,stroke_rgb=(/0,0,255/), &
      !	& change_marker=.true.,rotate=(/0.0_rp,0.0_rp,0.0_rp/), &
      !	& scalar_name=(/'property'/),scalar_value=(/3.14_rp/),id='3')
    !call SVG_Finalize(output_file='plot.svg')

    ! In contrast to the SVG 1.1 specification, the zero point of the y-axis in the
    ! present library is at the lower left point of the SVG viewBox. The
    ! y-coordinates are calculated with the help of variable "viewBox_height" from
    ! module "Config".

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk,warnings_pause,viewBox_width,viewBox_height
    use ReadWriteMod, only : FileManager, StringInteger, StringReal

    implicit none

    ! Argument variables:
    character,intent(in):: output_file*(*)
    real(rp),intent(in):: x(2),y(2),head_size,stroke_width
    integer,intent(in):: stroke_rgb(3)
    logical,optional,intent(in):: change_marker
    character,optional,intent(in):: scalar_name(:)*(*),id*(*)
    real(rp),optional,intent(in):: rotate(3),scalar_value(:)

    ! Private variables:
    integer:: err,io,i
    logical:: connected,internal_change_marker
    integer,save:: vector_id=0,internal_color(3)

    ! Variable initialization:
    io=FileManager(output_file,connected)
    internal_change_marker=.false.
    if (present(change_marker)) then
      if (change_marker) internal_change_marker=.true.
    end if
    if (vector_id==0) internal_change_marker=.true.

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_Vector"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (any((/x,y/)<0.0)) then
      write(*,*)"SVG_Vector"
      write(*,*)"WARNING: negative coordinates."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(y>viewBox_height)).or.(any(x>viewBox_width))) then
      write(*,*)"SVG_Vector"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((stroke_width<0.0).or.(head_size<0.0)) then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: invalid stroke parameters."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (head_size==0.0) then
      write(*,*)"SVG_Vector"
      write(*,*)"WARNING: this head size will render the vector point invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (stroke_width==0.0) then
      write(*,*)"SVG_Vector"
      write(*,*)"WARNING: this stroke-width will render the object invisible."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((any(stroke_rgb<0)).or.(any(stroke_rgb>255))) then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: invalid RGB channel value."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
      write(*,*)"without variable 'scalar_value'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
      write(*,*)"without variable 'scalar_name'."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      if (size(scalar_name)/=size(scalar_value)) then
        write(*,*)"SVG_Vector"
        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
        write(*,*)"same amount of elements."
        write(*,*)"Program terminated."
        read(*,*)
        stop
      end if
    end if

    ! ------------------------------------------------------------------------------

    ! Note:
    ! According to the SVG 1.1 specification the marker symbols do not inherit
    ! properties (like e.g. color) from the object implementing them. This means
    ! that the color of the marker should be set separatelly, something that causes
    ! problems in the case where the line of the vector has to vary in color (e.g.
    ! in the case of property collor mapping). Therefore, every vector line must
    ! refer to its own marker symbol that is printed in the SVG file using the
    ! vector line properties and a unique object id.

    if (internal_change_marker) then

      ! Increase the id_number
      vector_id=vector_id+1
      internal_color=stroke_rgb

      ! Define the marker symbol for the arrowhead:
      write(io,'(A6)')"<defs>"
      write(io,*)"<marker"
      write(io,*)" id='ArrowHead_"//trim(StringInteger(vector_id))//"'"
      write(io,*)" viewBox='0 0 10 10'"
      write(io,*)" refX='5'"
      write(io,*)" refY='5'"
      write(io,*)" markerUnits='strokeWidth'"
      write(io,*)" markerWidth='"//trim(StringReal(head_size))//"'"
      write(io,*)" markerHeight='"//trim(StringReal(head_size))//"'"
      write(io,*)" fill='rgb("// &
        & trim(StringInteger(internal_color(1)))//","// &
        & trim(StringInteger(internal_color(2)))//","// &
        & trim(StringInteger(internal_color(3)))//")'"
      write(io,*)" orient='auto'>"
      write(io,*)" <path"
      write(io,*)"  d='M 0,10 Q 5,5 10,10 L 5,0 z'"
      write(io,*)"  transform='rotate(90,5,5)'/>"
      write(io,*)"</marker>"
      write(io,'(A7)')"</defs>"
      write(io,*)

    end if

    ! Draw the vector.
    write(io,'(A5)',iostat=err)"<line"

    if (err/=0) then
      write(*,*)"SVG_Vector"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename: ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,*)"x1='"//trim(StringReal(x(1)))//"'"
    write(io,*)"y1='"//trim(StringReal(y(1)))//"'"
    write(io,*)"x2='"//trim(StringReal(x(2)))//"'"
    write(io,*)"y2='"//trim(StringReal(y(2)))//"'"
    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
    write(io,*)"stroke='rgb("// &
      & trim(StringInteger(internal_color(1)))//","// &
      & trim(StringInteger(internal_color(2)))//","// &
      & trim(StringInteger(internal_color(3)))//")'"

    if (present(rotate)) then
      write(io,*)"transform='rotate("// &
        & trim(StringReal(rotate(1)))//","// &
        & trim(StringReal(rotate(2)))//","// &
        & trim(StringReal(viewBox_height-rotate(3)))//")'"
    end if

    if ((present(scalar_name)).and.(present(scalar_value))) then
      do i=1,size(scalar_name)
        write(io,*)trim(scalar_name(i))//"='"// &
          & trim(StringReal(scalar_value(i)))//"'"
      end do
    end if

    ! Insert the arrow point.
    write(io,*)"marker-end='url(#ArrowHead_"//trim(StringInteger(vector_id))//")'"

    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)

  end subroutine SVG_Vector

  subroutine SVG_Transform(output_file, translateX, translateY, scaleX, scaleY, clip_path_id)
    !----------------------------------
    ! Subroutine scales image drawn in Cartesian coordinates
    ! in scaleFactor times and converts coordinates from
    ! Cartesian into SVG coordinate system
    ! In addition, if optional argument clipPath is presented,
    ! the image will be clipped, i.e. only the area within clip-path
    ! will be visible.
    !----------------------------------
    use Config, only: srk
    use ReadWriteMod, only : FileManager, StringReal
    implicit none

    character(len = *), intent(in) :: output_file
    real(rp), intent(in) :: scaleX
    real(rp), intent(in) :: scaleY
    real(rp), intent(in) :: translateX
    real(rp), intent(in) :: translateY
    character(len = *), intent(in), optional :: clip_path_id

    logical :: connected
    integer :: io

    io=FileManager(output_file,connected)

    write(io,*) '<g transform="translate('
    write(io,*) trim(StringReal(translateX))// ','
    write(io,*) trim(StringReal(translateY))
    write(io,*) ')'
    write(io,*) 'scale('
    write(io,*) trim(StringReal(scaleX))//  ','
    write(io,*) trim(StringReal(-1.0_rp*scaleY))
    if (present(clip_path_id)) then
      write(io,'(a)') ')"'
      write(io,'(a)') 'style="clip-path: url(#'//trim(clip_path_id)//');"'
      write(io,'(a)') '>'
    else
      write(io,*) ')">'
    end if
    return

  end subroutine

  subroutine SVG_ClipPathPolygon(output_file,x,y, clip_path_id)
  ! Adds clipping to the SVG file
    use Config, only : warnings_pause, viewBox_height, viewBox_width
    use ReadWriteMod, only : FileManager, StringReal
    implicit none

    character,intent(in) :: output_file*(*)
    real(rp), intent(in)  :: x(:)
    real(rp), intent(in)  :: y(:)
    character,intent(in) :: clip_path_id*(*)

    ! Private variables:
    integer:: err,io,i
    logical:: connected

    ! Variable initialization:
    io=FileManager(output_file,connected)

    ! ------------------------------------------------------------------------------

    ! Error control:

    if (.not.connected) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"WARNING: uninitialized SVG file."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (len_trim(output_file)<5) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"ERROR: invalid filename."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (output_file(len_trim(output_file)-3:len_trim(output_file))/='.svg') then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"ERROR: file must have the .svg extension."
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if ((any(y>viewBox_height)).or.(any(x>viewBox_width))) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"WARNING: coordinates beyond viewBox"
      write(*,*)"x: ",x
      write(*,*)"y: ",y
      write(*,*)"viewBox_height: ",viewBox_height
      write(*,*)"viewBox_width: ",viewBox_width
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if (size(x)/=size(y)) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"ERROR: coordinate arrays do not match."
      write(*,*)"size(x): ",size(x)
      write(*,*)"size(y): ",size(y)
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

!    if (stroke_width<0.0) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"ERROR: invalid stroke parameters."
!      write(*,*)"Program terminated."
!      read(*,*)
!      stop
!    end if

!    if ((1.0<fill_opacity).or.(fill_opacity<0.0)) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"ERROR: invalid paint parameters."
!      write(*,*)"Program terminated."
!      read(*,*)
!      stop
!    end if

!    if ((fill_opacity==0.0).and.(stroke_width==0.0)) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"WARNING: this combination of fill-opacity and"
!      write(*,*)"stroke-width will render the object invisible."
!      write(*,*)"Continue..."
!      if (warnings_pause) read(*,*)
!    end if

!    if ((any(fill_rgb<0)).or.(any(stroke_rgb<0)).or. &
!        & (any(fill_rgb>255)).or.(any(stroke_rgb>255))) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"ERROR: invalid RGB channel value."
!      write(*,*)"Program terminated."
!      read(*,*)
!      stop
!    end if

!    if ((present(scalar_name)).and.(.not.(present(scalar_value)))) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"ERROR: variable 'scalar_name' may not appear in the arguments"
!      write(*,*)"without variable 'scalar_value'."
!      write(*,*)"Program terminated."
!      read(*,*)
!      stop
!    end if
!
!    if ((present(scalar_value)).and.(.not.(present(scalar_name)))) then
!      write(*,*)"SVG_Polygon"
!      write(*,*)"ERROR: variable 'scalar_value' may not appear in the arguments"
!      write(*,*)"without variable 'scalar_name'."
!      write(*,*)"Program terminated."
!      read(*,*)
!      stop
!    end if
!
!    if ((present(scalar_name)).and.(present(scalar_value))) then
!      if (size(scalar_name)/=size(scalar_value)) then
!        write(*,*)"SVG_Polygon"
!        write(*,*)"ERROR: variables 'scalar_name' and 'scalar_value' must have the"
!        write(*,*)"same amount of elements."
!        write(*,*)"size(scalar_name): ",size(scalar_name)
!        write(*,*)"size(scalar_value): ",size(scalar_value)
!        write(*,*)"Program terminated."
!        read(*,*)
!        stop
!      end if
!    end if

    if (size(x)<3) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"WARNING: can not plot a polygon with less than three points"
      write(*,*)"No polygon will be plotted."
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

    if ((all(x==0)).or.(all(y==0))) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"WARNING: all array points are equal to zero"
      write(*,*)"Continue..."
      if (warnings_pause) read(*,*)
    end if

!    if (present(id)) then
!      if (len_trim(id)==0) then
!        write(*,*)"SVG_Polygon"
!        write(*,*)"WARNING: invalid id."
!        write(*,*)"Continue..."
!        if (warnings_pause) read(*,*)
!      end if
!    end if

    ! ------------------------------------------------------------------------------

    write(io, '(a)',iostat=err) '<clipPath id="'//trim(clip_path_id)//'">'

    if (err/=0) then
      write(*,*)"SVG_PolygonClipPath"
      write(*,*)"ERROR: writing to file was not possible."
      write(*,*)"filename : ",trim(output_file)
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    write(io,'(A8)',iostat=err)"<polygon"

    write(io,*)"points='"
    do i=1,size(x)-1
      write(io,*)trim(StringReal(x(i)))//","// &
        & trim(StringReal(y(i)))
    end do
    write(io,*)trim(StringReal(x(size(x))))//","// &
      & trim(StringReal(y(size(y))))//"'"

!    write(io,*)"stroke-width='"//trim(StringReal(stroke_width))//"'"
!    write(io,*)"fill-opacity='"//trim(StringReal(fill_opacity))//"'"
!    write(io,*)"stroke='rgb("// &
!      & trim(StringInteger(stroke_rgb(1)))//","// &
!      & trim(StringInteger(stroke_rgb(2)))//","// &
!      & trim(StringInteger(stroke_rgb(3)))//")'"
!    write(io,*)"fill='rgb("// &
!      & trim(StringInteger(fill_rgb(1)))//","// &
!      & trim(StringInteger(fill_rgb(2)))//","// &
!      & trim(StringInteger(fill_rgb(3)))//")'"
!
!    if (present(rotate)) then
!      write(io,*)"transform='rotate("// &
!        & trim(StringReal(rotate(1)))//","// &
!        & trim(StringReal(rotate(2)))//","// &
!        & trim(StringReal(viewBox_height-rotate(3)))//")'"
!    end if
!
!    if ((present(scalar_name)).and.(present(scalar_value))) then
!      do i=1,size(scalar_name)
!        write(io,*)trim(scalar_name(i))//"='"// &
!          & trim(StringReal(scalar_value(i)))//"'"
!      end do
!    end if
!
!    if (present(id)) write(io,*)"id='"//trim(id)//"'"

    write(io,'(A2)')"/>"
    write(io,*)
    write(io,'(a11)') "</clipPath>"
    write(io,*)


  end subroutine

end module
