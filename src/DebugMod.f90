module DebugMod
  use precision_mod
  contains

  subroutine ReadGammaChords(nameOfFile, nChordGam, nSegChordGam, lenChordGam, wghtChordGam, &
                             zoneChordGam, xChordGam, yChordGam, inSideGam, inSegGam,        &
                             inSectGam, inZoneGam, inHarmGam, nGamma, nGammaElem)
    use Config, only : str_len
    character(str_len), intent(in) :: nameOfFile

    integer, intent(out)              :: nChordGam         ! number of chords for integration of surface-to-surface CP (beta)
    integer, allocatable, intent(out) :: nSegChordGam(:)   ! number of segments along the chord for integration beta
    real(rp), allocatable, intent(out) :: lenChordGam(:,:)  ! length of segments along the chord
    real(rp), allocatable, intent(out) :: wghtChordGam(:)   ! weight of the beta chord
    integer, allocatable, intent(out) :: zoneChordGam(:,:) ! zones' numbers along the chord (for definition of the materials)
    real(rp), allocatable, intent(out) :: xChordGam(:,:)    ! x-coordinates of the segments along the chord
    real(rp), allocatable, intent(out) :: yChordGam(:,:)    ! y-coordinates of the segments along the chord
    integer, allocatable, intent(out) :: inSideGam(:)      ! number of input side for chord
    integer, allocatable, intent(out) :: inSegGam(:)       ! number of input segment for chord
    integer, allocatable, intent(out) :: inSectGam(:)      ! number of input sector for chord
    integer, allocatable, intent(out) :: inZoneGam(:)
    integer, allocatable, intent(out) :: inHarmGam(:)
    integer, allocatable, intent(out) :: nGamma(:)          ! number of beta element where chord contributes
    integer, intent(out)              :: nGammaElem         ! total number of beta elements

    integer :: fin, nSegMax, ich, iseg, i

    fin = 1548
    open(fin, file=nameOfFile)
    read(fin,*) nChordGam
    read(fin,*) nGammaElem
    allocate(nSegChordGam(nChordGam))
    nSegMax = 5
    allocate(lenChordGam(nChordGam,nSegMax))
    lenChordGam = 0.0_rp
    allocate(wghtChordGam(nChordGam))
    wghtChordGam = 0.0_rp
    allocate(zoneChordGam(nChordGam,nSegMax))
    zoneChordGam = 0
    allocate(xChordGam(nChordGam,nSegMax+1))
    xChordGam = 0.0_rp
    allocate(yChordGam(nChordGam,nSegMax+1))
    yChordGam = 0.0_rp
    allocate(inSideGam(nChordGam))
    inSideGam = 0
    allocate(inSegGam(nChordGam))
    inSegGam  = 0
    allocate(inSectGam(nChordGam))
    inSectGam = 0
    allocate(inZoneGam(nChordGam))
    inZoneGam = 0
    allocate(inHarmGam(nChordGam))
    inHarmGam = 0
    allocate(nGamma(nChordGam))
    nGamma = 0

    do i = 1, nChordGam
      read(fin,*) ich
      read(fin,*) wghtChordGam(ich)
      read(fin,*) nSegChordGam(ich)
      read(fin,*) inSideGam(ich), inSegGam(ich), inSectGam(ich)
      do iseg = 1, nSegChordGam(ich)
        read(fin,*) zoneChordGam(ich,iseg)
        read(fin,*) lenChordGam(ich,iseg)
        read(fin,*) xChordGam(ich,iseg)
        read(fin,*) yChordGam(ich,iseg)
        read(fin,*) xChordGam(ich,iseg+1)
        read(fin,*) yChordGam(ich,iseg+1)
      end do
    end do

    close(fin)

  end subroutine ReadGammaChords

  subroutine ReadBetaChords(nameOfFile, nChordBet, nSegChordBet, lenChordBet, wghtChordBet,      &
                            zoneChordBet, inSideBet, inSegBet, inSectBet, outSideBet, outSegBet, &
                            outSectBet, brc, nBeta, nBetaElem)
    use Config
    implicit none

! Input
    character(str_len), intent(in) :: nameOfFile

! Output
    integer, intent(out)              :: nChordBet         ! number of chords for integration of surface-to-surface CP (beta)
    integer, allocatable, intent(out) :: nSegChordBet(:)   ! number of segments along the chord for integration beta
    real(rp), allocatable, intent(out) :: lenChordBet(:,:)  ! length of segments along the chord
    real(rp), allocatable, intent(out) :: wghtChordBet(:)   ! weight of the beta chord
    integer, allocatable, intent(out) :: zoneChordBet(:,:) ! zones' numbers along the chord (for definition of the materials)
    integer, allocatable, intent(out) :: inSideBet(:)      ! number of input side for chord
    integer, allocatable, intent(out) :: inSegBet(:)       ! number of input segment for chord
    integer, allocatable, intent(out) :: inSectBet(:)      ! number of input sector for chord
    integer, allocatable, intent(out) :: outSideBet(:)     ! number of output side for chord
    integer, allocatable, intent(out) :: outSegBet(:)      ! number of output segment for chord
    integer, allocatable, intent(out) :: outSectBet(:)     ! number of output segment for chord
    real(rp), allocatable, intent(out) :: brc(:)            ! array with beta reciprocity coefficients
    integer, allocatable, intent(out) :: nBeta(:)          ! number of beta element where where chord contributes
    integer, intent(out)              :: nBetaElem         ! total number of beta elements

! Local
    integer :: fin, nSegMax, ich, iseg, i, i1
    integer :: inSide, inSeg, inSect, outSide, outSeg, outSect

    fin = 1350
    open(fin, file=nameOfFile)

    read(fin, *) nChordBet
    read(fin, *) nBetaElem

    nSegMax = 5
    allocate(nSegChordBet(nChordBet))
    nSegChordBet = 0
    allocate(lenChordBet(nChordBet,nSegMax))
    lenChordBet = 0.0_rp
    allocate(wghtChordBet(nChordBet))
    wghtChordBet = 0.0_rp
    allocate(zoneChordBet(nChordBet,nSegMax))
    zoneChordBet = 0
    allocate(inSideBet(nBetaElem))
    inSideBet = 0
    allocate(inSegBet(nBetaElem))
    inSegBet = 0
    allocate(inSectBet(nBetaElem))
    inSectBet = 0
    allocate(outSideBet(nBetaElem))
    outSideBet = 0
    allocate(outSegBet(nBetaElem))
    outSegBet = 0
    allocate(outSectBet(nBetaElem))
    outSectBet = 0
    allocate(brc(nBetaElem))
    brc = 0.0_rp
    allocate(nBeta(nChordBet))
    nBeta = 0

    do i = 1, nChordBet
      read(fin, *) ich
      read(fin, *) outSeg, outSect, outSide, inSeg, inSect, inSide
      read(fin, *) nBeta(ich)
      outSegBet(nBeta(ich)) = outSeg
      outSectBet(nBeta(ich)) = outSect
      outSideBet(nBeta(ich)) = outSide
      inSegBet(nBeta(ich)) = inSeg
      inSectBet(nBeta(ich)) = inSect
      inSideBet(nBeta(ich)) = inSide
      read(fin, *) wghtChordBet(ich)
      read(fin, *) nSegChordBet(ich)
      do iseg = 1, nSegChordBet(ich)
        read(fin, *) zoneChordBet(ich,iseg)
        read(fin, *) lenChordBet(ich,iseg)
      end do
    end do
    do i = 1, nBetaElem
      read(fin, *) brc(i)
    end do

    nBetaElem = nBetaElem/2

    do i = 1, nBetaElem
      i1 = nBetaElem + i
      inSideBet(i1) = outSideBet(i)
      inSegBet(i1) = outSegBet(i)
      inSectBet(i1) = outSectBet(i)
      outSideBet(i1) = inSideBet(i)
      outSegBet(i1) = inSegBet(i)
      outSectBet(i1) = inSectBet(i)
    end do

    return

  end subroutine ReadBetaChords

  subroutine ReadUChords(nChordU, nSegChordU, lenChordU, wghtChordU, zoneChordU, &
                         xChordU, yChordU)
    use Config, only : str_len
    implicit none

    integer, intent(out)              :: nChordU
    integer, allocatable, intent(out) :: nSegChordU(:)
    real(rp), allocatable, intent(out) :: wghtChordU(:)
    real(rp), allocatable, intent(out) :: lenChordU(:,:)
    integer, allocatable, intent(out) :: zoneChordU(:,:)
    real(rp), allocatable, intent(out) :: xChordU(:,:)
    real(rp), allocatable, intent(out) :: yChordU(:,:)

    integer :: fin, nSegMax, ich, iseg, i

    fin = 1547

    read(fin,*) nChordU
    nSegMax = 5
    allocate(nSegChordU(nChordU))
    nSegChordU = 0
    allocate(wghtChordU(nChordU))
    wghtChordU = 0.0_rp
    allocate(lenChordU(nChordU,nSegMax))
    lenChordU = 0.0_rp
    allocate(zoneChordU(nChordU,nSegMax))
    zoneChordU = 0
    allocate(xChordU(nChordU,nSegMax+1))
    xChordU = 0.0_rp
    allocate(yChordU(nChordU,nSegMax+1))
    yChordU = 0.0_rp

    do i = 1, nChordU
      read(fin, *) ich
      read(fin, *) wghtChordU(ich)
      read(fin, *) nSegChordU(ich)
      do iseg = 1, nSegChordU(ich)
        read(fin, *) zoneChordU(ich,iseg)
        read(fin, *) lenChordU(ich,iseg)
        read(fin, *) xChordU(ich,iseg)
        read(fin, *) yChordU(ich,iseg)
        read(fin, *) xChordU(ich,iseg+1)
        read(fin, *) yChordU(ich,iseg+1)
      end do
    end do

    close(fin)

  end subroutine ReadUChords

end module
