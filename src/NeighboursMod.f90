module NeighboursMod
  use precision_mod

  integer, allocatable :: KN(:,:)
  integer, allocatable :: KS(:,:)
!  integer, allocatable :: KT(:)
  real(rp), allocatable :: FORMI(:)
  integer :: NC
  integer :: IPGEOM
  integer :: NR

  contains

    subroutine GetNeighbours(ncells, assembly_type, isym, nrow, ncol, neighCell, neighSide, fact)
      use Config, only : str_len
      implicit none

      integer, intent(in)                    :: ncells
      character(str_len), intent(in) :: assembly_type
      integer, intent(in)                    :: isym
      integer, intent(in)                    :: nrow
      integer, intent(in)                    :: ncol
      integer, intent(out)                   :: neighCell(:,:)
      integer, intent(out)                   :: neighSide(:,:)
      real(rp), intent(out)                   :: fact(:)

      if (assembly_type == "regular hexagonal") then
        call GetNeighboursRegularHex(ncells, isym, nrow, neighCell, neighSide, fact)
        return
      elseif (assembly_type == "regular hexagonal with the gap") then
        call GetNeighboursRegularHex(ncells, isym, nrow, neighCell, neighSide, fact)
        return
      elseif (assembly_type == "regular square") then
        call GetNeighboursRegularSquare(ncells, isym, nrow, ncol, neighCell, neighSide, fact)
      else
        write(*,*) trim(assembly_type)//" is not supported at the moment."
        stop
      end if

    end subroutine GetNeighbours

    subroutine GetNeighboursRegularHex(ncells, isym, nrow, neighCell, neighSide, fact)
      implicit none

      integer, intent(in)  :: ncells
      integer, intent(in)  :: isym
      integer, intent(in)  :: nrow
      integer, intent(out) :: neighCell(:,:)
      integer, intent(out) :: neighSide(:,:)
      real(rp), intent(out) :: fact(:)

      integer :: icell, iside, iside_assmb, ncell_on_side, i

      integer :: corner_kn(6,6), side_kn(6,6)
      integer :: corner_ks(6,6), side_ks(6,6)

      corner_kn(1,:) = (/0,6,5,6,0,0/)
      corner_kn(2,:) = (/0,0,1,6,1,0/)
      corner_kn(3,:) = (/0,0,0,2,1,2/)
      corner_kn(4,:) = (/3,0,0,0,3,2/)
      corner_kn(5,:) = (/3,4,0,0,0,4/)
      corner_kn(6,:) = (/5,4,5,0,0,0/)

      side_kn(1,:) = (/0,0,1,6,0,0/)
      side_kn(2,:) = (/0,0,0,2,1,0/)
      side_kn(3,:) = (/0,0,0,0,3,2/)
      side_kn(4,:) = (/3,0,0,0,0,4/)
      side_kn(5,:) = (/5,4,0,0,0,0/)
      side_kn(6,:) = (/0,6,5,0,0,0/)

      corner_ks(1,:) = (/0,-3,-2,-3,0,0/)
      corner_ks(2,:) = (/0,0,-4,-3,-4,0/)
      corner_ks(3,:) = (/0,0,0,-5,-4,-5/)
      corner_ks(4,:) = (/-6,0,0,0,-6,-5/)
      corner_ks(5,:) = (/-6,-1,0,0,0,-1/)
      corner_ks(6,:) = (/-2,-1,-2,0,0,0/)

      side_ks(1,:) = (/0,0,-4,-3,0,0/)
      side_ks(2,:) = (/0,0,0,-5,-4,0/)
      side_ks(3,:) = (/0,0,0,0,-6,-5/)
      side_ks(4,:) = (/-6,0,0,0,0,-1/)
      side_ks(5,:) = (/-2,-1,0,0,0,0/)
      side_ks(6,:) = (/0,-3,-2,0,0,0/)

      if (allocated(KN)) deallocate(KN)
      if (allocated(KS)) deallocate(KS)
      if (allocated(FORMI)) deallocate(FORMI)
      allocate(KN(6,ncells))
      allocate(KS(6,ncells))
      allocate(FORMI(ncells))
      KN = 0
      KS = 0
      FORMI = 0.0_rp
      NC = ncells
      ipgeom = isym
      NR = nrow

      if (isym == 360) then
        if (nrow == 1) then
          neighCell(1:6,1) = 1
          do i = 1, 6
            neighSide(i,1) = -i
          end do
          fact(1) = 1.0_rp
          return
        end if
        call WSCALE(nrow,-ipgeom)
        neighCell = KN
        neighSide = KS
        fact = FORMI
        ncell_on_side = (nrow - 1)
        icell = 1
        do iside_assmb = 1, 6
          do i = 1, ncell_on_side
            if (i == 1) then ! corner cells
              do iside = 1, 6
                if (KN(iside,icell) == 0) then
                  neighCell(iside,icell) = KN(corner_kn(iside_assmb,iside),icell)
                  neighSide(iside,icell) = corner_ks(iside_assmb,iside)
                end if
              end do
              fact(icell) = 1.0_rp/3.0_rp
            else ! cells in the row
              do iside = 1, 6
                if (KN(iside,icell) == 0) then
                  neighCell(iside,icell) = KN(side_kn(iside_assmb,iside),icell)
                  neighSide(iside,icell) = side_ks(iside_assmb,iside)
                end if
              end do
              fact(icell) = 0.5_rp
            end if
            icell = icell + 1
          end do
        end do
        return
!        do icell = 1, ncells
!          do iside = 1, 6
!            if (neighCell(iside,icell) == 0 .or. neighSide(iside,icell) == 0) then
!              write(*,*) "Something went wrong!"
!              stop
!            end if
!          end do
!        end do
!
!        write(*,*) "neighCell was created somehow..."
!        do icell = 1, ncells
!          write(*,*) icell
!          write(*,*) (neighCell(iside,icell),iside=1,6)
!          write(*,*) (neighSide(iside,icell),iside=1,6)
!        end do
!        stop
      end if

      call WSCALE(nrow,ipgeom)
      neighCell = KN
      neighSide = KS
      fact = FORMI

      return

    end subroutine GetNeighboursRegularHex

    subroutine GetNeighboursRegularSquare(ncells, isym, nrow, ncol, neighCell, neighSide, fact)
      implicit none
      integer, intent(in) :: ncells
      integer, intent(in) :: isym
      integer, intent(in) :: nrow
      integer, intent(in) :: ncol
      integer, intent(inout) :: neighCell(:,:)
      integer, intent(inout) :: neighSide(:,:)
      real(rp), intent(inout) :: fact(:)

      integer :: ir, ic, isd, icell

      if (isym == 360) then
        icell = 1
        if (ncells == 1) then
          neighCell(1,icell) = icell
          neighCell(2,icell) = icell
          neighCell(3,icell) = icell
          neighCell(4,icell) = icell
          neighSide(1,icell) = -1
          neighSide(2,icell) = -2
          neighSide(3,icell) = -3
          neighSide(4,icell) = -4
          fact(icell) = 1.0_rp
          return
        end if
        do ir = 1, nrow
          do ic = 1, ncol
            if (ir == 1) then
              if (ic == 1) then
                neighCell(1,icell) = icell + 1
                neighCell(2,icell) = icell
                neighCell(3,icell) = icell
                neighCell(4,icell) = icell + ncol
                neighSide(1,icell) = 3
                neighSide(2,icell) = -2
                neighSide(3,icell) = -3
                neighSide(4,icell) = 2
              elseif (ic == ncol) then
                neighCell(1,icell) = icell
                neighCell(2,icell) = icell
                neighCell(3,icell) = icell - 1
                neighCell(4,icell) = icell + ncol
                neighSide(1,icell) = -1
                neighSide(2,icell) = -2
                neighSide(3,icell) = 1
                neighSide(4,icell) = 2
              else
                neighCell(1,icell) = icell + 1
                neighCell(2,icell) = icell
                neighCell(3,icell) = icell - 1
                neighCell(4,icell) = icell + ncol
                neighSide(1,icell) = 3
                neighSide(2,icell) = -2
                neighSide(3,icell) = 1
                neighSide(4,icell) = 2
              end if
            elseif (ir == nrow) then
              if (ic == 1) then
                neighCell(1,icell) = icell + 1
                neighCell(2,icell) = icell - ncol
                neighCell(3,icell) = icell
                neighCell(4,icell) = icell
                neighSide(1,icell) = 3
                neighSide(2,icell) = 4
                neighSide(3,icell) = -3
                neighSide(4,icell) = -4
              elseif (ic == ncol) then
                neighCell(1,icell) = icell
                neighCell(2,icell) = icell - ncol
                neighCell(3,icell) = icell - 1
                neighCell(4,icell) = icell
                neighSide(1,icell) = -1
                neighSide(2,icell) = 4
                neighSide(3,icell) = 1
                neighSide(4,icell) = -4
              else
                neighCell(1,icell) = icell + 1
                neighCell(2,icell) = icell - ncol
                neighCell(3,icell) = icell - 1
                neighCell(4,icell) = icell
                neighSide(1,icell) = 3
                neighSide(2,icell) = 4
                neighSide(3,icell) = 1
                neighSide(4,icell) = -4
              end if
            else
              if (ic == 1) then
                neighCell(1,icell) = icell + 1
                neighCell(2,icell) = icell - ncol
                neighCell(3,icell) = icell
                neighCell(4,icell) = icell + ncol
                neighSide(1,icell) = 3
                neighSide(2,icell) = 4
                neighSide(3,icell) = -3
                neighSide(4,icell) = 2
              elseif (ic == ncol) then
                neighCell(1,icell) = icell
                neighCell(2,icell) = icell - ncol
                neighCell(3,icell) = icell - 1
                neighCell(4,icell) = icell + ncol
                neighSide(1,icell) = -1
                neighSide(2,icell) = 4
                neighSide(3,icell) = 1
                neighSide(4,icell) = 2
              else
                neighCell(1,icell) = icell + 1
                neighCell(2,icell) = icell - ncol
                neighCell(3,icell) = icell - 1
                neighCell(4,icell) = icell + ncol
                neighSide(1,icell) = 3
                neighSide(2,icell) = 4
                neighSide(3,icell) = 1
                neighSide(4,icell) = 2
              end if
            end if
            fact(icell) = 1.0
            icell = icell + 1
          end do
        end do
      elseif (isym == 45) then ! neighbours for the case when symmetry line do not crosses the lower row (e.g. coincide with the lower cells boundary)
                               ! In this case full symmetry will have even number of the rows and columns
        icell = 1
        do ir = 1, nrow
          do ic = 1, ir
            if (icell == 1) then
              neighCell(1,icell) = 1
              neighCell(2,icell) = 1
              neighCell(3,icell) = 3
              neighCell(4,icell) = 3
              neighSide(1,icell) = -1
              neighSide(2,icell) = 1
              neighSide(3,icell) = -2
              neighSide(4,icell) = 2
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            elseif (ic == 1 .and. ir < nrow) then
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell + 1
              neighCell(3,icell) = icell + ir + 1
              neighCell(4,icell) = icell + ir + 1
              neighSide(1,icell) = 3
              neighSide(2,icell) = -3
              neighSide(3,icell) = -2
              neighSide(4,icell) = 2
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            elseif (ic == ir .and. ir < nrow) then
              neighCell(1,icell) = icell
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell + ir + 1
              neighSide(1,icell) = -1
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = 2
              fact(icell) = 1.0_rp
              icell = icell + 1
              cycle
            elseif (ic == 1 .and. ir == nrow) then
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell + 1
              neighCell(3,icell) = icell
              neighCell(4,icell) = icell
              neighSide(1,icell) = 3
              neighSide(2,icell) = -3
              neighSide(3,icell) = 4
              neighSide(4,icell) = -4
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            elseif (ic == nrow .and. ir == nrow) then
              neighCell(1,icell) = icell
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell
              neighSide(1,icell) = -1
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = -4
              fact(icell) = 1.0_rp
              icell = icell + 1
              cycle
            elseif (ir == nrow) then
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell
              neighSide(1,icell) = 3
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = -4
              fact(icell) = 1.0_rp
              icell = icell + 1
              cycle
            else
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell + ir + 1
              neighSide(1,icell) = 3
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = 2
              fact(icell) = 1.0_rp
              icell = icell + 1
              cycle
            end if
          end do
        end do
      elseif (isym == 451) then ! neighbours for the case when symmetry line crosses the lower row (i.e. full symmetry will have odd number of the rows and columns)
        icell = 1
        do ir = 1, nrow
          do ic = 1, ir
            if (icell == 1) then
              neighCell(1,icell) = 1
              neighCell(2,icell) = 1
              neighCell(3,icell) = 3
              neighCell(4,icell) = 3
              neighSide(1,icell) = -1
              neighSide(2,icell) = 1
              neighSide(3,icell) = -2
              neighSide(4,icell) = 2
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            elseif (ic == 1 .and. ir < nrow) then
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell + 1
              neighCell(3,icell) = icell + ir + 1
              neighCell(4,icell) = icell + ir + 1
              neighSide(1,icell) = 3
              neighSide(2,icell) = -3
              neighSide(3,icell) = -2
              neighSide(4,icell) = 2
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            elseif (ic == ir .and. ir < nrow) then
              neighCell(1,icell) = icell
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell + ir + 1
              neighSide(1,icell) = -1
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = 2
              fact(icell) = 1.0_rp
              icell = icell + 1
              cycle
            elseif (ic == 1 .and. ir == nrow) then
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell + 1
              neighCell(3,icell) = icell + 1
              neighCell(4,icell) = icell + 1
              neighSide(1,icell) = 3
              neighSide(2,icell) = -3
              neighSide(3,icell) = 3
              neighSide(4,icell) = -3
              fact(icell) = 0.25_rp
              icell = icell + 1
              cycle
            elseif (ic == nrow .and. ir == nrow) then
              neighCell(1,icell) = icell
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell - ir
              neighSide(1,icell) = -1
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = -4
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            elseif (ir == nrow) then
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell - ir
              neighSide(1,icell) = 3
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = -4
              fact(icell) = 0.5_rp
              icell = icell + 1
              cycle
            else
              neighCell(1,icell) = icell + 1
              neighCell(2,icell) = icell - ir
              neighCell(3,icell) = icell - 1
              neighCell(4,icell) = icell + ir + 1
              neighSide(1,icell) = 3
              neighSide(2,icell) = 4
              neighSide(3,icell) = 1
              neighSide(4,icell) = 2
              fact(icell) = 1.0_rp
              icell = icell + 1
              cycle
            end if
          end do
        end do
      else
        write(*,*) "The symmetry ", isym, " is not supported at the moment for the regular square assemblies without gap."
        stop
      end if

      return

    end subroutine

    subroutine Get_cell_edge_numbers()
      ! This subroutine fills the array
      ! num_cell_edge which will be used later for
      ! calculations with cell-dependent albedos
      use GlobalAssemblyMod, only : nCol, assembly_type, num_cell_edge, albedo, nCell
      implicit none

      integer :: i, j, ns
      integer :: isd, ic

      num_cell_edge = 0

      if (assembly_type == "regular square") then
        do i = 1, nCol
          ns = nCol - i + 1
          num_cell_edge(2,i) = ns
          num_cell_edge(1,i*nCol) = ns
          num_cell_edge(3,(i - 1)*nCol + 1) = i
          num_cell_edge(4,nCol*(nCol - 1) + i) = i
        end do
!        open(111, file="results/albedo_debug.out")
!        do isd = 1, 4
!          write(111, *) "SIDE ", isd
!          do ic = 1, nCell
!            if (num_cell_edge(isd, ic)> 0)  then
!              write(111, '(i4,f8.2)') ic, albedo(1,isd,num_cell_edge(isd, ic))
!            end if
!          end do
!        end do
!        stop
      else
        write(*,*) "Assembly type ", assembly_type, " is not supported at the moment."
        stop
      end if

      return

    end subroutine

    SUBROUTINE Wscale(nrow,igeom)
    ! ************* February 2000,February 2006 ****************
    !   DEFINITION OF CELLS' NEIGHBOURS, NEIGHBOURS' SIDES, WAGES OF CELLS
    !   FOR VARIOUS SYMMETRY ELEMENTS OF ASSEMBLY WITH THE FREE ESCAPE
    !   ON OUTER BOUNDARY. FOR NON-STACIONAR PROGRAM.
    ! **********************************************************
    !   MIRROR REFLECTION:
    !                  30  IGEO  1
    !                  60        2
    !                  90        3
    !                 180        6
    !   ROTATION SYMMETRY:
    !                 -60        1
    !                -120        2
    !                -180        3
    !                -360        6
    !   NR IS NUMBER OF ROWS IN ASSEMBLY
    !   KN IS CARTOGRAM OF NEIGHBOURS
    !   KS IS CARTOGRAM OF NEIGHBOURS' SIDES. KS(I,M)<0 IF NEIGHBOUR CELL
    !            IS IN UNOTHER SYMMETRY ELEMENT
    !   FORMI ARE WAGES OF CELLS
    !      use Global
        integer :: NK(12), MR(6,2), KS1(6,4), KN2(6,4), KN1(6,4)
        integer :: nrow, igeom
        integer :: nr
        integer :: N, L3, L2, L1, K6, K5, K4, K3, K2, K1
        integer :: K, JA, J3, J1, J, I3, I2, I1, I0, I
        integer :: IPGE, IPG, ipgeom, igeo1, igeo
        !common /SCALECONST/ NR, IPGEOM
        !common /NEIGH/KN(6,LPK),KS(6,LPK),NC,FORMI(LPK)
        !======================
        DATA KN1/6*-1,  -2,-1,-2,-1,-2,-1,  -1,-1,-2,-1,-1,-2, &
                    -2,-3,-4,-3,-2,-1/
        DATA KN2/-1,3,1,-3,-5,3,-2,3,2,-3,2,3, -2,3,2,3,2,3, &
                    -2,3,2,3,2,3/
        DATA KS1/6,-6,6,-6,6,-6, 6,-1,6,1,-6,1, 1,-1,6,1,-1,-6, &
                    -2,-1,6,1,2,3/
        DATA MR/0,0,-1,-1,-2,-2,   0,-1,-2,-3,-4,-5/
        DATA NK/1,2,3,4,5,6,1,2,3,4,5,6/

        100   format(i4,4x,2(6i5,3x))
        101   format(i4,2X,5f8.5)
    !**************** INPUT DATA *********************************
    !	OPEN (20,FILE='WSCALE.OUT')
    !	IPGEOM=30
    !	NR=13
    !*************************************************************
        L1 = 0
        L2 = 0
        if (nrow == 1 .and. igeom == -360) then
            KS = 1
            KN = 0
            FORMI = 1.
            NC = 1
            return
        end if
        nr=nrow
        ipgeom=igeom
        NC=1
        IF(NR.NE.1) GO TO 4
        DO I=1,6
            KN(I,1)=1
            KS(I,1)=-I
        END DO
        FORMI(1)=1.
        GO TO 20
        4 IF(IPGEOM.GT.0) THEN
        ! MIRROR REFLECTION
            IGEO=IPGEOM/30
        ! NUMBER OF CELLS
            if (IPGEOM == 360) then
              NC = 1
              do i = 1, NR - 1
                NC = NC + i * 6
              end do
            else
              DO I=2,NR
                I2=(I+1)/2
                I1=I2-I/2+1
                NC=NC+I2*IGEO+MR(IGEO,I1)
              END DO
            end if
        ELSE
        ! ROTATION  SYMMETRY
            IGEO=ABS(IPGEOM/60)
        ! NUMBER OF CELLS
            DO I=1,NR-1
                NC=NC+(NR-I)*IGEO
            END DO
        END IF
        DO J=1,NC
            DO I=1,6
                KS(I,J)=NK(3+I)
            END DO
            FORMI(J)=1.
        END DO
        IF(IPGEOM.GT.0) THEN
        ! ---- CALCULATION KN, KS, FORMI (MIRROR REFLECTION) --------------
            N=0
        ! CYCLE ON  ROWS
            DO I=NR,2,-1
                L1=L2
                I1=(I+1)/2  - I/2 +1
                I2=(I+1)/2
                L2=I2*IGEO+MR(IGEO,I1)
                K1=1
                K2=2
                K3=3
                K4=4
                K5=5
                K6=6
                N=N+1
                KN(1,N)=N+1
                KN(5,N)=N+1
                KN(6,N)=L2+N
                KS(1,N)=-2
                IF(I.EQ.2.AND.IPGEOM.EQ.30) THEN
                    KN(1,N)=N
                    KS(1,N)=5
              ENDIF
        ! FIRST CELL OF  ROW
                IF(I.NE.NR)    THEN
                  KN(2,N)=N-L1+1
                    KN(3,N)=N-L1
                    KN(4,N)=N-L1+1
                    KS(2,N)=-1
                  FORMI(N)=0.5
              ELSE
        ! FIRST CELL OF FIRST ROW
                    KN(2,N)=KN(6,N)
                  KS(2,N)=KS(6,N)
                  KN(4,N)=KN(6,N)
                  KS(4,N)=-KS(6,N)
                  KN(3,N)=KN(5,N)
                  KS(3,N)=KS(5,N)
                    FORMI(N)=0.16666667
                ENDIF
                JA=0
                J=0
        ! CYCLE ON  SECTORS 60
                DO WHILE (JA.LT.IPGEOM)
                    L3=I-2
                    JA=JA+60
                    J=J+1
                    IF(JA.GT.IPGEOM) L3=(I+1)/2-1
        ! CYCLE ON  CELLS IN SECTOR
                    DO J1=1,L3
                        N=N+1
                        KN(NK(K1),N)=N+L2-J
                        KN(NK(K2),N)=N-1
                        KN(NK(K3),N)=N-L1+J-1
                        KN(NK(K4),N)=N-L1+J
                        KN(NK(K5),N)=N+1
                        KN(NK(K6),N)=N+L2-J+1
        ! FOR THE CELLS ON OUTER BOUNDARY OF ASSEMBLY ONLY (FIRST ROW)
                        IF(I.EQ.NR)  THEN
                          KN(NK(K3),N)=KN(NK(K1),N)
                          KN(NK(K4),N)=KN(NK(K6),N)
                          KS(NK(K3),N)=-KS(NK(K1),N)
                          KS(NK(K4),N)=-KS(NK(K6),N)
                          FORMI(N)=0.5
                      ENDIF
                    END DO
                    SELECT CASE (JA-IPGEOM)
        ! END CYCLE ON SECTORS, BOUNDARY 30 OR 90
                        CASE(1:)
                            SELECT CASE (I1)
        ! EVEN ROW
                                CASE(1)
                                    KN(NK(K5),N)=N
                                    KS(NK(K5),N)=-NK(K5)
                                  FORMI(N)=1.
                                  IF(I.EQ.2.AND.IPGEOM.EQ.30)FORMI(N)=0.5
        ! ODD ROW
                                CASE(2)
                                    KN(NK(K4),N)=KN(NK(K3),N)
                                    KN(NK(K5),N)=KN(NK(K2),N)
                                    KN(NK(K6),N)=KN(NK(K1),N)
                                    KS(NK(K4),N)=-NK(K6)
                                    KS(NK(K5),N)=-NK(K5)
                                    KS(NK(K6),N)=-NK(K4)
                                  FORMI(N)=0.5
                              END SELECT
        ! FOR THE CELLS ON OUTER BOUNDARY OF ASSEMBLY ONLY (FIRST ROW)
                                IF(I.EQ.NR) THEN
                                  KN(NK(K3),N)=KN(NK(K1),N)
                                  KS(NK(K3),N)=-KS(NK(K1),N)
                                  KN(NK(K4),N)=KN(NK(K6),N)
                                  KS(NK(K4),N)=-KS(NK(K6),N)
                                  FORMI(N)=0.5*FORMI(N)
                                  IF(NR.EQ.2.AND.IPGEOM.GE.90) THEN
                                      KN(NK(K3),N)=KN(NK(K2),N)
                                      KS(NK(K3),N)=-KS(NK(K2),N)
                                      FORMI(N)=0.33333333
                                  END IF
                                  IF(NR.EQ.2.AND.IPGEOM.EQ.30)FORMI(N)=0.16666667
                                END IF
        ! END CYCLE ON SECTORS, BOUNDARY ANGLE 60 OR 180
                        CASE(0)
                            N=N+1
                            KN(NK(K1),N)=N+L2-J
                            KN(NK(K2),N)=N-1
                            KN(NK(K3),N)=N-L1+J-1
                            KN(NK(K4),N)=N-L1+J
                            KN(NK(K5),N)=N-L1+J-1
                            KN(NK(K6),N)=N-1
                            KS(NK(K5),N)=-NK(K6)
                            KS(NK(K6),N)=-NK(K5)
                          FORMI(N)=0.5
        ! FOR THE CELLS ON OUTER BOUNDARY OF ASSEMBLY ONLY (FIRST ROW)
                            IF(I.EQ.NR) THEN
                              KN(NK(K3),N)=KN(NK(K1),N)
                              KS(NK(K3),N)=-KS(NK(K1),N)
                              KN(NK(K5),N)=KN(NK(K1),N)
                              KS(NK(K5),N)=KS(NK(K1),N)
                              KN(NK(K4),N)=KN(NK(K2),N)
                              KS(NK(K4),N)=KS(NK(K2),N)
                              FORMI(N)=0.16666667
                            END IF
        ! INTERNAL ANGLE 60 OR 120
                        CASE(:-1)
                            K5=K5+1
                            K6=K6-1
                            N=N+1
                            KN(NK(K1),N)=N+L2-J
                            KN(NK(K2),N)=N-1
                            KN(NK(K3),N)=N-L1+J-1
                            KN(NK(K4),N)=N-L1+J
                            KN(NK(K5),N)=N+1
                            KN(NK(K6),N)=N-L1+J+1
        ! FOR THE CELLS ON OUTER BOUNDARY OF ASSEMBLY ONLY (FIRST ROW)
                        IF(I.EQ.NR) THEN
                          DO K=1,3
                              KN(NK(K2+K),N)=KN(NK(K1),N)
                              KS(NK(K2+K),N)=-KS(NK(K1),N)
                          END DO
                          KS(NK(K2+2),N)=KS(NK(K1),N)
                          FORMI(N)=0.33333333
                        END IF
                    END SELECT
                  IF(I.EQ.2.AND.IPGEOM.EQ.30.OR.IPGEOM.EQ.90.AND.J.EQ.2) THEN
                  ELSE
                  ENDIF
                    K1=K1+1
                    K2=K2+1
                    K3=K3+1
                    K4=K4+1
                    K6=K6+2
        ! END OF CYCLE ON  SECTORS 60
                END DO
        ! END OF CYCLE ON  ROWS
            END DO
        ! LAST CELL OF CARTOGRAM
            J3=IPGEOM/30
            IF(IPGEOM.EQ.180) J3=4
          IF(NR.EQ.2) THEN
                DO I=1,6
                    KS(I,1)=KN2(I,J3)
              END DO
          ENDIF
            DO I=1,6
                KN(I,NC)=NC+KN1(I,J3)
                KS(I,NC)=KS1(I,J3)
          END DO
          FORMI(NC)=IPGEOM/360.
          GO TO 20
        ELSE
        ! ---- CALCULATION KN AND KS, FORMI (ROTATION  SYMMETRY) --------------
            N=1
        !CCC	X0=H*NR
          L2=(2-NR)*IGEO
        ! CYCLE ON  ROWS
            DO I=NR,2,-1
                L1=L2
                L2=(I-1)*IGEO
                K1=1
                K2=2
                K3=3
                K4=4
                K5=5
                K6=6
        !CCC      X0=X0-H
        ! FIRST CELL OF FIRST ROW
                KN(1,N)=L2+N-1
                KN(5,N)=N+1
                KN(6,N)=L2+N
                KS(1,N)=NK(4+IGEO)
        ! FIRST CELL OF  ROW
                IF(I.NE.NR)    THEN
                    KN(2,N)=N-1
                    KN(3,N)=N-L1
                    KN(4,N)=N-L1+1
                    KS(2,N)=NK(5+IGEO)
              ELSE
        ! FOR THE CELLS ON OUTER BOUNDARY OF ASSEMBLY ONLY (FIRST ROW)
                    DO K=2,4
                      KN(K,N)=L2+1
                      KS(K,N)=-3
                  END DO
                  KS(3,1)=3
                  FORMI(1)=0.5
                  IF(IGEO.EQ.6)FORMI(1)=0.33333333
                    FORMI(1)=0.33333333
                ENDIF
        ! CYCLE ON  SECTORS
                DO J=1,IGEO
        !C CYCLE ON  CELLS IN SECTOR
                    DO J1=1,I-2
                        N=N+1
                        KN(NK(K1),N)=N+L2-J
                        KN(NK(K2),N)=N-1
                        KN(NK(K3),N)=N-L1+J-1
                        KN(NK(K4),N)=N-L1+J
                        KN(NK(K5),N)=N+1
                        KN(NK(K6),N)=N+L2-J+1
                      IF(I.EQ.NR)  THEN
                          KN(NK(K3),N)=KN(NK(K1),N)
                          KN(NK(K4),N)=KN(NK(K6),N)
                          KS(NK(K3),N)=-KS(NK(K1),N)
                            KS(NK(K4),N)=-KS(NK(K6),N)
                          FORMI(N)=0.5
                      ENDIF
                    END DO
                    IF(J.EQ.IGEO)     THEN
        ! LAST CELL OF ROW
                        KS(NK(4+IGEO),N)=1
                        IF(I.NE.2)KS(NK(5+IGEO),N)=2
                        KN(NK(4+IGEO),N)=N-L2+1
                        KN(NK(5+IGEO),N)=N+1
                      IF(I.EQ.NR)  THEN
        ! FIRST ROW
                            KS(NK(3+IGEO),N)=-KS(NK(K6),N)
                            KN(NK(3+IGEO),N)=KN(NK(K6),N)
                        ENDIF
                        N=N+1
                    ELSE
        ! FIRST CELL OF  OTHER SECTOR OF ROW (ANGLE)
                        K5=K5+1
                        K6=K6-1
                        N=N+1
                        KN(NK(K1),N)=N+L2-J
                        KN(NK(K2),N)=N-1
                        KN(NK(K3),N)=N-L1+J-1
                        KN(NK(K4),N)=N-L1+J
                        KN(NK(K5),N)=N+1
                        KN(NK(K6),N)=N-L1+J+1
                      IF(I.EQ.NR)  THEN
        ! FIRST ROW
                            DO K=1,3
                              KN(NK(K2+K),N)=KN(NK(K1),N)
                              KS(NK(K2+K),N)=-KS(NK(K1),N)
                          END DO
                          KS(NK(K2+2),N)=KS(NK(K1),N)
                          FORMI(N)=0.33333333
                      ENDIF
                    ENDIF
                    K1=K1+1
                    K2=K2+1
                    K3=K3+1
                    K4=K4+1
                    K6=K6+2
        ! END OF CYCLE ON  SECTORS
                END DO
        ! END OF CYCLE ON  ROWS
            END DO
        ! LAST CELL OF CARTOGRAM
            IGEO1=6/IGEO
            I1=8
            I2=3
          FORMI(NC)=1./FLOAT(IGEO1)
            DO J=1,IGEO1
                DO I=1,IGEO
                    KN(NK(I1),NC)=NC-I
                    KS(NK(I2),NC)=NK(5+I)
                    I1=I1-1
                    I2=I2+1
                END DO
            END DO
        ENDIF
        ! TAKING INTO ACCOUNT FREE ESCAPE ON OUTER BOUNDARY. KN=0
    !    20 IF(IPGEOM.GT.0) GO TO 19
    !D
        20 if(ipgeom.gt.0) return
    !D
        IPG=IPGEOM/(-60) !ROTATION
        22 IPGE=NR-2
        GO TO 21
        19 IPG=IPGEOM/60+1
        IF(IPGEOM.NE.30) GO TO 22
        IPGE=(NR+1)/2-1
        21 I0=0
        I1=2
        DO 24 I=1,IPG ! LOOP ON SECTORS OF ASSEMBLY
            I0=I0+1
          KN(I1,I0)=0
          I2=I1+1
          IF(I2.GT.6) I2=I2-6
          KN(I2,I0)=0
          I3=I2+1
          IF(I3.GT.6) I3=I3-6
          KN(I3,I0)=0
          DO J=1,IPGE ! LOOP ON TVS IN FIRST ROW OF SECTOR I
              I0=I0+1
              KN(I2,I0)=0
              KN(I3,I0)=0
          END DO
          I1=I2
          IF(IPGEOM.LE.30.OR.I.NE.IPG-1) GO TO 24
          IF(IPGEOM.EQ.90) THEN
              IPGE=(NR+1)/2-1
          ELSE
              IPGE=0
          END IF
        24 CONTINUE
        ! ÏÅ×ÀÒÜ ÂÐÅÌÅÍÍÀß
    !    if(ipgeom.gt.0)then
    !    !      WRITE(20,*)' Çåðêàëüíàÿ ñèììåòðèÿ: íà ãðàíèöå ðåøåòêè è âíóòðè'
    !    !     *           ,' - ñîñåäíèå ÿ÷åéêè'
    !    else
    !    !      write(20,*)' Ïîâîðîòíàÿ ñèììåòðèÿ: íà ãðàíèöå ðåøåòêè è âíóòðè'
    !    !     *           ,' - ñîñåäíèå ÿ÷åéêè'
    !    end if
    !    !	write(20,*)' N          kn'
    !    do j=1,nc
    !    !      write(20,100)j,(kn(i,j),i=1,6)
    !    end do
    !        !     write(20,*)' N          ks'
    !    do j=1,nc
    !    !	write(20,100)j,(ks(i,j),i=1,6)
    !    end do
    !    !	write(20,*)' N    formi'
    !    do j=1,nc
    !    !	write(20,101)j,formi(j)
    !    end do
        !      RETURN
        !	stop
        !kn11=kn
        !ks11=ks
        !do k=1,lk
        !    formi1(k)=formi(k)
        !end do
    !    where (KS < 0) KS = 0
        return
    END subroutine Wscale

    subroutine AddNeighboursGap(assembly_type, nrow, sym, nCellTypes, ncells, neighCell, neighSide, fact, kart)
      use Config, only : str_len
      use ReadWriteMod, only : get_cell_tot_number
      implicit none

      character(str_len), intent(in) :: assembly_type
      integer, intent(in)    :: nrow
      integer, intent(in)    :: sym
      integer, intent(in)    :: nCellTypes
      integer, intent(inout) :: ncells
      integer, allocatable, intent(inout) :: neighCell(:,:)
      integer, allocatable, intent(inout) :: neighSide(:,:)
      real(rp), allocatable, intent(inout) :: fact(:)
      integer, allocatable, intent(inout) :: kart(:)

      integer, allocatable :: kart_temp(:), neighCell_temp(:,:), neighSide_temp(:,:)
      integer :: ncells_old, iside_cell, icell1
      integer :: i, icell, icorner, iside, n_cells_on_side, s

      allocate(kart_temp(ncells))
      allocate(neighCell_temp(6,ncells))
      allocate(neighSide_temp(6,ncells))

      kart_temp(:) = kart(:)
      neighCell_temp(:,:) = neighCell(:,:)
      neighSide_temp(:,:) = neighSide(:,:)
      ncells_old = ncells

      deallocate(kart)
      deallocate(neighCell)
      deallocate(neighSide)
      deallocate(fact)

      if (assembly_type == "regular hexagonal with the gap") then
        if (sym == 360 .or. sym == -360) then
          ncells = get_cell_tot_number(assembly_type, nrow + 1, 0, sym)
          allocate(kart(ncells))
          allocate(neighCell(6,ncells))
          allocate(neighSide(6,ncells))
          allocate(fact(ncells))
          if (sym == 360) then
            call GetNeighbours(ncells, assembly_type, -sym, nrow + 1, 0, neighCell, neighSide, fact)
          else
            call GetNeighbours(ncells, assembly_type, sym, nrow + 1, 0, neighCell, neighSide, fact)
          end if
          n_cells_on_side = nrow
          icell = 1
          icorner = 6 * nrow + 1
          do iside = 1, 6
            do i = 1, n_cells_on_side
              if (i == 1) then
                kart(icell) = nCellTypes - 1
                neighCell(1,icell) = icorner
                if (icell == 1) then
                  neighCell(2,icell) = 6 * nrow
                else
                  neighCell(2,icell) = icell - 1
                end if
                neighCell(3,icell) = 0
                neighCell(4,icell) = 0
                neighCell(5,icell) = icell + 1
                neighCell(6,icell) = -1000 ! Just to be sure that in the further algorithm this side will not be used since corner cell has only 5 sides
                do s = 1, 6
                  if (neighCell(s,icorner) == icell) exit
                end do
                neighSide(s,icorner) = 1
                neighSide(1,icell)   = s
                neighSide(2,icell)   = 2
                neighSide(3,icell)   = 0
                neighSide(4,icell)   = 0
                neighSide(5,icell)   = 5
                iside_cell = icorner + 1
                icorner = icorner + nrow - 1
              else
                kart(icell) = nCellTypes
                neighCell(1,icell) = 0
                if (icell == 6 * nrow) then
                  neighCell(2,icell) = 1
                  neighCell(3,icell) = 6 * nrow + 1
                else
                  neighCell(2,icell) = icell + 1
                  neighCell(3,icell) = iside_cell
                end if
                neighCell(4,icell) = iside_cell - 1
                neighCell(5,icell) = icell - 1
                neighCell(6,icell) = -1000
                neighSide(1,icell) = 0
                if (i == n_cells_on_side) then
                  neighSide(2,icell) = 2
                else
                  neighSide(2,icell) = 5
                end if

                icell1 = neighCell(3,icell)
                do s = 1, 6
                  if (neighCell(s,icell1) == icell) exit
                end do
                neighSide(s,icell1) = 3
                neighSide(3,icell) = s
                icell1 = neighCell(4,icell)
                do s = 1, 6
                  if (neighCell(s,icell1) == icell) exit
                end do
                neighSide(s,icell1) = 4
                neighSide(4,icell) = s

                if (i == 2) then
                  neighSide(5,icell) = 5
                else
                  neighSide(5,icell) = 2
                end if
                iside_cell = iside_cell + 1
              end if
              neighSide(6,icell) = -1000
              icell = icell + 1
            end do
          end do
          kart(n_cells_on_side * 6 + 1 : ncells) = kart_temp(:)
          fact(:) = 1.0_rp
          if (sym == 360) then
            do icell = 1, ncells
              do s = 1, 6
                if (neighCell(s,icell) == 0) neighCell(s,icell) = icell
                if (neighSide(s,icell) == 0) neighSide(s,icell) = -s
              end do
            end do
          end if
        else
          write(*,*) "This symmetry is currently not supported"
          stop
        end if
      else
        write(*,*) "This assembly type is currently not supported."
        stop
      end if

      return

    end subroutine

end module
