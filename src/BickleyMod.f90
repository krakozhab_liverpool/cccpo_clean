module BickleyMod
  use precision_mod

  real :: bickley(100000)

  contains

  subroutine InitBickley
    implicit none

    integer :: i
    real :: al

    open(10, file = 'bickley.txt')
    do i = 1, 100000
      read(10, *) al, bickley(i)
    end do
    close(10)

  end subroutine InitBickley

  real function Ki3_r(tau)
    implicit none
    real :: tau

    real :: w1
    integer n1, n2

    if (tau >= 10.0) then
        Ki3_r = 0.0
        return
    end if

    n1 = int(tau/1e-4)
    n2 = n1 + 1

    w1 = tau/1e-4 - n1
    Ki3_r = bickley(n1) + w1*(bickley(n2)-bickley(n1))

  end function Ki3_r

  real(rp) recursive function Kin(n, x) result(resKin)
    implicit none
    real(rp), parameter :: pi = 3.14159265358979_rp
    integer :: n
    real(rp) :: x

    integer :: i, imin

    real(rp) :: x2
    real(rp) :: ak1, ak2, ak3
    real(rp) :: b0, b1, b2
    real(rp) :: a0, a1, a2
    real(rp) :: u
    real(rp) :: f1
    real(rp) :: kkin

    real(rp) :: A(27)
    real(rp) :: B(45)
    real(rp) :: C(3)
    real(rp) :: D(45)
    real(rp) :: E(57)

    integer :: inda(20)
    integer :: indb(25)

    data A /10.584648_rp,  6.545470_rp, 1.976457_rp,    0.333496_rp, 0.035407_rp, 0.002568_rp, 0.000135_rp, &
             0.000005_rp, 10.762666_rp, 5.623335_rp,    1.435437_rp, 0.212504_rp, 0.020365_rp, 0.001360_rp, &
             0.000067_rp,  0.000003_rp, 0.0004364_rp, -0.0003873_rp, 0.000274_rp,-0.0001581_rp, 0.0000763_rp, &
            -0.0000315_rp, 0.0000113_rp, -0.0000036_rp, 0.0000010_rp, -0.0000003_rp, 0.0000001_rp/

    data B /1.4534664_rp, -0.2436620_rp, 0.0258465_rp, -0.0029653_rp, 0.0005322_rp, -0.0001499_rp, &
            0.0000560_rp, -0.0000249_rp, 0.0000125_rp, -0.0000068_rp, 0.0000040_rp, -0.0000025_rp, &
            0.0000017_rp, -0.0000012_rp, 0.0000009_rp, -0.0000008_rp, 0.0000007_rp,  0.3039967_rp, &
           -0.2136079_rp,  0.0961280_rp,-0.0324165_rp,  0.0091054_rp,-0.0023228_rp,  0.0005813_rp, &
           -0.0001516_rp,  0.0000426_rp,-0.0000129_rp,  0.0000042_rp,-0.0000014_rp,  0.0000005_rp, &
           -0.0000002_rp,  0.0000001_rp, 0.0031373_rp, -0.0028564_rp, 0.0021670_rp, -0.0013874_rp, &
            0.0007615_rp, -0.0003642_rp, 0.0001541_rp, -0.0000585_rp, 0.0000202_rp, -0.0000064_rp, &
            0.0000019_rp, -0.0000005_rp, 0.0000001_rp/

    data C /0.4227843_rp, 0.0534107_rp, 0.08333333_rp/

    data D /0.7853961_rp, -0.9990226_rp, 0.7266088_rp, 0.7852024_rp, -0.9912340_rp, 0.6466375_rp, &
            0.7845986_rp, -0.9791293_rp, 0.5856605_rp, 0.7834577_rp, -0.9638914_rp, 0.5346648_rp, &
            0.7817094_rp, -0.9463843_rp, 0.4907827_rp, 0.7793031_rp, -0.9271152_rp, 0.4521752_rp, &
            0.7762107_rp, -0.9064822_rp, 0.4177388_rp, 0.7724519_rp, -0.8849865_rp, 0.3869945_rp, &
            0.7679903_rp, -0.8626685_rp, 0.3590753_rp, 0.7628988_rp, -0.8400133_rp, 0.3338676_rp, &
            0.7540982_rp, -0.8054172_rp, 0.2998569_rp, 0.7401279_rp, -0.7587821_rp, 0.2609154_rp, &
            0.7239594_rp, -0.7125290_rp, 0.2278226_rp, 0.7058777_rp, -0.6672761_rp, 0.1994999_rp, &
            0.6861762_rp, -0.6234536_rp, 0.1751248_rp/

    data E /0.7247294_rp, -0.7538355_rp, 0.3203223_rp, -5.337485E-2_rp, 0.6663720_rp, -0.6279752_rp, &
            0.2295280_rp, -3.146833E-2_rp, 0.5956163_rp, -0.5094124_rp, 0.1631667_rp, -1.906198E-2_rp, &
            0.5191031_rp, -0.4046007_rp, 0.1152418_rp, -1.174752E-2_rp, 0.4425954_rp, -0.3159648_rp, &
            8.097913E-2_rp, -7.328415E-3_rp, 0.3703178_rp, -0.2434341_rp, 5.669960E-2_rp, -4.617254E-3_rp, &
            0.1684022_rp, -7.158569E-2_rp, 7.923547E-3_rp,  0.1278307_rp,-5.016344E-2_rp,  5.095111E-3_rp, &
            9.611422E-2_rp, -3.501524E-2_rp, 3.286040E-3_rp, 7.170491E-2_rp, -2.437465E-2_rp, 2.126242E-3_rp, &
            4.616317E-2_rp, -1.425519E-2_rp, 1.123687E-3_rp, 2.475115E-2_rp, -6.810124E-3_rp, 4.762937E-4_rp, &
            1.302864E-2_rp, -3.232035E-3_rp, 2.031843E-4_rp, 6.749972E-3_rp, -1.524126E-3_rp, 8.701440E-5_rp, &
            3.454768E-3_rp, -7.157367E-4_rp, 3.742673E-5_rp, 1.749779E-3_rp, -3.349194E-4_rp, 1.615898E-5_rp, &
            7.522133E-4_rp, -1.314173E-4_rp, 5.777582E-6_rp/

    data inda /3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 33, 36, 36, 39, 39, 42, 42, 45, 45/

    data indb /4, 8,12, 16, 20, 24, 27, 30, 33, 36, 39, 39, 42, 42, 45, 45, 48, 48, 51, 51, &
               54, 54, 57, 57, 57/

    if (n < 1) then
        write(*, *) 'Function Kin: n must be > 0'
        stop
    end if
    if (x < -1e-10) then
        write(*, *) 'Function Kin: x must be >= 0, while x = ', x
        stop
    end if

    if (n == 1) then
        if (x <= 0.0) then
            resKin = pi/2.
        elseif (x < 0.04) then
            x2 = x*x
            resKin = pi/2. - x*((x2*C(2)+C(1)) - log(0.5*x)*(x2*C(3)+1.0))
        elseif (x < 6.0) then
            u = 0.11111111*x*x - 2.0
            i = 8
            imin = 1
            a0 = A(i)
            a1 = 0.0
            a2 = 0.0
            do while (i > imin)
                i = i - 1
                a2 = a1
                a1 = a0
                a0 = u*a1 - a2 + A(i)
            end do
            f1 = 0.5*(a0 - a2)
            i = 16
            imin = 9
            a0 = A(i)
            a1 = 0.0
            a2 = 0.0
            do while (i > imin)
                i = i - 1
                a2 = a1
                a1 = a0
                a0 = u*a1 - a2 + A(i)
            end do
            kkin = 0.5*(a0 - a2)
            resKin = pi/2. - x*(f1 - kkin*log(0.5*x))
        elseif (x < 15.0) then
            u = 0.44444444*x - 4.6666667
            i = 27
            imin = 17
            a0 = A(i)
            a1 = 0.0
            a2 = 0.0
            do while (i > imin)
                i = i - 1
                a2 = a1
                a1 = a0
                a0 = u*a1 - a2 + A(i)
            end do
            resKin = 0.5*(a0 - a2)
        else
            resKin = 0.0
        end if
    elseif (n == 2) then
        if (x <= 0.0) then
            resKin = 1.0
            return
        elseif (x < 0.5) then
            u = 8.0*x - 2.0
            i = 17
            imin = 1
        elseif (x < 4.0) then
            u = 1.1428571*x - 2.5714286
            i = 32
            imin = 18
        elseif (x < 15.0) then
            u = 0.36363636*x - 3.4545455
            i = 45
            imin = 33
        else
            resKin = 0.0
            return
        end if
        b0 = B(i)
        b1 = 0.0
        b2 = 0.0
        do while (i > imin)
            i = i - 1
            b2 = b1
            b1 = b0
            b0 = u*b1 - b2 + B(i)
        end do
        resKin = 0.5*(b0-b2)
    elseif (n == 3) then
        if (x <= 0.0) then
            resKin = pi/4.
        elseif (x < 1.0) then
            i = inda(int(20.0*x)+1)
            resKin = x*(x*D(i)+D(i-1))+D(i-2)
        elseif (x < 3.4) then
            i = indb(int(2.5*(x-1.0))+1)
            resKin = x*(x*(x*E(i)+E(i-1))+E(i-2))+E(i-3)
        elseif (x < 11.0) then
            i = indb(int(2.5*(x-1.0))+1)
            resKin = x*(x*E(i)+E(i-1))+E(i-2)
        else
            resKin = 0.0
        end if
    else
        ak1 = Kin(n-1, x)
        ak2 = Kin(n-2, x)
        ak3 = Kin(n-3, x)
        resKin = (ak3-ak1)*x/(n-1) + (n-2)*ak2/(n-1)
    end if

  end function Kin

  real(rp) function Ki3(x)
    implicit none

    real(rp) :: x

    Ki3 = Kin(3, x)

  end function Ki3

  real(rp) function Ki2(x)
    implicit none
    real(rp) :: x

    Ki2 = Kin(2, x)

  end function Ki2

  real(rp) function Ki1(x)
    implicit none
    real(rp) :: x

    Ki1 = Kin(1, x)

  end function Ki1

end module
