module DrawMod
  use precision_mod

  use Config, only : str_len

  real(rp) :: svgImageFactorX
  real(rp) :: svgImageFactorY

  character(str_len) :: colours(12)

  contains

  subroutine DrawAssembly(output_file, assembly_type, sym, nrow, ncol, kart, neighCell,    &
                          neighSide, nzones, nsides, cell_vertx, cell_verty, cell_rad, &
                          cell_pitch, assmb_pitch)
    use ConstantsMod, only : PI
    use Config, only : str_len, viewBox_centerX, viewBox_centerY
    use GlobalAssemblyMod, only : verbosity
    implicit none

    character(len=str_len), intent(in) :: output_file
    character(len=str_len), intent(in) :: assembly_type
    integer, intent(in) :: nrow
    integer, intent(in) :: ncol
    integer, intent(in) :: sym
    integer, intent(in) :: kart(:)
    integer, intent(in) :: neighCell(:,:)
    integer, intent(in) :: neighSide(:,:)
    integer, intent(in) :: nzones(:)
    integer, intent(in) :: nsides(:)
    real(rp), intent(in) :: cell_vertx(:,:)
    real(rp), intent(in) :: cell_verty(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_pitch
    real(rp), intent(in), optional :: assmb_pitch

    character(str_len) :: clip_path_id
    real(rp), allocatable :: assmb_vertx(:), assmb_verty(:)
    real(rp) :: assmb_rad, pitch, ap
!    integer, allocatable :: kart1(:)

    if (verbosity > 0) then
      write(*,*) "Drawing assembly..."
      write(*,*) "Initialising .svg file."
    end if
    call InitializeSVG(output_file)

    colours(1) = "lightbrown"
    colours(2) = "lightgray"
    colours(3) = "orange"
    colours(4) = "red"
    colours(5) = "lightviolet"
    colours(6) = "lightblue"
    colours(7) = "darkbrown"
    colours(8) = "yellow"
    colours(9) = "green"
    colours(10) = "blue"
    colours(11) = "indigo"
    colours(12) = "violet"

    if (verbosity > 0) then
      write(*,*) "Drawing cells in the assembly"
    end if
    if (sym == -360) then ! Rotational symmetry
      if (assembly_type == "regular hexagonal") then
        allocate(assmb_vertx(6))
        allocate(assmb_verty(6))
        assmb_rad = (nrow - 0.5_rp)*cell_pitch + (cell_pitch/6.0_rp)
        assmb_vertx(1) = assmb_rad
        assmb_vertx(2) = 0.5_rp * assmb_rad
        assmb_vertx(3) = -0.5_rp * assmb_rad
        assmb_vertx(4) = -assmb_rad
        assmb_vertx(5) = -0.5_rp*assmb_rad
        assmb_vertx(6) = 0.5_rp*assmb_rad
        ! Getting resolution
        svgImageFactorX = getImageFactorX(assmb_vertx)
        svgImageFactorY = svgImageFactorX
        call TransformImageSvg(output_file, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY)
        call DrawHexAssembly360Rot(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                   nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
        return
      elseif (assembly_type == "regular hexagonal with the gap") then
        call DrawHexAssemblyWithGap360(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                       cell_vertx, cell_verty, cell_rad, cell_pitch, assmb_pitch)
      else
        write(*,*) "Current assembly type ", assembly_type, " is not supported at the moment for assembly drawing."
        call FinalizeSVG(output_file)
        return
      end if
    elseif (sym == 360) then
      if (assembly_type == "regular hexagonal") then
        allocate(assmb_vertx(6))
        allocate(assmb_verty(6))
        assmb_vertx = 0.0_rp
        assmb_verty = 0.0_rp
        assmb_rad = (nrow - 1.0_rp)*cell_pitch
        pitch = sqrt(3.0_rp)*assmb_rad

        assmb_vertx(1) = assmb_rad
        assmb_vertx(2) = 0.5_rp * assmb_rad
        assmb_vertx(3) = -0.5_rp * assmb_rad
        assmb_vertx(4) = -assmb_rad
        assmb_vertx(5) = -0.5_rp*assmb_rad
        assmb_vertx(6) = 0.5_rp*assmb_rad

        assmb_verty(1) = 0.0_rp
        assmb_verty(2) = 0.5_rp*pitch
        assmb_verty(3) = 0.5_rp*pitch
        assmb_verty(4) = 0.0_rp
        assmb_verty(5) = -0.5_rp*pitch
        assmb_verty(6) = -0.5_rp*pitch
        ! Getting resolution
        svgImageFactorX = getImageFactorX(assmb_vertx)
        svgImageFactorY = svgImageFactorX
        clip_path_id = "asssmbBorder"
        call TransformImageSvg(output_file, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY,clip_path_id)
        call AddClipPathPolygon(output_file, 6, assmb_vertx, assmb_verty, clip_path_id)
        call DrawHexAssembly360Refl(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                    nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
      elseif (assembly_type == "regular hexagonal with the gap") then
        call DrawHexAssemblyWithGap360(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                       cell_vertx, cell_verty, cell_rad, cell_pitch, assmb_pitch)
      elseif (assembly_type == "regular square") then
        call DrawSquareAssembly360(output_file, nrow, ncol, kart, neighCell, neighSide, &
                                   nzones, cell_vertx, cell_verty, cell_rad, cell_pitch)
      end if
    elseif (sym == 30) then
      if (assembly_type == "regular hexagonal") then
        allocate(assmb_vertx(3))
        allocate(assmb_verty(3))
        assmb_vertx = 0.0_rp
        assmb_verty = 0.0_rp
        assmb_vertx(1) = (nrow - 1) * cell_pitch
        assmb_verty(1) = 0.0_rp
        assmb_vertx(2) = assmb_vertx(1) * cos(PI/6.0_rp) * cos(PI/6.0_rp)
        assmb_verty(2) = assmb_vertx(1) * cos(PI/6.0_rp) * sin(PI/6.0_rp)
        assmb_vertx(3) = 0.0_rp
        assmb_vertx(3) = 0.0_rp
        svgImageFactorX = getImageFactorX(assmb_vertx)
        svgImageFactorY = svgImageFactorX
        clip_path_id = "asssmbBorder"
        call TransformImageSvg(output_file, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY,clip_path_id)
        call AddClipPathPolygon(output_file, 3, assmb_vertx, assmb_verty, clip_path_id)
        call DrawHexAssembly30Refl(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                    nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
      end if
    else
      write(*,*) "Current symmetry ", sym, " is not supported at the moment for assembly drawing."
      call FinalizeSVG(output_file)
      return
    end if

    if (verbosity > 0) then
      write(*,*) "Finalising .svg file"
    end if
    call FinalizeSVG(output_file)

    return

  end subroutine DrawAssembly

  subroutine DrawIndividualCell(icell, cell_vertx, cell_verty, nsides, nzones, ncirc, cell_mat, &
                                cell_rad, cell_segboundx, cell_segboundy, cell_nsegments)
    use precision_mod
    use Config, only : str_len, viewBox_centerX, viewBox_centerY
    use ReadWriteMod, only : intToChar
    !---------------------------------------
    ! Draw individual cells
    !---------------------------------------
    implicit none

    integer,  intent(in) :: icell
    real(rp), intent(in) :: cell_vertx(:, :)
    real(rp), intent(in) :: cell_verty(:, :)
    integer,  intent(in) :: nsides(:)
    integer,  intent(in) :: nzones(:)
    integer,  intent(in) :: ncirc(:)
    integer,  intent(in) :: cell_mat(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_segboundx(:,:,:)
    real(rp), intent(in) :: cell_segboundy(:,:,:)
    integer,  intent(in) :: cell_nsegments(:,:)

    character(str_len) :: clr
    character(str_len) :: nameOfSVG
    integer :: i, j

    nameOfSVG = "ImageCell" // trim(intToChar(icell)) // ".svg"
    call InitializeSVG(nameOfSVG)
    svgImageFactorX = max(getImageFactorX(cell_vertx(icell,1:nsides(icell))),getImageFactorY(cell_verty(icell,1:nsides(icell))))
    svgImageFactorY = svgImageFactorX
    call TransformImageSvg(nameOfSVG, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY)
    ! Draw outer polygon
    if (cell_mat(icell,nzones(icell)) <= 12) then
      clr = colours(cell_mat(icell,nzones(icell)))
    else
      clr = "random_colour"
    end if
    call DrawPolygon(nameOfSVG, nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), 1.0_rp, 1.0_rp, "black", clr)
    ! Draw circles
    do i = ncirc(icell), 1, -1
      if (cell_mat(icell,i) <= 12) then
        clr = colours(cell_mat(icell,i))
      else
        clr = "random_colour"
      end if
      call DrawCircle(nameOfSVG, 0.0_rp, 0.0_rp, cell_rad(icell,i), 1.0_rp, 1.0_rp, "black", clr)
    end do
    ! Draw segment's boundaries
    do i = 1, nsides(icell)
      call DrawPoint(nameOfSVG, cell_vertx(icell,i), cell_verty(icell,i), 3.0_rp, "black")
      do j = 2, cell_nsegments(icell,i)
        call DrawPoint(nameOfSVG, cell_segboundx(icell,i,j), cell_segboundy(icell,i,j), 2.0_rp,"red")
      end do
    end do
    call FinalizeSVG(nameOfSVG)
  end subroutine

  subroutine DrawSquareAssembly360(svg_file, nrow, ncol, kart, neighCell, neighSide, nzones, &
                                   cell_vertx, cell_verty, cell_rad, cell_pitch)
    use ConstantsMod, only : PI
    use Config, only : str_len, viewBox_centerX, viewBox_centerY
    use ReadWriteMod, only : get_cell_tot_number
    use GlobalAssemblyMod, only : nCellTypes, assembly_type, cell_mat
    implicit none

    character(str_len), intent(in) :: svg_file
    integer, intent(in) :: nrow
    integer, intent(in) :: ncol
    integer, intent(in) :: kart(:)
    integer, intent(in) :: neighCell(:,:)
    integer, intent(in) :: neighSide(:,:)
    integer, intent(in) :: nzones(:)
    real(rp), intent(in) :: cell_vertx(:,:)
    real(rp), intent(in) :: cell_verty(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_pitch

    character(str_len) :: assmb_type
    integer :: icell, ncell, itype, iside, icell_neigh
    logical, allocatable :: isDrawn(:)

    real(rp) :: x0, y0
    real(rp) :: vertx(4), verty(4)
    real(rp) :: vx(4), vy(4)

    real(rp) :: assmb_vertx(4), assmb_verty(4)
    character(str_len) :: clip_path_id

    assmb_vertx(1) = ncol * cell_pitch * 0.5_rp
    assmb_vertx(2) = ncol * cell_pitch * 0.5_rp
    assmb_vertx(3) = -ncol * cell_pitch * 0.5_rp
    assmb_vertx(4) = -ncol * cell_pitch * 0.5_rp

    assmb_verty(1) = -nrow * cell_pitch * 0.5_rp
    assmb_verty(2) = nrow * cell_pitch * 0.5_rp
    assmb_verty(3) = nrow * cell_pitch * 0.5_rp
    assmb_verty(4) = -nrow * cell_pitch * 0.5_rp
    ! Getting resolution
    svgImageFactorX = getImageFactorX(assmb_vertx)
    svgImageFactorY = svgImageFactorX
    clip_path_id = "asssmbBorder"
    call TransformImageSvg(svg_file, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY,clip_path_id)
    call AddClipPathPolygon(svg_file, 4, assmb_vertx, assmb_verty, clip_path_id)

    assmb_type = "regular square"
    ncell = size(kart)
    allocate(isDrawn(ncell))
    isDrawn = .false.

    vx(1:4) = (/1.0_rp, 0.0_rp, -1.0_rp, 0.0_rp/)
    vy(1:4) = (/0.0_rp, 1.0_rp, 0.0_rp, -1.0_rp/)
!    vx(1:4) = (/-1.0_rp, 0.0_rp, 1.0_rp, 0.0_rp/)
!    vy(1:4) = (/0.0_rp, -1.0_rp, 0.0_rp, 1.0_rp/)

    x0 = -nrow * cell_pitch * 0.5_rp + 0.5*cell_pitch
    y0 = ncol * cell_pitch * 0.5_rp - 0.5_rp * cell_pitch

    icell = 1
    do
      if (all(isDrawn)) exit
      if (.not. isDrawn(icell)) then
        itype = kart(icell)
        vertx = cell_vertx(1,:)
        verty = cell_verty(1,:)
        call ShiftCoords(4, vertx, verty, x0, y0)
        call DrawCell(svg_file, 4, vertx, verty, nzones(itype), cell_mat(itype,:), &
                      x0, y0, cell_rad(itype,:))
        isDrawn(icell) = .true.
      end if
      do iside = 1, 4
        if (neighSide(iside,icell) < 0) then
          cycle
        end if
        if (.not. isDrawn(neighCell(iside,icell))) then
          icell = neighCell(iside,icell)
          itype = kart(icell)
          vertx = cell_vertx(itype,:)
          verty = cell_verty(itype,:)
          x0 = x0 + vx(iside) * cell_pitch
          y0 = y0 + vy(iside) * cell_pitch
          call ShiftCoords(4, vertx, verty, x0, y0)
          call DrawCell(svg_file, 4, vertx, verty, nzones(itype), cell_mat(itype,:), &
                        x0, y0, cell_rad(itype,:))
          isDrawn(icell) = .true.
          exit
        end if
      end do
    end do

    return
  end subroutine DrawSquareAssembly360

  subroutine DrawHexAssembly360Rot(svg_file, nrow, kart, neighCell, neighSide, nzones, &
                                   nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
    use ConstantsMod, only : PI
    use Config, only : str_len
    use ReadWriteMod, only : get_cell_tot_number
    use GlobalAssemblyMod, only : cell_mat
    implicit none

    character(str_len), intent(in) :: svg_file
    integer, intent(in) :: nrow
    integer, intent(in) :: kart(:)
    integer, intent(in) :: neighCell(:,:)
    integer, intent(in) :: neighSide(:,:)
    integer, intent(in) :: nzones(:)
    integer, intent(in) :: nsides(:)
    real(rp), intent(in) :: cell_vertx(:,:)
    real(rp), intent(in) :: cell_verty(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_pitch

    character(str_len) :: assmb_type
    integer :: icell, ncell, itype, iside, icell_neigh
    logical, allocatable :: isDrawn(:)

    real(rp) :: x0, y0
    real(rp) :: vertx(6), verty(6)
    real(rp) :: vx(6), vy(6)

    vx(1:6) = (/-sin(PI/6.0_rp),  sin(PI/6.0_rp), 1.0_rp, sin(PI/6.0_rp), -sin(PI/6.0_rp), -1.0_rp/)
    vy(1:6) = (/-cos(PI/6.0_rp), -cos(PI/6.0_rp), 0.0_rp, cos(PI/6.0_rp),  cos(PI/6.0_rp),  0.0_rp/)

    assmb_type = "regular hexagonal"
    ncell = get_cell_tot_number(assmb_type, nrow, 0, -360)

    allocate(isDrawn(ncell))
    isDrawn = .false.

    x0 = (nrow - 1) * cell_pitch
    y0 = 0.0_rp
    icell = 1
    do while (.not. all(isDrawn))
      itype = kart(icell)
      vertx = cell_vertx(itype,:)
      verty = cell_verty(itype,:)
      call ShiftCoords(6, vertx, verty, x0, y0)
      call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), cell_mat(itype,:), x0, y0, cell_rad(itype,:))
      isDrawn(icell) = .true.
      do iside = 1, 6
        icell_neigh = neighCell(iside,icell)
        if (icell_neigh == 0) cycle
        if (isDrawn(icell_neigh)) then
          cycle
        else
          x0 = x0 + vx(iside)*cell_pitch
          y0 = y0 + vy(iside)*cell_pitch
          icell = icell_neigh
          exit
        end if
      end do
    end do

    return

  end subroutine

  subroutine DrawHexAssembly30Refl(svg_file, nrow, kart, neighCell, neighSide, nzones, &
                                    nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
    use ConstantsMod, only : PI
    use Config, only : str_len
    use ReadWriteMod, only : get_cell_tot_number
    use GlobalAssemblyMod, only : nCellTypes, assembly_type, cell_mat
    implicit none

    character(str_len), intent(in) :: svg_file
    integer, intent(in) :: nrow
    integer, intent(in) :: kart(:)
    integer, intent(in) :: neighCell(:,:)
    integer, intent(in) :: neighSide(:,:)
    integer, intent(in) :: nzones(:)
    integer, intent(in) :: nsides(:)
    real(rp), intent(in) :: cell_vertx(:,:)
    real(rp), intent(in) :: cell_verty(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_pitch

    character(str_len) :: assmb_type
    integer :: icell, ncell, itype, iside, icell_neigh
    logical, allocatable :: isDrawn(:)

    real(rp) :: x0, y0
    real(rp) :: vertx(6), verty(6)
    real(rp) :: vx(6), vy(6)
    integer :: irow, k

    assmb_type = "regular hexagonal"
    ncell = size(kart)
    allocate(isDrawn(ncell))
    isDrawn = .false.

    vx(1:6) = (/-sin(PI/6.0_rp),  sin(PI/6.0_rp), 1.0_rp, sin(PI/6.0_rp), -sin(PI/6.0_rp), -1.0_rp/)
    vy(1:6) = (/-cos(PI/6.0_rp), -cos(PI/6.0_rp), 0.0_rp, cos(PI/6.0_rp),  cos(PI/6.0_rp),  0.0_rp/)

    icell = 1
    do irow = nrow, 1, -1
      y0 = 0.0_rp
      x0 = (irow - 1) * cell_pitch
      do k = 1, nint(irow/2.0)
        if (.not. isDrawn(icell)) then
          itype = kart(icell)
          if (assembly_type == "regular hexagonal") then ! drawing regular cell
            vertx = cell_vertx(itype,:)
            verty = cell_verty(itype,:)
            call ShiftCoords(6, vertx, verty, x0, y0)
            call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), cell_mat(itype,:), &
                          x0, y0, cell_rad(itype,:))
          else
            write(*,*) "Drawing of the assembly with the gap is not supported for this symmetry."
            return
          end if
          isDrawn(icell) = .true.
          icell = icell + 1
          x0 = x0 + vx(5) * cell_pitch
          y0 = y0 + vy(5) * cell_pitch
        end if
      end do
    end do

  end subroutine DrawHexAssembly30Refl

  subroutine DrawHexAssembly360Refl(svg_file, nrow, kart, neighCell, neighSide, nzones, &
                                    nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
    use ConstantsMod, only : PI
    use Config, only : str_len
    use ReadWriteMod, only : get_cell_tot_number
    use GlobalAssemblyMod, only : nCellTypes, assembly_type, cell_mat
    implicit none

    character(str_len), intent(in) :: svg_file
    integer, intent(in) :: nrow
    integer, intent(in) :: kart(:)
    integer, intent(in) :: neighCell(:,:)
    integer, intent(in) :: neighSide(:,:)
    integer, intent(in) :: nzones(:)
    integer, intent(in) :: nsides(:)
    real(rp), intent(in) :: cell_vertx(:,:)
    real(rp), intent(in) :: cell_verty(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_pitch

    character(str_len) :: assmb_type
    integer :: icell, ncell, itype, iside, icell_neigh
    logical, allocatable :: isDrawn(:)

    real(rp) :: x0, y0
    real(rp) :: vertx(6), verty(6)
    real(rp) :: vx(6), vy(6)

    assmb_type = "regular hexagonal"
    !ncell = get_cell_tot_number(assmb_type, nrow, 0, 360)
    ncell = size(kart)
    allocate(isDrawn(ncell))
    isDrawn = .false.

    vx(1:6) = (/-sin(PI/6.0_rp),  sin(PI/6.0_rp), 1.0_rp, sin(PI/6.0_rp), -sin(PI/6.0_rp), -1.0_rp/)
    vy(1:6) = (/-cos(PI/6.0_rp), -cos(PI/6.0_rp), 0.0_rp, cos(PI/6.0_rp),  cos(PI/6.0_rp),  0.0_rp/)

    x0 = 0.0_rp
    y0 = 0.0_rp
    do icell = ncell, (nrow - 1) * 6 + 1, -1
      if (all(isDrawn)) exit
      if (.not. isDrawn(icell)) then
        itype = kart(icell)
        if (assembly_type == "regular hexagonal") then ! drawing regular cell
          vertx = cell_vertx(itype,:)
          verty = cell_verty(itype,:)
          call ShiftCoords(6, vertx, verty, x0, y0)
          call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), cell_mat(itype,:), &
                        x0, y0, cell_rad(itype,:))
        elseif (assembly_type == "regular hexagonal with the gap") then ! drawing gap cell if any
          if (itype <= nCellTypes - 2) then
            vertx = cell_vertx(itype,:)
            verty = cell_verty(itype,:)
            call ShiftCoords(6, vertx, verty, x0, y0)
            call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), cell_mat(itype,:), &
                          x0, y0, cell_rad(itype,:))
          else
            vertx = cell_vertx(1,:)
            verty = cell_verty(1,:)
            call ShiftCoords(6, vertx, verty, x0, y0)
            call DrawCell(svg_file, nsides(itype), vertx, verty, 1, cell_mat(itype,:), &
                          x0, y0, cell_rad(itype,:))
          end if
        end if
!        vertx = cell_vertx(itype,:)
!        verty = cell_verty(itype,:)
!        call ShiftCoords(6, vertx, verty, x0, y0)
!        call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), x0, y0, cell_rad(itype,:))
        isDrawn(icell) = .true.
      end if
      if (icell < ncell) then
        do iside = 1, 6
          if (neighCell(iside,icell+1) == icell) then
            x0 = x0 + vx(iside) * cell_pitch
            y0 = y0 + vy(iside) * cell_pitch
            exit
          end if
        end do
      end if
      do iside = 1, 6
        icell_neigh = neighCell(iside,icell)
        if (isDrawn(icell_neigh)) then
          cycle
        else
          itype = kart(icell_neigh)
          if (assembly_type == "regular hexagonal") then
            vertx = cell_vertx(itype,:)
            verty = cell_verty(itype,:)
            call ShiftCoords(6, vertx, verty, x0 + vx(iside)*cell_pitch, y0 + vy(iside)*cell_pitch)
            call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), cell_mat(itype,:), &
                          x0 + vx(iside)*cell_pitch, y0 + vy(iside)*cell_pitch, cell_rad(itype,:))
          elseif (assembly_type == "regular hexagonal with the gap") then
            if (itype <= nCellTypes - 2) then ! drawing regular cell
              vertx = cell_vertx(itype,:)
              verty = cell_verty(itype,:)
              call ShiftCoords(6, vertx, verty, x0 + vx(iside)*cell_pitch, y0 + vy(iside)*cell_pitch)
              call DrawCell(svg_file, nsides(itype), vertx, verty, nzones(itype), cell_mat(itype, :), &
                            x0 + vx(iside)*cell_pitch, y0 + vy(iside)*cell_pitch, cell_rad(itype,:))
            else ! drawing gap cell if any
              vertx = cell_vertx(1,:)
              verty = cell_verty(1,:)
              call ShiftCoords(6, vertx, verty, x0 + vx(iside)*cell_pitch, y0 + vy(iside)*cell_pitch)
              call DrawCell(svg_file, nsides(itype), vertx, verty, 1, cell_mat(itype, :), &
                            x0 + vx(iside)*cell_pitch, y0 + vy(iside)*cell_pitch, cell_rad(itype,:))
            end if
          end if
          isDrawn(icell_neigh) = .true.
        end if
      end do
    end do

  end subroutine

  subroutine ShiftCoords(nverts, vertx, verty, x0, y0)
    implicit none

    integer, intent(in)    :: nverts
    real(rp), intent(inout) :: vertx(:)
    real(rp), intent(inout) :: verty(:)
    real(rp), intent(in) :: x0
    real(rp), intent(in) :: y0

    integer :: ivert

    do ivert = 1, nverts
      vertx(ivert) = x0 + vertx(ivert)
      verty(ivert) = y0 + verty(ivert)
    end do

    return

  end subroutine

  subroutine DrawCell(svg_file, nvert, vertx, verty, nzone, cell_mat, x0, y0, radii)
    use Config, only : str_len
    use GlobalAssemblyMod, only : nMat

    character(str_len), intent(in) :: svg_file
    integer, intent(in) :: nvert
    real(rp), intent(in) :: vertx(:)
    real(rp), intent(in) :: verty(:)
    integer, intent(in) :: nzone
    integer, intent(in) :: cell_mat(:)
    real(rp), intent(in) :: x0
    real(rp), intent(in) :: y0
    real(rp), intent(in) :: radii(:)

    integer :: icirc
    character(str_len) :: clr

    if (cell_mat(nzone) <= 12) then
      clr = colours(cell_mat(nzone))
    else
      clr = "random_colour"
    end if

    call DrawPolygon(svg_file, nvert, vertx, verty, 1.0_rp, 1.0_rp, "black", clr)

    do icirc = nzone - 1, 1, -1
      if (cell_mat(nzone) <= 12) then
        clr = colours(cell_mat(icirc))
      else
        clr = "random_colour"
      end if
      call DrawCircle(svg_file, x0, y0, radii(icirc), 1.0_rp, 1.0_rp, "black", clr)
    end do

  end subroutine

  subroutine DrawHexAssemblyWithGap360(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                       cell_vertx, cell_verty, cell_rad,   &
                                       cell_pitch, assmb_pitch)
    use Config, only : str_len, viewBox_centerX, viewBox_centerY
    implicit none

    character(str_len), intent(in) :: output_file
    integer, intent(in) :: nrow
    integer, intent(in) :: kart(:)
    integer, intent(in) :: neighCell(:,:)
    integer, intent(in) :: neighSide(:,:)
    integer, intent(in) :: nzones(:)
    real(rp), intent(in) :: cell_vertx(:,:)
    real(rp), intent(in) :: cell_verty(:,:)
    real(rp), intent(in) :: cell_rad(:,:)
    real(rp), intent(in) :: cell_pitch
    real(rp), intent(in) :: assmb_pitch

    real(rp) :: assmb_rad
    real(rp) :: assmb_vertx(6), assmb_verty(6)
    character(str_len) :: clip_path_id
    integer, allocatable :: nsides(:)
    integer :: n

    assmb_rad = assmb_pitch / sqrt(3.0)

    assmb_vertx(1) = assmb_rad
    assmb_vertx(2) = 0.5_rp * assmb_rad
    assmb_vertx(3) = -0.5_rp * assmb_rad
    assmb_vertx(4) = -assmb_rad
    assmb_vertx(5) = -0.5_rp*assmb_rad
    assmb_vertx(6) = 0.5_rp*assmb_rad

    assmb_verty(1) = 0.0_rp
    assmb_verty(2) = 0.5_rp*assmb_pitch
    assmb_verty(3) = 0.5_rp*assmb_pitch
    assmb_verty(4) = 0.0_rp
    assmb_verty(5) = -0.5_rp*assmb_pitch
    assmb_verty(6) = -0.5_rp*assmb_pitch
    ! Getting resolution
    svgImageFactorX = getImageFactorX(assmb_vertx)
    svgImageFactorY = svgImageFactorX
    clip_path_id = "asssmbBorder"
    call TransformImageSvg(output_file, viewBox_centerX, viewBox_centerY, svgImageFactorX, svgImageFactorY,clip_path_id)
    call AddClipPathPolygon(output_file, 6, assmb_vertx, assmb_verty, clip_path_id)
    n = size(kart)
    allocate(nsides(size(kart)))
    nsides = 6

    call DrawHexAssembly360Refl(output_file, nrow, kart, neighCell, neighSide, nzones, &
                                nsides, cell_vertx, cell_verty, cell_rad, cell_pitch)
    return

  end subroutine

  subroutine InitializeSVG(nameOfFile)
    !--------------------------------------
    ! Initialize SVG file for the following
    ! drawing of the figures in this file
    !
    ! Input:
    !   nameOfFile - name of the file
    !--------------------------------------
    use Config, only : str_len
    use SVGMod, only : SVG_Initialize

    implicit none

    character(str_len) :: nameOfFile

    call SVG_Initialize(nameOfFile)

    return

  end subroutine InitializeSVG

  subroutine FinalizeSVG(nameOfFile)
    !---------------------------------------
    ! Finalize SVG file with name
    ! nameOfFile
    !
    !   Input:
    !     nameOfFile - name of the SVG file
    !---------------------------------------
    use SVGMod, only : SVG_Finalize
    use Config, only : str_len
    implicit none

    character(str_len) :: nameOfFile ! name of the SVG file which should be finalized

    call SVG_Finalize(nameOfFile)

    return

  end subroutine FinalizeSVG

  subroutine DrawPolygon(nameOfFile, nvert, x, y, strokeWidth, fillOpacity, strokeColour, fillColour)
    !---------------------------------
    ! Draws polygon in nameOfFile file
    !
    ! Input:
    !   nameOfFile - name of the file
    !            nvert - number of polygon's vertices
    !            x - x coordinates of the polygon's vertices
    !            y - y coordinates of the polygon's vertices
    !--------------------------------------
    use Config, only : str_len
    use SVGMod, only : SVG_Polygon

    implicit none

    character(str_len) :: nameOfFile ! name of the file where polygon will be drawn
    integer :: nvert
    real(rp) :: x(:) ! array containing x coordinates of the polygon vertices
    real(rp) :: y(:) ! array containing y coordinates of the polygon vertices
    real(rp) :: strokeWidth ! width of the line (px)
    real(rp) :: fillOpacity ! fill opacity (0 <= opacity <= 1)
    character(len = *) :: strokeColour ! colour of the line in the terms of RGB
    character(len = *) :: fillColour ! colour of the polygon's filling in the terms of RGB

    integer :: lineRGB(3)
    integer :: fillRGB(3)

    lineRGB = colourToRGB(strokeColour)
    fillRGB = colourToRGB(fillColour)

    call SVG_Polygon(nameOfFile, x(1:nvert), y(1:nvert), strokeWidth/svgImageFactorX, fillOpacity, lineRGB, fillRGB)

  end subroutine DrawPolygon

  subroutine DrawPolygonWithNormVec(nameOfFile, nvert, verX, verY, normVecX, normVecY, polygonStrokeWidth, &
                                    vectorStrokeWidth, polygonStrokeColour, vectorStrokeColour, &
                                    vectorHeadSize, polygonFillColour, polygonFillOpacity)
    use Config, only : str_len
    use GeometryMod, only : getSegmentMiddle, setVecLength
    implicit none

    character(str_len) :: nameOfFile
    integer :: nvert
    real(rp) :: verX(:)
    real(rp) :: verY(:)
    real(rp) :: normVecX(:)
    real(rp) :: normVecY(:)
    real(rp) :: polygonStrokeWidth
    real(rp) :: vectorStrokeWidth
    character(len = *) :: polygonStrokeColour
    character(len = *) :: vectorStrokeColour
    real(rp) :: vectorHeadSize
    character(len = *) :: polygonFillColour
    real(rp) :: polygonFillOpacity

    real(rp) :: vecLen
    real(rp) :: xy(2), xy1(2)
    integer :: i

    call DrawPolygon(nameOfFile, nvert, verX, verY, polygonStrokeWidth, polygonFillOpacity, polygonStrokeColour, polygonFillColour)

    vecLen = 0.05_rp*min(maxval(verX)-minval(verX), maxval(verY)-minval(verY))

    do i = 1, size(verX) - 1
      xy = getSegmentMiddle(verX(i), verY(i), verX(i+1), verY(i+1))
      xy1 = setVecLength(normVecX(i), normVecY(i), vecLen)
      call DrawVector(nameOfFile, xy(1), xy(2), xy(1) + xy1(1), xy(2) + xy1(2), &
                      vectorStrokeWidth, vectorStrokeColour, vectorHeadSize)
    end do

    xy = getSegmentMiddle(verX(size(verX)), verY(size(verY)), verX(1), verY(1))
    xy1 = setVecLength(normVecX(size(verX)), normVecY(size(verY)), vecLen)
    call DrawVector(nameOfFile, xy(1), xy(2), xy(1) + xy1(1), xy(2) + xy1(2), &
                      vectorStrokeWidth, vectorStrokeColour, vectorHeadSize)
    return
  end subroutine DrawPolygonWithNormVec

  subroutine DrawCircle(nameOfFile, xC, yC, r, strokeWidth, fillOpacity, strokeColour, fillColour)
    use Config, only : str_len
    use SVGMod, only : SVG_Circle

    implicit none

    character(str_len) :: nameOfFile
    real(rp) :: xC
    real(rp) :: yC
    real(rp) :: r
    real(rp) :: strokeWidth ! width of the line (px)
    real(rp) :: fillOpacity ! fill opacity (0 <= opacity <= 1)
    character(len = *) :: strokeColour ! colour of the line in the terms of RGB
    character(len = *) :: fillColour ! colour of the polygon's filling in the terms of RGB

    integer :: lineRGB(3)
    integer :: fillRGB(3)

    lineRGB = colourToRGB(strokeColour)
    fillRGB = colourToRGB(fillColour)

    call SVG_Circle(nameOfFile, xC, yC, r, strokeWidth/svgImageFactorX, fillOpacity, lineRGB, fillRGB)

    return

  end subroutine DrawCircle

  subroutine DrawVector(nameOfFile, x0, y0, x1, y1, strokeWidth, strokeColour, headSize)
    use Config, only : str_len
    use SVGMod, only : SVG_Vector

    implicit none

    character(str_len) :: nameOfFile ! name of the file where vector will be drawn
    real(rp) :: x0   ! x coordinate of the vector begin
    real(rp) :: y0   ! y coordinate of the vector begin
    real(rp) :: x1   ! x coordinate of the vector end
    real(rp) :: y1   ! y coordinate of the vector end
    real(rp) :: strokeWidth ! width of the line (px)
    character(len = *) :: strokeColour ! colour of the line in the terms of RGB
    real(rp) :: headSize ! size of the vector's head

    integer :: lineRGB(3)
    real(rp) :: x(2), y(2)

    lineRGB = colourToRGB(strokeColour)
    x(1) = x0
    y(1) = y0
    x(2) = x1
    y(2) = y1

    call SVG_Vector(nameOfFile, x, y, strokeWidth/svgImageFactorX, lineRGB, headSize)

  end subroutine DrawVector

  subroutine DrawLine(nameOfFile, x0, y0, x1, y1, strokeWidth, strokeColour)
  !-------------------------------------------------------------------------
  ! Subroutine draws line from point x0, y0 to point x1, y1
  !-------------------------------------------------------------------------
    use Config, only : str_len
    use SVGMod, only : SVG_Line

    implicit none

    character(str_len) :: nameOfFile ! name of the file where vector will be drawn
    real(rp) :: x0   ! x coordinate of the vector begin
    real(rp) :: y0   ! y coordinate of the vector begin
    real(rp) :: x1   ! x coordinate of the vector end
    real(rp) :: y1   ! y coordinate of the vector end
    real(rp) :: strokeWidth ! width of the line (px)
    character(len = *) :: strokeColour ! colour of the line in the terms of RGB

    integer :: lineRGB(3)
    real(rp) :: x(2), y(2)

    lineRGB = colourToRGB(strokeColour)
    x(1) = x0
    y(1) = y0
    x(2) = x1
    y(2) = y1

    call SVG_Line(nameOfFile, x, y, strokeWidth/svgImageFactorX, lineRGB)

  end subroutine DrawLine

  subroutine TransformImageSvg(nameOfFile, translateX, translateY, scaleX, scaleY, clip_path_id)
    use Config, only : str_len
    use SVGMod, only : SVG_Transform

    implicit none

    character(str_len) :: nameOfFile
    real(rp) :: translateX
    real(rp) :: translateY
    real(rp) :: scaleX
    real(rp) :: scaleY
    character(str_len), optional :: clip_path_id

    if (present(clip_path_id)) then
      call SVG_Transform(nameOfFile, translateX, translateY, scaleX, scaleY, clip_path_id)
    else
      call SVG_Transform(nameOfFile, translateX, translateY, scaleX, scaleY)
    end if

    return

  end subroutine TransformImageSvg

  subroutine DrawPoint(nameOfFile, xp, yp, sz, colour)
    use Config, only : str_len
    implicit none

    character(str_len), intent(in) :: nameOfFile
    real(rp), intent(in) :: xp
    real(rp), intent(in) :: yp
    real(rp), intent(in) :: sz
    character(len = *), intent(in) :: colour

    real(rp) :: r

    r = sz/svgImageFactorX

    call DrawCircle(nameOfFile, xp, yp, r, 0.0_rp, 1.0_rp, colour, colour)

    return

  end subroutine DrawPoint

  subroutine AddClipPathPolygon(nameOfFile, nver, verx, very, clip_path_id)
    use Config, only : str_len
    use SVGMod, only : SVG_ClipPathPolygon

    character(str_len), intent(in) :: nameOfFile
    integer, intent(in) :: nver
    real(rp), intent(in) :: verx(:)
    real(rp), intent(in) :: very(:)
    character(str_len), intent(in) :: clip_path_id

    real(rp) :: x(nver), y(nver)

    x = verx(1:nver)
    y = very(1:nver)

    call SVG_ClipPathPolygon(nameOfFile, x, y, clip_path_id)

  end subroutine

  function colourToRGB(colour)
    !------------------------------------
    ! Function returns RGB code for given
    ! name of colour. If name of the colour
    ! is not in the list, returns random colour
    !-------------------------------------
    use GlobalAssemblyMod, only : AbortProgram
    use ReadWriteMod, only : WriteMessage
    implicit none

    character(len = *) :: colour

    integer :: colourToRGB(3)

    if (colour == "black") then
      colourToRGB = (/0, 0, 0/)
      return
    elseif (colour == "white") then
      colourToRGB = (/255, 255, 255/)
      return
    elseif (colour == "lightbrown") then
      colourToRGB = (/255, 235, 205/)
      return
    elseif (colour == "lightgray") then
      colourToRGB = (/211, 211, 211/)
      return
    elseif (colour == "lightblue") then
      colourToRGB = (/176, 224, 230/)
      return
    elseif (colour == "darkbrown") then
      colourToRGB = (/128, 0, 0/)
      return
    elseif (colour == "lightviolet") then
      colourToRGB = (/221, 160, 221/)
      return
    elseif (colour == "lightgreen") then
      colourToRGB = (/144, 238, 144/)
      return
    elseif (colour == "red") then
      colourToRGB = (/255, 0, 0/)
      return
    elseif (colour == "orange") then
      colourToRGB = (/255, 165, 0/)
      return
    elseif (colour == "yellow") then
      colourToRGB = (/255, 255, 0/)
      return
    elseif (colour == "green") then
      colourToRGB = (/0, 128, 0/)
      return
    elseif (colour == "blue") then
      colourToRGB = (/0, 0, 255/)
      return
    elseif (colour == "indigo") then
      colourToRGB = (/75, 0, 130/)
      return
    elseif (colour == "darkblue") then
      colourToRGB = (/0, 0, 139/)
      return
    elseif (colour == "violet") then
      colourToRGB = (/238, 130, 238/)
      return
    end if

    colourToRGB = randomColour()
    return

  end function colourToRGB

  function randomColour()
    implicit none

    integer :: randomColour(3)

    integer :: red, green, blue

    red = floor((randInt(0,255) + 255)*0.5)
    green = floor((randInt(0,255) + 255)*0.5)
    blue = floor((randInt(0,255) + 255)*0.5)

    randomColour = (/red, green, blue/)

    return

  end function randomColour

  integer function randInt(n, m)
    ! Function generates random integer number in the range from n to m
    implicit none

    integer, intent(in) :: n, m

    real(rp) :: u

    call random_number(u)

    randInt = n + floor((m+1-n)*u)

    return

  end function randInt

  function transformCoordX(coord)
    use Config, only : viewBox_width, viewBox_height, viewBox_centerX
    use GlobalAssemblyMod, only : AbortProgram
    use ReadWriteMod, only : WriteMessage
    implicit none

    real(rp), intent(in) :: coord
    real(rp) :: transformCoordX

    if (svgImageFactorX <= 0.0_rp) then
      call WriteMessage("Error! The SVG factor has zero or negative value.")
      call AbortProgram
    end if

    transformCoordX = svgImageFactorX*coord + viewBox_centerX

    return
  end function transformCoordX

  function transformCoordY(coord)
    use Config, only : viewBox_width, viewBox_height, viewBox_centerX, viewBox_centerY
    use GlobalAssemblyMod, only : AbortProgram
    use ReadWriteMod, only : WriteMessage
    implicit none

    real(rp), intent(in) :: coord
    real(rp) :: transformCoordY

    if (svgImageFactorY <= 0.0_rp) then
      call WriteMessage("Error! The SVG factor has zero or negative value.")
      call AbortProgram
    end if

    transformCoordY = svgImageFactorY*coord + viewBox_centerY

    return

  end function transformCoordY

  function transformArrayCoordX(coord)
    use Config, only : viewBox_width, viewBox_height, viewBox_centerX
    use GlobalAssemblyMod, only : AbortProgram
    use ReadWriteMod, only : WriteMessage
    implicit none

    real(rp), intent(in) :: coord(:)
    real(rp), allocatable :: transformArrayCoordX(:)

    integer :: i

    if (svgImageFactorX <= 0.0_rp) then
      call WriteMessage("Error! The SVG factor has zero or negative value.")
      call AbortProgram
    end if

    allocate(transformArrayCoordX(size(coord)))
    transformArrayCoordX = 0.0_rp

    do i = 1, size(coord)
      transformArrayCoordX(i) = svgImageFactorX*coord(i) + viewBox_centerX
    end do
    return
  end function transformArrayCoordX

  function transformArrayCoordY(coord)
    use Config, only : viewBox_width, viewBox_height, viewBox_centerX, viewBox_centerY
    use GlobalAssemblyMod, only : AbortProgram
    use ReadWriteMod, only : WriteMessage
    implicit none

    real(rp), intent(in) :: coord(:)
    real(rp), allocatable :: transformArrayCoordY(:)

    integer :: i

    if (svgImageFactorY <= 0.0_rp) then
      call WriteMessage("Error! The SVG factor has zero or negative value.")
      call AbortProgram
    end if

    allocate(transformArrayCoordY(size(coord)))
    transformArrayCoordY = 0.0_rp

    do i = 1, size(coord)
      transformArrayCoordY(i) = svgImageFactorY*coord(i) + viewBox_centerY
    end do

    return

  end function transformArrayCoordY

  function getImageFactorX(verX)
    !-------------------------------------------
    ! Function returns the factor for SVG image.
    ! ------------------------------------------
    use Config, only : viewBox_width
    implicit none

    real(rp) :: verX(:) ! array with x coordinates of the polygon

    real(rp) :: getImageFactorX

    real(rp) :: lenX

    if (maxval(verX) >= 0.0_rp .and. minval(verX) >= 0.0_rp) then
      lenX = 2.0_rp * (maxval(verX) - minval(verX))
    else
      lenX = maxval(verX) - minval(verX)
    end if


    getImageFactorX = 0.90_rp*viewBox_width/lenX

    return

  end function getImageFactorX

  function getImageFactorY(verY)
    !-------------------------------------------
    ! Function returns the factor for SVG image.
    ! ------------------------------------------
    use Config, only : viewBox_height, viewBox_width
    implicit none

    real(rp) :: verY(:) ! array with x coordinates of the polygon

    real(rp) :: getImageFactorY

    real(rp) :: lenY

    lenY = maxval(verY) - minval(verY)

    getImageFactorY = 0.95_rp*viewBox_height/lenY

    return

  end function getImageFactorY

end module
