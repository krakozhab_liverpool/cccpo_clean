subroutine set_ngroups(n_groups)
  use GlobalAssemblyMod, only : nGroups

  implicit none

!f2py intent(in) n_groups
  integer, intent(in) :: n_groups

  nGroups = n_groups

  return

end subroutine

subroutine set_assembly_type(assmb_type)
  use GlobalAssemblyMod, only : assembly_type
  implicit none

!f2py intent(in) assmb_type
  character*128, intent(in) :: assmb_type

  assembly_type = assmb_type

  return

end subroutine

subroutine set_nphi(n_phi)
  use precision_mod
  use ConstantsMod, only : PI
  use GlobalAssemblyMod, only : nPhi, phiSect, chi, verbosity

!f2py intent(in) n_phi
  integer, intent(in) :: n_phi

  real(rp) :: delta_phi
  integer  :: i

  nPhi = n_phi

  if (.not. allocated(phiSect)) allocate(phiSect(nPhi+1))
  delta_phi = PI/nPhi
  do i = 1, nPhi + 1
    phiSect(i) = -0.5_rp*PI + (i-1)*delta_phi
  end do

! Calculations of the weights chi of the phi sectors
  if (.not. allocated(chi)) allocate(chi(nPhi))
  chi = 0.0_rp
  do i = 1, nPhi
    chi(i) = 0.5_rp*(sin(phiSect(i+1)) - sin(phiSect(i)))
  end do

  if (verbosity > 0) then
    write(*,*) "<set_nphi>: phisect = ", phiSect
  end if

  return

end subroutine

subroutine set_dseg(d_seg)
  use Config, only : rp
  use GlobalAssemblyMod, only : dseg

  implicit none

!f2py intent(in) d_seg
  real(rp), intent(in) :: d_seg

  dseg = d_seg

  return

end subroutine

subroutine set_ntheta(n_theta)
  use precision_mod
  use ConstantsMod, only : PI, PI2
  use GlobalAssemblyMod, only : nTheta, thetaSect, eps

  implicit none

!f2py intent(in) n_theta
  integer, intent(in) :: n_theta

  real(rp) :: delta_theta
  real(rp) :: theta0, theta1
  integer  :: i

  nTheta = n_theta

  if (.not. allocated(thetaSect)) allocate(thetaSect(nTheta+1))
  delta_theta = 0.5_rp*PI/nTheta
  do i = 1, nTheta + 1
    thetaSect(i) = (i-1)*delta_theta
  end do

  if (.not. allocated(eps)) allocate(eps(nTheta))
  eps = 0.0_rp
  do i = 1, nTheta
    theta0 = thetaSect(i)
    theta1 = thetaSect(i+1)
    eps(i) = PI2*(theta1 - 0.5_rp*sin(2.0_rp*theta1) - theta0 + 0.5_rp*sin(2.0_rp*theta0))
  end do

  return

end subroutine

subroutine set_nrow(n_row)
  use GlobalAssemblyMod, only : nRow

  implicit none

!f2py intent(in) n_row
  integer, intent(in) :: n_row

  nRow = n_row

  return

end subroutine

subroutine set_ncol(n_col)
  use GlobalAssemblyMod, only : nCol

  implicit none

!f2py intent(in) n_col
  integer, intent(in) :: n_col

  nCol = n_col

  return

end subroutine

subroutine set_sym(symmetry)
  use GlobalAssemblyMod, only : sym

  implicit none

!f2py intent(in) symmetry
  integer, intent(in) :: symmetry

  sym = symmetry

  return

end subroutine

subroutine set_nmat(n_mat)
  use GlobalAssemblyMod, only : nMat

  implicit none

!f2py intent(in) n_mat
  integer, intent(in) :: n_mat

  nMat = n_mat

  return

end subroutine

subroutine set_ncelltypes(n_celltypes)
  use GlobalAssemblyMod, only : nCellTypes

  implicit none

!f2py intent(in) n_celltypes
  integer, intent(in) :: n_celltypes

  nCellTypes = n_celltypes

  return

end subroutine

subroutine set_ncell(nc)
  use GlobalAssemblyMod, only : nCell

  implicit none

!f2py intent(in) nc
  integer, intent(in) :: nc

  nCell = nc

  return

end subroutine

subroutine set_withgap(with_gap)
  use GlobalAssemblyMod, only : withGap

  implicit none

!f2py intent(in) with_gap
  logical, intent(in) :: with_gap

  withGap = with_gap

  return

end subroutine

subroutine set_kart_rectangular(n_row, n_col, kart2d)
  use GlobalAssemblyMod, only : kart
  implicit none

!f2py intent(in) n_row
  integer, intent(in) :: n_row
!f2py intent(in) n_col
  integer, intent(in) :: n_col
!f2py intent(in) kart2d
!f2py depend(n_row,n_col) kart2d
  integer, intent(in) :: kart2d(n_row, n_col)

  integer :: ir, ic, icell

  allocate(kart(n_row*n_col))
  kart = 0

  icell = 1
  do ir = 1, n_row
    do ic = 1, n_col
      kart(icell) = kart2d(ir, ic)
      icell = icell + 1
    end do
  end do

  return

end subroutine

subroutine set_albedos(alb, ng, n_sides, n_cell_side_max)
  use precision_mod
  use GlobalAssemblyMod, only : albedo, verbosity
  use NeighboursMod, only : Get_cell_edge_numbers
  implicit none

  integer :: ig, isd

! Number of energy groups
!f2py intent(in) ng
  integer, intent(in) :: ng
! Total number of the the outer sides in the geometry
!f2py intent(in) n_sides
  integer, intent(in) :: n_sides
! Maximal number of the cells on any side of the geometry
!f2py intent(in) n_cell_side_max
  integer, intent(in) :: n_cell_side_max
! Array containing albedos for each side of the geometry
!f2py intent(in) alb
!f2py depend(ng, n_sides, n_cell_side_max)
  real(rp), intent(in) :: alb(ng, n_sides, n_cell_side_max)

  allocate(albedo(ng, n_sides, n_cell_side_max))
  albedo = 0.
  albedo = alb

  call Get_cell_edge_numbers()

  if (verbosity > 0) then
    write(*, *) "Albedo from Fortran subroutine:"
    do ig = 1, ng
      write(*, *) "Group = ", ig
      do isd = 1, n_sides
        write(*, *) "Side ", isd
        write(*, *) albedo(ig, isd, :)
      end do
    end do
  end if

  return

end subroutine

subroutine set_irun(i_run)
  use GlobalAssemblyMod, only : irun
  implicit none

!f2py intent(in) i_run
  integer, intent(in) :: i_run

  irun = i_run

  return

end subroutine

subroutine set_idraw(i_draw)
  use GlobalAssemblyMod, only : idraw

!f2py intent(in) i_draw
  integer, intent(in) :: i_draw

  idraw = i_draw

  return

end subroutine

subroutine set_icompare(i_compare)
  use GlobalAssemblyMod, only : icompare

!f2py intent(in) i_compare
  integer, intent(in) :: i_compare

  icompare = i_compare

  return

end subroutine

subroutine set_ivtk(i_vtk)
  use GlobalAssemblyMod, only : ivtk

  implicit none

!f2py intent(in) i_vtk
  integer, intent(in) :: i_vtk

  ivtk = i_vtk

  return

end subroutine

subroutine set_ixs(i_xs)
  use GlobalAssemblyMod, only : ixs

  implicit none

!f2py intent(in) i_xs
  integer, intent(in) :: i_xs

  ixs = i_xs

  return

end subroutine

subroutine set_iout(i_out)
  use GlobalAssemblyMod, only : iout

  implicit none

!f2py intent(in) i_out
  integer, intent(in) :: i_out

  iout = i_out

  return

end subroutine

subroutine set_ng_collaps(ng_col)
  use GlobalAssemblyMod, only : ng_collaps

  implicit none

!f2py intent(in) ng_col
  integer, intent(in) :: ng_col

  ng_collaps = ng_col

  return

end subroutine

subroutine set_group_bounds_collapsed(gr_collapsed, ng_col)
  use GlobalAssemblyMod, only : group_bounds_collapsed

!f2py intent(in) gr_collapsed
!f2py depend(ng_col) gr_collapsed
  integer, intent(in) :: gr_collapsed(ng_col+1)
!f2py intent(in) ng_col
  integer, intent(in) :: ng_col

  if (.not. allocated(group_bounds_collapsed)) allocate(group_bounds_collapsed(ng_col+1))
  group_bounds_collapsed = gr_collapsed

  write(*, *) group_bounds_collapsed

  return

end subroutine

subroutine set_eps_f(eps)
  use precision_mod
  use GlobalAssemblyMod, only : eps_f

!f2py intent(in) eps
  real(rp), intent(in) :: eps

  eps_f = eps

  return

end subroutine

subroutine set_eps_k(eps)
  use precision_mod
  use GlobalAssemblyMod, only : eps_k

  implicit none

!f2py intent(in) eps
  real(rp), intent(in) :: eps

  eps_k = eps

  return

end subroutine

subroutine set_iter_max(n_iter)
  use GlobalAssemblyMod, only : iter_max

  implicit none

!f2py intent(in) n_iter
  integer, intent(in) :: n_iter

  iter_max = n_iter

  return

end subroutine

subroutine set_kiter_max(n_iter)
  use GlobalAssemblyMod, only : kiter_max

  implicit none

!f2py intent(in) n_iter
  integer, intent(in) :: n_iter

  kiter_max = n_iter

  return

end subroutine

subroutine set_stot(sigma_total, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, STOT

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) sigma_total
!f2py depend(n_mat, ng) sigma_total
  real(rp), intent(in) :: sigma_total(n_mat, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_stot>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(STOT)) allocate(STOT(n_mat, ng))
  STOT = sigma_total

  return

end subroutine

subroutine set_ssca(sigma_scattering, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, SSCA

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) sigma_scattering
!f2py depend(n_mat, ng, ng) sigma_scattering
  real(rp), intent(in) :: sigma_scattering(n_mat, ng, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_ssca>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(SSCA)) allocate(SSCA(n_mat, ng, ng))
  SSCA = sigma_scattering

  return

end subroutine

subroutine set_schi(chi, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, SCHI

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) chi
!f2py depend(n_mat, ng) chi
  real(rp), intent(in) :: chi(n_mat, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_schi>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(SCHI)) allocate(SCHI(n_mat, ng))
  SCHI = chi

  return

end subroutine

subroutine set_sfis(sigma_fission, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, SFIS

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) sigma_fission
!f2py depend(n_mat, ng) sigma_fission
  real(rp), intent(in) :: sigma_fission(n_mat, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_sfis>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(SFIS)) allocate(SFIS(n_mat, ng))
  SFIS = sigma_fission

  return

end subroutine

subroutine set_sfnf(sigma_nu_fission, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, SFNF

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) sigma_nu_fission
!f2py depend(n_mat, ng) sigma_nu_fission
  real(rp), intent(in) :: sigma_nu_fission(n_mat, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_sfnf>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(SFNF)) allocate(SFNF(n_mat, ng))
  SFNF = sigma_nu_fission

  return

end subroutine

subroutine set_sour(source, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, SOUR

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) source
!f2py depend(n_mat, ng) source
  real(rp), intent(in) :: source(n_mat, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_sour>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(SOUR)) allocate(SOUR(n_mat, ng))
  SOUR = source

  return

end subroutine

subroutine set_sabs(sigma_absorption, n_mat, ng)
  use precision_mod
  use GlobalAssemblyMod, only : nGroups, nMat, SABS

!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) n_mat
  integer, intent(in) :: n_mat
!f2py intent(in) sigma_absorption
!f2py depend(n_mat, ng) sigma_absorption
  real(rp), intent(in) :: sigma_absorption(n_mat, ng)

  if (n_mat /= nMat .or. ng /= nGroups) then
    write(*,*) "PyInterface <set_sabs>: n_mat or ng do not correspond to the nGroups or nMat."
    write(*,*) "Check, whether nMat and nGroups were set or not."
    stop
  end if
  if (.not. allocated(SABS)) allocate(SABS(n_mat, ng))
  SABS = sigma_absorption

  return

end subroutine

subroutine set_all_cell_types_to_regular()
  use GlobalAssemblyMod, only : nCellTypes, cell_type

  implicit none

  integer :: i

  if (nCellTypes <= 0) then
    write(*,*) "PyInterface <set_all_cell_types_to_regular>: nCellTypes can not be 0 or less."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_type)) allocate(cell_type(nCellTypes))
  do i = 1, nCellTypes
    cell_type(i) = "regular"
  end do

  return

end subroutine

subroutine set_cell_types(types, n_cell_types)
  use GlobalAssemblyMod, only : nCellTypes, cell_type

  implicit none

!f2py intent(in) types
!f2py depend(n_cell_types) types
  character*128, intent(in) :: types(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in)       :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_types>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_type)) allocate(cell_type(n_cell_types))
  cell_type = types

  return

end subroutine

subroutine set_cell_nsides(n_sides, n_cell_types)
  use GlobalAssemblyMod, only : nsides, nCellTypes, verbosity

  implicit none
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types
!f2py intent(in,out) n_sides
!f2py depend(n_cell_types) n_sides
  integer, intent(inout) :: n_sides(n_cell_types)

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nsides>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(nsides)) allocate(nsides(n_cell_types))
  nsides = n_sides

  if (verbosity > 0) then
    write(*,*) "PyInterface. Writing nsides array:"
    write(*,*) nsides
  end if

  return

end subroutine

subroutine set_cell_nharms(n_harms, n_cell_types)
  use GlobalAssemblyMod, only : nharms, nCellTypes

  implicit none

!f2py intent(in) n_harms
!f2py depend(n_cell_types) n_harms
  integer, intent(in) :: n_harms(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nharms>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(nharms)) allocate(nharms(n_cell_types))
  nharms = n_harms

  return

end subroutine

subroutine calculate_regular_cells_vertxy(pitch, n_cell_types)
  use precision_mod
  use GlobalAssemblyMod, only : cell_vertx, cell_verty, nCellTypes, nsides, dpitch
  use GeometryMod, only : GetVerticesPolygon

  implicit none

!f2py intent(in) pitch
!f2py depend(n_cell_types) pitch
  real(rp), intent(in) :: pitch
!f2py intent(in) n_cell_types
  integer, intent(in)  :: n_cell_types

  integer :: icell

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_regular_cells_vertxy>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes is set or not."
    stop
  end if

  if (.not. allocated(nsides)) then
    write(*,*) "PyInterface <set_regular_cells_vertxy>: number of sides for each cell (nsides) should be set first."
    stop
  end if

  if (.not. allocated(cell_vertx)) allocate(cell_vertx(n_cell_types, maxval(nsides)))
  if (.not. allocated(cell_verty)) allocate(cell_verty(n_cell_types, maxval(nsides)))

  dpitch = pitch

  do icell = 1, n_cell_types
    call GetVerticesPolygon(nsides(icell), pitch, cell_vertx(icell,:), cell_verty(icell,:))
  end do

  return

end subroutine

subroutine set_assmb_pitch(pitch)
  use precision_mod
  use GlobalAssemblyMod, only : assmb_pitch

  implicit none

!f2py intent(in) pitch
  real(rp), intent(in) :: pitch

  assmb_pitch = pitch

end subroutine

subroutine set_cell_ncirc(n_cell_types, n_circ)
  use GlobalAssemblyMod, only : nCellTypes, ncirc, nzones

!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types
!f2py intent(in) n_circ
!f2py depend(n_cell_types) n_circ
  integer, intent(in) :: n_circ(n_cell_types)

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_ncirc>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(ncirc)) allocate(ncirc(n_cell_types))
  if (.not. allocated(nzones)) allocate(nzones(n_cell_types))
  ncirc = n_circ
  nzones = ncirc + 1

  return


end subroutine

subroutine set_cell_circ_radii(rad, n_cell_types, n_circ_max)
  use precision_mod
  use GlobalAssemblyMod, only : nCellTypes, cell_rad, ncirc

  implicit none

!f2py intent(in) rad
!f2py depend(n_cell_types, n_circ_max) rad
  real(rp), intent(in) :: rad(n_cell_types, n_circ_max)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types
!f2py intent(in) n_circ_max
  integer, intent(in) :: n_circ_max

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nharms>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(ncirc)) then
    write(*,*) "PyInterface <set_cell_circ_radii>: ncirc should be set first."
    stop
  end if

  if (maxval(ncirc) /= n_circ_max) then
    write(*,*) "PyInterface <set_cell_circ_radii>: maximal number of circles in ncirc does not equal to n_circ_max"
    stop
  end if

  if (.not. allocated(cell_rad)) allocate(cell_rad(n_cell_types,n_circ_max))
  cell_rad = rad

  return

end subroutine

subroutine set_cell_mat(mat, n_cell_types, n_zones_max)
  use GlobalAssemblyMod, only : nCellTypes, cell_mat, nzones

  implicit none

!f2py intent(in) mat
!f2py depend(n_cell_types, n_zones_max) mat
  integer, intent(in) :: mat(n_cell_types, n_zones_max)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types
!f2py intent(in) n_zones_max
  integer, intent(in) :: n_zones_max

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nharms>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(nzones)) then
    write(*,*) "PyInterface <set_cell_mat>: nzones should be set first."
    stop
  end if

  if (maxval(nzones) /= n_zones_max) then
    write(*,*) "PyInterface <set_cell_mat>: maximal number of zones in nzones does not equal to n_zones_max"
    stop
  end if

  if (.not. allocated(cell_mat)) allocate(cell_mat(n_cell_types,n_zones_max))
  cell_mat = mat

  return

end subroutine

subroutine set_cell_nphib(nphib, n_cell_types)
  use GlobalAssemblyMod, only : nCellTypes, cell_nphib

  implicit none

!f2py intent(in) nphib
!f2py depend(n_cell_types) nphib
  integer, intent(in) :: nphib(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nphib>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_nphib)) allocate(cell_nphib(n_cell_types))
  cell_nphib = nphib

  return

end subroutine

subroutine set_cell_nphig(nphig, n_cell_types)
  use GlobalAssemblyMod, only : nCellTypes, cell_nphig

  implicit none

!f2py intent(in) nphig
!f2py depend(n_cell_types) nphig
  integer, intent(in) :: nphig(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nphig>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_nphig)) allocate(cell_nphig(n_cell_types))
  cell_nphig = nphig

  return

end subroutine

subroutine set_cell_nphiu(nphiu, n_cell_types)
  use GlobalAssemblyMod, only : nCellTypes, cell_nphiu

  implicit none

!f2py intent(in) nphiu
!f2py depend(n_cell_types) nphiu
  integer, intent(in) :: nphiu(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_nphiu>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_nphiu)) allocate(cell_nphiu(n_cell_types))
  cell_nphiu = nphiu

  return

end subroutine

subroutine set_cell_dcb(dcb, n_cell_types)
  use precision_mod
  use GlobalAssemblyMod, only : cell_dcb, nCellTypes

  implicit none

!f2py intent(in) dcb
!f2py depend(n_cell_types) dcb
  real(rp), intent(in) :: dcb(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_dcb>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_dcb)) allocate(cell_dcb(n_cell_types))
  cell_dcb = dcb

  return

end subroutine

subroutine set_cell_dcg(dcg, n_cell_types)
  use precision_mod
  use GlobalAssemblyMod, only : cell_dcg, nCellTypes

  implicit none

!f2py intent(in) dcg
!f2py depend(n_cell_types) dcg
  real(rp), intent(in) :: dcg(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_dcg>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_dcg)) allocate(cell_dcg(n_cell_types))
  cell_dcg = dcg

  return

end subroutine

subroutine set_cell_dcu(dcu, n_cell_types)
  use precision_mod
  use GlobalAssemblyMod, only : cell_dcu, nCellTypes

  implicit none

!f2py intent(in) dcu
!f2py depend(n_cell_types) dcu
  real(rp), intent(in) :: dcu(n_cell_types)
!f2py intent(in) n_cell_types
  integer, intent(in) :: n_cell_types

  if (n_cell_types /= nCellTypes) then
    write(*,*) "PyInterface <set_cell_dcu>: n_cell_types do not correspond to the nCellTypes."
    write(*,*) "Check, whether nCellTypes set or not."
    stop
  end if

  if (.not. allocated(cell_dcu)) allocate(cell_dcu(n_cell_types))
  cell_dcu = dcu

  return

end subroutine

subroutine set_verbosity(verb)
  use GlobalAssemblyMod, only : verbosity

  implicit none

!f2py intent(in) verb
  integer, intent(in) :: verb

  verbosity = verb

  return

end subroutine

subroutine get_neighbours()
  use precision_mod
  use NeighboursMod, only : GetNeighbours
  use GlobalAssemblyMod, only : neighCell, neighSide, nCell, assembly_type, fact, nsides, &
                                nCol, nRow, sym

  implicit none

  if (.not. allocated(neighCell)) allocate(neighCell(maxval(nsides),nCell))
  if (.not. allocated(neighSide)) allocate(neighSide(maxval(nsides),nCell))
  if (.not. allocated(fact)) allocate(fact(nCell))
  neighCell = 0
  neighSide = 0
  fact = 0.0_rp
  call GetNeighbours(nCell, assembly_type, sym, nRow, nCol, neighCell, neighSide, fact)
end subroutine

subroutine get_cell_edge_num()
  use GlobalAssemblyMod, only : num_cell_edge, assembly_type, nCell
  use NeighboursMod, only : Get_cell_edge_numbers

  implicit none

  integer :: n_assmb_sides

  if (assembly_type == "regular square") then
    n_assmb_sides = 4
  else
    write(*, *) "PyInterface <get_cell_edge_num>: assembly type", trim(assembly_type), " is not supported at the moment."
  end if

  if (.not. allocated(num_cell_edge)) allocate(num_cell_edge(n_assmb_sides,nCell))
  call Get_cell_edge_numbers()

  return

end subroutine

subroutine read_gauss_points()
  use MathMod, only : readGaussPoints

  call readGaussPoints()

  return

end subroutine

subroutine get_bickley()
  use SplineMod, only : read_bickley_data, allocate_bickley, spline_coefficients_bickley, n_tau
  use GlobalAssemblyMod, only : nTheta

  implicit none

  call read_bickley_data(nTheta)
  call allocate_bickley(nTheta, n_tau)
  call spline_coefficients_bickley(nTheta, n_tau)

  return

end subroutine

subroutine get_num_segments()
  use precision_mod
  use GlobalAssemblyMod, only : nsides, cell_vertx, cell_verty, dseg, cell_nsegments, nCellTypes
  use GeometryMod, only : GetNumberOfSegments

  if (.not. allocated(cell_nsegments)) allocate(cell_nsegments(nCellTypes,maxval(nsides)))
  cell_nsegments = 0

  if (nCellTypes <= 0) then
    write(*, *) "PyInterface <get_num_segments>: Number of the cell types should be defined first."
  end if

  do icell = 1, nCellTypes
    call GetNumberOfSegments(cell_vertx(icell,:), cell_verty(icell,:), nsides(icell), dseg, cell_nsegments(icell,:))
  end do

  return

end subroutine

subroutine calculate_zone_volumes()
  use precision_mod
  use MathMod, only : GetZoneVolumes
  use GlobalAssemblyMod, only : nCellTypes, ncirc, nsides, cell_vertx, cell_verty, &
                                cell_rad, cell_vol
  implicit none

  integer :: icell

  if (.not. allocated(cell_vol)) allocate(cell_vol(nCellTypes,maxval(ncirc)+1))
  cell_vol = 0.0_rp

  do icell = 1, nCellTypes
    if (ncirc(icell) <= 0) then
      call GetZoneVolumes(nvert=nsides(icell), ncirc=ncirc(icell), x=cell_vertx(icell,:), &
                          y=cell_verty(icell,:), vol=cell_vol(icell,:))
    else
      call GetZoneVolumes(nvert=nsides(icell), ncirc=ncirc(icell), x=cell_vertx(icell,:), &
                          y=cell_verty(icell,:), r=cell_rad(icell,:), vol=cell_vol(icell,:))
    end if
  end do

  return

end subroutine

subroutine calculate_orthogonal_coefficients()
  use precision_mod
  use OrtMod, only : GetOrthogonalCoefficients
  use GlobalAssemblyMod, only : nsides, cell_vertx, cell_verty, ncirc, cell_rad, nzones, nharms, &
                                cell_vol, cell_ort

  if (.not. allocated(cell_ort)) allocate(cell_ort(nCellTypes,maxval(nzones),maxval(nharms),maxval(nharms)))
  cell_ort = 0.0_rp

  call GetOrthogonalCoefficients(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), &
                                 ncirc(icell), cell_rad(icell,:), nzones(icell), nharms(icell), &
                                 cell_vol(icell,:), cell_ort(icell,:,:,:))
  return

end subroutine

subroutine calculate_collision_probabilities()
  use precision_mod
  use Config, only : str_len
  use ArrayMod, only : SetArraySize2DReal, SetArraySize2DInt
  use CPMod, only : allocate_cp_arrays, allocate_cp_mg, get_beta_size, increase_beta_arrays_size, &
                    pass_data_to_cp_arrays, PhiBoundariesBeta, PhiBoundariesGamma, PhiBoundariesU, &
                    ChordsBetaHash, ChordsGamma, ChordsU
  use IntegrateCPMod, only : IntegrateBetaHash, IntegrateGamma, IntegrateV, IntegrateU, OrthogonalizeU, &
                             NormalizeU
  use DrawMod, only : DrawIndividualCell
  use GeometryMod, only : GetNormVecPolygon, GetNormVecPolygon, GetSegmentBoundaries
  use OrtMod, only : GetOrthogonalCoefficients
  use GlobalMod, only : Gamm, V, G, U, nBeta_key, nBetaElem, harmFromU, harmToU, inHarmGam, inSectGam,  &
                        inSectGam, inSegGam, inSideGam, inZoneGam, lenChordBet, lenChordGam, lenChordU, &
                        nAngBeta, nAngGamm, nAngU, nChordBet, nChordGam, nChordU, nGamma, nGammaElem,   &
                        nSegChordBet, nSegChordGam, nSegChordU, nU, nUElem, ort, phiBeta, phiGamm, phiU,&
                        phiGamm, phiBeta, wghtChordBet, wghtChordGam, wghtChordU, xChordGam,   &
                        xChordU, yChordGam, yChordU, zoneChordBet, zoneChordGam, zoneChordU, zoneFromU, &
                        zoneToU
  use GlobalAssemblyMod, only : nGroups, nCellTypes, cell_vertx, cell_verty, nsides, cell_nsegments,      &
                                nPhi, nTheta, cell_segboundx, cell_segboundy, cell_segsdarr, ncirc,       &
                                cell_rad, nzones, nharms, cell_vol, cell_ort, cell_mat,  cell_type,       &
                                STOT, cell_nbetaelem, cell_beta, cell_brc, cell_insidebet, cell_insegbet, &
                                cell_insectbet, cell_outsidebet, cell_outsegbet, cell_outsectbet,         &
                                cell_thetasectbet, cell_G, cell_V, cell_U, chi, eps, idraw, cell_nphib,   &
                                cell_nphig, cell_nphiu, cell_dcb, cell_dcg, cell_dcu, phiSect, verbosity
  use GlobalMod, only : inSideBet, inSegBet, inSectBet, outSideBet, outSectBet, outSegBet, thetaSectBet
  implicit none

  integer :: icell
  integer :: ig, n, s
  character(str_len) :: clr
  real(rp), allocatable :: normVecX(:)
  real(rp), allocatable :: normVecY(:)
  real(rp), allocatable :: lenSeg(:,:)
  real(rp), allocatable :: Beta(:)
  real(rp), allocatable :: brc(:)
  real(rp), allocatable :: cx(:), cy(:)
  real(rp) :: x0, y0, x1, y1

  call allocate_cp_mg(nGroups)

  allocate(normVecX(maxval(nsides)))
  allocate(normVecY(maxval(nsides)))
  normVecX = 0.0_rp
  normVecY = 0.0_rp

  if (.not. allocated(cell_segboundx)) allocate(cell_segboundx(nCellTypes,maxval(nsides),maxval(cell_nsegments)+1))
  if (.not. allocated(cell_segboundy)) allocate(cell_segboundy(nCellTypes,maxval(nsides),maxval(cell_nsegments)+1))
  if (.not. allocated(cell_segsdarr)) allocate(cell_segsdarr(nCellTypes,maxval(nsides),maxval(cell_nsegments)+1))
  cell_segboundx = 0.0_rp
  cell_segboundy = 0.0_rp
  cell_segsdarr = 0.0_rp

  if (.not. allocated(cx)) allocate(cx(maxval(ncirc)))
  if (.not. allocated(cy)) allocate(cy(maxval(ncirc)))
  cx = 0.0_rp
  cy = 0.0_rp

  if (.not. allocated(cell_ort)) allocate(cell_ort(nCellTypes,maxval(nzones),maxval(nharms),maxval(nharms)))
  if (.not. allocated(ort)) allocate(ort(maxval(nzones),maxval(nharms),maxval(nharms)))
  cell_ort = 0.0_rp
  ort = 0.0_rp


  write(*,*) "*****************Starting simulations********************"
  do icell = 1, nCellTypes
    write(*, '(1a,1i3)') "Evaluating the CPs for cell type ", icell
    if (verbosity > 0) then
      write(*, '(1a,1i3)') "    Number of regions      : ", nzones(icell)
      write(*, '(1a,1i3)') "    Number of harmonics    : ", nharms(icell)
      write(*, '(1a,1i3)') "    Number of sides        : ", nsides(icell)
      write(*, '(1a,1i3)') "    Max. number of segments: ", maxval(cell_nsegments(icell,:))
      write(*, '(1a,1i3)') "    Number of azim. sectors: ", nPhi
      write(*, '(1a,1i3)') "    Number of polar sectors: ", nTheta
    end if

    call GetNormVecPolygon(cell_vertx(icell,:), cell_verty(icell,:), nsides(icell), normVecX, normVecY)

    call GetSegmentBoundaries(cell_vertx(icell,:), cell_verty(icell,:), nsides(icell), cell_nsegments(icell,:), &
                              cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), cell_segsdarr(icell,:,:))

    if (idraw > 0) then
      call DrawIndividualCell(icell, cell_vertx, cell_verty, nsides, nzones, ncirc, cell_mat, cell_rad, &
                              cell_segboundx, cell_segboundy, cell_nsegments)
    end if

    call GetOrthogonalCoefficients(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), &
                      ncirc(icell), cell_rad(icell,:), nzones(icell), nharms(icell),        &
                      cell_vol(icell,:), cell_ort(icell,:,:,:))

    call PhiBoundariesBeta(nsides(icell), cell_nsegments(icell,:), cell_segboundx(icell,:,:), &
                           cell_segboundy(icell,:,:), normVecX, normVecY, ncirc(icell),       &
                           cell_rad(icell,:), cx, cy, phiSect, nPhi, cell_nphib(icell), &
                           nAngBeta, phiBeta)

    call PhiBoundariesGamma(nsides(icell), cell_nsegments(icell,:), cell_segboundx(icell,:,:), &
                            cell_segboundy(icell,:,:), normVecX, normVecY, ncirc(icell),       &
                            cell_rad(icell,:), cx, cy, phiSect, nPhi, cell_nphig(icell), &
                            nAngGamm, phiGamm)

    call PhiBoundariesU(nsides(icell), cell_nsegments(icell,:), cell_segboundx(icell,:,:), &
                        cell_segboundy(icell,:,:), normVecX, normVecY, ncirc(icell),       &
                        cell_rad(icell,:), cx, cy, cell_nphiu(icell), nAngU, phiU)
! Set of chords for following evaluation of beta probabilities
    call ChordsBetaHash(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), cell_nsegments(icell,:), &
                        cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), cell_segsdarr(icell,:,:),   &
                        nPhi, phiSect, normVecX, normVecY, ncirc(icell), cell_rad(icell,:), cx, cy,       &
                        nAngBeta, phiBeta, cell_dcb(icell))
! Calculaton of the gamma chords
    call ChordsGamma(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), cell_nsegments(icell,:),     &
                     cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), cell_segsdarr(icell,:,:), nPhi, &
                     phiSect, normVecX, normVecY, ncirc(icell), cell_rad(icell,:), cx, cy, nAngGamm,       &
                     phiGamm, cell_dcg(icell), nChordGam, nSegChordGam, lenChordGam, wghtChordGam,         &
                     zoneChordGam, xChordGam, yChordGam, inSideGam, inSegGam, inSectGam, inZoneGam,        &
                     inHarmGam, nGamma, nGammaElem)
! Evaluation of the chords for U calculations
    call ChordsU(nsides(icell), cell_vertx(icell,:), cell_verty(icell,:), cell_nsegments(icell,:),           &
                 cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), nPhi, phiSect, normVecX, normVecY,    &
                 ncirc(icell), cell_rad(icell,:), cx, cy, nAngU, phiU, cell_dcu(icell), nChordU, nSegChordU, &
                 lenChordU, wghtChordU, zoneChordU, xChordU, yChordU, zoneFromU, zoneToU, harmFromU,         &
                 harmToU, nU, nUElem)
! Allocation and filling of the array lenSeg containing segment lengths on the sides of the cells
    if (.not. allocated(lenSeg)) allocate(lenSeg(nsides(icell),maxval(cell_nsegments)))
    lenSeg = 0.0_rp
    do s = 1, nsides(icell)
      do n = 1, cell_nsegments(icell,s)
        x0 = cell_segboundx(icell,s,n)
        y0 = cell_segboundy(icell,s,n)
        x1 = cell_segboundx(icell,s,n+1)
        y1 = cell_segboundy(icell,s,n+1)
        lenSeg(s,n) = sqrt((x1-x0)**2 + (y1-y0)**2)
      end do
    end do
! Calculation of the collision probabilities for each energy group
    do ig = 1, nGroups
      if (verbosity > 0) then
        write(*,*) "  Group ", ig
      end if
! Integration of the Beta elements using previously calculated chords
      call IntegrateBetaHash(nChordBet, nSegChordBet, lenChordBet, wghtChordBet, zoneChordBet, cell_segsdarr(icell,:,:), &
                             cell_mat(icell,:), nBeta_key, STOT(:,ig), maxval(cell_nsegments), cell_type(icell), nBetaElem, &
                             Beta, brc)
      if (icell == 1 .and. ig == 1) then
        allocate(cell_nbetaelem(nCellTypes))
        allocate(cell_beta(nCellTypes,nBetaElem))
        allocate(cell_brc(nCellTypes,nBetaElem))
        allocate(cell_insidebet(nCellTypes,nBetaElem))
        allocate(cell_insegbet(nCellTypes,nBetaElem))
        allocate(cell_insectbet(nCellTypes,nBetaElem))
        allocate(cell_outsidebet(nCellTypes,nBetaElem))
        allocate(cell_outsegbet(nCellTypes,nBetaElem))
        allocate(cell_outsectbet(nCellTypes,nBetaElem))
        allocate(cell_thetasectbet(nCellTypes,nBetaElem))
      end if
      if (nBetaElem > size(cell_beta,2)) then
        call SetArraySize2DReal(cell_beta, nCellTypes, nBetaElem)
        call SetArraySize2DReal(cell_brc, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_insidebet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_insegbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_insectbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_outsidebet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_outsegbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_outsectbet, nCellTypes, nBetaElem)
        call SetArraySize2DInt(cell_thetasectbet, nCellTypes, nBetaElem)
      end if

      cell_nbetaelem(icell) = nBetaElem
      cell_beta(icell,1:nBetaElem) = Beta(1:nBetaElem)
      cell_brc(icell,1:nBetaElem) = brc(1:nBetaElem)
      cell_insidebet(icell,1:nBetaElem) = inSideBet(1:nBetaElem)
      cell_insegbet(icell,1:nBetaElem) = inSegBet(1:nBetaElem)
      cell_insectbet(icell,1:nBetaElem) = inSectBet(1:nBetaElem)
      cell_outsidebet(icell,1:nBetaElem) = outSideBet(1:nBetaElem)
      cell_outsegbet(icell,1:nBetaElem) = outSegBet(1:nBetaElem)
      cell_outsectbet(icell,1:nBetaElem) = outSectBet(1:nBetaElem)
      cell_thetasectbet(icell,1:nBetaElem) = thetaSectBet(1:nBetaElem)
      if (allocated(Beta))         deallocate(Beta)
      if (allocated(inSideBet))    deallocate(inSideBet)
      if (allocated(inSegBet))     deallocate(inSegBet)
      if (allocated(inSectBet))    deallocate(inSectBet)
      if (allocated(outSideBet))   deallocate(outSideBet)
      if (allocated(outSegBet))    deallocate(outSegBet)
      if (allocated(outSectBet))   deallocate(outSectBet)
      if (allocated(thetaSectBet)) deallocate(thetaSectBet)

! Allocation of the Gamma array
      if (icell == 1 .and. ig == 1) then
        allocate(Gamm(nGammaElem))
      end if
! Integration of the elements of the gamma matrix
      if (icell == 1 .and. ig == 1) then
        allocate(cell_G(nCellTypes,maxval(nzones),maxval(nharms),maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
        cell_G = 0.0_rp
      end if
      Gamm = 0.0_rp
      if (.not. allocated(G)) allocate(G(maxval(nzones),maxval(nharms),maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
      ort = cell_ort(icell,:,:,:)

      call IntegrateGamma(nChordGam, nSegChordGam, lenChordGam, wghtChordGam, zoneChordGam, xChordGam, yChordGam, &
                          nsides(icell), cell_nsegments(icell,:), nzones(icell), cell_mat(icell,:), STOT(:,ig),   &
                          nharms(icell))
      cell_G(icell,:,:,:,:,:,:) = G
!      call NormaliseGamma(cell_G(icell,:,:,:,:,:,:), cell_beta(icell,:), cell_outsegbet(icell,:),         &
!                          cell_outsectbet(icell,:), cell_outsidebet(icell,:), cell_thetasectbet(icell,:), &
!                          cell_nbetaelem(icell), nsides(icell), cell_nsegments(icell,:), nPhi, nTheta,    &
!                          nzones(icell))
! Allocation and initialization of the V matrix
      if (icell == 1 .and. ig == 1) then
        allocate(cell_V(nCellTypes,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
        cell_V = 0.0_rp
        allocate(V(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
      end if
      V = 0.0_rp
! Evaluation of the elements of the V matrix
      call IntegrateV(nzones(icell), nharms(icell), nsides(icell), cell_nsegments(icell,:), nPhi, nTheta, &
                      cell_segboundx(icell,:,:), cell_segboundy(icell,:,:), chi, eps, cell_mat(icell,:),  &
                      cell_vol(icell,:), STOT(:,ig), G, V)
      cell_V(icell,:,:,:,:,:,:) = V
! Evaluation of the elements of the U matrix
      if (icell == 1 .and. ig == 1) then
        allocate(cell_U(nCellTypes,maxval(nzones),maxval(nharms),maxval(nzones),maxval(nharms)))
        cell_U = 0.0_rp
        allocate(U(maxval(nzones),maxval(nharms),maxval(nzones),maxval(nharms)))
      end if
      U = 0.0_rp
      call IntegrateU(nChordU, nSegChordU, lenChordU, wghtChordU, zoneChordU, xChordU, yChordU, &
                      cell_mat(icell,:), cell_vol(icell,:), STOT(:,ig),  nzones(icell), nharms(icell), U)
! Orthogonalization of the U matrix
      call OrthogonalizeU(nzones(icell), nharms(icell), U)
! Normalization of the U matrix
      call NormalizeU(nzones(icell), nharms(icell), nsides(icell), cell_nsegments(icell,:), nPhi, nTheta, &
                      V, U)
      cell_U(icell,:,:,:,:) = U
! Allocation of the memory for collision probabilities for each group
! Beta
      if (icell == 1) then
        call allocate_cp_arrays(ig, nCellTypes, nBetaElem, nPhi, nTheta, nzones, &
                                nharms, nsides, cell_nsegments)
      end if
      if (nBetaElem > get_beta_size(ig)) then
        call increase_beta_arrays_size(ig, nCellTypes, nBetaElem)
      end if
! Passing the data into the multigroup collision probabilities
      call pass_data_to_cp_arrays(ig, icell, cell_nbetaelem(icell), nBetaElem, cell_beta, cell_brc, cell_insidebet, &
                                  cell_insegbet, cell_insectbet, cell_outsidebet, cell_outsegbet, cell_outsectbet,  &
                                  cell_thetasectbet, cell_G, cell_V, cell_U)
    end do

  end do
end subroutine

subroutine run_flux_solver()
  use GlobalAssemblyMod, only : irun, eps_k, kiter_max
  use CPMod, only : convert_cps_to_arrays
  use FluxMod, only : TransportSweepHelios, GetFluxAssemblyMG

  implicit none

  if (kiter_max <= 0) then
    write(*, *) "PyInterface <run_flux_solver>: kiter_max is less or eaqual to 0. Please, check or set kiter_max."
    stop
  end if

  write(*, *) "******************* Flux calculations ************************"
  if (irun == 0) then
    call GetFluxAssemblyMG()
  elseif (irun == 1) then
    call convert_cps_to_arrays()
    call TransportSweepHelios(eps_k, kiter_max)
  elseif (irun < 0 .or. irun > 1) then
    write(*, *) "PyInterface <run_flux_solver>: irun can't be more than 1 or less than 0 "
    stop
  end if
end subroutine

subroutine get_precision(prec)
  use precision_mod, only : rp
  implicit none

!f2py intent(inout) prec
  integer, intent(inout) :: prec

  prec = rp

  return

end subroutine

subroutine get_fluxes(flux, num_energy_groups, num_cells, max_num_zones, max_num_harms)
  use precision_mod
  use GlobalAssemblyMod, only : cell_flux, nGroups, nCell, nzones, nharms

!f2py intent(inout) flux
!f2py depend(num_energy_groups,num_cells,max_num_zones,max_num_harms) flux
  real(rp),intent(inout) :: flux(num_energy_groups,num_cells,max_num_zones,max_num_harms)
  integer, intent(in)    :: num_energy_groups
  integer, intent(in)    :: num_cells
  integer, intent(in)    :: max_num_zones
  integer, intent(in)    :: max_num_harms

  if (num_energy_groups /= nGroups) then
    write(*,*) "PyInterface <get_fluxes>: Number of the input energy groups does not correspond " //&
               "to the actual number of energy groups"
    stop
  end if

  if (num_cells /= nCell) then
    write(*,*) "PyInterface <get_fluxes>: Number of the input number of cells does not correspond " //&
               "to the actual number of the cells."
    stop
  end if

  if (max_num_zones /= maxval(nzones)) then
    write(*,*) "PyInterface <get_fluxes>: Maximal zones number of the input does not correspond to" //&
               " the actual maximal number of the zones."
    stop
  end if

  if (max_num_harms /= maxval(nharms)) then
    write(*,*) "PyInterface <get_fluxes>: Maximal number of the spatial modes input does not " //&
               "correspond to the actual maximal number of the spatial modes."
    stop
  end if

  flux = cell_flux

  return

end subroutine

subroutine get_cell_flux_at(flu, ng, nc, nr, nh)
  ! Returns flux at specific energy group, cell, region, spatial mode
  use precision_mod
  use GlobalAssemblyMod, only : cell_flux

  implicit none

!f2py intent(inout) flu
  real(rp), intent(inout) :: flu
!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) nc
  integer, intent(in) :: nc
!f2py intent(in) nr
  integer, intent(in) :: nr
!f2py intent(in) nh
  integer, intent(in) :: nh

  flu = cell_flux(ng, nc, nr, nh)

  return

end subroutine

subroutine get_cell_current_at(cur, ng, nc, nside, nseg, nphi, ntheta)
  ! Returns cell's current at specific energy group, side, segment, azim. sector, polar sector
  use precision_mod
  use GlobalAssemblyMod, only : cell_curr

  implicit none

!f2py intent(inout) cur
  real(rp), intent(inout) :: cur
!f2py intent(in) ng
  integer, intent(in) :: ng
!f2py intent(in) nc
  integer, intent(in) :: nc
!f2py intent(in) nside
  integer, intent(in) :: nside
!f2py intent(in) nseg
  integer, intent(in) :: nseg
!f2py intent(in) nphi
  integer, intent(in) :: nphi
!f2py intent(in) ntheta
  integer, intent(in) :: ntheta

  cur = cell_curr(ng, nc, nside, nseg, nphi, ntheta)

  return

end subroutine

subroutine get_neigh_cell_side(neigh_cell, neigh_side, n_cells, n_sides)
  use precision_mod
  use GlobalAssemblyMod, only : neighCell, neighSide, fact, nCell, assembly_type, sym, &
                                nrow, ncol, nsides, neighCell, neighSide, fact
  use NeighboursMod, only : GetNeighbours

  implicit none

!f2py intent(inout) neigh_cell
!f2py depend(n_sides, n_cells) neigh_cell
  integer, intent(inout) :: neigh_cell(n_sides, n_cells)
!f2py intent(inout) neigh_side
!f2py depend(n_sides, n_cells) neigh_side
  integer, intent(inout) :: neigh_side(n_sides, n_cells)
!f2py intent(in) n_cells
  integer, intent(in)    :: n_cells
!f2py intent(in) n_sides
  integer, intent(in)    :: n_sides

  if (.not. allocated(neighCell)) then
    allocate(neighCell(maxval(nsides),nCell))
    allocate(neighSide(maxval(nsides),nCell))
    allocate(fact(nCell))
    neighCell = 0
    neighSide = 0
    fact = 0.0_rp
    call GetNeighbours(nCell, assembly_type, sym, nRow, nCol, neighCell, neighSide, fact)
  end if

  neigh_cell = neighCell
  neigh_side = neighSide

  return

end subroutine

subroutine collapse_by_cells(tot_xs, scat_xs, abs_xs, chi_xs, sfnf_xs, sf_xs, &
                             cell_numbers, ngr, nsets, ncells)
  use precision_mod
  use GlobalAssemblyMod, only : ng_collaps, nGroups, cell_flux, xs_abs, xs_chi, &
                                xs_scat, xs_sf, xs_sfnf, xs_tot
  use ReadWriteMod, only : CollapseByCells
  implicit none

!f2py intent(inout) tot_xs
!f2py depend(ngr, nsets)
  real(rp), intent(inout) :: tot_xs(ngr, nsets)
!f2py intent(inout) scat_xs
!f2py depend(ngr, ngr, nsets)
  real(rp), intent(inout) :: scat_xs(ngr, ngr, nsets)
!f2py intent(inout) abs_xs
!f2py depend(ngr, nsets)
  real(rp), intent(inout) :: abs_xs(ngr, nsets)
!f2py intent(inout) chi_xs
!f2py depend(ngr, nsets)
  real(rp), intent(inout) :: chi_xs(ngr, nsets)
!f2py intent(inout) sfnf_xs
!f2py depend(ngr, nsets)
  real(rp), intent(inout) :: sfnf_xs(ngr, nsets)
!f2py intent(inout) sfnf_xs
!f2py depend(ngr, nsets)
  real(rp), intent(inout) :: sf_xs(ngr, nsets)
!f2py intent(inout) cell_numbers
!f2py depend(ncells)
  integer, intent(inout) :: cell_numbers(ncells)
!f2py intent(in) ngr
  integer, intent(in) :: ngr
!f2py intent(in) nsets
  integer, intent(in) :: nsets
!f2py intent(in) ncells
  integer, intent(in) :: ncells

  if (ngr /= nGroups) then
    write(*, *) "<collapse_by_cells>"
    write(*, *) "Number of groups passed to the fortran subroutine does not correspond to the nGroups used for simulations."
    stop
  end if

  call CollapseByCells(nGroups, ng_collaps, ncells, cell_numbers, cell_flux)

  tot_xs = xs_tot
  scat_xs = xs_scat
  abs_xs = xs_abs
  chi_xs = xs_chi
  sfnf_xs = xs_sfnf
  sf_xs = xs_sf

  return

end subroutine

subroutine draw_assembly()
  use Config, only : str_len
  use DrawMod, only : DrawAssembly
  use GlobalAssemblyMod, only : assembly_type, sym, nRow, nCol, kart, neighCell, neighSide, &
                                nzones, nsides, cell_vertx, cell_verty, cell_rad, dpitch,   &
                                assmb_pitch, verbosity

  implicit none

  character(str_len) :: nameOfSVG

  nameOfSVG = "image_assembly.svg"
  if (verbosity > 0) then
    write(*,*) "nameOfSVG:", nameOfSVG
    write(*,*) "assembly_type:", assembly_type
    write(*,*) "sym:", sym
    write(*,*) "nRow:", nRow
    write(*,*) "nCol:", nCol
    write(*,*) "kart:", kart
    write(*,*) "neighCell:", neighCell
    write(*,*) "neighSide:", neighSide
    write(*,*) "nzones:", nzones
    write(*,*) "nsides:", nsides
    write(*,*) "cell_vertx:", cell_vertx
    write(*,*) "cell_verty:", cell_verty
    write(*,*) "cell_rad:", cell_rad
    write(*,*) "dpitch:", dpitch
    write(*,*) "assmb_pitch:", assmb_pitch
  end if
  call DrawAssembly(nameOfSVG, assembly_type, sym, nRow, nCol, kart, neighCell, neighSide, &
                      nzones, nsides, cell_vertx, cell_verty, cell_rad, dpitch, assmb_pitch)
  return

end subroutine

subroutine print_beta(ig, icell)
  use CPMod, only : b_mg
  implicit none

  integer, intent(in) :: ig
  integer, intent(in) :: icell

  integer :: i

  write(*,*) "Surface to surface transmission probability for cell ", icell, " group ", ig

  write(*,*) "in_side, in_seg, in_sect, out_side, out_seg, out_sect, beta:"
  do i = 1, b_mg(ig)%nbetaelem(icell)
    write(*,*) b_mg(ig)%insidebet(icell,i), b_mg(ig)%insegbet(icell,i), b_mg(ig)%insectbet(icell,i), &
               b_mg(ig)%outsidebet(icell,i), b_mg(ig)%outsegbet(icell,i), b_mg(ig)%outsectbet(icell,i), &
               b_mg(ig)%beta(icell, i)
  end do

end subroutine

subroutine print_gamma(ig, icell)
  use GlobalAssemblyMod, only : nsides, cell_nsegments, nPhi, nzones, nharms
  use CPMod, only : g_mg
  implicit none

  integer, intent(in) :: ig
  integer, intent(in) :: icell

  integer :: s, n, m, i, k

  write(*,*) "Surface to region collision probabilities for cell ", icell, " group", ig

  write(*,*) "in_side, in_seg, in_sect, in_zone, in_harm, gamma"
  do s = 1, nsides(icell)
    do n = 1, cell_nsegments(icell, s)
      do m = 1, nPhi
        do i = 1, nzones(icell)
          do k = 1, nharms(icell)
            write(*,*) s, n, m, i, k, g_mg(ig)%g(icell,i,k,s,n,m,1)
          end do
        end do
      end do
    end do
  end do

end subroutine

subroutine print_v(ig, icell)
  use CPMod, only : v_mg
  use GlobalAssemblyMod, only : nzones, nsides, cell_nsegments, nPhi, nharms
  implicit none

  integer, intent(in) :: ig
  integer, intent(in) :: icell

  integer :: s, n, m, i, k, l

  write(*,*) "Region to surface escape probabilities for cell ", icell, " group", ig

  write(*,*) "from_zone, from_harm, out_side, out_seg, out_sect, v:"
  do i = 1, nzones(icell)
    do k = 1, nharms(icell)
      do s = 1, nsides(icell)
        do n = 1, cell_nsegments(icell,s)
          do m = 1, nPhi
            write(*,*) i, k, s, n, m, v_mg(ig)%v(icell,s,n,m,1,i,k)
          end do
        end do
      end do
    end do
  end do

end subroutine

subroutine print_u(ig, icell)
  use CPMod, only : u_mg
  use GlobalAssemblyMod, only : nzones, nharms
  implicit none

  integer, intent(in) :: ig
  integer, intent(in) :: icell

  integer :: i1, k1, i, k

  write(*,*) "Region to region collision probability for cell ", icell, " group", ig

  write(*,*) "zone_from, harm_from, zone_to, harm_to:"

  do i1 = 1, nzones(icell)
    do k1 = 1, nharms(icell)
      do i = 1, nzones(icell)
        do k = 1, nharms(icell)
          write(*,*) i1, k1, i, k, u_mg(ig)%u(icell, i1, k1, i, k)
        end do
      end do
    end do
  end do

end subroutine
