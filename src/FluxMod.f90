module FluxMod
  use precision_mod

  real(rp), allocatable :: Qf_p(:,:,:)
  real(rp), allocatable :: Qf(:,:,:)
  integer :: max_elem_bet
  integer, allocatable :: inside(:,:)
  integer, allocatable :: inseg(:,:)
  integer, allocatable :: insect(:,:)
  integer, allocatable :: outside(:,:)
  integer, allocatable :: outseg(:,:)
  integer, allocatable :: outsect(:,:)
  integer, allocatable :: thetasect(:,:)
  real(rp), allocatable :: beta(:,:)
  real(rp), allocatable :: vv(:,:,:,:,:,:,:)

  integer, allocatable :: MC(:,:,:,:,:)

  integer,  allocatable :: ind_g(:,:)
  integer,  allocatable :: ind_v(:,:)
  integer,  allocatable :: ind_b(:,:)
  integer,  allocatable :: ind_b_full(:,:,:)
  integer,  allocatable :: ind_g_full(:,:,:)
  integer,  allocatable :: ind_v_full(:,:,:)
  real(rp), allocatable :: val_b(:)
  real(rp), allocatable :: val_rc(:)
  real(rp), allocatable :: val_g(:)
  real(rp), allocatable :: val_v(:)
  real(rp), allocatable :: jin(:,:,:)

  type current
    real(rp), allocatable :: curr(:,:,:,:)
  end type

  integer, allocatable :: recipr_ind_table(:,:)

  contains

  subroutine GetFlux(nZoneMax, nHarm, nSide, nSeg, nSect, nTheta, lenSeg, chi, eps, St, Ss, &
                     vol, q0, mat, nElemBet, sFromBet, nFromBet, mFromBet, sToBet, nToBet, &
                     mToBet, thetaSect, Bet, G, V, U, flux)
    implicit none

    integer, intent(in) :: nZoneMax
    integer, intent(in) :: nHarm
    integer, intent(in) :: nSide
    integer, intent(in) :: nSeg(:)
    integer, intent(in) :: nSect
    integer, intent(in) :: nTheta
    real(rp), intent(in) :: lenSeg(:,:)
    real(rp), intent(in) :: chi(:)
    real(rp), intent(in) :: eps(:)
    real(rp), intent(in) :: vol(:)
    real(rp), intent(in) :: q0(:)
!    real(rp), intent(in) :: Stot(:)
!    real(rp), intent(in) :: Sscat(:)
    real(rp), intent(in) :: St(:)
    real(rp), intent(in) :: Ss(:)
    integer, intent(in) :: mat(:)
    integer, intent(in) :: nElemBet
    integer, intent(in) :: sFromBet(:)
    integer, intent(in) :: nFromBet(:)
    integer, intent(in) :: mFromBet(:)
    integer, intent(in) :: sToBet(:)
    integer, intent(in) :: nToBet(:)
    integer, intent(in) :: mToBet(:)
    integer, intent(in) :: thetaSect(:)
    real(rp), intent(in) :: Bet(:)
    real(rp), intent(in) :: G(:,:,:,:,:,:)
    real(rp), intent(in) :: V(:,:,:,:,:,:)
    real(rp), intent(in) :: U(:,:,:,:)

    real(rp), intent(out) :: flux(:)

    real(rp) :: Jout(nSide,maxval(nSeg),nSect,nTheta)
    real(rp) :: Jin(nSide,maxval(nSeg),nSect,nTheta)

    real(rp) :: Sabs(nZoneMax), Stot(nZoneMax), Sscat(nZoneMax), qq0(nZoneMax)
    real(rp) :: Qt, Qa, ra
    real(rp) :: Q(nZoneMax, nHarm), F(nZoneMax, nHarm), FQ(nZoneMax, nHarm)

    real(rp) :: r1, r2, r3
    real(rp) :: tol
    integer :: i, n, m, s, k, k1
    integer :: n1, m1, s1, i1, l
    integer :: iter, ierr
    integer :: iterMax
    logical :: flagRen

    flagRen = .false.
! Initialization of the arrays and variables
    Sabs = 0.0_rp
    Stot = 0.0_rp
    Sscat = 0.0_rp
    qq0  = 0.0_rp
    do i = 1, nZoneMax
      Stot(i) = St(mat(i))
      Sscat(i) = Ss(mat(i))
      qq0(i)  = q0(mat(i))
    end do
    do i = 1, nZoneMax
        Sabs(i) = Stot(i) - Sscat(i)
    end do
    F =  0.0_rp
    Q =  0.0_rp
    Jin = 0.0_rp
    Jout = 0.0_rp
    Qt = 0.0_rp
    Qa = 0.0_rp
    FQ = 0.0_rp
    do i = 1, nZoneMax
        Qt = Qt + qq0(i)*Vol(i)
        Qa = Qa + Sabs(i)*Vol(i)
    end do
    do i = 1, nZoneMax
      Q(i,1) = (Qt/Qa)*Sscat(i) + qq0(i)
      F(i,1) = Qt/Qa
    end do

    do s = 1, nSide
        do n = 1, nSeg(s)
            do m = 1, nSect
              do l = 1, nTheta
                Jout(s,n,m,l) = 0.25_rp*(Qt/Qa)*lenSeg(s,n)*chi(m)*eps(l)
              end do
            end do
        end do
    end do
! End of the initialization
!---------------- Begining of the iterations ---------------------!
    iterMax = 1000
    iter = 0
    tol = 1.0d-5
    do
        ierr = 0
        iter = iter + 1
        do s = 1, nSide
            do n = 1, nSeg(s)
                do m = 1, nSect
                    m1 = nSect - m + 1
                    do l = 1, nTheta
                      Jin(s,n,m,l) = Jout(s,n,m1,l)
                    end do
                end do
            end do
        end do
! Part of flux from gamma
        FQ = 0.0_rp
        do i = 1, nZoneMax
            do k = 1, nHarm
                do s = 1, nSide
                    do n = 1, nSeg(s)
                        do m = 1, nSect
                            do l = 1, nTheta
                              if (abs(G(i,k,s,n,m,l)) <= 1.0d-8) cycle
                              FQ(i,k) = FQ(i,k) + G(i,k,s,n,m,l)*Jin(s,n,m,l)
                            end do
                        end do
                    end do
                end do
            end do
        end do

        where (abs(FQ(1:nZoneMax,1:nHarm)) <= 1.0d-8)
          FQ(1:nZoneMax,1:nHarm) = 0.0_rp
        end where

         do i = 1, nZoneMax
           do k = 1, nHarm
             r3 = vol(i)*Stot(i)
             r1 = FQ(i,k)
             do i1 = 1, nZoneMax
               do k1 = 1, nHarm
                 r2 = vol(i1)
                 r1 = r1 + Q(i1,k1)*r2*U(i,k,i1,k1)
               end do
             end do
             r1 = r1/r3
             if (ierr == 0 .and. k == 1) then
               if (abs(F(i,1)/r1 - 1.) > tol) ierr = 1
             end if
             if (k == 1) then
              Q(i,k) = r1*Sscat(i) + QQ0(i)
             else
              Q(i,k) = r1*Sscat(i)
             end if
             F(i,k) = r1
           end do
         end do

! Currents going out of the cell (part from current (in))
        Jout = 0.0_rp
        do i = 1, nElemBet
            s1 = sFromBet(i)
            n1 = nFromBet(i)
            m1 = mFromBet(i)
            s = sToBet(i)
            n = nToBet(i)
            m = mToBet(i)
            l = thetaSect(i)
            Jout(s,n,m,l) = Jout(s,n,m,l) + Jin(s1,n1,m1,l)*Bet(i)
        end do
! Currents going out of the cell (part from source)
        do i = 1, nZoneMax
          do k = 1, nHarm
            do s = 1, nSide
              do n = 1, nSeg(s)
                do m = 1, nSect
                  do l = 1, nTheta
                    if (abs(V(s,n,m,l,i,k)) <= 1.0d-8) cycle
                    Jout(s,n,m,l) = Jout(s,n,m,l) + Q(i,k)*V(s,n,m,l,i,k)*Vol(i)
                  end do
                end do
              end do
            end do
          end do
        end do

        if (flagRen) then
          ra = sum(Vol*Sabs*F(1:nZoneMax,1))
          r1 = Qt/ra
          F(1:nZoneMax,1) = r1*F(1:nZoneMax,1)
          Jout = r1*Jout
        end if
        if (ierr == 0 .or. iter > iterMax) then
            exit
        end if
    end do

     flux(1:nZoneMax) = F(1:nZoneMax,1)

     return

  end subroutine GetFlux

  subroutine GetFluxAssembly()
    use GlobalAssemblyMod
    use NeighboursMod, only : GetNeighbours

    implicit none

    real(rp), allocatable :: F(:,:,:) ! F(nCell,maxval(nzones),maxval(nharms))
    real(rp), allocatable :: FQ(:,:,:) ! F(nCell,maxval(nzones),maxval(nharms))
    real(rp), allocatable :: Q(:,:,:) ! Q(nCell,maxval(nzones),maxval(nharms))
    real(rp), allocatable :: Jin(:,:,:,:) ! Jin(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta)
    real(rp), allocatable :: Jout(:,:,:,:,:) ! Jin(nCell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta)
    real(rp), allocatable :: Qt(:) !
    real(rp), allocatable :: Qa(:)
    real(rp), allocatable :: qq0(:,:) ! qq0(nCell,maxval(nzones))
    real(rp), allocatable :: St(:,:)  ! St()
    real(rp), allocatable :: Sa(:,:)  ! Sa()
    real(rp), allocatable :: Ss(:,:)  ! Ss()

    integer :: icell, imat, iz
    integer :: i, k, s, n, m, l
    integer :: i1, k1, s1, n1, m1, m2
    integer :: iterMax, iter, ierr
    integer :: nSect, icell1
    real(rp) :: tol, r1, r2, r3, ra, sqt
    real(rp) :: sglngth

    allocate(F(nCell,maxval(nzones),maxval(nharms)))
    allocate(FQ(nCell,maxval(nzones),maxval(nharms)))
    allocate(Q(nCell,maxval(nzones),maxval(nharms)))
    allocate(Qt(nCell))
    allocate(Qa(nCell))
    allocate(Jin(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    allocate(Jout(nCell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    allocate(qq0(nCell,maxval(nzones)))
    allocate(St(nCell,maxval(nzones)))
    allocate(Sa(nCell,maxval(nzones)))
    allocate(Ss(nCell,maxval(nzones)))

    allocate(cell_flux(1,nCell,maxval(nzones),maxval(nharms)))
    cell_flux = 0.0_rp

    F =  0.0_rp
    Q =  0.0_rp
    Jin = 0.0_rp
    Jout = 0.0_rp
    Qt = 0.0_rp
    Qa = 0.0_rp
    FQ = 0.0_rp
    qq0 = 0.0_rp
    St = 0.0_rp
    Sa = 0.0_rp
    Ss = 0.0_rp

    do icell = 1, nCell
      do iz = 1, nzones(kart(icell))
        imat = cell_mat(kart(icell),iz)
        qq0(icell,iz) = SOUR(imat,1)
        St(icell,iz) = STOT(imat,1)
        Sa(icell,iz) = SABS(imat,1)
        Ss(icell,iz) = SSCA(imat,1,1)
      end do
    end do

    ! Initial initialization of the fluxes and currents in the regions of the cells
    do icell = 1, nCell
      do iz = 1, nzones(kart(icell))
        Qt(icell) = Qt(icell) + qq0(icell,iz)*cell_vol(kart(icell),iz)
        Qa(icell) = Qa(icell) + Sa(icell,iz)*cell_vol(kart(icell),iz)
      end do
    end do
    do icell = 1, nCell
      do iz = 1, nzones(kart(icell))
        Q(icell,iz,1) = (Qt(icell)/Qa(icell))*Ss(icell,iz) + qq0(icell,iz)
        F(icell,iz,1) = Qt(icell)/Qa(icell)
      end do
    end do

    do icell = 1, nCell
      do s = 1, nsides(kart(icell))
        do n = 1, cell_nsegments(kart(icell),s)
          sglngth = cell_segsdarr(kart(icell),s,n+1) - cell_segsdarr(kart(icell),s,n)
          do m = 1, nPhi
            do l = 1, nTheta
              Jout(icell,s,n,m,l) = 0.25_rp*(Qt(icell)/Qa(icell))*sglngth*chi(m)*eps(l)
            end do
          end do
        end do
      end do
    end do
! End of the initialization

!---------------- Begining of the iterations ---------------------!
    iterMax = 1000
    iter = 0
    tol = 1.0d-5
    nSect = nPhi
! Debug printing
    if (nCell == 1) then
      open(101, file = "results/debug_flux_1cell.out")
    else
      open(101, file = "results/debug_flux_7cell.out")
    end if
! --------------
    do
      ierr = 0
      iter = iter + 1
      if (mod(iter,10) == 0) write(*,'(1a,i3)') "Iteration ", iter
      do icell = 1, nCell
        if (sym < 0) then
          do s = 1, nsides(kart(icell))
            iCell1 = neighCell(s,icell)
            s1 = neighSide(s,icell)
            do n = 1, cell_nsegments(kart(icell),s)
              n1 = cell_nsegments(kart(icell),s) - n + 1
              if (iCell1 == 0) then
                n1 = n
                do m = 1, nPhi
                  m1 = nPhi - m + 1
                  do l = 1, nTheta
                    ! Debug
                    ! if (iCell == 1 .and. iter == 1) write(101,*) Jout(icell,s,n1,m1,l)
                    !------
                    Jin(s,n,m,l) = Jout(icell,s,n1,m1,l)
                  end do
                end do
              else
                do m = 1, nPhi
                  do l = 1, nTheta
                    ! Debug
                    ! if (iCell == 1 .and. iter == 1) write(101,*) Jout(icell1,s1,n1,m,l)
                    !------
                    Jin(s,n,m,l) = Jout(icell1,s1,n1,m,l)
                  end do
                end do
              end if
            end do
          end do
        else
          do s = 1, nsides(kart(icell))
            s1 = neighSide(s,icell)
            icell1 = neighCell(s,icell)
            if (s1 < 0) then
              s1 = -s1
              m2 = -1
            else
              m2 = 1
            end if
            do m = 1, nPhi
              if (m2 < 0) then
                m1 = nPhi - m + 1
              else
                m1 = m
              end if
              do n = 1, cell_nsegments(kart(icell),s)
                if (m2 < 0) then
                  n1 = n
                  do l = 1, nTheta
                    Jin(s,n,m,l) = 0.
                  end do
                  cycle
                else
                  n1 = cell_nsegments(kart(icell),s) - n + 1
                end if
                do l = 1, nTheta
                  Jin(s,n,m,l) = Jout(icell1,s1,n1,m1,l) !*1.013
                end do
              end do
            end do
          end do
        end if

!      do icell = 1, nCell
!        do s = 1, nsides(icell)
!          icell1 = neighCell(s,icell)
!          s1 = neighSide(s,icell)
!          do n = 1, cell_nsegments(icell,s)
!            do m = 1, nSect
!              if (s1 > 0) then
!                m1 = nSect - m + 1
!              else
!                s1 = -s1
!                m1 = m
!              end if
!              do l = 1, nTheta
!                Jin(icell,s,n,m,l) = Jout(icell1,s1,n,m1,l)
!              end do
!            end do
!          end do
!        end do
!      end do
! Part of flux from gamma
        FQ(icell,:,:) = 0.0_rp
        do i = 1, nzones(kart(icell))
            do k = 1, nharms(kart(icell))
                do s = 1, nsides(kart(icell))
                    do n = 1, cell_nsegments(kart(icell),s)
                        do m = 1, nPhi
                            do l = 1, nTheta
                              if (abs(cell_G(kart(icell),i,k,s,n,m,l)) <= 1.0d-8) cycle
                              FQ(icell,i,k) = FQ(icell,i,k) + cell_G(kart(icell),i,k,s,n,m,l)*Jin(s,n,m,l)
                              ! Debug
                              ! if (iCell == 1 .and. iter == 1) write(101,*) cell_G(kart(icell),i,k,s,n,m,l), Jin(s,n,m,l)
                              !------
                            end do
                        end do
                    end do
                end do
            end do
        end do

        where (abs(FQ) <= 1.0d-8)
          FQ = 0.0_rp
        end where

        do i = 1, nzones(kart(icell))
          do k = 1, nharms(kart(icell))
            r3 = cell_vol(kart(icell),i)*St(icell,i)
            r1 = FQ(icell,i,k)
            do i1 = 1, nzones(kart(icell))
              do k1 = 1, nharms(kart(icell))
                r2 = cell_vol(kart(icell),i1)
                r1 = r1 + Q(icell,i1,k1)*r2*cell_U(kart(icell),i,k,i1,k1)
                ! Debug
                ! if (iCell == 1 .and. iter == 1) write(101,*) Q(icell,i1,k1),r2,cell_U(kart(icell),i,k,i1,k1)
                !------
              end do
            end do
            r1 = r1/r3
            if (ierr == 0 .and. k == 1) then
              if (abs(F(icell,i,1)/r1 - 1.) > tol) ierr = 1
            end if
            if (k == 1) then
              Q(icell,i,k) = r1*Ss(icell,i) + QQ0(icell,i)
                ! Debug
                ! if (iCell == 1 .and. iter == 1) write(101,*) r1,Ss(icell,i),QQ0(icell,i)
                !------
            else
              Q(icell,i,k) = r1*Ss(icell,i)
            end if
              F(icell,i,k) = r1
          end do
        end do

! Currents going out of the cell (part from current (in))
        Jout(icell,:,:,:,:) = 0.0_rp
!        r1 = 1.0_rp
!        do s = 1, nsides(icell)
!          do n = 1, cell_nsegments(kart(icell),1)
!            do m = 1, nPhi
!              Jin(s,n,m,1) = r1
!              r1 = r1 + 1.0_rp
!            end do
!          end do
!        end do
        do i = 1, cell_nbetaelem(kart(icell))
          s1 = cell_insidebet(kart(icell),i)
          n1 = cell_insegbet(kart(icell),i)
          m1 = cell_insectbet(kart(icell),i)
          s = cell_outsidebet(kart(icell),i)
          n = cell_outsegbet(kart(icell),i)
          m = cell_outsectbet(kart(icell),i)
          l = cell_thetasectbet(kart(icell),i)
          Jout(icell,s,n,m,l) = Jout(icell,s,n,m,l) + Jin(s1,n1,m1,l)*cell_beta(kart(icell),i)
          ! Debug
          ! if (iCell == 1 .and. iter == 1) write(101,*) Jin(s1,n1,m1,l),cell_beta(kart(icell),i)
          !------
        end do

! Currents going out of the cell (part from source)
        do i = 1, nzones(kart(icell))
          do k = 1, nharms(kart(icell))
            do s = 1, nsides(kart(icell))
              do n = 1, cell_nsegments(kart(icell),s)
                do m = 1, nPhi
                  do l = 1, nTheta
                    if (abs(cell_V(kart(icell),s,n,m,l,i,k)) <= 1.0d-8) cycle
                    Jout(icell,s,n,m,l) = Jout(icell,s,n,m,l) + &
                                          Q(icell,i,k)*cell_V(kart(icell),s,n,m,l,i,k)*cell_vol(kart(icell),i)
                    ! Debug
                    ! if (iCell == 1 .and. iter == 1) write(101,*) Q(icell,i,k),cell_V(kart(icell),s,n,m,l,i,k),cell_vol(kart(icell),i)
                    !------
                  end do
                end do
              end do
            end do
          end do
        end do

      end do ! End of cycle on cells

      ! Renormalization of the fluxes and currents if required
      if (flagRen) then
        do icell = 1, nCell
          ra = sum(cell_vol(kart(icell),:)*Sa(icell,:)*F(icell,1:nzones(kart(icell)),1))
        end do
        r1 = sum(Qt)/ra
        do icell = 1, nCell
          F(icell,1:nzones(kart(icell)),1) = r1*F(icell,1:nzones(kart(icell)),1)
          Jout(icell,:,:,:,:) = r1*Jout(icell,:,:,:,:)
        end do
      end if

      ! Checking the convergence criteria. If converged - finish the iterations
!      write(*,*) "Iteration ", iter
!      write(*,*) "Flux = "
!      do icell = 1, nCell
!        write(*,'(1i4,3F10.5)') icell, (F(icell,i,1),i=1,nzones(kart(icell)))
!      end do
!      read(*,*)
      if (ierr == 0 .or. iter > iterMax) then
        exit
      end if
    end do

    do icell = 1, nCell
      do k = 1, nharms(kart(icell))
        cell_flux(1,icell,1:nzones(kart(icell)),k) = F(icell,1:nzones(kart(icell)),k) !/fact(icell)
      end do
    end do

    write(*,'(1a,i3)') "Total number of iterations is ", iter

    return

  end subroutine GetFluxAssembly

  subroutine GetFluxAssemblyMG()
    use GlobalAssemblyMod
    implicit none

    real(rp), allocatable :: Q(:,:,:,:)  ! Source (total)
    real(rp), allocatable :: Qf_p(:,:,:) ! Fission source on the previous iteration step
    real(rp), allocatable :: Qf(:,:,:) ! Fission source on the current iteration step
    real(rp), allocatable :: F_p(:,:,:,:) ! Flux on previous iteration step
    real(rp), allocatable :: F(:,:,:,:) ! Flux on the current iteration step
    real(rp), allocatable :: F_1g(:,:,:) ! One-group flux
    real(rp), allocatable :: Jout(:,:,:,:,:,:) !Output currents for each cell and group

    integer :: ig, ig1, ir, ic, it, imt, s, n, m, k, l
    integer :: ierr, kiter
    real(rp) :: vsa, qq, sglngth, qsum, qscat, r
    real(rp) :: finit(nGroups)
    real(rp) :: eff, effmax, effmin
    real(rp) :: fnorm
    real(rp) :: om

    om = 1.0

    allocate(cell_flux(nGroups,nCell,maxval(nzones),maxval(nharms)))
    cell_flux = 0.0_rp
! Allocating memory for the sources, fluxes, currents, etc.
    allocate(Q(nGroups,nCell,maxval(nzones),maxval(nharms)))
    Q = 0.0_rp
    allocate(Qf_p(nCell,maxval(nzones),maxval(nharms)))
    Qf_p = 0.0_rp
    allocate(Qf(nCell,maxval(nzones),maxval(nharms)))
    Qf = 0.0_rp
    allocate(F_p(nGroups,nCell,maxval(nzones),maxval(nharms)))
    F_p = 1.0_rp
    allocate(F(nGroups,nCell,maxval(nzones),maxval(nharms)))
    F = 1.0_rp
    allocate(F_1g(nCell,maxval(nzones),maxval(nharms)))
    F_1g = 0.0_rp
    allocate(Jout(nGroups,nCell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    Jout = 0.0_rp

!    do ig = 1, nGroups
!      vsa = 0.
!      qq  = 0.
!      do ic = 1, nCell
!        it = kart(ic)
!        do ir = 1, nzones(it)
!          imt = cell_mat(kart(ic),ir)
!          vsa = vsa + SABS(imt,ig)*cell_vol(it,ir) !*fact(ic)
!          qq  = qq + SOUR(imt,ig)*cell_vol(it,ir)  !*fact(ic)
!        end do
!      end do
!      finit(iG) = qq/vsa
!    end do
!! Initialization of the sources and currents
!    do ig = 1, nGroups
!      do ic = 1, nCell
!        it = kart(ic)
!        do ir = 1, nzones(it)
!          qsum = 0.
!          imt = cell_mat(kart(ic),ir)
!          do ig1 = 1, nGroups
!            qsum = qsum + finit(iG1)*SSCA(imt,iG1,iG)
!          end do
!          Q(ig,ic,ir,1) = (qsum + SOUR(imt,ig))*cell_vol(it,ir) !*fact(ic)
!        end do
!      end do
!    end do
    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do s = 1, nsides(it)
          do n = 1, cell_nsegments(it,s)
            sglngth = cell_segsdarr(kart(ic),s,n+1) - cell_segsdarr(kart(ic),s,n)
            do m = 1, nPhi
              do l = 1, nTheta
                Jout(ig,ic,s,n,m,l) = 0.25_rp*sglngth*chi(m)*eps(l)
              end do
            end do
          end do
        end do
      end do
    end do
 ! End of the initialization

    call InitialiseFlux(F)
    call InitialiseSources(F,Q)

    ierr = 1
    kiter = 1
    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
        imt = cell_mat(it,ir)
        if (SFNF(imt,ig) /= 0.) then
          Qf_p(ic,ir,1) = Qf_p(ic,ir,1) + F(ig,ic,ir,1)*SFNF(imt,iG)*cell_vol(it,ir) !*fact(ic)
        end if
        end do
      end do
    end do
!    do ig = 1, nGroups
!      do ic = 1, nCell
!        it = kart(ic)
!        do ir = 1, nzones(it)
!          imt = cell_mat(it,ir)
!          do k = 1, nharms(it)
!            Q(ig,ic,ir,k) = Qf_p(ic,ir,k)*SCHI(imt,ig)
!          end do
!        end do
!      end do
!    end do

!----------------
!    Qf_p = 1.0_rp
    eff = 1.
    kiter = 1
!    Jout = 1.0_rp
    do while (kiter <= kiter_max)
      effmax = -1.0e05
      effmin = 1.0e05
      ierr = 0
      !$omp parallel do shared(F) private(ig, F_1g)
      do ig = 1, nGroups
        call GetFluxAssembly1G(ig, Q(ig,:,:,:),Jout(ig,:,:,:,:,:),F_1g)
        F(ig,:,:,:) = F_1g
      end do
      !$omp end paralleldo

!      do ig = 1, nGroups
!        do ic = 1, nCell
!          it = kart(ic)
!          do ir = 1, nzones(it)
!            if (abs(F_p(ig,ic,ir,1)/F(ig,ic,ir,1) - 1.0_rp) >= eps_f) ierr = 1
!          end do
!        end do
!      end do

!----------------------------
! Recalculation of the sources
!----------------------------
      Qf = 0.
      do ig = 1, nGroups
        do ic = 1, nCell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it,ir)
            do k = 1, nharms(it)
              Qf(ic,ir,k) = Qf(ic,ir,k) + F(ig,ic,ir,k)*SFNF(imt,iG)
            end do
          end do
        end do
      end do

      if (maxval(SOUR) > 1.0e-8) then
        do ig = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
!              qscat = 0.0_rp
!              do ig1 = 1, nGroups
!                qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,1)
!              end do
              do k = 1, nharms(it)
                qscat = 0.0_rp
                do ig1 = 1, nGroups
                  qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
                end do
                Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k)+SOUR(imt,iG))*cell_vol(it,ir) !*fact(ic)
              end do
            end do
          end do
        end do
      else
        do iG = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              if (SFNF(imt,ig) /= 0.0_rp) then
                r = Qf(ic,ir,1)/Qf_p(ic,ir,1)
                if (r > effmax) then
                  effmax = r
                end if
                if (r < effmin) then
                  effmin = r
                end if
              end if
            end do
          end do
        end do
        if (abs((effmax + effmin)*0.5 - eff) <= eps_k .and. abs(effmax-effmin) <= eps_k) then
          exit
        else
          eff = (effmax + effmin)*0.5
          Qf = Qf/eff
        end if
        Qf_p = Qf
        do ig = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              do k = 1, nharms(it)
                qscat = 0.0_rp
                do ig1 = 1, nGroups
                  qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
                end do
                Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k))*cell_vol(it,ir) !*fact(ic)
              end do
            end do
          end do
        end do
      end if
      F_p = F
      if (int(kiter/10)*10 == kiter) write(*,'(1i5,3f10.5)') kiter, effmin, effmax, eff
      kiter = kiter + 1
    end do
    write(*,'(1i5,3f10.5)') kiter, effmin, effmax, eff
    keff = eff
    cell_flux(:,:,:,:) = F(:,:,:,:)

    allocate(cell_curr(nGroups,nCell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    cell_curr = Jout

!    fnorm = sqrt(sum(F**2))
!    write(*,*) fnorm
!    do ic = 1, nCell
!      write(*,*) "Cell ", ic
!      it = kart(ic)
!      do ig = 1, nGroups
!        do ir = 1, nzones(it)
!          imt = cell_mat(it,ir)
!          do k = 1, nharms(it)
!            write(*,'(3i5,1f13.8)') ig, ir, k, F(ig,ic,ir,k)/fnorm
!          end do
!        end do
!      end do
!    end do

  end subroutine GetFluxAssemblyMG

  subroutine GetFluxAssembly1g(ig,Q,Jout,F)
    use GlobalAssemblyMod, only : nCell, nPhi, nTheta, sym, nzones, nharms, nsides, &
                                  cell_nsegments, kart, cell_mat, STOT, SABS,       &
                                  neighCell, neighSide, cell_vol
    use CPMod, only : g_mg, u_mg, b_mg, v_mg
    implicit none

    integer, intent(in)    :: ig
    real(rp), intent(in)    :: Q(:,:,:)
    real(rp), intent(inout) :: Jout(:,:,:,:,:)
    real(rp), intent(out) :: F(:,:,:)

    real(rp), allocatable :: FQ(:,:,:) ! F(nCell,maxval(nzones),maxval(nharms))
    real(rp), allocatable :: Jin(:,:,:,:) ! Jin(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta)
    real(rp), allocatable :: St(:,:)  ! St()
    real(rp), allocatable :: Sa(:,:)  ! Sa()
    real(rp), allocatable :: Ss(:,:)  ! Ss()

    integer :: icell, imat, iz
    integer :: i, k, s, n, m, l
    integer :: ic, it, ir
    integer :: i1, k1, s1, n1, m1, m2
    integer :: iterMax, iter, ierr
    integer :: nSect, icell1
    real(rp) :: tol, r1, r2, r3, ra, sqt
    real(rp) :: sglngth, flu, ff, qq, bb, brc

    allocate(FQ(nCell,maxval(nzones),maxval(nharms)))
    allocate(Jin(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    allocate(St(nCell,maxval(nzones)))
    allocate(Sa(nCell,maxval(nzones)))
    allocate(Ss(nCell,maxval(nzones)))

!    allocate(cell_flux(nCell,maxval(nzones),maxval(nharms)))
!    cell_flux = 0.0_rp

    F =  0.0_rp
    Jin = 0.0_rp
    FQ = 0.0_rp
    St = 0.0_rp
    Sa = 0.0_rp
    Ss = 0.0_rp

    do icell = 1, nCell
      do iz = 1, nzones(kart(icell))
        imat = cell_mat(kart(icell),iz)
        St(icell,iz) = STOT(imat,ig)
        Sa(icell,iz) = SABS(imat,ig)
      end do
    end do

!---------------- Begining of the iterations ---------------------!
    iterMax = 1000
    iter = 0
    tol = 1.0d-5
    nSect = nPhi

      do icell = 1, nCell
        it = kart(icell)
        if (sym < 0) then
          do s = 1, nsides(it)
            iCell1 = neighCell(s,icell)
            s1 = neighSide(s,icell)
            do n = 1, cell_nsegments(it,s)
              n1 = cell_nsegments(it,s) - n + 1
              if (iCell1 == 0) then
                n1 = n
                do m = 1, nPhi
                  m1 = nPhi - m + 1
                  do l = 1, nTheta
                    Jin(s,n,m,l) = Jout(icell,s,n1,m1,l)
                  end do
                end do
              else
                do m = 1, nPhi
                  do l = 1, nTheta
                    Jin(s,n,m,l) = Jout(icell1,s1,n1,m,l)
                  end do
                end do
              end if
            end do
          end do
        else
          do s = 1, nsides(it)
            s1 = neighSide(s,icell)
            icell1 = neighCell(s,icell)
            if (s1 < 0) then
              s1 = -s1
              m2 = -1
            else
              m2 = 1
            end if
            do m = 1, nPhi
              if (m2 < 0) then
                m1 = nPhi - m + 1
              else
                m1 = m
              end if
              do n = 1, cell_nsegments(it,s)
                if (m2 < 0) then
                  n1 = n
                else
                  n1 = cell_nsegments(it,s) - n + 1
                end if
                do l = 1, nTheta
                  Jin(s,n,m,l) = Jout(icell1,s1,n1,m1,l)
                end do
              end do
            end do
          end do
        end if

! Part of flux from gamma
!        FQ(icell,:,:) = 0.0_rp
        do i = 1, nzones(it)
          do k = 1, nharms(it)
            ff = 0.0_rp
            do s = 1, nsides(it)
              do n = 1, cell_nsegments(it,s)
                do m = 1, nPhi
                  do l = 1, nTheta
                    if (abs(g_mg(ig)%g(it,i,k,s,n,m,l)) <= 1.0d-8) cycle
                    ff = ff + g_mg(ig)%g(it,i,k,s,n,m,l)*Jin(s,n,m,l)
                  end do
                end do
              end do
            end do
            FQ(icell,i,k) = ff
          end do
        end do

!        where (abs(FQ) <= 1.0d-8)
!          FQ = 0.0_rp
!        end where

!        do i = 1, nzones(it)
!          do k = 1, nharms(it)
!            r3 = cell_vol(it,i)*St(icell,i)
!            r1 = FQ(icell,i,k)
!            do i1 = 1, nzones(it)
!              do k1 = 1, nharms(it)
!                r2 = cell_vol(it,i1)
!                r1 = r1 + Q(icell,i1,k1)*r2*u_mg(ig)%u(it,i,k,i1,k1)
!              end do
!            end do
!            r1 = r1/r3
!            F(icell,i,k) = r1
!          end do
!        end do
!        FQ= 0.0
        do i = 1, nzones(it)
          do k = 1, nharms(it)
            do i1 = 1, nzones(it)
              do k1 = 1, nharms(it)
                FQ(icell,i1,k1) = FQ(icell,i1,k1) + Q(icell,i,k)*u_mg(ig)%u(it,i1,k1,i,k)
              end do
            end do
          end do
        end do

! Currents going out of the cell (part from current (in))
!        open(1121, file='debug/beta.out')
        Jout(icell,:,:,:,:) = 0.0_rp
        do i = 1, b_mg(ig)%nbetaelem(it)
          s1 = b_mg(ig)%insidebet(it,i)
          n1 = b_mg(ig)%insegbet(it,i)
          m1 = b_mg(ig)%insectbet(it,i)
          s = b_mg(ig)%outsidebet(it,i)
          n = b_mg(ig)%outsegbet(it,i)
          m = b_mg(ig)%outsectbet(it,i)
          l = b_mg(ig)%thetasectbet(it,i)
          bb = b_mg(ig)%beta(it,i)
          brc = b_mg(ig)%brc(it,i)
!          write(1121,'(6i5,1f19.12)') i, s1, m1, s, m, (m-1)*nsides + s - 1, b_mg(ig)%beta(it,i)
          Jout(icell,s,n,m,l) = Jout(icell,s,n,m,l) + Jin(s1,n1,m1,l)*bb
          bb = bb*brc
          Jout(icell,s1,n1,m1,l) = Jout(icell,s1,n1,m1,l) + Jin(s,n,m,l)*bb
        end do
!        close(1121)
!        Jout(icell,:,:,:,:) = 0.0_rp
! Currents going out of the cell (part from source)
        do i = 1, nzones(it)
          do k = 1, nharms(it)
            qq = Q(icell,i,k)
            do s = 1, nsides(it)
              do n = 1, cell_nsegments(it,s)
                do m = 1, nPhi
                  do l = 1, nTheta
                    if (abs(v_mg(ig)%v(it,s,n,m,l,i,k)) <= 1.0d-8) cycle
                    Jout(icell,s,n,m,l) = Jout(icell,s,n,m,l) + qq*v_mg(ig)%v(it,s,n,m,l,i,k) !*cell_vol(it,i)
                  end do
                end do
              end do
            end do
          end do
        end do

      end do ! End of cycle on cells

    do ic = 1, nCell
      it = kart(ic)
      do ir = 1, nzones(it)
        imat = cell_mat(it,ir)
        do k = 1, nharms(it)
          flu = Fq(ic,ir,k)/(cell_vol(it,ir)*STOT(imat,ig))
          if (abs(flu) <= 1.0d-08) then
            F(ic,ir,k) = 0.0d0
          else
            F(ic,ir,k) = flu
          end if
        end do
      end do
    end do

    return

  end subroutine GetFluxAssembly1g

  subroutine MergeFlux(flux, vol, merge_arr, nZone, flux_merged, vol_merged, nZoneMerged)
    implicit none

    real(rp), intent(in) :: flux(:)
    real(rp), intent(in) :: vol(:)
    integer, intent(in) :: merge_arr(:)
    integer, intent(in) :: nZone

    real(rp), intent(out) :: flux_merged(nZone)
    real(rp), intent(out) :: vol_merged(nZone)
    integer, intent(out) :: nZoneMerged

    integer :: i

    nZoneMerged = maxval(merge_arr)
    flux_merged = 0.0_rp
    vol_merged  = 0.0_rp

    do i = 1, nZone
      flux_merged(merge_arr(i)) = flux_merged(merge_arr(i)) + flux(i)*vol(i)
      vol_merged(merge_arr(i)) = vol_merged(merge_arr(i)) + vol(i)
    end do

    do i = 1, nZoneMerged
      flux_merged(i) = flux_merged(i)/vol_merged(i)
    end do

  end subroutine MergeFlux

  subroutine FluxSweep(Q, Jout, F)
    use GlobalAssemblyMod, only : nGroups, nCell, kart, nzones, nharms, &
                                  nsides, cell_nsegments, nPhi, nTheta, cell_mat,   &
                                  cell_vol, STOT
    use CPMod, only : g_mg, u_mg
    implicit none

    real(rp), intent(in)    :: Q(:,:,:,:)
    real(rp), intent(in)    :: Jout(:,:,:,:,:,:)
    real(rp), intent(inout) :: F(:,:,:,:)

    real(rp) :: cin
    integer :: ig, ic, it, imat, ir, i1, k1, i, k, s, n, m, l, ic1, s1, n1, m1

    cin = 0.0_rp
    do ig = 1, nGroups ! Cycle on groups
      do ic = 1, nCell ! Cycle on cells
        F(ig,ic,:,:) = 0.0_rp
        it = kart(ic)
        ! Currents contribution into the flux
        do l = 1, nTheta
          do s = 1, nsides(kart(ic))
            do n = 1, cell_nsegments(kart(ic),s)
              do m = 1, nPhi
                ic1 = MC(ic,s,n,m,1)
                s1  = MC(ic,s,n,m,2)
                n1  = MC(ic,s,n,m,3)
                m1  = MC(ic,s,n,m,4)
                cin = Jout(ig,ic1,s1,n1,m1,l)
                do i = 1, nzones(kart(ic))
                  do k = 1, nharms(kart(ic))
!                    if (abs(g_mg(ig)%g(kart(ic),i,k,s,n,m,l)) <= 1.0d-8) cycle
!                    F(ig,ic,i,k) = F(ig,ic,i,k) + g_mg(ig)%g(it,i,k,s,n,m,l)*Jin(ig,ic,s,n,m,l)
                    F(ig,ic,i,k) = F(ig,ic,i,k) + g_mg(ig)%g(it,i,k,s,n,m,l)*cin
                  end do
                end do
              end do
            end do
          end do
        end do
        where (abs(F) <= 1.0d-8)
          F = 0.0_rp
        end where
        ! Source contribution into the flux
        do i = 1, nzones(kart(ic))
          do k = 1, nharms(kart(ic))
            do i1 = 1, nzones(kart(ic))
              do k1 = 1, nharms(kart(ic))
!                if (abs(g_mg(ig)%g(kart(ic),i,k,s,n,m,l)) <= 1.0d-8) cycle
                F(ig,ic,i1,k1) = F(ig,ic,i1,k1) + Q(ig,ic,i,k)*u_mg(ig)%u(it,i1,k1,i,k)
              end do
            end do
          end do
        end do
      end do ! End of cycle on cells
    end do ! End of cycle on groups

    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          do k = 1, nharms(it)
            F(ig,ic,ir,k) = F(ig,ic,ir,k)/(cell_vol(it,ir)*STOT(imat,ig))
          end do
        end do
      end do
    end do

  end subroutine FluxSweep

  subroutine CurrSweep(Q, Jout)
    use GlobalAssemblyMod, only : nGroups, nCell, kart, nzones, &
                                  nharms, nsides, cell_nsegments, nPhi, nTheta
    use CPMod, only : b_mg, v_mg
    implicit none

    real(rp), intent(in)    :: Q(:,:,:,:)
!    real(rp), intent(in)    :: Jin(:,:,:,:,:,:)
    real(rp), intent(inout) :: Jout(:,:,:,:,:,:)

    integer :: ig, ic, i, k, s1, n1, m1, s, n, m, l, it, s2, n2, m2, ic2, ic1
!    real(rp) :: cout(nGroups,nCell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta)
    real(rp) :: cin(maxval(nsides),maxval(cell_nsegments),nPhi,nTheta)

    do ig = 1, nGroups
      inside(:,:) = b_mg(ig)%insidebet(:,:)
      inseg(:,:) = b_mg(ig)%insegbet(:,:)
      insect(:,:) = b_mg(ig)%insectbet(:,:)
      outside(:,:) = b_mg(ig)%outsidebet(:,:)
      outseg(:,:) = b_mg(ig)%outsegbet(:,:)
      outsect(:,:) = b_mg(ig)%outsectbet(:,:)
      thetasect(:,:) = b_mg(ig)%thetasectbet(:,:)
      beta(:,:) = b_mg(ig)%beta(:,:)
      vv(:,:,:,:,:,:,:) = v_mg(ig)%v(:,:,:,:,:,:,:)
      do ic = 1, nCell
        it = kart(ic)
        do s = 1, nsides(it)
          do n = 1, cell_nsegments(it,s)
            do m = 1, nPhi
              ic1 = MC(ic,s,n,m,1)
              s1  = MC(ic,s,n,m,2)
              n1  = MC(ic,s,n,m,3)
              m1  = MC(ic,s,n,m,4)
              do l = 1, nTheta
                cin(s,n,m,l) = Jout(ig,ic1,s1,n1,m1,l)
              end do
            end do
          end do
        end do
        Jout(ig,ic,:,:,:,:) = 0.0_rp
        do i = 1, b_mg(ig)%nbetaelem(it)
          l = thetasect(it,i)
          s1 = inside(it,i)
          n1 = inseg(it,i)
          m1 = insect(it,i)
          s = outside(it,i)
          n = outseg(it,i)
          m = outsect(it,i)
!          Jout(ig,ic,s,n,m,l) = Jout(ig,ic,s,n,m,l) + Jin(ig,ic,s1,n1,m1,l)*beta(it,i)
          Jout(ig,ic,s,n,m,l) = Jout(ig,ic,s,n,m,l) + cin(s1,n1,m1,l)*beta(it,i)
!          cout(ig,ic,s,n,m,l) = cout(ig,ic,s,n,m,l) + cin*beta(it,i)
        end do
! Currents going out of the cell (part from source)
        do i = 1, nzones(it)
          do k = 1, nharms(it)
            do s = 1, nsides(it)
              do n = 1, cell_nsegments(it,s)
                do m = 1, nPhi
                  do l = 1, nTheta
                    ! if (abs(v_mg(ig)%v(kart(ic),s,n,m,l,i,k)) <= 1.0d-8) cycle
                    Jout(ig,ic,s,n,m,l) = Jout(ig,ic,s,n,m,l) + Q(ig,ic,i,k)*vv(it,s,n,m,l,i,k) !*cell_vol(it,i)
!                    cout(ig,ic,s,n,m,l) = cout(ig,ic,s,n,m,l) + Q(ig,ic,i,k)*vv(it,s,n,m,l,i,k)
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
    end do

  end subroutine

  subroutine ResendCurrents(Jout, Jin)
    use GlobalAssemblyMod, only : kart, nGroups, nCell, sym, nsides, neighCell, &
                                  neighSide, cell_nsegments, nPhi, nTheta
    implicit none

    real(rp), intent(in)    :: Jout(:,:,:,:,:,:)
    real(rp), intent(inout) :: Jin(:,:,:,:,:,:)

    integer :: ig, ic, ic1, s, n, m, l, s1, n1, m1, m2

    do ig = 1, nGroups
      do ic = 1, nCell
        if (sym < 0) then
          do s = 1, nsides(kart(ic))
            ic1 = neighCell(s,ic)
            s1 = neighSide(s,ic)
            do n = 1, cell_nsegments(kart(ic),s)
              n1 = cell_nsegments(kart(ic),s) - n + 1
              if (ic1 == 0) then
                n1 = n
                do m = 1, nPhi
                  m1 = nPhi - m + 1
                  do l = 1, nTheta
                    Jin(ig,ic,s,n,m,l) = Jout(ig,ic,s,n1,m1,l)
                  end do
                end do
              else
                do m = 1, nPhi
                  do l = 1, nTheta
                    Jin(ig,ic,s,n,m,l) = Jout(ig,ic1,s1,n1,m,l)
                  end do
                end do
              end if
            end do
          end do
        else
          do s = 1, nsides(kart(ic))
            s1 = neighSide(s,ic)
            ic1 = neighCell(s,ic)
            if (s1 < 0) then
              s1 = -s1
              m2 = -1
            else
              m2 = 1
            end if
            do m = 1, nPhi
              if (m2 < 0) then
                m1 = nPhi - m + 1
              else
                m1 = m
              end if
              do n = 1, cell_nsegments(kart(ic),s)
                if (m2 < 0) then
                  n1 = n
                else
                  n1 = cell_nsegments(kart(ic),s) - n + 1
                end if
                do l = 1, nTheta
                  Jin(ig,ic,s,n,m,l) = Jout(ig,ic1,s1,n1,m1,l)
                end do
              end do
            end do
          end do
        end if
      end do
    end do
  end subroutine

  subroutine UpdateSources(F, Q, effmax, effmin, eff)
    use GlobalAssemblyMod, only : nGroups, nzones, nharms, kart, cell_mat, &
                                  SFNF, SOUR, SSCA, SCHI, cell_vol, nCell
    implicit none

    real(rp), intent(in)    :: F(:,:,:,:)
    real(rp), intent(inout) :: Q(:,:,:,:)
    real(rp), intent(out)   :: effmax
    real(rp), intent(out)   :: effmin
    real(rp), intent(out)   :: eff

    real(rp) :: qscat, r
    integer :: ig, ic, it, ir, imt, k, ig1

    Qf = 0.0_rp
    effmax = -10000.0_rp
    effmin = 10000.0_rp

    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
          imt = cell_mat(it,ir)
          do k = 1, nharms(it)
            Qf(ic,ir,k) = Qf(ic,ir,k) + F(ig,ic,ir,k)*SFNF(imt,iG)
          end do
        end do
      end do
    end do

    if (maxval(SOUR) > 1.0e-8) then
      do ig = 1, nGroups
        do ic = 1, nCell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it,ir)
            do k = 1, nharms(it)
              qscat = 0.0_rp
              do ig1 = 1, nGroups
                qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
              end do
              Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k)+SOUR(imt,iG))*cell_vol(it,ir) !*fact(ic)
            end do
          end do
        end do
      end do
    else
      do iG = 1, nGroups
        do ic = 1, nCell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it,ir)
            if (SFNF(imt,ig) /= 0.0_rp) then
              r = Qf(ic,ir,1)/Qf_p(ic,ir,1)
              if (r > effmax) then
                effmax = r
              end if
              if (r < effmin) then
                effmin = r
              end if
            end if
          end do
        end do
      end do
!      if (abs((effmax + effmin)*0.5 - eff) <= eps_k .and. abs(effmax-effmin) <= eps_k) then
!        exit
!      else
        eff = (effmax + effmin)*0.5
        Qf = Qf/eff
!      end if
      Qf_p = Qf
      do ig = 1, nGroups
        do ic = 1, nCell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it,ir)
            do k = 1, nharms(it)
              qscat = 0.0_rp
              do ig1 = 1, nGroups
                qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
              end do
              Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k))*cell_vol(it,ir) !*fact(ic)
            end do
          end do
        end do
      end do
    end if

  end subroutine

  subroutine GetFluxAssemblyMG_new(itermax, tol)
    use GlobalAssemblyMod, only : nGroups, nCell, nzones, nharms, cell_nsegments, &
                                  nsides, nPhi, nTheta, cell_flux, kart,    &
                                  nCellTypes
    use CPMod, only :  b_mg
    implicit none

    integer, intent(in) :: itermax
    real(rp), intent(in) :: tol

    integer :: iter, ig, ic, it
    real(rp) :: effmax, effmin, eff, eff_old

    real(rp), allocatable :: Q(:,:,:,:)
    real(rp), allocatable :: F(:,:,:,:)
    real(rp), allocatable :: Jin(:,:,:,:,:,:)
    real(rp), allocatable :: Jout(:,:,:,:,:,:)

    allocate(Q(nGroups,ncell,maxval(nzones),maxval(nharms)))
    allocate(F(nGroups,ncell,maxval(nzones),maxval(nharms)))
    allocate(Jin(nGroups,ncell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    allocate(Jout(nGroups,ncell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    allocate(Qf(nCell,maxval(nzones),maxval(nharms)))
    allocate(Qf_p(nCell,maxval(nzones),maxval(nharms)))

    Qf = 0.0_rp
    Qf_p = 1.0_rp
    Q = 0.0_rp
    F = 0.0_rp
    Jin = 0.0_rp
    Jout = 0.0_rp
    effmax = -10000
    effmin = 10000
    eff_old = 1.0_rp

    call InitialiseFlux(F)
    call InitialiseSources(F,Q)
    call InitialiseCurrents(Jin)

    max_elem_bet = -1
    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        if (b_mg(ig)%nbetaelem(it) > max_elem_bet) then
          max_elem_bet = b_mg(ig)%nbetaelem(it)
        end if
      end do
    end do
    allocate(inside(nCellTypes,max_elem_bet))
    allocate(inseg(nCellTypes,max_elem_bet))
    allocate(insect(nCellTypes,max_elem_bet))
    allocate(outside(nCellTypes,max_elem_bet))
    allocate(outseg(nCellTypes,max_elem_bet))
    allocate(outsect(nCellTypes,max_elem_bet))
    allocate(thetasect(nCellTypes,max_elem_bet))
    allocate(beta(nCellTypes,max_elem_bet))
    allocate(vv(nCellTypes,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta,maxval(nzones),maxval(nharms)))
    allocate(MC(nCell,maxval(nsides),maxval(cell_nsegments),nPhi,4))
    inside = 0
    inseg = 0
    insect = 0
    outside = 0
    outseg = 0
    outsect = 0
    thetasect = 0
    MC = 0
    beta = 0.0_rp
    vv = 0.0_rp

    call MakeConnectivityMatrix(MC)

    do iter = 1, itermax
      call FluxSweep(Q, Jout, F)
      call UpdateSources(F, Q, effmax, effmin, eff)
      if (abs(eff_old - eff) <= tol .and. abs(effmax-effmin) <= tol) then
        exit
      end if
      if (int(iter/10)*10 == iter) write(*,'(1i5,4f10.5)') iter, effmin, effmax, eff, (eff - eff_old)
      eff_old = eff
      call CurrSweep(Q, Jout)
!      call ResendCurrents(Jout, Jin)
    end do
    write(*,'(1i5,3f10.5)') iter, effmin, effmax, eff

    deallocate(inside)
    deallocate(inseg)
    deallocate(insect)
    deallocate(outside)
    deallocate(outseg)
    deallocate(outsect)
    deallocate(thetasect)
    deallocate(MC)
    deallocate(beta)
    deallocate(vv)

    allocate(cell_flux(nGroups,nCell,maxval(nzones),maxval(nharms)))
    cell_flux = F

  end subroutine

  subroutine InitialiseFlux(F)
    use GlobalAssemblyMod, only : nGroups, ncell, kart, nzones, &
                                  cell_mat, cell_vol, SABS, SFNF, &
                                  STOT, SCHI
    implicit none

    real(rp), intent(inout) :: F(:,:,:,:)

    integer :: ig, ic, it, ir, imt
    real(rp) :: vsa, qq

    do ig = 1, nGroups
      vsa = 0.
      qq  = 0.
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
          imt = cell_mat(kart(ic),ir)
          vsa = vsa + SABS(imt,ig)*cell_vol(it,ir) !*fact(ic)
          qq  = qq + SFNF(imt,ig)*SCHI(imt,ig)*cell_vol(it,ir)  !*fact(ic)
          !F(ig,ic,ir,1) = qq/vsa
          F(ig,ic,ir,1) = cell_vol(it,ir)
        end do
      end do
    end do

  end subroutine

  subroutine InitialiseSources(F, Q)
    use GlobalAssemblyMod, only : nGroups, nCell, kart, nzones,   &
                                  cell_mat, cell_vol, SSCA, SOUR
    implicit none

    real(rp), intent(in)  :: F(:,:,:,:)
    real(rp), intent(out) :: Q(:,:,:,:)

    integer :: ig, ig1, ic, it, ir, imt
    real(rp) :: qsum

    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
          qsum = 0.
          imt = cell_mat(kart(ic),ir)
          do ig1 = 1, nGroups
            qsum = qsum + F(ig1,ic,ir,1)*SSCA(imt,iG1,iG)
          end do
          Q(ig,ic,ir,1) = (qsum + SOUR(imt,ig))*cell_vol(it,ir)
        end do
      end do
    end do

  end subroutine

  subroutine InitialiseCurrents(Jin)
    use GlobalAssemblyMod, only : nGroups, nCell, kart, nsides,  &
                                  cell_segsdarr, chi, eps, nPhi, &
                                  nTheta, cell_nsegments
    implicit none

    real(rp), intent(inout) :: Jin(:,:,:,:,:,:)

    integer :: ig, ic, it, s, n, m, l
    real(rp) :: sglngth

    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do s = 1, nsides(it)
          do n = 1, cell_nsegments(it,s)
            sglngth = cell_segsdarr(kart(ic),s,n+1) - cell_segsdarr(kart(ic),s,n)
            do m = 1, nPhi
              do l = 1, nTheta
                Jin(ig,ic,s,n,m,l) = 0.25_rp**sglngth*chi(m)*eps(l)
              end do
            end do
          end do
        end do
      end do
    end do

  end subroutine

  subroutine MakeConnectivityMatrix(MC)
    use GlobalAssemblyMod, only : nCell, sym, neighCell, neighSide, kart, &
                                  nsides, cell_nsegments, nPhi
    implicit none

    integer, intent(inout) :: MC(:,:,:,:,:)

    integer :: ic, s, ic1, s1, n, n1, m, m1, m2

    do ic = 1, nCell
      if (sym < 0) then
        do s = 1, nsides(kart(ic))
          ic1 = neighCell(s,ic)
          s1 = neighSide(s,ic)
          do n = 1, cell_nsegments(kart(ic),s)
            n1 = cell_nsegments(kart(ic),s) - n + 1
            if (ic1 == 0) then
              n1 = n
              do m = 1, nPhi
                m1 = nPhi - m + 1
                MC(ic,s,n1,m1,1) = ic
                MC(ic,s,n1,m1,2) = s
                MC(ic,s,n1,m1,3) = n
                MC(ic,s,n1,m1,4) = m
!                  Jin(ig,ic,s,n,m,l) = Jout(ig,ic,s,n1,m1,l)
              end do
            else
              do m = 1, nPhi
                MC(ic1,s1,n1,m,1) = ic
                MC(ic1,s1,n1,m,2) = s
                MC(ic1,s1,n1,m,3) = n
                MC(ic1,s1,n1,m,4) = m
!                  Jin(ig,ic,s,n,m,l) = Jout(ig,ic1,s1,n1,m,l)
              end do
            end if
          end do
        end do
      else
        do s = 1, nsides(kart(ic))
          s1 = neighSide(s,ic)
          ic1 = neighCell(s,ic)
          if (s1 < 0) then
            s1 = -s1
            m2 = -1
          else
            m2 = 1
          end if
          do m = 1, nPhi
            if (m2 < 0) then
              m1 = nPhi - m + 1
            else
              m1 = m
            end if
            do n = 1, cell_nsegments(kart(ic),s)
              if (m2 < 0) then
                n1 = n
              else
                n1 = cell_nsegments(kart(ic),s) - n + 1
              end if
              MC(ic1,s1,n1,m1,1) = ic
              MC(ic1,s1,n1,m1,2) = s
              MC(ic1,s1,n1,m1,3) = n
              MC(ic1,s1,n1,m1,4) = m
            end do
          end do
        end do
      end if
    end do

  end subroutine

  subroutine InitialiseOutputCurrents(ngroups, ntheta, ncells, nsides, nsegments, nphi, jout)
    use GlobalAssemblyMod, only : nCell, cell_nsegments, cell_segsdarr, kart, &
                                  chi, eps
    implicit none

    integer, intent(in) :: ngroups
    integer, intent(in) :: ntheta
    integer, intent(in) :: ncells
    integer, intent(in) :: nsides(:)
    integer, intent(in) :: nsegments(:,:)
    integer, intent(in) :: nphi
    type(current), intent(out) :: jout(:,:)

    integer :: ig, ic, it, s, n, m, l
    integer :: ns, nseg
    real(rp) :: sglngth

    ns = maxval(nsides)
    nseg = maxval(nsegments)
    do ig = 1, ngroups
      do l = 1, ntheta
        allocate(jout(ig,l)%curr(ncells,ns,nseg,nphi))
        do ic = 1, ncells
          it = kart(ic)
          do s = 1, nsides(it)
            do n = 1, cell_nsegments(it,s)
              sglngth = cell_segsdarr(it,s,n+1) - cell_segsdarr(it,s,n)
              do m = 1, nPhi
                jout(ig,l)%curr(ic,s,n,m) = 0.25_rp*sglngth*chi(m)*eps(l)
              end do
            end do
          end do
        end do
      end do
    end do

  end subroutine

  subroutine TransportSweepMG(eps_k, kiter_max)
    use GlobalAssemblyMod, only : nGroups, nTheta, nCell, nsides, cell_nsegments, nPhi, nzones, nharms, &
                                  kart, cell_mat,  SFNF, SOUR, SSCA, SCHI, cell_vol, cell_flux
    use CPMod, only : beta_cp, v_cp, g_cp, u_mg, n_beta_cp, n_v_cp, n_g_cp
    implicit none

    real(rp), intent(in) :: eps_k
    integer, intent(in) :: kiter_max

    integer :: ig, l
    type(current) :: out_curr(nGroups,nTheta)
    real(rp), allocatable :: F(:,:,:,:)
    real(rp), allocatable :: Fgl(:,:,:)
    real(rp), allocatable :: Q(:,:,:,:)
    real(rp), allocatable :: Qf(:,:,:)
    real(rp), allocatable :: Qf_p(:,:,:)

    real(rp) :: eff, effmax, effmin, qscat, r
    integer :: kiter, ierr, ic, it, ir, imt, k, ig1

    allocate(cell_flux(nGroups,nCell,maxval(nzones),maxval(nharms)))
    allocate(F(nGroups,nCell,maxval(nzones),maxval(nharms)))
    allocate(Fgl(nCell,maxval(nzones),maxval(nharms)))
    allocate(Q(nGroups,nCell,maxval(nzones),maxval(nharms)))
    allocate(Qf(nCell,maxval(nzones),maxval(nharms)))
    allocate(Qf_p(nCell,maxval(nzones),maxval(nharms)))
    Qf_p = 1.0_rp
    cell_flux = 0.0_rp

    call InitialiseFlux(F)
    call InitialiseSources(F, Q)
    call InitialiseOutputCurrents(nGroups, nTheta, nCell, nsides, cell_nsegments, nPhi, out_curr)

    eff = 1.
    kiter = 1
    do while (kiter <= kiter_max)
      effmax = -1.0e05
      effmin = 1.0e05
      ierr = 0
      F = 0.0_rp
      !!$omp parallel do shared(F) private(ig,l,Fgl) collapse(2)
      !$omp parallel do private(Fgl) collapse(2)
      do ig = 1, nGroups
        do l = 1, nTheta
          call TransportSweep1G(ig, l, Q(ig,:,:,:), beta_cp(ig,l,:,:), v_cp(ig,l,:,:), g_cp(ig,l,:,:), u_mg(ig)%u, &
                                n_beta_cp, n_v_cp, n_g_cp, out_curr(ig, l)%curr, Fgl)
          F(ig,:,:,:) = F(ig,:,:,:) + Fgl
        end do
      end do
      !$omp end paralleldo

!----------------------------
! Recalculation of the sources
!----------------------------
      Qf = 0.
      do ig = 1, nGroups
        do ic = 1, nCell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it,ir)
            do k = 1, nharms(it)
              Qf(ic,ir,k) = Qf(ic,ir,k) + F(ig,ic,ir,k)*SFNF(imt,iG)
            end do
          end do
        end do
      end do

      if (maxval(SOUR) > 1.0e-8) then
        do ig = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              do k = 1, nharms(it)
                qscat = 0.0_rp
                do ig1 = 1, nGroups
                  qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
                end do
                Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k)+SOUR(imt,iG))*cell_vol(it,ir) !*fact(ic)
              end do
            end do
          end do
        end do
      else
        do iG = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              if (SFNF(imt,ig) /= 0.0_rp) then
                r = Qf(ic,ir,1)/Qf_p(ic,ir,1)
                if (r > effmax) then
                  effmax = r
                end if
                if (r < effmin) then
                  effmin = r
                end if
              end if
            end do
          end do
        end do
        if (abs((effmax + effmin)*0.5 - eff) <= eps_k .and. abs(effmax-effmin) <= eps_k) then
          exit
        else
          eff = (effmax + effmin)*0.5
          Qf = Qf/eff
        end if
        Qf_p = Qf
        do ig = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              do k = 1, nharms(it)
                qscat = 0.0_rp
                do ig1 = 1, nGroups
                  qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
                end do
                Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k))*cell_vol(it,ir)
              end do
            end do
          end do
        end do
      end if
      if (int(kiter/10)*10 == kiter) write(*,'(1i5,3f10.5)') kiter, effmin, effmax, eff
      kiter = kiter + 1
    end do
    write(*,'(1i5,3f10.5)') kiter, effmin, effmax, eff
    !write(*,*) F

!    do ic = 1, nCell
!      it = kart(ic)
!      do ir = 1, nzones(it)
!        imt = cell_mat(it,ir)
!        do ig = 1, nGroups
!          do k = 1, nharms(it)
!            F(ig,ic,ir,k) = F(ig,ic,ir,k)*cell_vol(it,ir)*STOT(imt,ig)
!          end do
!        end do
!      end do
!    end do

    cell_flux(:,:,:,:) = F(:,:,:,:)

  end subroutine TransportSweepMG

  subroutine TransportSweep1G(igroup, itheta, Q, b, v, g, u, nb, nv, ng, jout, F)
    use GlobalAssemblyMod, only : nCell, nsides, nharms, nPhi, &
                                  cell_nsegments, kart, nzones, cell_mat, cell_vol, STOT, &
                                  nCellTypes
    use CPMod, only : StoSCP, StoRCP, RtoSCP
    implicit none

    integer, intent(in)      :: igroup
    integer, intent(in)      :: itheta
    real(rp), intent(in)      :: Q(:,:,:)
    type(StoSCP), intent(in) :: b(:,:)
    type(RtoSCP), intent(in) :: v(:,:)
    type(StoRCP), intent(in) :: g(:,:)
    real(rp), intent(in)      :: u(:,:,:,:,:)
    integer, intent(in)      :: nb(:)
    integer, intent(in)      :: nv(:)
    integer, intent(in)      :: ng(:)
    real(rp), intent(out)     :: F(:,:,:)
    real(rp), intent(out)     :: jout(:,:,:,:)

    integer :: i, k, s, n, m, i1, k1, m1, n1, s1
    integer :: ib, ig, iv, ic, imat, ir, it
    real(rp) :: bb

    if (.not. allocated(ind_g)) allocate(ind_g(maxval(ng),5))
    if (.not. allocated(ind_v)) allocate(ind_v(maxval(nv),5))
    if (.not. allocated(ind_b)) allocate(ind_b(maxval(nb),6))
    if (.not. allocated(ind_b_full)) allocate(ind_b_full(nCellTypes,maxval(nb),6))
    if (.not. allocated(ind_g_full)) allocate(ind_g_full(nCellTypes,maxval(ng),5))
    if (.not. allocated(ind_v_full)) allocate(ind_v_full(nCellTypes,maxval(nv),5))
    if (.not. allocated(jin)) allocate(jin(maxval(nsides),maxval(cell_nsegments),nPhi))
    if (.not. allocated(val_b))  allocate(val_b(maxval(nb)))
    if (.not. allocated(val_rc)) allocate(val_rc(maxval(nb)))
    if (.not. allocated(val_g))  allocate(val_g(maxval(ng)))
    if (.not. allocated(val_v))  allocate(val_v(maxval(nv)))

    do ic = 1, nCell
      call Get_jin(igroup, ic, jout, jin)
      ! Getting appropriate collision probabilities and indices
      it = kart(ic)
      do ib = 1, nb(it)
        ind_b(ib,:) = b(it,ib)%ind
      end do
      val_b = b(it,1:nb(it))%val
      val_rc = b(it,1:nb(it))%rc
      do ig = 1, ng(it)
        ind_g(ig,:) = g(it,ig)%ind
      end do
      val_g = g(it,1:ng(it))%val
      do iv = 1, nv(it)
        ind_v(iv,:) = v(it,iv)%ind
      end do
      val_v = v(it,1:nv(it))%val
      ! Flux from input current
      F(ic,:,:) = 0.0_rp
      do ig = 1, ng(it)
        s = ind_g(ig,1)
        n = ind_g(ig,2)
        m = ind_g(ig,3)
        i = ind_g(ig,4)
        k = ind_g(ig,5)
        F(ic,i,k) = F(ic,i,k) + val_g(ig)*jin(s,n,m)
      end do
      ! Flux from region to region
      if (itheta == 1) then
        do i = 1, nzones(it)
          do k = 1, nharms(it)
            do i1 = 1, nzones(it)
              do k1 = 1, nharms(it)
                F(ic,i1,k1) = F(ic,i1,k1) + Q(ic,i,k)*u(it,i1,k1,i,k)
              end do
            end do
          end do
        end do
      end if
      ! Output currents due to the input currents
      jout(ic,:,:,:) = 0.0_rp
      do ib = 1, nb(it)
        s1 = ind_b(ib,1)
        n1 = ind_b(ib,2)
        m1 = ind_b(ib,3)
        s =  ind_b(ib,4)
        n =  ind_b(ib,5)
        m =  ind_b(ib,6)
        bb = val_b(ib)
        jout(ic,s,n,m) = jout(ic,s,n,m) + jin(s1,n1,m1)*bb
        bb = bb*val_rc(ib)
        jout(ic,s1,n1,m1) = jout(ic,s1,n1,m1) + jin(s,n,m)*bb
      end do
      ! Output currents due to the source
      do iv = 1, nv(it)
        i = ind_v(iv,1)
        k = ind_v(iv,2)
        s = ind_v(iv,3)
        n = ind_v(iv,4)
        m = ind_v(iv,5)
        jout(ic,s,n,m) = jout(ic,s,n,m) + Q(ic,i,k)*val_v(iv)
      end do
    end do

    do ic = 1, nCell
      it = kart(ic)
      do ir = 1, nzones(it)
        imat = cell_mat(it,ir)
        do k = 1, nharms(it)
          F(ic,ir,k) = F(ic,ir,k)/(cell_vol(it,ir)*STOT(imat,igroup))
        end do
      end do
    end do

  end subroutine

  subroutine CalculateCurrents1G1L(ig, Q, b, v, nb, nv, jout)
    use GlobalAssemblyMod, only : nCell, nsides, nharms, nPhi, &
                                  cell_nsegments, kart, nzones, cell_mat, cell_vol, nCellTypes
    use CPMod, only : StoSCP, RtoSCP
    implicit none

    integer,  intent(in)     :: ig ! Group number
    real(rp), intent(in)     :: Q(:,:,:)
    type(StoSCP), intent(in) :: b(:,:)
    type(RtoSCP), intent(in) :: v(:,:)
    integer, intent(in)      :: nb(:)
    integer, intent(in)      :: nv(:)
    real(rp), intent(out)    :: jout(:,:,:,:)

    real(rp) :: jin(maxval(nsides),maxval(cell_nsegments),nPhi)
    integer :: i, k, s, n, m, m1, n1, s1
    integer :: ib, iv, ic, it
    integer :: ind_v(maxval(nv),5), ind_b(maxval(nb),6)
!    integer :: ind_b_full(nCellTypes,maxval(nb),6), ind_g_full(nCellTypes,maxval(ng),5), ind_v_full(nCellTypes,maxval(nv),5)
    real(rp) :: val_b(maxval(nb)), val_rc(maxval(nb)), val_v(maxval(nv))
    real(rp) :: bb, vv, qq

    do ic = 1, nCell
      call Get_jin(ig, ic, jout, jin)
      ! Getting appropriate collision probabilities and indices
      it = kart(ic)
      do ib = 1, nb(it)
        ind_b(ib,:) = b(it,ib)%ind
      end do
      val_b = b(it,1:nb(it))%val
      val_rc = b(it,1:nb(it))%rc
      ! Output currents due to the input currents
      jout(ic,:,:,:) = 0.0_rp
      do ib = 1, nb(it)
        s1 = ind_b(ib,1)
        n1 = ind_b(ib,2)
        m1 = ind_b(ib,3)
        s =  ind_b(ib,4)
        n =  ind_b(ib,5)
        m =  ind_b(ib,6)
        bb = val_b(ib)
        jout(ic,s,n,m) = jout(ic,s,n,m) + jin(s1,n1,m1)*bb
        bb = bb*val_rc(ib)
        jout(ic,s1,n1,m1) = jout(ic,s1,n1,m1) + jin(s,n,m)*bb
      end do
      ! Output currents due to the source
      do iv = 1, nv(it)
        i = v(it,iv)%ind(1)
        k = v(it,iv)%ind(2)
        s = v(it,iv)%ind(3)
        n = v(it,iv)%ind(4)
        m = v(it,iv)%ind(5)
        jout(ic,s,n,m) = jout(ic,s,n,m) + Q(ic,i,k)*v(it, iv)%val
      end do
    end do

  end subroutine

  subroutine IterateCurrentsMG(itermax, eps, Qin, jout)
    use GlobalAssemblyMod, only : nCell, nTheta, nGroups, nTheta, &
                                  nsides, cell_nsegments, kart,   &
                                  nPhi, verbosity
    use CPMod, only : StoSCP, RtoSCP, beta_cp, v_cp, n_beta_cp, n_v_cp
    use omp_lib
    implicit none

    integer, intent(in)        :: itermax
    real(rp), intent(in)        :: eps
    real(rp), intent(in)        :: Qin(:,:,:,:)
    type(current), intent(inout) :: jout(:,:)

    type(current) :: jout_old(nGroups,nTheta)
    integer :: iter, ig, l, it, s, n, m, ic, ierr
    real(rp) :: currmax, cm
    real(rp) :: om

    om = 1.0_rp
    currmax = -1.0_rp
    do iter = 1, itermax
      jout_old = jout
      ! Cycle on groups and polar sectors for evaluation of output currents
      ierr = 0
      do ig = 1, nGroups
!        !$omp paralleldo
        do l = 1, nTheta
          call CalculateCurrents1G1L(ig, Qin(ig,:,:,:), beta_cp(ig,l,:,:), v_cp(ig,l,:,:), n_beta_cp, n_v_cp, jout(ig,l)%curr)
          cm = maxval(jout(ig,l)%curr)
          if (currmax < cm) currmax = cm
          ! Check the convergense of the currents
          do ic = 1, nCell
            if (ierr == 1) exit
            it = kart(ic)
            do s = 1, nsides(it)
              do n = 1, cell_nsegments(it,s)
                do m = 1, nPhi
                  if (abs(jout(ig,l)%curr(ic,s,n,m) - jout_old(ig,l)%curr(ic,s,n,m)) > eps*currmax) then
                    ierr = 1
                  end if
                end do
              end do
            end do
          end do
        end do
!        !$omp end paralleldo
      end do
      if (ierr == 0) then
        exit
      else ! New currents
        do ig = 1, nGroups
          do l = 1, nTheta
            do ic = 1, nCell
              it = kart(ic)
              do s = 1, nsides(it)
                do n = 1, cell_nsegments(it,s)
                  do m = 1, nPhi
                    jout(ig,l)%curr(ic,s,n,m) = jout_old(ig,l)%curr(ic,s,n,m) + &
                                    om*(jout(ig,l)%curr(ic,s,n,m) - jout_old(ig,l)%curr(ic,s,n,m))
                  end do
                end do
              end do
            end do
          end do
        end do
      end if
      ! Check whether currents converged or not
!      ierr = 0
!      do ig = 1, nGroups
!        do l = 1, nTheta
!          do ic = 1, nCell
!            it = kart(ic)
!            do s = 1, nsides(it)
!              do n = 1, cell_nsegments(it,s)
!                do m = 1, nPhi
!                  ! New values of currents
!
!                end do
!              end do
!            end do
!          end do
!        end do
!      end do
    end do
    if (verbosity > 0) then
      write(*,*) 'Internal iterations: ', iter - 1
    end if

  end subroutine

  subroutine CalculateFluxes(Q, g, ng, u, jout, F)
    use GlobalAssemblyMod, only : nCell, nTheta, nsides, cell_nsegments, nPhi, nharms, &
                                  kart, nGroups, nzones, cell_mat, cell_vol, STOT
    use CPMod, only : umg, StoRCP
    implicit none

    real(rp), intent(in)       :: Q(:,:,:,:)
    type(StoRCP), intent(in)  :: g(:,:,:,:)
    type(umg), intent(in)     :: u(:)
    integer, intent(in)       :: ng(:)
    type(current), intent(in) :: jout(:,:)
    real(rp), intent(out)      :: F(:,:,:,:)

    real(rp) :: jin(maxval(nsides),maxval(cell_nsegments),nPhi)
    integer :: ind_g(maxval(ng),5)
    real(rp) :: val_g(maxval(ng))

    integer :: ic, ig, igam, l, i, s, n, m, k, it, i1, k1, ir, imat

    do ig = 1, nGroups
      do l = 1, nTheta
        do ic = 1, nCell
          it = kart(ic)
          call Get_jin(ig, ic, jout(ig,l)%curr, jin)
          ! Getting appropriate collision probabilities and indices
          it = kart(ic)
          do igam = 1, ng(it)
            ind_g(igam,:) = g(ig,l,it,igam)%ind
          end do
          val_g(1:ng(it)) = g(ig,l,it,1:ng(it))%val
          ! Flux from input current
          if (l == 1) then
            F(ig,ic,:,:) = 0.0_rp
          end if
          do igam = 1, ng(it)
            s = ind_g(igam,1)
            n = ind_g(igam,2)
            m = ind_g(igam,3)
            i = ind_g(igam,4)
            k = ind_g(igam,5)
            F(ig,ic,i,k) = F(ig,ic,i,k) + val_g(igam)*jin(s,n,m)
          end do
          ! Flux from region to region
          if (l == 1) then
            do i = 1, nzones(it)
              do k = 1, nharms(it)
                do i1 = 1, nzones(it)
                  do k1 = 1, nharms(it)
                    F(ig,ic,i1,k1) = F(ig,ic,i1,k1) + Q(ig,ic,i,k)*u(ig)%u(it,i1,k1,i,k)
                  end do
                end do
              end do
            end do
          end if

        end do
      end do
    end do

    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do ir = 1, nzones(it)
          imat = cell_mat(it,ir)
          do k = 1, nharms(it)
            F(ig,ic,ir,k) = F(ig,ic,ir,k)/(cell_vol(it,ir)*STOT(imat,ig))
          end do
        end do
      end do
    end do

  end subroutine

  subroutine TransportSweepHelios(eps_k, kiter_max)
    use GlobalAssemblyMod, only : nGroups, nTheta, nCell, nsides, cell_nsegments, nPhi, nzones, nharms, &
                                   kart, cell_mat, &
                                  SFNF, SOUR, SSCA, SCHI, cell_vol, cell_flux, iter_max, eps_f, cell_curr
    use CPMod, only : beta_cp, v_cp, g_cp, u_mg, n_beta_cp, n_v_cp, n_g_cp
    implicit none

    real(rp), intent(in) :: eps_k
    integer, intent(in)  :: kiter_max

    integer :: ig, l
    integer :: isd, iphi, isg, itheta
    type(current) :: out_curr(nGroups,nTheta)
    real(rp), allocatable :: F(:,:,:,:)
    real(rp), allocatable :: Fgl(:,:,:)
    real(rp), allocatable :: Q(:,:,:,:)
    real(rp), allocatable :: Qf(:,:,:)
    real(rp), allocatable :: Qf_p(:,:,:)

    real(rp) :: eff, effmax, effmin, qscat, r
    integer :: kiter, ierr, ic, it, ir, imt, k, ig1

    allocate(cell_flux(nGroups,nCell,maxval(nzones),maxval(nharms)))
    allocate(F(nGroups,nCell,maxval(nzones),maxval(nharms)))
    allocate(Fgl(nCell,maxval(nzones),maxval(nharms)))
    allocate(Q(nGroups,nCell,maxval(nzones),maxval(nharms)))
    allocate(Qf(nCell,maxval(nzones),maxval(nharms)))
    allocate(Qf_p(nCell,maxval(nzones),maxval(nharms)))
    Qf_p = 1.0_rp
    cell_flux = 0.0_rp

    call InitialiseFlux(F)
    call InitialiseSources(F, Q)
    call InitialiseOutputCurrents(nGroups, nTheta, nCell, nsides, cell_nsegments, nPhi, out_curr)

    eff = 1.
    kiter = 1
    do while (kiter <= kiter_max)
      effmax = -1.0e05
      effmin = 1.0e05
      ierr = 0
      F = 0.0_rp
      call IterateCurrentsMG(iter_max, eps_f, Q, out_curr)
      call CalculateFluxes(Q, g_cp, n_g_cp, u_mg, out_curr, F)
!----------------------------
! Recalculation of the sources
!----------------------------
      Qf = 0.
      do ig = 1, nGroups
        do ic = 1, nCell
          it = kart(ic)
          do ir = 1, nzones(it)
            imt = cell_mat(it,ir)
            do k = 1, nharms(it)
              Qf(ic,ir,k) = Qf(ic,ir,k) + F(ig,ic,ir,k)*SFNF(imt,iG)
            end do
          end do
        end do
      end do

      if (maxval(SOUR) > 1.0e-8) then
        do ig = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              do k = 1, nharms(it)
                qscat = 0.0_rp
                do ig1 = 1, nGroups
                  qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
                end do
                Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k)+SOUR(imt,iG))*cell_vol(it,ir) !*fact(ic)
              end do
            end do
          end do
        end do
      else
        do iG = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              if (SFNF(imt,ig) /= 0.0_rp) then
                r = Qf(ic,ir,1)/Qf_p(ic,ir,1)
                if (r > effmax) then
                  effmax = r
                end if
                if (r < effmin) then
                  effmin = r
                end if
              end if
            end do
          end do
        end do
        if (abs((effmax + effmin)*0.5 - eff) <= eps_k .and. abs(effmax-effmin) <= eps_k) then
          exit
        else
          eff = (effmax + effmin)*0.5
          Qf = Qf/eff
        end if
        Qf_p = Qf
        do ig = 1, nGroups
          do ic = 1, nCell
            it = kart(ic)
            do ir = 1, nzones(it)
              imt = cell_mat(it,ir)
              do k = 1, nharms(it)
                qscat = 0.0_rp
                do ig1 = 1, nGroups
                  qscat = qscat + SSCA(imt,iG1,iG)*F(ig1,ic,ir,k)
                end do
                Q(ig,ic,ir,k) = (qscat + SCHI(imt,ig)*Qf(ic,ir,k))*cell_vol(it,ir)
              end do
            end do
          end do
        end do
      end if
      if (int(kiter/10)*10 == kiter) then
        write(*,'(a12,1i5)') "Iteration = ", kiter
        write(*,'(a12,f10.5,a12,f10.5,a8,f10.5)') " Keff_min = ", effmin, " Keff_max = ", effmax, " Keff = ", eff
      end if
      kiter = kiter + 1
    end do
    write(*, *) "******************** Flux calculations finished ***********************"
    write(*,'(a35,i5)')     "Total number of outer iterations = ", kiter
    write(*,'(a13,f10.5)')  "Final Keff = ", eff

    allocate(cell_curr(nGroups,nCell,maxval(nsides),maxval(cell_nsegments),nPhi,nTheta))
    do ig = 1, nGroups
      do ic = 1, nCell
        it = kart(ic)
        do isd = 1, nsides(it)
          do isg = 1, cell_nsegments(it,isd)
            do iphi = 1, nPhi
              do itheta = 1, nTheta
                cell_curr(ig,ic,isd,isg,iphi,itheta) = out_curr(ig,itheta)%curr(ic,isd,isg,iphi)
              end do
            end do
          end do
        end do
      end do
    end do

    !write(*,*) F

!    do ic = 1, nCell
!      it = kart(ic)
!      do ir = 1, nzones(it)
!        imt = cell_mat(it,ir)
!        do ig = 1, nGroups
!          do k = 1, nharms(it)
!            F(ig,ic,ir,k) = F(ig,ic,ir,k)*cell_vol(it,ir)*STOT(imt,ig)
!          end do
!        end do
!      end do
!    end do

    cell_flux(:,:,:,:) = F(:,:,:,:)

  end subroutine

  subroutine GetFluxCorrectedAlbedos(filename_corner, eps_f, eps_k, kiter_max)
    use Config, only : str_len
    implicit none

    character(str_len), intent(in) :: filename_corner
    real(rp), intent(in) :: eps_f
    real(rp), intent(in) :: eps_k
    integer, intent(in)  :: kiter_max



  end subroutine

  subroutine Get_jin(ig, ic, jout, jin)
    use GlobalAssemblyMod, only : kart, neighCell, neighSide, sym, nsides, &
                                  cell_nsegments, nPhi, albedo, num_cell_edge
    implicit none

    integer, intent(in)   :: ig
    integer, intent(in)   :: ic
    real(rp), intent(in)  :: jout(:,:,:,:)
    real(rp), intent(out) :: jin(:,:,:)

    integer  :: it, s, n, m, s1, n1, m1, m2, ic1
    integer  :: nce
    real(rp) :: alb

    it = kart(ic)
    if (sym < 0) then
      do s = 1, nsides(it)
        ic1 = neighCell(s,ic)
        s1 = neighSide(s,ic)
        do n = 1, cell_nsegments(it,s)
          n1 = cell_nsegments(it,s) - n + 1
          if (ic1 == 0) then
            n1 = n
            do m = 1, nPhi
              m1 = nPhi - m + 1
              jin(s,n,m) = jout(ic,s,n1,m1)
            end do
          else
            do m = 1, nPhi
              Jin(s,n,m) = jout(ic1,s1,n1,m)
            end do
          end if
        end do
      end do
    else
      do s = 1, nsides(it)
        s1 = neighSide(s,ic)
        ic1 = neighCell(s,ic)
        if (s1 < 0) then
          s1 = -s1
          m2 = -1
        else
          m2 = 1
        end if
        do m = 1, nPhi
          if (m2 < 0) then
            m1 = nPhi - m + 1
          else
            m1 = m
          end if
          do n = 1, cell_nsegments(it,s)
            if (m2 < 0) then
              n1 = n
            else
              n1 = cell_nsegments(it,s) - n + 1
            end if
            if (neighSide(s,ic) >= 0) then
              jin(s,n,m) = jout(ic1,s1,n1,m1)
            else
!              if (ic == 17) then
!                write(*, *) "Hey! I am there!"
!                continue
!              end if
              nce = num_cell_edge(s,ic)
              alb = albedo(ig,s,nce)
              jin(s,n,m) = alb * jout(ic1,s1,n1,m1)
            end if
          end do
        end do
      end do
    end if

  end subroutine

  subroutine IterateCurrents(b, nb, v, nv, H, nj, Q, itermax, eps, jout)
    use GlobalAssemblyMod, only : nCell
    use CPMod, only : StoSCP, RtoSCP
    implicit none

    type(StoSCP), intent(in) :: b(:)
    integer, intent(in)      :: nb(:)
    type(RtoSCP), intent(in) :: v(:)
    integer, intent(in)      :: nv(:)
    integer, intent(in)      :: H(:)
    integer, intent(in)      :: nj
    real(rp), intent(in)      :: Q(:,:,:)
    integer, intent(in)      :: itermax
    real(rp), intent(in)      :: eps
    real(rp), intent(out)     :: jout(:)

    integer :: ind_b(maxval(nb),6)
    integer :: ind_v(maxval(nv),5)
    real(rp) :: val_b(maxval(nb)), val_rc(maxval(nb))
    real(rp) :: val_v(maxval(nv))

    integer :: ic
    integer :: iter
    real(rp) :: jo, ji

    jout = 0.0d0

    do ic = 1, nCell
!      ind_b_in(ic,:) = get_curr_ind(l,ic,b(ic)%ind(1:3))
!      val_b(ic) = b(ic)%val
!      val_rc(ic) = b(ic)%rc
!      ind_v(ic,:) = v(ic)%ind
!      val_v(ic) = v(ic)%val
    end do

    do iter = 1, itermax
      do ic = 1, nCell

      end do
    end do

  end subroutine

  subroutine CurrentsTo1D(jmd, nj, j1d)
    use GlobalAssemblyMod, only : nCell, nsides, cell_nsegments, nPhi, nTheta, &
                                  kart
    implicit none

    type(current), intent(in)  :: jmd(:)
    integer, intent(out)              :: nj
    real(rp), allocatable, intent(out) :: j1d(:)

    integer :: ig, it, l, ic, s, n, m, j

    ! Evaluating total number of elements in j1d array, defining indices of reciprocity table
    allocate(recipr_ind_table(nTheta,nCell))
    nj = 0
      do l = 1, nTheta
        do ic = 1, nCell
          recipr_ind_table(l,ic) = nj
          it = kart(ic)
          do s = 1, nsides(it)
            do n = 1, cell_nsegments(it,s)
              do m = 1, nPhi
                nj = nj + 1
              end do
            end do
          end do
        end do
      end do

    ! Creating 1D array out of multidimensional array
    allocate(j1d(nj))
    j = 0
    do l = 1, nTheta
      do ic = 1, nCell
        it = kart(ic)
        do s = 1, nsides(it)
          do n = 1, cell_nsegments(it,s)
            do m = 1, nPhi
              j = j + 1
              j1d(j) = jmd(l)%curr(ic,s,n,m)
            end do
          end do
        end do
      end do
    end do

    return

  end subroutine

  subroutine MakeH(nj, H)
    use GlobalAssemblyMod, only : nCell, nGroups, nsides, cell_nsegments, nPhi, nTheta, &
                                  kart, sym, neighCell, neighSide
    implicit none

    integer, intent(in)  :: nj
    integer, intent(out) :: H(nj)

    integer :: ig, l, ic, s, n, m, ic1, s1, n1, m1, m2, j
    integer :: ind

    j = 1
    do l = 1, nTheta
      do ic = 1, nCell
        if (sym < 0) then
          do s = 1, nsides(kart(ic))
            ic1 = neighCell(s,ic)
            s1 = neighSide(s,ic)
            do n = 1, cell_nsegments(kart(ic),s)
              n1 = cell_nsegments(kart(ic),s) - n + 1
              if (ic1 == 0) then
                n1 = n
                do m = 1, nPhi
                  m1 = nPhi - m + 1
                  ind = get_h_ind(nsides(kart(ic)),cell_nsegments(kart(ic),s),nPhi,ig,l,ic,s,n1,m1)
                  H(ind) = j
                  j = j + 1
    !                  Jin(ig,ic,s,n,m,l) = Jout(ig,ic,s,n1,m1,l)
                end do
              else
                do m = 1, nPhi
                  ind = get_h_ind(nsides(kart(ic1)),cell_nsegments(kart(ic1),s),nPhi,ig,l,ic1,s1,n1,m)
                  H(ind) = j
                  j = j + 1
    !                 Jin(ig,ic,s,n,m,l) = Jout(ig,ic1,s1,n1,m,l)
                end do
              end if
            end do
          end do
        else
          do s = 1, nsides(kart(ic))
            s1 = neighSide(s,ic)
            ic1 = neighCell(s,ic)
            if (s1 < 0) then
              s1 = -s1
              m2 = -1
            else
              m2 = 1
            end if
            do m = 1, nPhi
              if (m2 < 0) then
                m1 = nPhi - m + 1
              else
                m1 = m
              end if
              do n = 1, cell_nsegments(kart(ic),s)
                if (m2 < 0) then
                  n1 = n
                else
                  n1 = cell_nsegments(kart(ic),s) - n + 1
                end if
                ind = get_h_ind(nsides(kart(ic1)),cell_nsegments(kart(ic1),s),nPhi,ig,l,ic1,s1,n1,m1)
                H(ind) = j
                j = j + 1
!       jin(s,n,m) = jout(ic1,s1,n1,m1)
              end do
            end do
          end do
        end if
      end do
    end do

  end subroutine

  integer function get_h_ind(nsides, nsegments, nphi, ig, l, ic, s, n, m)
    use GlobalAssemblyMod, only : nGroups, nTheta, nCell
    implicit none

    integer, intent(in) :: nsides
    integer, intent(in) :: nsegments(nsides)
    integer, intent(in) :: nphi
    integer, intent(in) :: ig
    integer, intent(in) :: l
    integer, intent(in) :: ic
    integer, intent(in) :: s
    integer, intent(in) :: n
    integer, intent(in) :: m

    integer :: s1, n1, m1
    integer :: ind

    ind = recipr_ind_table(l,ic)

    do s1 = 1, nsides
      do n1 = 1, nsegments(s)
        do m1 = 1, nphi
          if (s1 == s .and. n1 == n .and. m1 == m) then
            get_h_ind = ind + 1
            return
          end if
          ind = ind + 1
        end do
      end do
    end do

    return

  end function get_h_ind

end module
