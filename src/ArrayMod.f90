module ArrayMod
  !------------------------------------------------
  ! The module contains subroutines and functions
  ! for manipulation with arrays
  !------------------------------------------------
  use precision_mod
  integer, parameter :: nRealInc = 10000
contains

  subroutine ArrayExpandCharacter(array_pointer,scalar_value)

    ! ArrayExpandCharacter will take a one-dimensional array and a scalar value and
    ! add it at the end of the array.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! array_pointer: character, pointer (1D). The original values of the array.
    ! scalar_value: character, scalar. The value that will be added to the array.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! array_pointer: character, pointer (1D). The updated version of the array.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !subroutine ArrayExpandCharacter(array_pointer,scalar_value)
    !use Config, only: srk
    !implicit none
    !character,intent(in):: scalar_value*(*)
    !character,pointer:: array_pointer(:)*(*)
    !end subroutine ArrayExpandCharacter
    !end interface

    !character:: scalar_value*(str_len)
    !character,pointer:: array_pointer(:)*(str_len)
    !write(*,*)"Enter as many values as you wish. Enter zero to end input."
    !do
    !read(*,*)scalar_value
    !if (scalar_value=='0') exit
    !call ArrayExpandCharacter(array_pointer,scalar_value)
    !end do
    !write(*,*)"list size = ",size(array_pointer)
    !write(*,*)array_pointer

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk, str_len

    implicit none

    ! Argument variables:
    character,intent(in):: scalar_value*(*)
    character,pointer:: array_pointer(:)*(*)

    ! Private variables:
    character,allocatable,target,save:: temp(:)*(str_len)
    integer:: err

    ! ------------------------------------------------------------------------------

    ! Variable initialization :
    if (.not.associated(array_pointer)) allocate(array_pointer(0))
    if (allocated(temp)) deallocate(temp)
    allocate(temp(size(array_pointer)+1),stat=err)

    if (err/=0) then
      write(*,*)"ArrayExpandCharacter"
      write(*,*)"ERROR: array allocation failed."
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    temp(size(temp))=scalar_value
    temp(1:size(array_pointer))=array_pointer

    ! Pointer "array_pointer" may be left uninitialized in the calling code.
    if (associated(array_pointer)) nullify(array_pointer)
    array_pointer=>temp

  end subroutine ArrayExpandCharacter

  subroutine ArrayExpandReal(array_pointer,scalar_value)

    ! ArrayExpandReal will take a one-dimensional array and a scalar value and add
    ! it at the end of the array.

    !*******************************************************************************
    ! Evangelos Bertakis, F.EN.I.A. Project, 2008                                  *
    !*******************************************************************************

    ! INPUT (REQUIRED):
    ! array_pointer: real, pointer (1D). The original values of the array.
    ! scalar_value: real, scalar. The value that will be added to the array.

    ! INPUT (OPTIONAL):
    ! No optional input arguments. All arguments are required.

    ! OUTPUT (REQUIRED):
    ! array_pointer: real, pointer (1D). The updated version of the array.

    ! OUTPUT (OPTIONAL):
    ! No optional output arguments. All arguments are required.

    ! Example of usage:

    !interface
    !subroutine ArrayExpandReal(array_pointer,scalar_value)
    !use Config, only: srk
    !implicit none
    !real(rp),intent(in):: scalar_value
    !real(rp),pointer:: array_pointer(:)
    !end subroutine ArrayExpandReal
    !end interface

    !real(rp):: scalar_value
    !real(rp),pointer:: array_pointer(:)
    !write(*,*)"Enter as many values as you wish. Enter zero to end input."
    !do
    !read(*,*)scalar_value
    !if (scalar_value==0.0) exit
    !call ArrayExpandReal(array_pointer,scalar_value)
    !end do
    !write(*,*)"list size = ",size(array_pointer)
    !write(*,*)array_pointer

    ! ------------------------------------------------------------------------------

    ! External variables:
    use Config, only: srk

    implicit none

    ! Argument variables:
    real(rp),intent(in):: scalar_value
    real(rp),pointer:: array_pointer(:)

    ! Private variables:
    real(rp),allocatable,target,save:: temp(:)
    integer:: err

    ! ------------------------------------------------------------------------------

    ! Variable initialization :
    if (.not.associated(array_pointer)) allocate(array_pointer(0))
    if (allocated(temp)) deallocate(temp)
    allocate(temp(size(array_pointer)+1),stat=err)

    if (err/=0) then
      write(*,*)"ArrayExpandReal"
      write(*,*)"ERROR: array allocation failed."
      write(*,*)"error flag : ",err
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    temp(size(temp))=scalar_value
    temp(1:size(array_pointer))=array_pointer

    ! Pointer "array_pointer" may be left uninitialized in the calling code.
    if (associated(array_pointer)) nullify(array_pointer)
    array_pointer=>temp

  end subroutine ArrayExpandReal

  subroutine ArrayInsertValueReal(array_pointer, vle)
    use ConstantsMod, only : DELTA_MIN
    implicit none

    ! Argument variables:
    real(rp),intent(in):: vle
    real(rp),pointer:: array_pointer(:)

    ! Private variables:
    real(rp),allocatable,target,save:: temp(:)
    real(rp),allocatable,target,save:: temp1(:)
    integer :: er, er1
    integer :: i

    ! ------------------------------------------------------------------------------

    ! Variable initialization :
    if (.not.associated(array_pointer)) allocate(array_pointer(0))
    if (allocated(temp)) deallocate(temp)
    allocate(temp(size(array_pointer)+1),stat=er)
    if (allocated(temp1)) deallocate(temp1)
    allocate(temp1(size(array_pointer)), stat = er1)

    if (er /= 0) then
      write(*,*)"ArrayExpandReal"
      write(*,*)"ERROR: array allocation temp failed."
      write(*,*)"error flag : ",er
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if
    if (er1 /= 0) then
      write(*,*)"ArrayExpandReal"
      write(*,*)"ERROR: array allocation temp1 failed."
      write(*,*)"error flag : ",er1
      write(*,*)"Program terminated."
      read(*,*)
      stop
    end if

    if (size(temp) == 1) then
      temp(1) = vle
      if (associated(array_pointer)) nullify(array_pointer)
      array_pointer=>temp
      deallocate(temp)
      return
    end if

    temp1(1:size(array_pointer))=array_pointer

    do i = 1, size(temp1) - 1
      if (abs(temp1(i) - vle) <= DELTA_MIN .or. abs(temp1(i+1) - vle) <= DELTA_MIN) then
        if (associated(array_pointer)) nullify(array_pointer)
        array_pointer => temp1
        return
      end if
      if (temp1(i) < vle .and. vle < temp1(i+1)) then
        temp(1:i) = temp1(1:i)
        temp(i+1) = vle
        temp(i+2:size(temp)) = temp1(i+1:size(temp1))
        if (associated(array_pointer)) nullify(array_pointer)
        array_pointer => temp
        return
      end if
    end do

  end subroutine ArrayInsertValueReal

  subroutine ArrExpReal(array)
    implicit none

    real(rp), allocatable :: array(:)

    real(rp) :: temp(size(array))

    temp = array

    deallocate(array)

    allocate(array(size(temp)+nRealInc))

    array = 0.0_rp

    array(1:size(temp)) = temp

    return

  end subroutine ArrExpReal

  subroutine ArrExpInt(array)
    implicit none

    integer, allocatable, intent(inout) :: array(:)

    integer, allocatable :: temp(:)


    allocate(temp(size(array)))
    temp(1:size(array)) = array

    deallocate(array)

    allocate(array(size(temp)+nRealInc))
    array = 0
    array(1:size(temp)) = temp

    return

  end subroutine ArrExpInt

  subroutine ArrInsValReal(array, vle, nElem)
    use ConstantsMod, only : DELTA_MIN
    implicit none

    real(rp), allocatable, intent(inout) :: array(:)
    real(rp), intent(in)              :: vle
    integer, intent(inout)           :: nElem  ! Number of elements in array. It differs from size(array) since not all of the
                                               ! elements are taken into account.

    integer :: i
    real(rp) :: temp(nElem)

    if (.not.(allocated(array))) then
      allocate(array(nRealInc))
      array = 0.0_rp
      array(1) = vle
      nElem = 1
      return
    end if

    temp = 0.0_rp

    if (abs(array(1) - vle) <= DELTA_MIN) then
      return
    end if

    if (vle < array(1)) then
      temp(1:nElem) = array(1:nElem)
      if (nElem + 1 > size(array)) call ArrExpReal(array)
      array(1) = vle
      array(2:nElem+1) = temp(1:nElem)
      nElem = nElem + 1
      return
    end if

    do i = 1, nElem - 1
      if (abs(array(i) - vle) < DELTA_MIN .or. abs(array(i+1) - vle) < DELTA_MIN) then
        return
      end if
      if (array(i) < vle .and. vle < array(i+1)) then ! Insert vle into array
        if (nElem + 1 > size(array)) call ArrExpReal(array)
        temp(i+1:nElem) = array(i+1:nElem)
        array(i+1) = vle
        array(i+2:nElem+1) = temp(i+1:nElem)
        nElem = nElem + 1
        return
      end if
    end do

    array(nElem+1) = vle
    nElem = nElem + 1
    return

  end subroutine ArrInsValReal

  subroutine SetArraySize2DReal(array, arr_size1, arr_size2)
    !---------------------------------------------------
    ! Subroutine sets array size for 2-dimensional
    ! real array keeping information in the input array
    ! The subroutine works only in the case when arr_size1
    ! and arr_size2 >= of the current sizes of the array.
    ! In opposite case program stops.
    !---------------------------------------------------
    implicit none

    real(rp), allocatable, intent(inout) :: array(:,:)
    integer, intent(in) :: arr_size1
    integer, intent(in) :: arr_size2

    real(rp) :: tmp(arr_size1,arr_size2)
    integer :: dim1, dim2

    if (.not.(allocated(array))) then
      allocate(array(arr_size1, arr_size2))
      array = 0.0_rp
      return
    end if

    dim1 = size(array,1)
    dim2 = size(array,2)

    if (arr_size1 < dim1 .or. arr_size2 < dim2) then
      write(*,*) 'Subroutine SetArraySize2DReal.'
      write(*,*) 'The new size of the array is less than current ones.'
      write(*,*) 'Program is stopped.'
      stop
    end if

    tmp(1:dim1,1:dim2) = array(:,:)

    deallocate(array)

    allocate(array(arr_size1,arr_size2))
    array = 0.0_rp
    array(1:dim1,1:dim2) = tmp(1:dim1,1:dim2)

    return

  end subroutine SetArraySize2DReal

  subroutine SetArraySize2DInt(array, arr_size1, arr_size2)
    !---------------------------------------------------
    ! Subroutine sets array size for 2-dimensional
    ! integer array keeping information of the input array
    ! The subroutine works only in the case when arr_size1
    ! and arr_size2 >= of the current sizes of the array.
    ! In opposite case program stops.
    !---------------------------------------------------
    implicit none

    integer, allocatable, intent(inout) :: array(:,:)
    integer, intent(in) :: arr_size1
    integer, intent(in) :: arr_size2

    integer :: tmp(arr_size1,arr_size2)
    integer :: dim1, dim2

    if (.not.(allocated(array))) then
      allocate(array(arr_size1, arr_size2))
      array = 0
      return
    end if

    dim1 = size(array,1)
    dim2 = size(array,2)

    if (arr_size1 < dim1 .or. arr_size2 < dim2) then
      write(*,*) 'Subroutine SetArraySize2DInt.'
      write(*,*) 'The new size of the array is less then current ones.'
      write(*,*) 'Program is stopped.'
      stop
    end if

    tmp(1:dim1,1:dim2) = array(1:dim1,1:dim2)

    deallocate(array)

    allocate(array(arr_size1,arr_size2))
    array = 0
    array(1:dim1,1:dim2) = tmp(1:dim1,1:dim2)

    return

  end subroutine SetArraySize2DInt

end module
