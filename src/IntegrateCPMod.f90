module IntegrateCPMod
    use precision_mod

    real(rp) :: gpoint(7) = (/0.014_rp, 0.157_rp, 0.45_rp, 0.85_rp, 1.3_rp, 1.9_rp, 2.5_rp/)

    contains

    subroutine IntegrateBetaHash(nChordBet, nSegChordBet, lenChordBet, wghtChordBet, zoneChordBet, &
                                 segSdArr, mat, nBeta_key, Stot, ns, cell_type, nBetaElem, Beta, brc)
    !--------------------------------------------------
    !  Subroutine for integration of surface to surface
    !  collision probability
    !--------------------------------------------------
      use Config, only : str_len
      use GlobalMod, only : inSideBet, inSegBet, inSectBet, outSideBet, outSegBet, outSectBet, &
                            thetaSectBet
      use GlobalAssemblyMod, only : nTheta, phiSect, verbosity
      use ints_module
      use fhash_module__ints_double
      use BickleyMod, only : Ki3
      use SplineMod, only : Ki3_partial
      use ConstantsMod, only : PI, PI2
      implicit none

! Input variables
      integer, intent(in) :: nChordBet         ! number of chords for integration of surface-to-surface CP (beta)
      integer, intent(in) :: nSegChordBet(:)   ! number of segments along the chord for integration beta
      real(rp), intent(in) :: lenChordBet(:,:)  ! length of segments along the chord
      real(rp), intent(in) :: wghtChordBet(:)   ! weight of the beta chord
      integer, intent(in) :: zoneChordBet(:,:) ! zones' numbers along the chord (for definition of the materials)
      real(rp), intent(in) :: segSdArr(:,:)     ! Array with one-dimensional coordinates of the segments on the cell's sides
      integer, intent(in) :: mat(:)            ! Material numbers in the regions of the cell
      integer, intent(in) :: nBeta_key(:,:)    ! number of beta element where chord contributes
      real(rp), intent(in) :: Stot(:)           ! Total cross sections in the regions (zones) of the cell
      integer, intent(in) :: ns                ! number of sides for the cell
      character(str_len), intent(in) :: cell_type ! Geometrical type of the cell - regular or arbitrary

! Output variables
      integer, intent(out)              :: nBetaElem         ! total number of beta elements
      real(rp), allocatable, intent(out) :: Beta(:)
      real(rp), allocatable, intent(out) :: brc(:)

! Local variables
      integer :: iChord ! Number of the current chord
      integer :: iSeg   ! Number of the current segment of the chord
      integer :: iBet   ! , iBet1
      real(rp) :: tau    ! Optical length along the chord
      real(rp) :: coef, brc_coef
      real(rp) :: beta_val, bb1, bb2
      real(rp) :: phiWghtIn, phiWghtOut, sgLngthIn, sgLngthOut
      logical :: success
      integer :: status
      type(fhash_type__ints_double) :: h_beta
      type(ints_type)               :: key_beta, key_beta1
      type(fhash_type_iterator__ints_double) :: it_beta
      integer :: i_theta, imat

      coef = 4.0_rp/PI

      call h_beta%reserve(150000)
      if (.not. allocated(key_beta%ints)) allocate(key_beta%ints(7))
      key_beta%ints = 0

      if (verbosity > 0) then
        write(*,*) "Nchordbet = ", nChordBet
      end if
      do iChord = 1, nChordBet
        key_beta%ints(1:6) = nBeta_key(iChord,1:6)
        tau = 0.0_rp
        do iSeg = 1, nSegChordBet(iChord)
          imat = mat(zoneChordBet(iChord,iSeg))
          tau = tau + lenChordBet(iChord,iSeg)*Stot(imat)
        end do
        do i_theta = 1, nTheta
          key_beta%ints(7) = i_theta
          call h_beta%get(key_beta, beta_val, success)
          if (success) then
            beta_val = beta_val + Ki3_partial(i_theta, tau)*wghtChordBet(iChord)
          else
            beta_val = Ki3_partial(i_theta, tau)*wghtChordBet(iChord)
          end if
          call h_beta%set(key_beta, beta_val)
        end do
      end do

      nBetaElem = h_beta%key_count()
      if (.not. allocated(Beta))         allocate(Beta(nBetaElem))
      if (.not. allocated(brc))          allocate(brc(nBetaElem))
      if (.not. allocated(inSideBet))    allocate(inSideBet(nBetaElem))
      if (.not. allocated(inSegBet))     allocate(inSegBet(nBetaElem))
      if (.not. allocated(inSectBet))    allocate(inSectBet(nBetaElem))
      if (.not. allocated(outSideBet))   allocate(outSideBet(nBetaElem))
      if (.not. allocated(outSegBet))    allocate(outSegBet(nBetaElem))
      if (.not. allocated(outSectBet))   allocate(outSectBet(nBetaElem))
      if (.not. allocated(thetaSectBet)) allocate(thetaSectBet(nBetaElem))
      Beta = 0.0_rp
      inSideBet = 0
      inSegBet = 0
      inSectBet = 0
      outSideBet = 0
      outSegBet = 0
      outSectBet = 0
      thetaSectBet = 0

      call it_beta%begin(h_beta)

      if (.not. allocated(key_beta1%ints)) allocate(key_beta1%ints(7))
      key_beta1%ints = 0
      iBet = 1
      do while (.true.)
        call it_beta%next(key_beta, beta_val, status)
        if (status /= 0) exit
        inSideBet(iBet) = key_beta%ints(1)
        inSegBet(iBet)  = key_beta%ints(2)
        inSectBet(iBet) = key_beta%ints(3)
        outSideBet(iBet) = key_beta%ints(4)
        outSegBet(iBet) = key_beta%ints(5)
        outSectBet(iBet) = key_beta%ints(6)
        thetaSectBet(iBet) = key_beta%ints(7)
! Symmetry properties of the cells
!        if (cell_type == "regular") then
!          bb1 = beta_val
!          key_beta1%ints = key_beta%ints
!          key_beta1%ints(2) = ns - key_beta%ints(2) + 1
!          key_beta1%ints(5) = ns - key_beta%ints(5) + 1
!          call h_beta%get(key_beta1, bb2, success)
!          beta_val = (bb1 + bb2)*0.5_rp
!          call h_beta%set(key_beta, beta_val)
!          call h_beta%set(key_beta1, beta_val)
!        end if
        Beta(iBet) = beta_val

!        inSideBet(nBetaElem + iBet) = outSideBet(iBet)
!        inSegBet(nBetaElem + iBet) = outSegBet(iBet)
!        inSectBet(nBetaElem + iBet) = outSectBet(iBet)
!        outSideBet(nBetaElem + iBet) = inSideBet(iBet)
!        outSegBet(nBetaElem + iBet) = inSegBet(iBet)
!        outSectBet(nBetaElem + iBet) = inSectBet(iBet)
!        thetaSectBet(nBetaElem + iBet) = thetaSectBet(iBet)

        phiWghtIn  = (sin(phiSect(inSectBet(iBet)+1)) - sin(phiSect(inSectBet(iBet))))*0.5_rp
        phiWghtOut = (sin(phiSect(outSectBet(iBet)+1)) - sin(phiSect(outSectBet(iBet))))*0.5_rp
        sgLngthIn = segSdArr(inSideBet(iBet),inSegBet(iBet)+1) - segSdArr(inSideBet(iBet),inSegBet(iBet))
        sgLngthOut = segSdArr(outSideBet(iBet),outSegBet(iBet)+1) - segSdArr(outSideBet(iBet),outSegBet(iBet))

        brc_coef = sgLngthIn*phiWghtIn/(sgLngthOut*phiWghtOut)
        brc(iBet) = brc_coef

!        Beta(nBetaElem + iBet) = brc_coef*Beta(iBet)
!
        iBet = iBet + 1
      end do

      Beta = coef*Beta

!      nBetaElem = 2*nBetaElem

      call h_beta%clear()
      if (allocated(key_beta%ints)) deallocate(key_beta%ints)

      return

    end subroutine IntegrateBetaHash

    subroutine IntegrateGamma(nChordGam, nSegChordGam, lenChordGam, wghtChordGam, zoneChordGam, &
                              xChordGam, yChordGam, nSides, nSegments, nZone, mat, Stot, nHarm)
!--------------------------------------------------
!  Subroutine for integration of surface to region
!  collision probability
!--------------------------------------------------
      use ConstantsMod, only : PI4
      use GlobalMod, only : G, inSegGam, inSectGam, inSideGam, xg => xGauss, wg => wGauss
      use GlobalAssemblyMod, only : nPhi, nTheta
      use BickleyMod, only : Ki2
      use SplineMod, only : Ki2_partial
      use MathMod, only : P
      implicit none
! Input
      integer, intent(in) :: nChordGam         ! number of chords for integration of surface-to-surface CP (beta)
      integer, intent(in) :: nSegChordGam(:)   ! number of segments along the chord for integration beta
      real(rp), intent(in) :: lenChordGam(:,:)  ! length of segments along the chord
      real(rp), intent(in) :: wghtChordGam(:)   ! weight of the gamma chord
      integer, intent(in) :: zoneChordGam(:,:) ! zones' numbers along the chord (for definition of the materials)
      real(rp), intent(in) :: xChordGam(:,:)    ! x-coordinates of the segments along the chord
      real(rp), intent(in) :: yChordGam(:,:)    ! y-coordinates of the segments along the chord
      integer, intent(in) :: mat(:)            ! Material numbers in the cell's regions
      real(rp), intent(in) :: Stot(:)           ! total cross sections in the regions (zones) of the cell
      integer, intent(in) :: nSegments(:)
      integer, intent(in) :: nHarm             ! number of spatial harmonics
      integer, intent(in) :: nZone
      integer, intent(in) :: nSides
! Output

! Local
      integer :: i, k, s, n, m
      integer :: ig, imat
      integer :: iChord, iSeg, i_theta
      integer :: ngp
      real(rp) :: w1
      real(rp) :: t1, t2
      real(rp) :: a1, a2
      real(rp) :: x1, y1, x2, y2
      real(rp) :: x, y, tau
      real(rp) :: tt0, tt1, tt
      real(rp) :: tau1, tau2, bick, lngth, wght

      G = 0.0_rp

        do iChord = 1, nChordGam
          s = inSideGam(iChord)
          n = inSegGam(iChord)
          m = inSectGam(iChord)
          w1 = wghtChordGam(iChord)*PI4
          tau1 = 0.0_rp
          tt0 = 0.0_rp
          do iSeg = 1, nSegChordGam(iChord)
            i = zoneChordGam(ichord, iSeg)
            imat = mat(zoneChordGam(ichord, iSeg))
            lngth = lenChordGam(iChord,iSeg)
            tt1 = tt0 + lngth
            x1 = xChordGam(iChord,iSeg)
            x2 = xChordGam(iChord,iSeg+1)
            y1 = yChordGam(iChord,iSeg)
            y2 = yChordGam(iChord,iSeg+1)
            tau2 = tau1 + Stot(imat)*lngth
            t1 = 0.5_rp*(tau2 - tau1)
            t2 = 0.5_rp*(tau2 + tau1)
            a1 = 0.5_rp*(tt1 - tt0)
            a2 = 0.5_rp*(tt1 + tt0)
            bick = 0.0_rp
            ngp = get_np(t1)
            do ig = 1, ngp
              wght = w1*wg(ngp,ig)*lngth*Stot(imat)*0.5_rp
              tau  = xg(ngp,ig)*t1 + t2
              tt = xg(ngp,ig)*a1 + a2
              x = x1 + (x2 - x1)*(tt - tt0)/lngth
              y = y1 + (y2 - y1)*(tt - tt0)/lngth
              do k = 1, nHarm
                do i_theta = 1, nTheta
                  G(i,k,s,n,m,i_theta) = G(i,k,s,n,m,i_theta) + wght*Ki2_partial(i_theta, tau)*P(k,x,y)
                end do
              end do
            end do
            tau1 = tau2
            tt0 = tt1
          end do
        end do

      call OrthogonaliseGamma(nZone, nHarm, nSides, nSegments, nPhi, nTheta, G)

      return

    end subroutine IntegrateGamma

    subroutine OrthogonaliseGamma(nZoneMax, nHarm, nSide, nSeg, nSect, nTheta, G)
      implicit none

      integer, intent(in)    :: nZoneMax
      integer, intent(in)    :: nHarm
      integer, intent(in)    :: nSide
      integer, intent(in)    :: nSeg(:)
      integer, intent(in)    :: nSect
      integer, intent(in)    :: nTheta
      real(rp), intent(inout) :: G(:,:,:,:,:,:)

      integer :: i, k, s, n, m, l
      real(rp) :: GamOrt(nZoneMax,nHarm,nSide,maxval(nSeg),nSect,nTheta)

      GamOrt = 0.0_rp
      do i = 1, nZoneMax
        do s = 1, nSide
          do n = 1, nSeg(s)
            do m = 1, nSect
              do k = 1, nHarm
                do l = 1, nTheta
                    GamOrt(i,k,s,n,m,l) = sumOrtG(i,k,s,n,m,l,G)
                    if (abs(GamOrt(i,k,s,n,m,l)) < 1.0d-08) GamOrt(i,k,s,n,m,l) = 0.0_rp
                end do
              end do
            end do
          end do
        end do
      end do

#ifdef __INTEL_COMPILER
      G = GamOrt
#else
      G(1:nZoneMax,1:nHarm,1:nSide,1:maxval(nSeg),1:nSect,1:nTheta) = &
      GamOrt(:,:,:,:,:,:)
#endif
      continue
    end subroutine OrthogonaliseGamma

    subroutine NormaliseGamma(G, Bet, nToBet, mToBet, sToBet, thetaSectBet, nElemBet, nSide, nSegments, nSectPhi, nSectTheta, nZone)
      implicit none

      real(rp), intent(inout) :: G(:,:,:,:,:,:)
      real(rp), intent(in)    :: Bet(:)
      integer, intent(in)    :: nToBet(:)
      integer, intent(in)    :: mToBet(:)
      integer, intent(in)    :: sToBet(:)
      integer, intent(in)    :: thetaSectBet(:)
      integer, intent(in)    :: nelemBet
      integer, intent(in)    :: nSide
      integer, intent(in)    :: nSegments(:)
      integer, intent(in)    :: nSectPhi
      integer, intent(in)    :: nSectTheta
      integer, intent(in)    :: nZone

      integer :: i, n, m, s, l
      integer :: maxS, maxN, maxM
      real(rp), allocatable :: bnr(:,:,:,:) !n,m,s
      real(rp), allocatable :: gnr(:,:,:,:)

      maxN = maxval(nToBet)
      maxM = maxval(mToBet)
      maxS = maxval(sToBet)

      allocate(bnr(maxN,maxM,maxS,nSectTheta))
      allocate(gnr(maxN,maxM,maxS,nSectTheta))
      bnr = 0.0_rp
      gnr = 0.0_rp

      do i = 1, nElemBet
        n = nToBet(i)
        m = mToBet(i)
        s = sToBet(i)
        l = thetaSectBet(i)
        bnr(n,m,s,l) = bnr(n,m,s,l) + Bet(i)
      end do

      do s = 1, nSide
        do n = 1, nSegments(s)
          do m = 1, nSectPhi
            do l = 1, nSectTheta
              do i = 1, nZone
                gnr(n,m,s,l) = gnr(n,m,s,l) + G(i,1,s,n,m,l)
              end do
            end do
          end do
        end do
      end do

      do n = 1, maxN
        do m = 1, maxM
          do s = 1, maxS
            do l = 1, nSectTheta
              gnr(n,m,s,l) = (1.0_rp-bnr(n,m,s,l))/gnr(n,m,s,l)
            end do
          end do
        end do
      end do

      do s = 1, nSide
        do n = 1, nSegments(s)
          do m = 1, nSectPhi
            do l = 1, nSectTheta
              do i = 1, nZone
                G(i,1,s,n,m,l) = G(i,1,s,n,m,l)*gnr(n,m,s,l)
              end do
            end do
          end do
        end do
      end do

    end subroutine NormaliseGamma

    subroutine IntegrateV(nZones, nHarm, nSides, nSegments, nSect, nTheta, segBoundX, segBoundY, chi, eps, &
                          mat, vol, St, G, V)
      implicit none

      integer, intent(in) :: nZones
      integer, intent(in) :: nHarm
      integer, intent(in) :: nSides
      integer, intent(in) :: nSegments(:)
      integer, intent(in) :: nSect
      integer, intent(in) :: nTheta
      real(rp), intent(in) :: segBoundX(:,:)
      real(rp), intent(in) :: segBoundY(:,:)
      real(rp), intent(in) :: chi(:)
      real(rp), intent(in) :: eps(:)
      integer, intent(in) :: mat(:)
      real(rp), intent(in) :: vol(:)
      real(rp), intent(in) :: St(:)
      real(rp), intent(in) :: G(:,:,:,:,:,:)
      real(rp), intent(out):: V(:,:,:,:,:,:)

      integer :: i, k, s, n, m, l
      real(rp) :: w, x0, y0, x1, y1
      real(rp), allocatable :: Stot(:)
      real(rp) :: lenSeg

      allocate(Stot(nZones))
      Stot = 0.0_rp
      do i = 1, nZones
        Stot(i) = St(mat(i))
      end do

      do i = 1, nZones
        do k = 1, nHarm
          do s = 1, nSides
            do n = 1, nSegments(s)
              x0 = segBoundX(s,n)
              y0 = segBoundY(s,n)
              x1 = segBoundX(s,n+1)
              y1 = segBoundY(s,n+1)
              lenSeg = sqrt((x1-x0)**2 + (y1-y0)**2)
              do m = 1, nSect
                do l = 1, nTheta
                  if (abs(G(i,k,s,n,m,l)) <= 1.0d-8) cycle
                  w = lenSeg*chi(m)*eps(l)/(4.0_rp*vol(i)*Stot(i))
                  V(s,n,m,l,i,k) = G(i,k,s,n,m,l)*w
                end do
              end do
            end do
          end do
        end do
      end do

    end subroutine IntegrateV

    real(rp) function sumOrtG(i,k,s,n,m,l,G)
      use GlobalMod, only : ort
      implicit none

      integer, intent(in) :: i
      integer, intent(in) :: k
      integer, intent(in) :: s
      integer, intent(in) :: n
      integer, intent(in) :: m
      integer, intent(in) :: l
      real(rp), intent(in) :: G(:,:,:,:,:,:)

      integer :: j

      sumOrtG = 0.0_rp
      do j = 1, k
        sumOrtG = sumOrtG + ort(i,k,j)*G(i,j,s,n,m,l)
      end do

    end function sumOrtG

    subroutine IntegrateU(nChordU, nSegChordU, lenChordU, wghtChordU, zoneChordU, xChordU,      &
                           yChordU, mat, vol, St, nzone, nHarm, U)
!--------------------------------------------------
!  Subroutine for integration of region to region
!  collision probability
!--------------------------------------------------
      use ConstantsMod, only : PI
      use GlobalMod, only : xg => xGauss, wg => wGauss
      use BickleyMod, only : Ki1
      use SplineMod, only : Ki1_partial
      use MathMod, only : P
      implicit none

! Input
      integer, intent(in) :: nChordU         ! number of chords for integration of region-to-region CP (U)
      integer, intent(in) :: nSegChordU(:)   ! number of segments along the chord for integration U
      real(rp), intent(in) :: lenChordU(:,:)  ! length of segments along the chord
      real(rp), intent(in) :: wghtChordU(:)   ! weight of the U chord
      integer, intent(in) :: zoneChordU(:,:) ! zones' numbers along the chord (for definition of the materials)
      real(rp), intent(in) :: xChordU(:,:)    ! x-coordinates of the segments along the chord
      real(rp), intent(in) :: yChordU(:,:)    ! y-coordinates of the segments along the chord
      integer, intent(in) :: mat(:)          !
      real(rp), intent(in) :: St(:)           ! Total cross sections
      real(rp), intent(in) :: vol(:)          ! Volumes of cell's regions
      integer, intent(in) :: nZone           ! Number of zones
      integer, intent(in) :: nHarm           ! total number of the harmonics

! Output
      real(rp), intent(out) :: U(:,:,:,:)

! Local
      real(rp) :: pi4, wch, a1, a2, len1, tau1, c1, c2, t1
      real(rp) :: x1, y1, w1, len, d1, d2, t, x, y, w, tau
      real(rp) :: w5, b1, b2
      integer :: ich, iseg1, z1, ngp1, iseg, z, ig1, ngp
      integer :: ig, k1, k, i

      real(rp) :: bick
      real(rp) :: pk1
      real(rp), allocatable :: Utmp(:,:,:,:)
      real(rp), allocatable :: Stot(:)

      allocate(Utmp(nZone,nHarm,nZone,nHarm))
      allocate(Stot(nZone))

      U = 0.0_rp
      Stot = 0.0_rp
      do i = 1, nZone
        Stot(i) = st(mat(i))
      end do
      pi4 = 4.0_rp/pi

      do ich = 1, nChordU
        wch = 0.25_rp*pi4*wghtChordU(ich)
        do iseg1 = 1, nSegChordU(ich)
          a1 = getTStart(iseg1, ich, lenChordU)
          a2 = getTFinish(iseg1, ich, lenChordU)
          z1 = zoneChordU(ich, iseg1)
          len1 = a2 - a1
          tau1 = Stot(z1)*(a2 - a1)
          ngp1 = get_np(tau1)
          c1 = (a2 - a1)*0.5_rp
          c2 = (a2 + a1)*0.5_rp
          do iseg = 1, nSegChordU(ich)
            if (iseg == iseg1) then
              z = z1
              do ig1 = 1, ngp1
                t1 = c1*xg(ngp1,ig1) + c2
                x1 = getXCoord(t1,iseg1,ich,xChordU,lenChordU)
                y1 = getYCoord(t1,iseg1,ich,yChordU,lenChordU)
                w1 = wg(ngp1,ig1)
                len = a2 - t1
                ngp = get_np((a2 - t1)*Stot(z))
                d1 = (a2 - t1)*0.5_rp
                d2 = (a2 + t1)*0.5_rp
                do ig = 1, ngp
                  t = d1*xg(ngp,ig) + d2
                  x = getXCoord(t,iseg,ich,xChordU,lenChordU)
                  y = getYCoord(t,iseg,ich,yChordU,lenChordU)
                  w = wg(ngp,ig)
                  tau = getTau2(t1, t, iseg1, iseg, ich, Stot, zoneChordU, lenChordU)
                  w5 = wch*w*w1*len*len1
                  bick = Ki1(tau)
                  do k1 = 1, nHarm
                    pk1 = P(k1, x1, y1)
                    do k = 1, nHarm
                      U(z1,k1,z,k) = U(z1,k1,z,k) + w5*bick*pk1*P(k,x,y)
                    end do
                  end do
                end do
                len = t1 - a1
                ngp = get_np(len*Stot(z))
                d1 = (t1 - a1)*0.5_rp
                d2 = (a1 + t1)*0.5_rp
                do ig = 1, ngp
                  t = d1*xg(ngp,ig) + d2
                  x = getXCoord(t,iseg,ich,xChordU,lenChordU)
                  y = getYCoord(t,iseg,ich,yChordU,lenChordU)
                  w = wg(ngp,ig)
                  tau = getTau2(t1, t, iseg1, iseg, ich, Stot, zoneChordU, lenChordU)
                  w5 = wch*w*w1*len*len1
!                  bick = Ki1(tau)
                  bick = Ki1_partial(1, tau)
                  do k1 = 1, nHarm
                    pk1 = P(k1, x1, y1)
                    do k = 1, nHarm
                      U(z1,k1,z,k) = U(z1,k1,z,k) + w5*bick*pk1*P(k,x,y)
                    end do
                  end do
                end do
              end do
            else
              z = zoneChordU(ich,iseg)
              b1 = getTStart(iseg,ich,lenChordU)
              b2 = getTFinish(iseg,ich,lenChordU)
              len = b2 - b1
              ngp = get_np((b2-b1)*Stot(z))
              d1 = (b2 - b1)*0.5_rp
              d2 = (b1 + b2)*0.5_rp
              do ig1 = 1, ngp1
                t1 = c1*xg(ngp1,ig1) + c2
                x1 = getXCoord(t1,iseg1,ich,xChordU,lenChordU)
                y1 = getYCoord(t1,iseg1,ich,yChordU,lenChordU)
                w1 = wg(ngp1,ig1)
                do ig = 1, ngp
                  t = d1*xg(ngp,ig) + d2
                  x = getXCoord(t,iseg,ich,xChordU,lenChordU)
                  y = getYCoord(t,iseg,ich,yChordU,lenChordU)
                  w = wg(ngp,ig)
                  tau = getTau2(t1, t, iseg1, iseg, ich, Stot, zoneChordU, lenChordU)
                  w5 = wch*w*w1*len*len1
!                  bick = Ki1(tau)
                  bick = Ki1_partial(1, tau)
                  do k1 = 1, nHarm
                    pk1 = P(k1,x1,y1)
                    !$omp simd
                    do k = 1, nHarm
                      U(z1,k1,z,k) = U(z1,k1,z,k) + w5*bick*pk1*P(k,x,y)
                    end do
                  end do
                end do
              end do
            end if
          end do
        end do
      end do

      do z1 = 1, nZone
        do z = z1 + 1, nZone
          do k1 = 1, nHarm
            do k = k1 + 1, nHarm
              c1 = 0.5_rp*(U(z1,k1,z,k) + U(z,k,z1,k1))
              U(z1,k1,z,k) = c1
              U(z,k,z1,k1) = c1
             end do
           end do
        end do
      end do

      Utmp = 0.0_rp

      do z1 = 1, nZone
        do z = z1, nZone
          do k1 = 1, nHarm
            do k = 1, nHarm
              if (z /= z1) then
                c1 = U(z1,k1,z,k)
              else
                c1 = U(z1,k1,z,k)
              end if
              if (abs(c1) <= 1.0d-5) then
                c1 = 0.0_rp
              end if
              Utmp(z1,k1,z,k) = c1*Stot(z1)/Vol(z)
              Utmp(z,k,z1,k1) = c1*Stot(z)/Vol(z1)
            end do
          end do
        end do
      end do

#ifdef __INTEL_COMPILER
      U = Utmp
#else
      U(1:nZone,1:nHarm,1:nZone,1:nHarm) = Utmp(:,:,:,:)
#endif

      deallocate(Utmp)

    end subroutine IntegrateU

    subroutine OrthogonalizeU(nZone, nHarm, U)
      use GlobalMod, only : ort
      implicit none

      integer, intent(in)  :: nZone
      integer, intent(in)  :: nHarm
      real(rp), intent(out) :: U(:,:,:,:)

      real(rp) :: Uort(nZone,nHarm,nZone,nHarm)
      integer :: i1,k1,i,k

      do i = 1, nZone
        ort(i,1:nHarm,1:nHarm) = transpose(ort(i,1:nHarm,1:nHarm))
      end do
      Uort = 0.0_rp

      do i1 = 1, nZone
        do k1 = 1, nHarm
          do i = 1, nZone
            do k = 1, nHarm
              Uort(i1,k1,i,k) = sumOrtU(i1,k1,i,k,nHarm,U)
            end do
          end do
        end do
      end do

#ifdef __INTEL_COMPILER
      U = Uort
#else
      U(1:nZone,1:nHarm,1:nZone,1:nHarm) = Uort(:,:,:,:)
#endif

    end subroutine OrthogonalizeU

    real(rp) function sumOrtU(iZone1,iHarm1,iZone,iHarm,nHarm,U)
      use GlobalMod, only : ort
      implicit none

      integer, intent(in) :: iZone1
      integer, intent(in) :: iHarm1
      integer, intent(in) :: iZone
      integer, intent(in) :: iHarm
      integer, intent(in) :: nHarm
      real(rp), intent(in) :: U(:,:,:,:)

      real(rp) :: b(nHarm,nHarm), b1(nHarm,nHarm)
      integer :: k1, k
      integer :: mat(nHarm,nHarm)


      sumOrtU = 0.0_rp
      b = ort(iZone,1:nHarm,1:nHarm)
      b1 = ort(iZone1,1:nHarm,1:nHarm)
      mat = 0
      do k1 = 1, nHarm
        do k = k1,nHarm
            if (b(k1,k) /= 0) then
                mat(k1,k) = 1
                mat(k,k1) = 1
            end if
        end do
      end do
      if (mat(iHarm1,iharm) /= 0) then
      do k1 = 1, iHarm1
        do k = 1, iHarm
            sumOrtU = sumOrtU + b(k,iHarm)*b1(k1,iHarm1)*U(iZone1,k1,iZone,k)
        end do
      end do
      end if
      return
    end function sumOrtU

    subroutine NormalizeU(nZone, nHarm, nSides, nSegments, nSect, nTheta, V, U)

      implicit none

      integer, intent(in) :: nZone
      integer, intent(in) :: nHarm
      integer, intent(in) :: nSides
      integer, intent(in) :: nSegments(:)
      integer, intent(in) :: nSect
      integer, intent(in) :: nTheta
      real(rp), intent(in) :: V(:,:,:,:,:,:)
      real(rp), intent(inout) :: U(:,:,:,:)

      integer :: s, n, m, i, i1, l
      integer :: maxSeg
      real(rp) :: dvnr(nZone,nHarm)
      real(rp) :: r1

      maxSeg = maxval(nSegments)
      dvnr = 0.0_rp
      do i = 1, nZone
        do s = 1, nSides
          do n = 1, nSegments(s)
            do m = 1, nSect
              do l = 1, nTheta
                dvnr(i,1) = dvnr(i,1) + V(s,n,m,l,i,1)
              end do
            end do
          end do
        end do
      end do

      r1 = 0.0_rp
      do i1 = 1, nZone
        r1 = 1.0_rp - dvnr(i1,1)
        do i = 1, nZone
          if (i1 /= i) then
            r1 = r1 - U(i,1,i1,1)
          end if
        end do
        U(i1,1,i1,1) = r1
      end do

      return

    end subroutine NormalizeU

    integer function get_np(tau)
      implicit none

      real(rp) :: tau
      integer :: i

      do i = 1, 7
        if (tau < gpoint(i)) then
          get_np = 2*i
          return
        end if
      end do

      get_np = 2*i
      return

    end function get_np

    real(rp) function getTau(t, iSeg, iZoneSegGam, lenSegChordGam, Stot)
    !-------------------------------------------
    ! This function returns optical path from
    ! point t on the chord iChord to the side
    ! of the cell
    !
    ! INPUT
    !
    ! t - point on the chord iChord in the range
    !     from 0 to the length of the chord
    ! iSeg - number of the segment where point t
    !        is located
    ! iChord - number of the chord
    ! Stot -   total cross sections in the cell
    !
    ! OUTPUT
    !
    ! getTau - optical length from t to the side
    !          of the cell
    !-------------------------------------------
      implicit none

      real(rp), intent(in) :: t
      integer, intent(in) :: iSeg
      integer, intent(in) :: iZoneSegGam(:)
      real(rp), intent(in) :: lenSegChordGam(:)
      real(rp), intent(in) :: Stot(:)

      integer :: iSeg1
      real(rp) :: dt, dTau

      getTau = 0.0
      dt = t - getT1(iSeg, lenSegChordGam)
      dTau = dt*Stot(iZoneSegGam(iSeg))
      do iSeg1 = 1, iSeg - 1
        getTau = getTau + lenSegChordGam(iSeg1)*Stot(iZoneSegGam(iSeg1))
      end do
      getTau = getTau + dTau

      return

    end function getTau

    real(rp) function getTau2(t1, t, iseg1, iseg, ich, Stot, zoneChordU, lenChordU)
      implicit none
      real(rp) :: t1, t
      real(rp) :: Stot(:)
      integer :: zoneChordU(:,:)
      real(rp) :: lenChordU(:,:)
      integer :: iseg1, iseg, ich

      integer :: i

      getTau2 = 0.0_rp

      if (iseg1 == iseg) then
        getTau2 = abs(t1-t)*Stot(zoneChordU(ich,iseg))
        return
      else
        if (t1 == t) then
          getTau2 = 0.0_rp
          return
        end if
        if (t1 > t) then
          getTau2 = getTau2 + (getTFinish(iseg,ich,lenChordU) - t)*Stot(zoneChordU(ich,iseg))
          getTau2 = getTau2 + (t1 - getTStart(iseg1,ich,lenChordU))*Stot(zoneChordU(ich,iseg1))
          do i = iseg + 1, iseg1 - 1
            getTau2 = getTau2 + lenChordU(ich,i)*Stot(zoneChordU(ich,i))
          end do
          return
        else
          getTau2 = getTau2 + (getTFinish(iseg1,ich,lenChordU) - t1)*Stot(zoneChordU(ich,iseg1))
          getTau2 = getTau2 + (t - getTStart(iseg,ich,lenChordU))*Stot(zoneChordU(ich,iseg))
          do i = iseg1 + 1, iseg - 1
            getTau2 = getTau2 + lenChordU(ich,i)*Stot(zoneChordU(ich,i))
          end do
          return
        end if
      end if
    end function getTau2

    real(rp) function getT2(iSeg, lenSegChordGam)
    !---------------------------------------------------
    ! Function returns the total length of the segments
    ! from the segment 1 to segment iSeg
    !
    ! INPUT
    ! iSeg - number of the segment of interest
    ! lenSegChordGam - lengths of the segments along the
    !                  chord
    !
    ! OUTPUT
    ! getT2 - total length of the segments from 1 to iSeg
    !----------------------------------------------------
      implicit none

      integer, intent(in) :: iseg
      real(rp), intent(in) :: lenSegChordGam(:)

      integer :: i

      getT2 = 0.0_rp
      do i = 1, iseg
        getT2 = getT2 + lenSegChordGam(i)
      end do
      return

    end function getT2

    real(rp) function getT1(iSeg, lenSegChordGam)
    !---------------------------------------------------
    ! Function returns the total length of the segments
    ! from the segment 1 to segment iSeg - 1
    !
    ! INPUT
    ! iSeg - number of the segment of interest
    ! lenSegChordGam - lengths of the segments along the
    !                  chord
    !
    ! OUTPUT
    ! getT1 - total length of the segments from 1 to iSeg - 1
    !----------------------------------------------------
      implicit none

      integer, intent(in) :: iseg
      real(rp), intent(in) :: lenSegChordGam(:)

      integer :: i

      getT1 = 0.0_rp
      do i = 1, iseg - 1
        getT1 = getT1 + lenSegChordGam(i)
      end do
      return

    end function getT1

    real(rp) function getTStart(iseg, ich, lenChordU)
      implicit none
      integer :: iseg, ich
      real(rp) :: lenChordU(:,:)
      integer :: i

      getTStart = 0.0_rp
      do i = 1, iseg - 1
        getTStart = getTStart + lenChordU(ich,i)
      end do
      return

    end function getTStart

    real(rp) function getTFinish(iseg, ich, lenChordU)
      implicit none
      integer :: iseg, ich
      integer :: i
      real(rp) :: lenChordU(:,:)

      getTFinish = 0.0_rp
      do i = 1, iseg
        getTFinish = getTFinish + lenChordU(ich,i)
      end do
      return

    end function getTFinish

    real(rp) function getXCoord(t,iseg,ich,xCoordU,lenSegChordU)
      implicit none
      real(rp), intent(in) :: t
      integer, intent(in) :: iseg, ich
      real(rp), intent(in) :: xCoordU(:,:)
      real(rp), intent(in) :: lenSegChordU(:,:)

      real(rp) :: x1, x2, len, t0

      x1 = xCoordU(ich,iseg)
      x2 = xCoordU(ich,iseg+1)
      len = lenSegChordU(ich,iseg)
      t0 = getTStart(iseg,ich,lenSegChordU)

      getXCoord = (x2-x1)*(t-t0)/len + x1

    end function getXCoord

    real(rp) function getYCoord(t,iseg,ich,yCoordU,lenSegChordU)
      implicit none
      real(rp), intent(in) :: t
      integer, intent(in) :: iseg, ich
      real(rp), intent(in) :: yCoordU(:,:)
      real(rp), intent(in) :: lenSegChordU(:,:)

      real(rp) :: y1, y2, len, t0

      y1 = yCoordU(ich,iseg)
      y2 = yCoordU(ich,iseg+1)
      len = lenSegChordU(ich,iseg)
      t0 = getTStart(iseg,ich,lenSegChordU)

      getYCoord = (y2-y1)*(t-t0)/len + y1

    end function getYCoord

end module IntegrateCPMod
